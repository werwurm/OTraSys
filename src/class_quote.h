/* class_quote.h
 * declarations for class_quote.c, interface for "quote" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_quote.h
 * @brief Header file for class_quote.c, public member declarations
 *
 * This file contains the "public" available data and functions of the quote
 * "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_QUOTE_H
#define CLASS_QUOTE_H

#include "bstrlib.h"

struct _class_quotes; 

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyQuotesFunc)(struct _class_quotes*);
typedef struct _class_quotes * (*cloneQuotesFunc)(const struct _class_quotes*);
typedef void (*printQuoteTableFunc)(const struct _class_quotes*);
typedef void (*loadQuotesFunc)(struct _class_quotes*);
typedef void (*copyDataVecFunc)(const struct _class_quotes*, struct _class_quotes*);
typedef void (*newSymbolFunc)(struct _class_quotes*, char*, char*);
typedef long int (*findDaynrIndexFunc)(const struct _class_quotes*, unsigned int);
typedef unsigned int (*findDaynrIndex_lBFunc)(const struct _class_quotes*, unsigned int);
typedef void (*appendQuoteRecordsFunc)(struct _class_quotes*, unsigned int);
typedef void (*appendQuoteTupelFunc)(struct _class_quotes*, bstring*, unsigned int, float);
typedef void (*setQuoteTupelFunc)(struct _class_quotes*, unsigned int,  bstring*, unsigned int, float);

struct _class_quotes
{
	// public part
	// DATA
	unsigned int nr_elements;	/**< nr of data sets */
	bstring *symbol; 		/**< market symbol */
	bstring *identifier;    /**< market identifier */
	bstring *quotetype;		/**< type of quotes, e.g. close, open, high, low */
	bstring *datevec;		/**< bstring vector with dates YYYY-MM-DD */
	unsigned int *daynrvec; 	/**< uint vector with dates as nr of days since 01.01.1900 */
	float *quotevec;		/**< float vector with the price/value the dates */	
	unsigned int nr_digits;		/**< nr of significant digits */
	bstring *tablename;     /**< mysql table name where quote is stored */
    // METHODS, call like: foo_obj->bar_method(foo_obj)
	destroyQuotesFunc destroy;	/**< "destructor", see class_quotes_destroyImpl() */
	cloneQuotesFunc clone; 		/**< create new object with same data as given one, see  class_quotes_cloneImpl() */
    printQuoteTableFunc printTable;	/**<  prints out quotes in tabular format, see class_quotes_printTableImpl() */
	loadQuotesFunc loadFromDB; 	/**< load quotes from db, see class_quotes_loadQuotesImpl()  */
	copyDataVecFunc copyDataVec;	/**< copy datevec, daynrvec and quotevec, see class_quotes_copyDataVecImpl() */
	newSymbolFunc newSymbol;	/**< change symbol (useful after cloning, see class_quotes_newSymbolImpl() */
	findDaynrIndexFunc findDaynrIndex; /**< return index nr of specific daynr, or -1 if daynr not in array, see class_quotes_findDaynrIndexImpl() */
	findDaynrIndex_lBFunc findDaynrIndexlB; /**< return lower boundary index of specific daynr: index or next smaller index if daynr not in array, see class_quotes_findDaynrIndex_lBImpl() */
	appendQuoteRecordsFunc appendQuoteRecords; /**< append an (empty) nr. of quote records to existing object, see class_quotes_appendQuoteRecordsImpl() */
	appendQuoteTupelFunc   appendQuoteTupel;   /**< append a (single) {date, daynr, quote} tupel to existing object, see class_quotes_appendQuoteTupelImpl() */
	setQuoteTupelFunc      setQuoteTupel;      /**< fill an (existing) single {date, daynr, quote} tupel of object, see class_quotes_setQuoteTupelImpl() */
};

typedef struct _class_quotes class_quotes; 

class_quotes* class_quotes_init(char* quote_symbol, char* identifier, unsigned int nr, unsigned int nrOfDigits, char* quotetype, char* tablename); 	// "constructor"
#endif // CLASS_QUOTE_H

// eof
