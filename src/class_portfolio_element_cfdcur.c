/* class_portfolio_element_cfdcur.c
 * Implements a "class"-like struct in C which handles portfolio elements
 * of type non currency CFDs
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_portfolio_element_cfdcur.c
 * @brief routines that implement a class-like struct which represents 
 * portfolio_elements of type non currency CFDs
 *
 * This file contains the definitions of the "class" 
 * portfolio_elements_cfdcur. Note that this class inherits everything
 * from the base class portfolio_elements and adds CFD-specific
 * methods, e.g. P/L calculation
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>

#include "arrays.h"
#include "debug.h"

#include "class_portfolio_element_cfdcur.h"
#include "class_portfolio_element.h"

static void class_portfolio_element_cfdcur_setMethods(class_portfolio_element_cfdcur* self);
void class_portfolio_element_cfdcur_update_byMarketImpl(void* self, 
                    float new_sl, bool new_isl_flag, unsigned int new_trading_days, 
                    const class_market* the_market, unsigned int quotenr, float account_balance);
static class_portfolio_element* class_portfolio_element_cfdcur_cloneImpl(const void* self);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for portfolio_element_cfdcur objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. portfolio_element_cfdcur_init takes a market 
 * as main parameter, so the object can be initialized using the information 
 * stored within the market object
 * 
 * 
 * @param the_market pointer to market object with needed info
 * @param longorshort	signal_type (long/short) for new element
 * @param quant	float with quantity bought
 * @param quotenr use this quote/date/daynr dataset
 * @param indicatorpos position of the indicator within market object that
 * 			triggered the signal
 * @param signalstring char ptr with signal name
 * @param the_account pointer to accounts object
 * @return pointer to new object of class portfolio_element_cfdcur
*/
///////////////////////////////////////////////////////////////////////////////
class_portfolio_element_cfdcur* class_portfolio_element_cfdcur_init(const class_market* the_market,
                                                                    const signal_type longorshort, 
                                                                    const float quant, 
                                                                    const unsigned int quotenr, 
                                                                    const unsigned int indicatorpos, 
                                                                    const char* signalstring, 
                                                                    const class_accounts* the_account,
                                                                    const int marketListPosition, 
                                                                    const int sl_list_idx)
{   
    class_portfolio_element_cfdcur *self = NULL;
    
    // basically a portfolio_element_cfdcur object inherits almost
    // everything from its parent, portfolio_element
    self = (class_portfolio_element_cfdcur*) class_portfolio_element_init(
                            the_market, longorshort, quant, quotenr, 
                            indicatorpos, signalstring, the_account, marketListPosition, sl_list_idx);
    
    // set class specific methods
    class_portfolio_element_cfdcur_setMethods(self);
    
    return self;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "alternative constructor" function for portfolio_element_cfdcur 
 * objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. This implementation does not use market
 * object and should not be used when market object information is not
 * available (e.g. when reading from database). Furthermore it makes 
 * portfolio_element_clonePortfolioElementImpl easier to implement
 * 
 * @return pointer to new object of class portfolio_element_cfdcur
*/
///////////////////////////////////////////////////////////////////////////////
class_portfolio_element_cfdcur* class_portfolio_element_cfdcur_init_manually(
                        char* symbol, char* identifier, char* markettype, char* market_currency, 
                        signal_type longorshort, float quant, float market_quote, 
                        unsigned int mq_digits, char* mq_date, unsigned int mq_daynr,
						float account_quote, unsigned int aq_digits, char* aq_date, 
                        unsigned int aq_daynr, float signal_quote, unsigned int sq_digits, 
                        char* sq_date, unsigned int sq_daynr, char* signalstring, 
                        const int marketListPosition, const int sl_list_idx)
{
    class_portfolio_element_cfdcur *self = NULL;
    
    class_portfolio_element *tmp_element = NULL;
    
    // basically a portfolio_element_stocks object inherits almost
    // everything from its parent, portfolio_element
    tmp_element = class_portfolio_element_init_manually(symbol, identifier, markettype, market_currency, longorshort,
						quant, market_quote, mq_digits, mq_date, mq_daynr, account_quote, aq_digits, aq_date, aq_daynr,
						signal_quote, sq_digits, sq_date, sq_daynr, signalstring, marketListPosition, sl_list_idx);
    
    // now clone the freshly created element and cast it to correct type
    self = (class_portfolio_element_cfdcur*) tmp_element->clone(tmp_element);
    
    tmp_element->destroy(tmp_element);
    
    // set class specific methods
    class_portfolio_element_cfdcur_setMethods(self);
    
    return self;    
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for portfolio_element_cfdcur
 * objects
 *
 * This function set the portfolio_element_cfdcur specific methods.
 * All member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to portfolio_element_cfdcur struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_portfolio_element_cfdcur_setMethods(class_portfolio_element_cfdcur* self)
{
    self->clone = class_portfolio_element_cfdcur_cloneImpl;
    self->update = class_portfolio_element_cfdcur_update_byMarketImpl;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone a portfolio_element_cfdcur object
 *
 * clone complete portfolio_element object (deep copy original into clone), if 
 * original object gets modified or destroy()ed, the clone will still live
 * @param original portfolio_element pointer to original 
 * @returns portfolio_element pointer to object that will hold copy of 
 * original
 */
///////////////////////////////////////////////////////////////////////////////
static class_portfolio_element* class_portfolio_element_cfdcur_cloneImpl(const void* original)
{
    // first upcast void pointer of argument to right type
    class_portfolio_element_cfdcur* tmp_original = NULL;
    tmp_original = (class_portfolio_element_cfdcur*) original;
    
    // create pointer for cloned object
    class_portfolio_element_cfdcur* clone = NULL;
	
	// first "clone" object with basic init data
	clone = class_portfolio_element_cfdcur_init_manually(bdata(*tmp_original->symbol), bdata(*tmp_original->identifier), bdata(*tmp_original->markettype), bdata(*tmp_original->market_currency), 
					tmp_original->type, tmp_original->quantity, *tmp_original->Buyquote_market->quotevec, 
					tmp_original->Buyquote_market->nr_digits, bdata(*tmp_original->Buyquote_market->datevec), *tmp_original->Buyquote_market->daynrvec,
					*tmp_original->Buyquote_account->quotevec, tmp_original->Buyquote_account->nr_digits, bdata(*tmp_original->Buyquote_account->datevec), 
					*tmp_original->Buyquote_account->daynrvec, *tmp_original->Buyquote_signal->quotevec, tmp_original->Buyquote_signal->nr_digits, 
					bdata(*tmp_original->Buyquote_signal->datevec), *tmp_original->Buyquote_signal->daynrvec, bdata(*tmp_original->signalname), 
                    tmp_original->getMarketListPosition(tmp_original), tmp_original->getSL_list_idx(tmp_original));
	
	// now copy data from tmp_original to clone
	bdestroy(*clone->currQuote_account->datevec);	// free bstrings before assigning new
	bdestroy(*clone->currQuote_market->datevec);	// dates to prevent memory leaks
	*clone->currQuote_account->datevec = bstrcpy(*tmp_original->currQuote_account->datevec);
	*clone->currQuote_account->daynrvec = *tmp_original->currQuote_account->daynrvec;
	*clone->currQuote_account->quotevec = *tmp_original->currQuote_account->quotevec;
	*clone->currQuote_market->datevec = bstrcpy(*tmp_original->currQuote_market->datevec);
	*clone->currQuote_market->daynrvec = *tmp_original->currQuote_market->daynrvec;
	*clone->currQuote_market->quotevec = *tmp_original->currQuote_market->quotevec;
	
	clone->type = tmp_original->type;
	clone->stoploss = tmp_original->stoploss;
	clone->isl_flag = tmp_original->isl_flag;
	clone->pos_size = tmp_original->pos_size;
	clone->quantity = tmp_original->quantity;
	clone->trading_days = tmp_original->trading_days;
	clone->current_value = tmp_original->current_value;
	clone->invested_value = tmp_original->invested_value;
	clone->risk_free_value = tmp_original->risk_free_value;
	clone->p_l = tmp_original->p_l;
	clone->risk_free_p_l = tmp_original->risk_free_p_l;
	clone->p_l_percent = tmp_original->p_l_percent;
    clone->equity = tmp_original->equity;
    clone->rf_equity = tmp_original->rf_equity;
	
    // downcast object to parent class pointer and return
	return (class_portfolio_element*) clone;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief update values of portfolio element of type cfdcur "automatically" 
 * with given market
 *
 * This function updates a portfolio_element_cfdcur, calculating new values by 
 * using given market
 * 
 * @param self pointer to portfolio_element struct
 * @param new_sl float with latest stop loss
 * @param new_isl_flag boolean with updated initial stop loss flag (true/false)
 * @param new_trading_days uint with updated nr. of holding days (this element)
 * @param the_market pointer to market object
 * @param quotenr position of current price within market´s close QuoteObj
 * @param account_balance current balance of account
 */
///////////////////////////////////////////////////////////////////////////////
void class_portfolio_element_cfdcur_update_byMarketImpl(void* self, 
                    float new_sl, bool new_isl_flag, unsigned int new_trading_days, 
                    const class_market* the_market, unsigned int quotenr, float account_balance)
{
     // first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
	// find position of "close" indicator within market object
	const int closePos = the_market->getIndicatorPos(the_market, "close");
	check(closePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error, bdata(*the_market->symbol));

	float translation_quote = the_market->getTranslationCurrencyQuotebyIndex(the_market, quotenr);
	
    // reset nr of digits for all QuoteObj objects, as they
    // might have been overwritten after reading from database
    const unsigned int nr_digits_market = the_market->nr_digits;

	tmp_self->Buyquote_market->nr_digits = nr_digits_market;
    tmp_self->Buyquote_signal->nr_digits = nr_digits_market;
    tmp_self->currQuote_market->nr_digits = nr_digits_market;
    
	*tmp_self->currQuote_market->quotevec = the_market->Ind[closePos]->QUOTEVEC[quotenr];
	bdestroy(*tmp_self->currQuote_market->datevec); // bdestroy current string to prevent memory leaks when overwriting pointer
  	*tmp_self->currQuote_market->datevec = bstrcpy(the_market->Ind[closePos]->DATEVEC[quotenr]);
  	*tmp_self->currQuote_market->daynrvec = the_market->Ind[closePos]->DAYNRVEC[quotenr];
  	*tmp_self->currQuote_account->quotevec = *tmp_self->currQuote_market->quotevec / translation_quote;
  	*tmp_self->currQuote_account->daynrvec = the_market->getTranslationCurrencyDaynrbyIndex(the_market, quotenr);
	bdestroy(*tmp_self->currQuote_account->datevec); // bdestroy current string to prevent memory leaks when overwriting pointer

	bstring* tmpdate = NULL;
	tmpdate = the_market->getTranslationCurrencyDatebyIndex(the_market, quotenr);
	*tmp_self->currQuote_account->datevec = bstrcpy(*tmpdate);
 	free_1d_array_bstring(tmpdate,1);
	
	tmp_self->stoploss = new_sl;
	tmp_self->isl_flag = new_isl_flag;
	tmp_self->trading_days = new_trading_days;	
	
	switch(tmp_self->type)
	{
		case longsignal:
			{
                tmp_self->p_l = ((*tmp_self->currQuote_market->quotevec - *tmp_self->Buyquote_market->quotevec) / *tmp_self->currQuote_market->quotevec) * the_market->contract_size * tmp_self->quantity;
				tmp_self->risk_free_p_l = ((tmp_self->stoploss - *tmp_self->Buyquote_market->quotevec) / *tmp_self->currQuote_market->quotevec) * the_market->contract_size * tmp_self->quantity;
                break;
			}
		case shortsignal:
			{			
                tmp_self->p_l = (*tmp_self->currQuote_market->quotevec - *tmp_self->Buyquote_market->quotevec) / *tmp_self->currQuote_market->quotevec;
				tmp_self->p_l = (-1.0) * tmp_self->p_l * the_market->contract_size * tmp_self->quantity;
				tmp_self->risk_free_p_l = (*tmp_self->Buyquote_market->quotevec - tmp_self->stoploss) / *tmp_self->currQuote_market->quotevec * the_market->contract_size * tmp_self->quantity;
                break;
			}
		case undefined:
		default:
			break;
	}
	
    tmp_self->current_value = tmp_self->p_l;
    tmp_self->risk_free_value = tmp_self->risk_free_p_l;
    
    tmp_self->equity = tmp_self->p_l;
    tmp_self->rf_equity = tmp_self->risk_free_p_l;
    
    // the profit/loss in percent is related to account balance	
    if(tmp_self->p_l != 0)
    {
        tmp_self->p_l_percent = (tmp_self->p_l / account_balance) * 100;
    }
    else 
    {
        tmp_self->p_l_percent = 0.0;
    }
    
	return;
	
error:
	// clean up at least what is accessible from here
	tmp_self->destroy(tmp_self);
	exit(EXIT_FAILURE);
}


// end of file
