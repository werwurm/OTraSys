/* p1d_wrapper.h
 * declarations for p1d_wrapper.cpp
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file p1d_wrapper.h
 * @brief Header file for p1d_wrapper.cpp
 * 
 * This file contains the declarations for p1d_wrapper.cpp
 * @see persistence1d.hpp
 * @author Denis Zetzmann
 */

#ifndef P1DWRAPPER_H
#define P1DWRAPPER_H

// create "Class"
typedef void Class_Persistence1D;

// For C++ compiler, make public methods accessible to C
#ifdef __cplusplus
extern "C" {
#endif
Class_Persistence1D *WRAPPER_Persistence1D_new();
void WRAPPER_Persistence1D_delete(Class_Persistence1D *object);
float *WRAPPER_array2vector(float *datavec, int nr_data);
int get_extrema_indices(float *datavec, int nr_data, unsigned int **minima,unsigned int **maxima, unsigned int *nr_min);
#ifdef __cplusplus
}
#endif

#endif 	// P1DWRAPPER_H
/* End of file */

