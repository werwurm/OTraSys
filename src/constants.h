/* constants.h
 * globally used constants for better code readability/maintainability
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define COL_QUOTES_DAILY_OPEN	0
#define COL_QUOTES_DAILY_HIGH	1
#define COL_QUOTES_DAILY_LOW	2
#define COL_QUOTES_DAILY_CLOSE	3

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[93m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
	
#define HTML_RED_TEXT   bfromcstr("\"><strong><span style=\"color: #ff0000;")
#define	HTML_BLUE_TEXT  bfromcstr("\"><strong><span style=\"color: #0000ff;")
#define HTML_GREEN_TEXT bfromcstr("\"><strong><span style=\"color: #008000;")

#endif 	// CONSTANTS_H
/* End of file */
