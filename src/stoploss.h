/* stoploss.h
 * header file for stoploss.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file stoploss.h
 * @brief Header file for stoploss.c
 *
 * This file contains the declarations for stoploss.c 
 * @author Denis Zetzmann
 */

#ifndef STOPLOSS_H
#define STOPLOSS_H

#include <stdbool.h>
#include "class_market.h"

float new_stop_loss(class_market* the_market, unsigned int quote_idx, 
					signal_type the_type);
float percentage_stop(float percentage_at_risk, float price,
					signal_type type);
float chandelier_stop(float hh_ll, float atr, float atr_factor, signal_type type);
float atr_stop(float price, float atr, float atr_factor, signal_type type);
bool new_sl_needed(float newstop, float oldstop, signal_type type);

#endif // STOPLOSS_H 
