/* class_portfolio_element_cfdcur.h
 * declarations for class_portfolio_element_CFDFUT.h, interface 
 * for "portfolio_element_CFDFUT" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_portfolio_element_cfdcur.h
 * @brief Header file for class_portfolio_element_CFDFUT.h, public 
 * member declarations
 *
 * This file contains the "public" available data and functions of the 
 * portfolio_element_CFDFUT "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_PORTFOLIO_ELEMENT_CFDCUR_H
#define CLASS_PORTFOLIO_ELEMENT_CFDCUR_H

#include <stdbool.h>

#include "class_portfolio_element.h"

typedef struct _class_portfolio_element_cfdcur class_portfolio_element_cfdcur;

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a portfolio element containing currency CFD´s
 *          and its describing properties
 * 
 * This struct describes all the properties that are needed to describe an 
 * element of the portfolio (of type currency CDFs). All values are 
 * generated within the program, most are stored in the database and can 
 * retrieved from the database. The information of a portfolio element is 
 * based of the information of a signal, if the signal is executed.
 */ 
struct _class_portfolio_element_cfdcur
{
    struct _class_portfolio_element;   // inherits everything from portfolio_element
  
	// inherited METHODS, call like: foo_obj->bar_method(foo_obj)
 	// destroyPortfolioElementFunc destroy; /**< "destructor", foo_obj->destroy(foo_obj) */
    // 	clonePortfolioElementFunc clone;	/**< create new object with same data as given one */
	// printPortfolioElementTableFunc printTable;	/**< prints out quotes in tabular format, foo_obj->printTable(foo_obj) */
    // 	updatePortfolioElementbyMarketFunc update; /** update portfolio element "automatically" by market information */

    //private part
};

// "constructor" for portfolio_element "objects"
class_portfolio_element_cfdcur* class_portfolio_element_cfdcur_init(const class_market* the_market,const signal_type longorshort, 
                        const float quant, const unsigned int quotenr, const unsigned int indicatorpos, const char* signalstring, 
                        const class_accounts* the_account, const int marketListPosition, const int sl_list_idx);
class_portfolio_element_cfdcur* class_portfolio_element_cfdcur_init_manually(char* symbol, char* identifier, char* markettype, char* market_currency, 
						signal_type longorshort, float quant, float market_quote, unsigned int mq_digits, char* mq_date, unsigned int mq_daynr,
						float account_quote, unsigned int aq_digits, char* aq_date, unsigned int aq_daynr,
						float signal_quote, unsigned int sq_digits, char* sq_date, unsigned int sq_daynr, char* signalstring, 
                        const int marketListPosition, const int sl_list_idx);
#endif // CLASS_PORTFOLIO_ELEMENT_CFDFUT_H
// eof
