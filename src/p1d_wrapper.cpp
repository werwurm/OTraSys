/* p1d_wrapper.cpp
 * C wrapper for persistence1D C++ class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file p1d_wrapper.cpp
 * @brief C wrapper for persistence1D C++ class
 *
 * This file implements the public methods to create/destroy a p1d class
 * instance. Further it contains a function that calls the class methods
 * from within C++ and exposes them in a way that makes them accessible from C
 * @see persistence1d.hpp
 * @author Denis Zetzmann
 */

#include "p1d_wrapper.h"
#include "persistence1d.hpp"
#include "constants.h"

using namespace std;
using namespace p1d;

extern "C" {
	
///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "Constructor function" to Persistence1D object
 * 
 * This is the wrapping function for the Persistence1D object constructor, to
 * make it available from C
 * @return Pointer to new object
*/
///////////////////////////////////////////////////////////////////////////////
Class_Persistence1D *WRAPPER_Persistence1D_new()
{
	Persistence1D *object = new Persistence1D;
	return (Class_Persistence1D *) object;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "Destructor function" to Persistence1D object
 * 
 * This is the wrapping function for the Persistence1D object destructor, to
 * make it available from C
 * @param object Pointer to object
*/
///////////////////////////////////////////////////////////////////////////////
void WRAPPER_Persistence1D_delete(Class_Persistence1D *object)
{
	Persistence1D *t = (Persistence1D *) object;
	delete t;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief fills C++ float vector with data from C-style float Nx1 matrix
 * 
 * fills C++ float vector with data from C-style float Nx1 matrix
 * @param datavec pointer to float Nx1 matrix
 * @param nr_data number of values in matrix/vector
 * @returns pointer to vector
*/
///////////////////////////////////////////////////////////////////////////////
float *WRAPPER_array2vector(float *datavec, int nr_data)
{
	//create vector
	vector< float > data;

	//fill vector with data (from C-style matrix)
	int i;
	for(i=0; i< nr_data; i++)
		data.push_back(datavec[i]);
	
	return &data[0];
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief wrapping function to get local minima/maxima from Nx1 float matrix
 * 
 * This function utilizes the Persistence1D class functions to determince
 * local minima and maxima in a given float Nx1 matrix. It provides the 
 * interface in a way that makes it possible to call this function from C code.
 * @note the arrays minima and maxima, that contain the indices of the local
 * extrema, are resized and filled within this function 
 * @note Due to the nature of the algorithm there will always be a global 
 * minimum, which is determined separately by the class function. Thus
 * the number of minima will always be by 1 higher than the number of maxima.
 * A global maximum is not calculated by the library.
 * @param datavec pointer to float Nx1 matrix with data points
 * @param nr_data number of values in matrix
 * @param minima unsigned int matrix with minima
 * @param maxima unsigned int matrix with maxima
 * @param nr_min nr of local minima (equals nr. of local maxima)
 * @returns 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int get_extrema_indices(float *datavec, int nr_data, unsigned int **minima, unsigned int **maxima, unsigned int *nr_min)
{
	//create vector
	vector< float > data;

	//fill vector with data (from C-style matrix)
	int i;
	for(i=0; i< nr_data; i++)
		data.push_back(datavec[i]);
	
	// create vectors for local minima/maxima and paired extrema
	vector<int> min, max;			
	vector<TPairedExtrema> pairs;	
	
	// create instance of persistence1d object, run iton the data
	Persistence1D p;
	p.RunPersistence(data);
	
	// extract the indices of local extrema
	p.GetExtremaIndices(min, max);
	p.GetPairedExtrema(pairs);
	
//	p.PrintResults();
	
  	float filterThreshold = (pairs.front().Persistence + pairs.back().Persistence)/2*0.8;
 	p.GetExtremaIndices(min, max, filterThreshold);
 	p.GetPairedExtrema(pairs, filterThreshold);
	
	// create C-style parameters that store numbers of extremas  
	unsigned int nr_local_minima = static_cast<unsigned int>(min.size());
	unsigned int nr_local_maxima = static_cast<unsigned int>(max.size());
	
	*nr_min = nr_local_minima;
	
	// resize the given extrema arrays according to nr of found extremas
	*minima = (unsigned int*) realloc(*minima, (nr_local_minima+1) * sizeof(unsigned int));
	*maxima = (unsigned int*) realloc(*maxima, nr_local_maxima * sizeof(unsigned int));
	
    if(minima == NULL || maxima == NULL)
    {
        printf(ANSI_COLOR_RED "\nMemory allocation failed!" ANSI_COLOR_RESET);
        exit(EXIT_FAILURE);
    }
    
	// fill arrays with indices of extremas
	for(i=0; (unsigned int) i < nr_local_minima; i++)
	{
		(*minima)[i]=min[i];
		(*maxima)[i]=max[i];
	}

	// finally add global minimum index
	(*minima)[nr_local_minima] = p.GetGlobalMinimumIndex();
	
	// thats it :)
	return 0;
}


}
