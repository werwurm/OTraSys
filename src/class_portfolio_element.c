/* class_portfolio_element.c
 * Implements a "class"-like struct in C which handles portfolio elements
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_portfolio_element.c
 * @brief routines that implement a class-like struct which represents 
 * portfolio_elements
 *
 * This file contains the definitions of the "class" portfolio_elements
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "arrays.h"
#include "bstrlib.h"
#include "debug.h"

#include "class_account.h"
#include "class_portfolio.h"
#include "class_quote.h"
#include "datatypes.h"
#include "readconf.h"

static void class_portfolio_element_setMethods(class_portfolio_element* self);
static void class_portfolio_element_destroyPortfolioElementImpl(void* self);
static class_portfolio_element* class_portfolio_element_clonePortfolioElementImpl(const void* original);
static void class_portfolio_element_printPortfolioElementTableImpl(const void* self); 
static void class_portfolio_element_update_byMarketImpl(void *self, float new_sl, bool new_isl_flag, unsigned int new_trading_days, const class_market* the_market, unsigned int quotenr, float account_cash);
static int class_portfolio_element_getMarketListPositionImpl(const void* self);
static void class_portfolio_element_setMarketListPositionImpl(void* self, const int pos);
static int class_portfolio_element_getSL_list_idxImpl(const void* self);
static void class_portfolio_element_setSL_list_idxImpl(void* self, const int idx);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "private" parts of portfolio_element class. Do not touch your 
 * private parts. 
 * 
 * This struct holds the data that is considered to be private, e.g. cannot be
 * accessed outside this file (to do that use the getter/setter functions).
 * The struct is refered by an opaque pointer of "accounts" object
 * @see portfolio_class.h definition of portfolio class 
 * 
 */
///////////////////////////////////////////////////////////////////////////////
struct _portfolio_element_private{
	int posMarketList;	/**< position of particular market in market_list */
	int sl_list_idx;  /**< index of this element within a stoploss_list */
};
typedef struct _portfolio_element_private portfolio_element_private;

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for portfolio_element objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. portfolio_element_init takes a market as 
 * main parameter, so the object can
 * be initialized using the information stored within the market object
 * 
 * 
 * @param the_market pointer to market object with needed info
 * @param longorshort	signal_type (long/short) for new element
 * @param quant	float with quantity bought
 * @param quotenr use this quote/date/daynr dataset
 * @param indicatorpos position of the indicator within market object that
 * 			triggered the signal
 * @param signalstring char ptr with signal name
 * @param the_account pointer to accounts object
 * @return pointer to new object of class portfolio_element
*/
///////////////////////////////////////////////////////////////////////////////
class_portfolio_element* class_portfolio_element_init(const class_market* the_market,
					const signal_type longorshort, const float quant, 
					const unsigned int quotenr, const unsigned int indicatorpos, 
					const char* signalstring, const class_accounts* the_account, 
					const int marketListPosition, const int sl_list_idx)
{
	class_portfolio_element *self = NULL;
	
	// allocate memory for object
	self = (class_portfolio_element*) ZCALLOC(1, sizeof(class_portfolio_element));

	// reserve memory for private parts
	self->portfolio_element_private = (struct _portfolio_element_private*) ZCALLOC(1, sizeof(portfolio_element_private));

	bstring the_symbol = bstrcpy(*the_market->symbol);
    bstring the_identifier = bstrcpy(*the_market->identifier);
	
	// find position of "close" indicator within market object
	const int closePos = the_market->getIndicatorPos(the_market, "close");
	check(closePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error, bdata(*the_market->symbol));
	
	//call inherited class constructors
	self->Buyquote_market = class_quotes_init(bdata(the_symbol), bdata(the_identifier), 1, the_market->Ind[closePos]->NR_DIGITS, "buyprice in market currency", "none");
 	self->Buyquote_account = class_quotes_init(bdata(*the_account->currency), bdata(the_identifier), 1, the_account->nr_digits, "buyprice in account currency", "none");
 	self->Buyquote_signal = class_quotes_init(bdata(the_symbol), bdata(the_identifier), 1, the_market->Ind[indicatorpos]->QuoteObj->nr_digits, "signal quote in market currency", "none");
 	self->currQuote_market = class_quotes_init(bdata(the_symbol), bdata(the_identifier),  1, the_market->Ind[closePos]->NR_DIGITS, "current quote market currency", "none");
 	self->currQuote_account = class_quotes_init(bdata(*the_account->currency), bdata(the_identifier), 1, the_account->nr_digits, "current price in account currency", "none");
 
 	//allocate memory for rest of the object
 	self->symbol = init_1d_array_bstring(1);
    self->identifier = init_1d_array_bstring(1);
	self->markettype = init_1d_array_bstring(1);
	self->market_currency = init_1d_array_bstring(1);
 	self->signalname = init_1d_array_bstring(1);
 
 	//init data
 	*self->symbol = bstrcpy(the_symbol);
    *self->identifier = bstrcpy(the_identifier);
	*self->markettype = bstrcpy(*the_market->markettype);
	*self->market_currency = bstrcpy(*the_market->currency);
 	*self->signalname = bfromcstr(signalstring);
	
 	// init buyprice in different currencies
 	// as for portfolio elements the Quote Obj contains only 1st element, skip ...XYZvec[0]
 	*self->Buyquote_market->quotevec = the_market->Ind[closePos]->QUOTEVEC[quotenr];
 	*self->Buyquote_market->datevec = bstrcpy(the_market->Ind[closePos]->DATEVEC[quotenr]);
 	*self->Buyquote_market->daynrvec = the_market->Ind[closePos]->DAYNRVEC[quotenr];
	*self->Buyquote_account->quotevec = the_market->Ind[closePos]->QUOTEVEC[quotenr] / the_market->getTranslationCurrencyQuotebyIndex(the_market, quotenr);
	*self->Buyquote_account->datevec = bstrcpy(the_market->Ind[closePos]->DATEVEC[quotenr]);
 	*self->Buyquote_account->daynrvec = the_market->Ind[closePos]->DAYNRVEC[quotenr];

	*self->Buyquote_signal->quotevec = the_market->Ind[indicatorpos]->QUOTEVEC[quotenr];
 	*self->Buyquote_signal->datevec = bstrcpy(the_market->Ind[indicatorpos]->DATEVEC[quotenr]);
 	*self->Buyquote_signal->daynrvec = the_market->Ind[indicatorpos]->DAYNRVEC[quotenr];
	
 	//init current quotes, which equals Buyquotes 
 	*self->currQuote_market->quotevec = *self->Buyquote_market->quotevec;
 	*self->currQuote_market->datevec = bstrcpy(*self->Buyquote_market->datevec);
	*self->currQuote_market->daynrvec = *self->Buyquote_market->daynrvec;
 	*self->currQuote_account->quotevec = *self->Buyquote_account->quotevec;
 	*self->currQuote_account->datevec = bstrcpy(*self->Buyquote_account->datevec);
	*self->currQuote_account->daynrvec = *self->Buyquote_account->daynrvec;

	self->type = longorshort;
 	self->stoploss = 0.0;
 	self->isl_flag = true;
 	self->pos_size = 0.0;
 	self->quantity = quant;
 	self->trading_days = 0;
	self->current_value = 0.0;
	self->invested_value = *self->Buyquote_account->quotevec * quant;
    self->risk_free_value = 0.0;
 	self->p_l = 0.0;
 	self->risk_free_p_l = 0.0;
	self->p_l_percent = 0.0;
    self->equity = 0.0;
    self->rf_equity = 0.0;
	
	// init private variables
	self->portfolio_element_private->posMarketList = marketListPosition;
    self->portfolio_element_private->sl_list_idx = sl_list_idx;

 	// set methods
    class_portfolio_element_setMethods(self);
	
	bdestroy(the_symbol);
	return self;	
	
error:
	bdestroy(the_symbol);
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for portfolio_elemet objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to portfolio_element struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_portfolio_element_setMethods(class_portfolio_element* self)
{
 	self->destroy = class_portfolio_element_destroyPortfolioElementImpl;
 	self->clone = class_portfolio_element_clonePortfolioElementImpl;
 	self->printTable = class_portfolio_element_printPortfolioElementTableImpl;
	self->update = class_portfolio_element_update_byMarketImpl;    
	self->getMarketListPosition = class_portfolio_element_getMarketListPositionImpl;
	self->setMarketListPosition = class_portfolio_element_setMarketListPositionImpl;
    self->getSL_list_idx = class_portfolio_element_getSL_list_idxImpl;
    self->setSL_list_idx = class_portfolio_element_setSL_list_idxImpl;
    
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys a portfolio_element object
 *
 * This function destroys a portfolio_element struct by freeing the occupied 
 * memory
 * 
 * @param self pointer to portfolio_element struct
 */
///////////////////////////////////////////////////////////////////////////////
void class_portfolio_element_destroyPortfolioElementImpl(void* self)
{
    
    // first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
    // free private data
	free(tmp_self->portfolio_element_private);
    tmp_self->portfolio_element_private=NULL;
    
	tmp_self->Buyquote_market->destroy(tmp_self->Buyquote_market);
	tmp_self->Buyquote_account->destroy(tmp_self->Buyquote_account);
	tmp_self->Buyquote_signal->destroy(tmp_self->Buyquote_signal);
	tmp_self->currQuote_account->destroy(tmp_self->currQuote_account);
	tmp_self->currQuote_market->destroy(tmp_self->currQuote_market);
	
	free_1d_array_bstring(tmp_self->symbol, 1);
    free_1d_array_bstring(tmp_self->identifier, 1);
	free_1d_array_bstring(tmp_self->market_currency, 1);
	free_1d_array_bstring(tmp_self->signalname, 1);
	free_1d_array_bstring(tmp_self->markettype, 1);
	free(tmp_self); 
    tmp_self=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone a portfolio_element object
 *
 * clone complete portfolio_element object (deep copy original into clone), if 
 * original object gets modified or destroy()ed, the clone will still live
 * @param original portfolio_element pointer to original 
 * @returns portfolio_element pointer to object that will hold copy of original
 */
///////////////////////////////////////////////////////////////////////////////
static class_portfolio_element* class_portfolio_element_clonePortfolioElementImpl(const void* original)
{
	// first upcast void pointer of argument to right type
    class_portfolio_element* tmp_original = NULL;
    tmp_original = (class_portfolio_element*) original;
    
    class_portfolio_element* clone = NULL;
	
	// first "clone" object with basic init data
	clone = class_portfolio_element_init_manually(bdata(*tmp_original->symbol), bdata(*tmp_original->identifier), bdata(*tmp_original->markettype), bdata(*tmp_original->market_currency), 
					tmp_original->type, tmp_original->quantity, *tmp_original->Buyquote_market->quotevec, 
					tmp_original->Buyquote_market->nr_digits, bdata(*tmp_original->Buyquote_market->datevec), *tmp_original->Buyquote_market->daynrvec,
					*tmp_original->Buyquote_account->quotevec, tmp_original->Buyquote_account->nr_digits, bdata(*tmp_original->Buyquote_account->datevec), 
					*tmp_original->Buyquote_account->daynrvec, *tmp_original->Buyquote_signal->quotevec, tmp_original->Buyquote_signal->nr_digits, 
					bdata(*tmp_original->Buyquote_signal->datevec), *tmp_original->Buyquote_signal->daynrvec, bdata(*tmp_original->signalname), 
                    tmp_original->getMarketListPosition(tmp_original), tmp_original->portfolio_element_private->sl_list_idx);
	
	// now copy data from original to clone
	bdestroy(*clone->currQuote_account->datevec);	// free bstrings before assigning new
	bdestroy(*clone->currQuote_market->datevec);	// dates to prevent memory leaks
	*clone->currQuote_account->datevec = bstrcpy(*tmp_original->currQuote_account->datevec);
	*clone->currQuote_account->daynrvec = *tmp_original->currQuote_account->daynrvec;
	*clone->currQuote_account->quotevec = *tmp_original->currQuote_account->quotevec;
	*clone->currQuote_market->datevec = bstrcpy(*tmp_original->currQuote_market->datevec);
	*clone->currQuote_market->daynrvec = *tmp_original->currQuote_market->daynrvec;
	*clone->currQuote_market->quotevec = *tmp_original->currQuote_market->quotevec;
	
	clone->type = tmp_original->type;
	clone->stoploss = tmp_original->stoploss;
	clone->isl_flag = tmp_original->isl_flag;
	clone->pos_size = tmp_original->pos_size;
	clone->quantity = tmp_original->quantity;
	clone->trading_days = tmp_original->trading_days;
	clone->current_value = tmp_original->current_value;
	clone->invested_value = tmp_original->invested_value;
	clone->risk_free_value = tmp_original->risk_free_value;
	clone->p_l = tmp_original->p_l;
	clone->risk_free_p_l = tmp_original->risk_free_p_l;
	clone->p_l_percent = tmp_original->p_l_percent;
	clone->equity = tmp_original->equity;
    clone->rf_equity = tmp_original->rf_equity;
    
    clone->portfolio_element_private->posMarketList = tmp_original->portfolio_element_private->posMarketList;
	return clone;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "alternative constructor" function for portfolio_element objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. This implementation does not use market
 * object and should not be used when market object information is not
 * available (e.g. when reading from database). Furthermore it makes 
 * portfolio_element_clonePortfolioElementImpl easier to implement
 * 
 * @return pointer to new object of class portfolio_element
*/
///////////////////////////////////////////////////////////////////////////////
class_portfolio_element* class_portfolio_element_init_manually(char* symbol, char* identifier, char* markettype, char* market_currency, signal_type longorshort,
						float quant, float market_quote, unsigned int mq_digits, char* mq_date, unsigned int mq_daynr,
						float account_quote, unsigned int aq_digits, char* aq_date, unsigned int aq_daynr,
						float signal_quote, unsigned int sq_digits, char* sq_date, unsigned int sq_daynr, char* signalstring, const int marketListPosition, const int sl_list_idx)
{
	class_portfolio_element *self = NULL;
	
	// allocate memory for object
	self = (class_portfolio_element*) ZCALLOC(1, sizeof(class_portfolio_element));
	
	// reserve memory for private parts
	self->portfolio_element_private = (struct _portfolio_element_private*) ZCALLOC(1, sizeof(portfolio_element_private));
	
	//call inherited class constructors
	self->Buyquote_market = class_quotes_init(symbol, identifier, 1, mq_digits, "buyprice in market currency", "none");
	self->Buyquote_account = class_quotes_init(symbol, identifier, 1, aq_digits, "buyprice in account currency", "none");
	self->Buyquote_signal = class_quotes_init(symbol, identifier, 1, sq_digits, "signal quote in market currency", "none");
	self->currQuote_market = class_quotes_init(symbol, identifier, 1, mq_digits, "current quote market currency", "none");
	self->currQuote_account = class_quotes_init(symbol, identifier, 1, aq_digits, "current price in account currency", "none");

	//allocate memory for rest of the object
	self->symbol = init_1d_array_bstring(1);
    self->identifier = init_1d_array_bstring(1);
	self->market_currency = init_1d_array_bstring(1);
	self->signalname = init_1d_array_bstring(1);
	self->markettype = init_1d_array_bstring(1);

	//init data
	*self->symbol = bfromcstr(symbol);
    *self->identifier = bfromcstr(identifier);
	*self->market_currency = bfromcstr(market_currency);
	*self->signalname = bfromcstr(signalstring);
	*self->markettype = bfromcstr(markettype);
	
	// init buyprice in different currencies
	// as for portfolio elements the Quote Obj contains only 1st element, skip ...XYZvec[0]
	*self->Buyquote_market->quotevec = market_quote;
	*self->Buyquote_market->datevec = bfromcstr(mq_date);
	*self->Buyquote_market->daynrvec = mq_daynr;
	*self->Buyquote_account->quotevec = account_quote;
	*self->Buyquote_account->datevec = bfromcstr(aq_date);
	*self->Buyquote_account->daynrvec = aq_daynr;
	*self->Buyquote_signal->quotevec = signal_quote;
	*self->Buyquote_signal->datevec = bfromcstr(sq_date);
	*self->Buyquote_signal->daynrvec = sq_daynr;
	//init current quotes, which equals Buyquotes 
	*self->currQuote_market->quotevec = market_quote;
	*self->currQuote_market->datevec = bfromcstr(mq_date);
	*self->currQuote_market->daynrvec = mq_daynr;
	*self->currQuote_account->quotevec = account_quote;
	*self->currQuote_account->datevec = bfromcstr(aq_date);
	*self->currQuote_account->daynrvec = aq_daynr;
	
	self->type = longorshort;
	self->stoploss = 0.0;
	self->isl_flag = true;
	self->pos_size = 0.0;
	self->quantity = quant;
	self->trading_days = 0;
	self->current_value = 0.0;
    self->invested_value = *self->Buyquote_account->quotevec * quant;
	self->risk_free_value = 0.0;
	self->p_l = 0.0;
	self->risk_free_p_l = 0.0;
	self->p_l_percent = 0.0;
	
	// init private variables
	self->portfolio_element_private->posMarketList = marketListPosition;
    self->portfolio_element_private->sl_list_idx = sl_list_idx;
	
	// set methods
    class_portfolio_element_setMethods(self);
	return self;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints portfolio_element info
 *
 * This function prints portfolio_element info to terminal
 * 
 * @param self pointer to portfolio_element struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_portfolio_element_printPortfolioElementTableImpl(const void* self)
{
    // first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
    extern parameter parms; // parms are declared global in main.c
	bstring typestring=NULL;
	switch(tmp_self->type)
	{
		case longsignal:
			typestring=bfromcstr("long");
			break;
		case shortsignal:
			typestring=bfromcstr("short");
			break;
		case undefined:
		default: 
			typestring=bfromcstr("undefined");			
			break;
	}
	
	//symbol (identifier) buydate type stoploss iSL   quantity buy price  last price  P/L  P/L @SL days    signalname
		
	printf("\n%s (%s)  %s  %s  %s  SL:%.*f  quant:%.2f  buy qte:%.*f  last qte:%.*f  P/L:%.*f %s  P/L@sl:%.*f %s  days:%u  %s", 
		bdata(*tmp_self->symbol), bdata(*tmp_self->identifier), 
        bdata(*tmp_self->markettype), bdata(*tmp_self->Buyquote_market->datevec), bdata(typestring), 
		tmp_self->Buyquote_market->nr_digits, tmp_self->stoploss,  
		tmp_self->quantity,
		tmp_self->Buyquote_market->nr_digits, *tmp_self->Buyquote_market->quotevec,
		tmp_self->currQuote_market->nr_digits, *tmp_self->currQuote_market->quotevec,
		tmp_self->Buyquote_account->nr_digits, tmp_self->p_l,
		bdata(parms.ACCOUNT_CURRENCY),
		tmp_self->Buyquote_account->nr_digits, tmp_self->risk_free_p_l,
		bdata(parms.ACCOUNT_CURRENCY),
		tmp_self->trading_days, bdata(*tmp_self->signalname));

	bdestroy(typestring);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief update values of portfolio element "automatically" with given market
 *
 * This function updates a portfolio_element, calculating new values by using
 * given market
 * 
 * @see portfolio_element_2_updateImpl for "manual" update
 * 
 * @param self pointer to portfolio_element struct
 * @param new_sl float with latest stop loss
 * @param new_isl_flag boolean with updated initial stop loss flag (true/false)
 * @param new_trading_days uint with updated nr. of holding days (this element)
 * @param the_market pointer to market object
 * @param quotenr position of current price within market´s close QuoteObj
 * @param account_cash current cash of account
 */
///////////////////////////////////////////////////////////////////////////////
void class_portfolio_element_update_byMarketImpl(void* self, float new_sl, bool new_isl_flag, unsigned int new_trading_days, const class_market* the_market, unsigned int quotenr, float account_cash)
{
    // first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;

    log_warn("virtual function called for market %s.. treating as markettype=STOCK", bdata(*tmp_self->symbol));

	// find position of "close" indicator within market object
	const int closePos = the_market->getIndicatorPos(the_market, "close");
	check(closePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error, bdata(*the_market->symbol));

	float translation_quote = the_market->getTranslationCurrencyQuotebyIndex(the_market, quotenr);
	
    // reset nr of digits for all QuoteObj objects, as they
    // might have been overwritten after reading from database
    const unsigned int nr_digits_market = the_market->nr_digits;

	tmp_self->Buyquote_market->nr_digits = nr_digits_market;
    tmp_self->Buyquote_signal->nr_digits = nr_digits_market;
    tmp_self->currQuote_market->nr_digits = nr_digits_market;
    
	*tmp_self->currQuote_market->quotevec = the_market->Ind[closePos]->QUOTEVEC[quotenr];
	bdestroy(*tmp_self->currQuote_market->datevec); // bdestroy current string to prevent memory leaks when overwriting pointer
  	*tmp_self->currQuote_market->datevec = bstrcpy(the_market->Ind[closePos]->DATEVEC[quotenr]);
  	*tmp_self->currQuote_market->daynrvec = the_market->Ind[closePos]->DAYNRVEC[quotenr];
  	*tmp_self->currQuote_account->quotevec = *tmp_self->currQuote_market->quotevec / translation_quote;
  	*tmp_self->currQuote_account->daynrvec = the_market->getTranslationCurrencyDaynrbyIndex(the_market, quotenr);
	bdestroy(*tmp_self->currQuote_account->datevec); // bdestroy current string to prevent memory leaks when overwriting pointer

	bstring* tmpdate = NULL;
	tmpdate = the_market->getTranslationCurrencyDatebyIndex(the_market, quotenr);
	*tmp_self->currQuote_account->datevec = bstrcpy(*tmpdate);
 	free_1d_array_bstring(tmpdate,1);
	
	tmp_self->stoploss = new_sl;
	tmp_self->isl_flag = new_isl_flag;
	tmp_self->trading_days = new_trading_days;	
	
	tmp_self->current_value = *tmp_self->currQuote_market->quotevec / translation_quote * tmp_self->quantity * the_market->contract_size;
	tmp_self->risk_free_value = tmp_self->stoploss / translation_quote * tmp_self->quantity;

	// TODO: self->p_l_percent makes more sense when related to current account´s cash/ cash+equity ==> change that
	
	switch(tmp_self->type)
	{
		case longsignal:
			{
				tmp_self->p_l = ((*tmp_self->currQuote_account->quotevec - *tmp_self->Buyquote_account->quotevec) * the_market->contract_size) * tmp_self->quantity;
				tmp_self->risk_free_p_l = (tmp_self->stoploss - *tmp_self->Buyquote_market->quotevec) / translation_quote  * the_market->contract_size * tmp_self->quantity;
                break;
			}

		case shortsignal:
			{			
				tmp_self->p_l = ((*tmp_self->Buyquote_account->quotevec - *tmp_self->currQuote_account->quotevec) * the_market->contract_size) * tmp_self->quantity;
				tmp_self->risk_free_p_l = (*tmp_self->Buyquote_market->quotevec - tmp_self->stoploss) / translation_quote  * the_market->contract_size * tmp_self->quantity;
                break;
			}
		case undefined:
		default:
			break;
	}
	
	if(tmp_self->p_l != 0)
    {
        tmp_self->p_l_percent = (tmp_self->p_l / account_cash) * 100;
    }
    else 
    {
        tmp_self->p_l_percent = 0.0;
    }
	
	return;
	
error:
	// clean up at least what is accessible from here
	tmp_self->destroy(tmp_self);
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief returns the position of a portfolio_element`s market within a 
 * market_list
 *
 * This function returns the position of the portfolio_element´s market. The 
 * position is stored as private variable of the object, the function serves
 * as a simple getter function (the position is not calculated here but 
 * determined within the constructor class_portfolio_element_init()).
 * @Note while theoretically a trading system can use several market_lists
 * (however I cannot imagine the practical use of that), this single variable
 * can store only _one_ certain position.
 * 
 * @param self pointer to portfolio_element struct
 * @return position as int
 */
///////////////////////////////////////////////////////////////////////////////
static int class_portfolio_element_getMarketListPositionImpl(const void* self)
{
	// first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
	return tmp_self->portfolio_element_private->posMarketList;
}

//////////////////////////////////////////////////////////////////////////////
/**
 * @brief sets the position of a portfolio_element`s market within a 
 * market_list
 *
 * This function sets the position of the portfolio_element´s market. The 
 * position is stored as private variable of the object, the function serves
 * as a simple setter function.
 * @Note in general, the position is set by the constructor, so the caller of
 * the constructor is responsible for determining the right position. However
 * there might be situations when the caller has no idea of this position, so
 * it has to be set later by this setter method (for example if loading the 
 * portfolio from database, the database function reads the values directly 
 * into portfolio_lement objects without knowing anything about a market_list
 * 
 * @param self pointer to portfolio_element struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_portfolio_element_setMarketListPositionImpl(void* self, const int pos)
{
	// first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
    tmp_self->portfolio_element_private->posMarketList = pos;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief returns the position of a portfolio_element`s sl_record within a 
 * sl_record_list
 *
 * This function returns the position of a portfolio_element`s sl_record within 
 * a sl_record_list
 * 
 * @param self pointer to portfolio_element struct
 * @return index as int
 */
///////////////////////////////////////////////////////////////////////////////
static int class_portfolio_element_getSL_list_idxImpl(const void* self)
{
    // first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
    return tmp_self->portfolio_element_private->sl_list_idx;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief sets the position of a portfolio_element`s sl_record within a 
 * sl_record_list
 *
 * This function sets the position of a portfolio_element`s sl_record within 
 * a sl_record_list
 * 
 * @param self pointer to portfolio_element struct
 * @param index as int
 */
///////////////////////////////////////////////////////////////////////////////
static void class_portfolio_element_setSL_list_idxImpl(void* self, const int idx)
{
    // first upcast void pointer of argument to right type
    class_portfolio_element* tmp_self = NULL;
    tmp_self = (class_portfolio_element*) self;
    
    tmp_self->portfolio_element_private->sl_list_idx = idx;
}
// eof
