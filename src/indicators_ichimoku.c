/* indicators_ichimoku.c
 * routines to calculate ichimoku-specific indicators
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file indicators_ichimoku.c
 * @brief calculation of various ichimoku-specific indicators
 *
 * This file implements ichimoku-specific indicators, like Kijun-Sen,
 * Tenkan-Sen, Chikou-Span, Senkou A and Senkou B
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <math.h>

#include "arrays.h"
#include "date.h"
#include "database.h"
#include "indicators_general.h"
#include "indicators_ichimoku.h"
#include "readconf.h"


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the ichimoku Tenkan-Sen indicator 
 *
 * Calculates Tenkan Sen by averaging short term highest highs/lowest 
 * lows (ichimoku averaging period default 9 days). Result is
 * stored in parameter indicator *tenkan_sen
 *
 * @param period uint, averaging period
 * @param hhshort pointer to indicator with highest highs (short term)
 * @param llshort pointer to indicator with lowest lows (short term)
 * @param tenkan_sen pointer to indicator struct with tenkan_sen
 */
///////////////////////////////////////////////////////////////////////////////
void ichimoku_tenkan(unsigned int period, class_indicators *hhshort, class_indicators *llshort, \
		class_indicators *tenkan_sen)
{
	CHECK_EQUAL_LENGTH(hhshort, llshort);
	CHECK_EQUAL_LENGTH(llshort, tenkan_sen);
	for (unsigned int i=period-1; i< tenkan_sen->NR_ELEMENTS; i++)
		tenkan_sen->QUOTEVEC[i]=(hhshort->QUOTEVEC[i] + llshort->QUOTEVEC[i]) / 2.0;
	
	for(unsigned int i=0; i<period-1; i++)
		tenkan_sen->QUOTEVEC[i]=0.0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the ichimoku Kijun-Sen indicator 
 *
 * Calculates Kijun Sen by averaging mid term highest highs/lowest 
 * lows (ichimoku averaging period default 26 days). Result is
 * stored in parameter indicator *tenkan_sen
 * 
 * @param period uint, averaging period
 * @param hhmid pointer to indicator with highest highs (mid term)
 * @param llmid pointer to indicator with lowest lows (mid term)
 * @param kijun_sen pointer to indicator struct with kijun_sen
 */
///////////////////////////////////////////////////////////////////////////////
void ichimoku_kijun(unsigned int period, class_indicators *hhmid, class_indicators *llmid, \
		class_indicators *kijun_sen)
{
	CHECK_EQUAL_LENGTH(hhmid, llmid);
	CHECK_EQUAL_LENGTH(llmid, kijun_sen);
	
	for (unsigned int i=period-1; i< (kijun_sen->NR_ELEMENTS); i++)
		kijun_sen->QUOTEVEC[i]=(hhmid->QUOTEVEC[i] + llmid->QUOTEVEC[i]) / 2.0;
		
	for (unsigned int i=0; i<period-1; i++)
		kijun_sen->QUOTEVEC[i]=0.0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the ichimoku chikou span indicator 
 *
 * Calculates chikou span by lagging the close quotes about the given period
 * (ichimoku period default 26 days). Result isstored in parameter 
 * indicator *chikou_span
 * 
 * @param lag_period uint, lagging period
 * @param closes pointer to indicator with close quotes
 * @param chikou_span: pointer to indicator struct with chikou span
 */
///////////////////////////////////////////////////////////////////////////////
void ichimoku_chikou(unsigned int lag_period, class_indicators* closes, class_indicators* chikou_span)
{
	CHECK_EQUAL_LENGTH(closes, chikou_span);
	for(unsigned int i = 0; i< closes->NR_ELEMENTS - lag_period; i++)
	{
// 		bstring* tempdate;
// 		tempdate = get_bstr_from_daynr(closes->DAYNRVEC[i]); // save content of pointer to prevent memory leak
		bdestroy(chikou_span->DATEVEC[i]);
		chikou_span->DATEVEC[i] = bstrcpy(closes->DATEVEC[i]);
		chikou_span->DAYNRVEC[i] = closes->DAYNRVEC[i];
		chikou_span->QUOTEVEC[i] = closes->QUOTEVEC[i+lag_period];
// 		free_1d_array_bstring(tempdate, 1);
	}
	// check if the following is ok with the database queries...if not find a way
	// to snipp the trailing "non data" sets
	for(unsigned int i = closes->NR_ELEMENTS - lag_period; i < closes->NR_ELEMENTS; i++)
	{
		chikou_span->QUOTEVEC[i] = NAN;	
	}
		
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the ichimoku Senkou Span A indicator 
 *
 * Calculates Senkou Span A by averaging tenkan and kijun. Result is
 * stored in parameter indicator *senkou_span_A
 * @note this function does shift the dates by period days into the future
 * as senkou Span A is a leading indicator (ichimoku default 26 days).
 *
 * @param shift_period uint nr of days that indicator is shifted into future
 * @param tenkan_sen pointer to indicator with Tenkan Sen
 * @param kijun_sen pointer to indicator with Kijun Sen
 * @param senkou_span_A pointer to indicator struct with Senkou Span A
 */
///////////////////////////////////////////////////////////////////////////////
void ichimoku_senkou_A(unsigned int shift_period, class_indicators *tenkan_sen, class_indicators *kijun_sen, \
		class_indicators *senkou_span_A)
{
	CHECK_EQUAL_LENGTH(tenkan_sen, kijun_sen);
	CHECK_EQUAL_LENGTH(kijun_sen, senkou_span_A);
	
	// create first daynr for senkouA, shift first tenkan daynr period days into future
	senkou_span_A->DAYNRVEC[0] = kijun_sen->DAYNRVEC[0 + shift_period];
	bstring *tmp = NULL;
	tmp = get_bstr_from_daynr(senkou_span_A->DAYNRVEC[0]);
	bdestroy(senkou_span_A->DATEVEC[0]);
	senkou_span_A->DATEVEC[0] = bstrcpy(*tmp);
	free_1d_array_bstring(tmp,1);
	
	// now shift existing dayrs (in kijun sen) by period days into the 
	// future. we use kijun sen here, because quotes are pulled in from
	// db, so they won't contain weekends and holidays
	for(unsigned int i = 1; i<(kijun_sen->NR_ELEMENTS-shift_period); i++)
		senkou_span_A->DAYNRVEC[i] = kijun_sen->DAYNRVEC[i + shift_period];
	
	// for the remaining period days we have to extrapolate. because we
	// have no clue about local (=markets) holidays, we estimate only
	// workdays, skipping weekends. The remaining holes (caused by holidays)
	// will be cleared out later in the database (by looking back where 
	// calculated senkou values are where no quotes are present
	for(unsigned int i=kijun_sen->NR_ELEMENTS-shift_period; i< kijun_sen->NR_ELEMENTS; i++)
	{
		unsigned int nextday = senkou_span_A->DAYNRVEC[i-1] + 1;
		
		if(!daynrIsWeekday(nextday) && !daynrIsWeekday(nextday+1))	// nextday is a saturday
			nextday = nextday + 2;	// so add two more days
		
		senkou_span_A->DAYNRVEC[i] = nextday;	
		
	}
	
	// calc senkou, pull first quote element out of next loop
	if ((senkou_span_A->QUOTEVEC[0] !=0) && (kijun_sen->QUOTEVEC[0] !=0))
		senkou_span_A->QUOTEVEC[0]= \
			(senkou_span_A->QUOTEVEC[0] + kijun_sen->QUOTEVEC[0]) / 2.0;
	else
		senkou_span_A->QUOTEVEC[0] = 0;	
		
	// now loop from 2nd element 'til the end
	for (unsigned int i=1; i< (tenkan_sen->NR_ELEMENTS); i++)
	{
		bstring* tempdate;
		
		tempdate = get_bstr_from_daynr(senkou_span_A->DAYNRVEC[i]); // save content of pointer to prevent memory leak
		bdestroy(senkou_span_A->DATEVEC[i]);
		senkou_span_A->DATEVEC[i] = bstrcpy(*tempdate);
		
		free_1d_array_bstring(tempdate, 1);

		
		if ((tenkan_sen->QUOTEVEC[i] !=0) && (kijun_sen->QUOTEVEC[i] !=0))
			senkou_span_A->QUOTEVEC[i]= \
				(tenkan_sen->QUOTEVEC[i] + kijun_sen->QUOTEVEC[i]) / 2.0;
		else
			senkou_span_A->QUOTEVEC[i] = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the ichimoku Senkou Span B indicator 
 *
 * Calculates Senkou Span B by averaging highest highs/lowest lows (for
 * long period, default: 52 days). Result is stored in parameter 
 * indicator *senkou_span_B
 * 
 * @note this function does shift the dates by period days into the future
 * as senkou Span A is a leading indicator (ichimoku default 26 days).
 *
 * @param shift_period uint nr of days that indicator is shifted into future
 * @param hh_long pointer to indicator with highest highs (long term)
 * @param ll_long pointer to indicator with lowest lows (long term)
 * @param senkou_span_B pointer to indicator struct with Senkou Span B
 */
///////////////////////////////////////////////////////////////////////////////
void ichimoku_senkou_B(unsigned int shift_period, class_indicators *hh_long, class_indicators *ll_long, \
		class_indicators *senkou_span_B)
{
	CHECK_EQUAL_LENGTH(hh_long, ll_long);
	CHECK_EQUAL_LENGTH(ll_long, senkou_span_B);
	
	// create first daynr for senkouB, shift first llLong daynr period days into future
	senkou_span_B->DAYNRVEC[0] = ll_long->DAYNRVEC[0 + shift_period];
	bstring *tmp;
	tmp = get_bstr_from_daynr(senkou_span_B->DAYNRVEC[0]);
	bdestroy(senkou_span_B->DATEVEC[0]);
	senkou_span_B->DATEVEC[0] = bstrcpy(*tmp);
	free_1d_array_bstring(tmp,1);
	
	// now shift existing dayrs (in ll_long) by period days into the 
	// future. we use ll_long here, because quotes are pulled in from
	// db, so they won't contain weekends and holidays
	for(unsigned int i = 1; i<(ll_long->NR_ELEMENTS-shift_period); i++)
		senkou_span_B->DAYNRVEC[i] = ll_long->DAYNRVEC[i + shift_period];
	
	// for the remaining period days we have to extrapolate. because we
	// have no clue about local (=markets) holidays, we estimate only
	// workdays, skipping weekends. The remaining holes (caused by holidays)
	// will be cleared out later in the database (by looking back where 
	// calculated senkou values are where no quotes are present
	for(unsigned int i=ll_long->NR_ELEMENTS-shift_period; i< ll_long->NR_ELEMENTS; i++)
	{
		unsigned int nextday = senkou_span_B->DAYNRVEC[i-1] + 1;
		
		if(!daynrIsWeekday(nextday) && !daynrIsWeekday(nextday+1))	// nextday is a saturday
			nextday = nextday + 2;	// so add two more days
		
		senkou_span_B->DAYNRVEC[i] = nextday;	
		
	}
	
	// calc senkouB, pull first quote element out of next loop
	if ((senkou_span_B->QUOTEVEC[0] !=0) && (ll_long->QUOTEVEC[0] !=0))
		senkou_span_B->QUOTEVEC[0]= \
			(senkou_span_B->QUOTEVEC[0] + ll_long->QUOTEVEC[0]) / 2.0;
	else
		senkou_span_B->QUOTEVEC[0] = 0;	
		
	// now loop from 2nd element 'til the end
	for (unsigned int i=1; i< (ll_long->NR_ELEMENTS); i++)
	{
		bstring* tempdate;
		
		tempdate = get_bstr_from_daynr(senkou_span_B->DAYNRVEC[i]); // save content of pointer to prevent memory leak
		bdestroy(senkou_span_B->DATEVEC[i]);
		senkou_span_B->DATEVEC[i] = bstrcpy(*tempdate);
		
		free_1d_array_bstring(tempdate, 1);
		
		if ((hh_long->QUOTEVEC[i]!=0) && (ll_long->QUOTEVEC[i]!=0))
			senkou_span_B->QUOTEVEC[i]= \
				(hh_long->QUOTEVEC[i] + ll_long->QUOTEVEC[i]) / 2;
		else senkou_span_B->QUOTEVEC[i] = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates the trend normality indicator for market regime
 * 
 * This function calculates the trend normality indicator, as introduced by
 * @tasciccac It uses the ichimoku components and puts them into a relation to
 * find out if a market is trending, not trending and even if overbought/
 * oversold.
 * 
 * the indicator is calculated as follows:
 * tn = (tenkan-kijun) / (senkouA - senkouB)
 * 
 * if tn < 0 current trend is reversing -> do not trade
 * if tn > 1 current trend is oversold/overbought
 * 0 <= tn <= 1 healthy trend
 * 
 * to use it as a market filter, values < 0 and > 1 will set tni to "1" (filter
 * true), values between 0 and 1 will set the filter to 0 (filter false/off)
 * 
 * @see https://twitter.com/tasciccac/status/867392176296210432
 * 
 * @param tenkan pointer to indicator struct containing tenkan sen
 * @param kijun pointer to indicator struct containing kijun sen
 * @param senkouA pointer to indicator struct containing senkou span a
 * @param senkouB pointer to indicator struct containing senkou span b
 * @param tni pointer to indicator struct that will contain tni
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int ichimoku_tni(class_indicators *tenkan, class_indicators *kijun, class_indicators *senkouA, class_indicators *senkouB, class_indicators *tni)
{
	unsigned int i = 0;
	float *trend_normality=NULL;
	
	trend_normality = init_1d_array_float(tenkan->NR_ELEMENTS);

	for(i=0; i<tenkan->NR_ELEMENTS; i++)
	{
		if((senkouA->QUOTEVEC[i] - senkouB->QUOTEVEC[i]) != 0) //make sure we avoid division by 0
			trend_normality[i] = (tenkan->QUOTEVEC[i] - kijun->QUOTEVEC[i]) / (senkouA->QUOTEVEC[i] - senkouB->QUOTEVEC[i]);
		else
			trend_normality[i] = 1.1; 	// has to be only >1, will be a filtered trade anyhow

			// determine tni as a "switch" (see comments in function description)
		if((trend_normality[i] < 0) || (trend_normality[i] > 1))
			tni->QUOTEVEC[i] = 1;
		else
			tni->QUOTEVEC[i] = 0;
	}
	
	free_1d_array_float(trend_normality);
	return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Updates all ichimoku indicators in database
 *
 * Updates all indicators in database (and database only, no data
 * exchange outside of function). First gets the lates quotes from db
 * (quotes in db should be updated outside this function), calculates 
 * indicators and stores them in database again. 
 * 
 * @param the_market class_market object
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int  ichimoku_update_indicators(class_market* the_market)
{
 	extern parameter parms; // parms are declared global in main.c

	the_market->addNewIndicator(the_market, "HH_short", parms.INDI_DAYS_TO_UPDATE, "highest highs for short period", "indicators_daily");
	the_market->addNewIndicator(the_market, "HH_mid", parms.INDI_DAYS_TO_UPDATE, "highest highs for mid period", "indicators_daily");
	the_market->addNewIndicator(the_market, "HH_long", parms.INDI_DAYS_TO_UPDATE, "highest highs for long period", "indicators_daily");
	the_market->addNewIndicator(the_market, "LL_short", parms.INDI_DAYS_TO_UPDATE, "lowest lows for short period", "indicators_daily");
	the_market->addNewIndicator(the_market, "LL_mid", parms.INDI_DAYS_TO_UPDATE, "lowest lows for mid period", "indicators_daily");
	the_market->addNewIndicator(the_market, "LL_long", parms.INDI_DAYS_TO_UPDATE, "lowest lows for long period", "indicators_daily");
	
	the_market->addNewIndicator(the_market, "tenkan", parms.INDI_DAYS_TO_UPDATE, "Tenkan-Sen (short term average)", "ichimoku_daily");
	the_market->addNewIndicator(the_market, "kijun", parms.INDI_DAYS_TO_UPDATE, "Kijun-Sen (mid term average)", "ichimoku_daily");
	the_market->addNewIndicator(the_market, "chikou", parms.INDI_DAYS_TO_UPDATE, "Chikou Span (lagging line)", "ichimoku_daily");
	the_market->addNewIndicator(the_market, "senkou_A", parms.INDI_DAYS_TO_UPDATE, "Senkou Span A: (Tenkan+Kijun):2, future shift (mid term)", "ichimoku_daily");
	the_market->addNewIndicator(the_market, "senkou_B", parms.INDI_DAYS_TO_UPDATE, "Senkou Span B: long term average, future shift (mid term)", "ichimoku_daily");

 	const int closePos = the_market->IndPos.close;
	const int highPos = the_market->IndPos.high;
	const int lowPos = the_market->IndPos.low;
	
	const int hhshortPos = the_market->getIndicatorPos(the_market, "HH_short");
	const int hhmidPos = the_market->getIndicatorPos(the_market, "HH_mid");
	const int hhlongPos = the_market->getIndicatorPos(the_market, "HH_long");
	const int llshortPos = the_market->getIndicatorPos(the_market, "LL_short");
	const int llmidPos = the_market->getIndicatorPos(the_market, "LL_mid");
	const int lllongPos = the_market->getIndicatorPos(the_market, "LL_long");
	
	const int tenkanPos = the_market->getIndicatorPos(the_market, "tenkan");
	const int kijunPos = the_market->getIndicatorPos(the_market, "kijun");
	const int chikouPos = the_market->getIndicatorPos(the_market, "chikou");
	const int senkouAPos = the_market->getIndicatorPos(the_market, "senkou_A");
	const int senkouBPos = the_market->getIndicatorPos(the_market, "senkou_B");
	
 	the_market->copyIndicatorQuotes(the_market, "close", "HH_short");
	the_market->copyIndicatorQuotes(the_market, "close", "HH_mid");
	the_market->copyIndicatorQuotes(the_market, "close", "HH_long");
 	the_market->copyIndicatorQuotes(the_market, "close", "LL_short");
	the_market->copyIndicatorQuotes(the_market, "close", "LL_mid");
	the_market->copyIndicatorQuotes(the_market, "close", "LL_long");
	the_market->copyIndicatorQuotes(the_market, "close", "tenkan");
	the_market->copyIndicatorQuotes(the_market, "close", "kijun");
 	the_market->copyIndicatorQuotes(the_market, "close", "chikou");
	the_market->copyIndicatorQuotes(the_market, "close", "senkou_A");
	the_market->copyIndicatorQuotes(the_market, "close", "senkou_B");
	
	highest_high(parms.ICHI_PERIOD_SHORT, the_market->Ind[highPos], the_market->Ind[hhshortPos]);
	highest_high(parms.ICHI_PERIOD_MID, the_market->Ind[highPos], the_market->Ind[hhmidPos]);
	highest_high(parms.ICHI_PERIOD_LONG, the_market->Ind[highPos], the_market->Ind[hhlongPos]);
	lowest_low(parms.ICHI_PERIOD_SHORT, the_market->Ind[lowPos], the_market->Ind[llshortPos]);
	lowest_low(parms.ICHI_PERIOD_MID, the_market->Ind[lowPos], the_market->Ind[llmidPos]);
	lowest_low(parms.ICHI_PERIOD_LONG, the_market->Ind[lowPos], the_market->Ind[lllongPos]);
	
	ichimoku_tenkan(parms.ICHI_PERIOD_SHORT, the_market->Ind[hhshortPos], the_market->Ind[llshortPos], the_market->Ind[tenkanPos]);
	ichimoku_kijun(parms.ICHI_PERIOD_MID, the_market->Ind[hhmidPos], the_market->Ind[llmidPos], the_market->Ind[kijunPos]);
	ichimoku_chikou(parms.ICHI_PERIOD_MID, the_market->Ind[closePos], the_market->Ind[chikouPos] );
	ichimoku_senkou_A(parms.ICHI_PERIOD_MID, the_market->Ind[tenkanPos], the_market->Ind[kijunPos], the_market->Ind[senkouAPos]);
	ichimoku_senkou_B(parms.ICHI_PERIOD_MID, the_market->Ind[hhlongPos], the_market->Ind[lllongPos],the_market->Ind[senkouBPos]);
	
    // create trend normality indicator if market regime filter is configured
	// note that there is no special "TNI" field/table as TNI is only 
	// one option for market regime filter, so it will be stored as "regime
	// filter" in the table indicators_daily
	if(biseqcstr(parms.SIGNAL_REGIME_FILTER, "TNI"))
	{
		the_market->addNewIndicator(the_market, "regime_filter", parms.INDI_DAYS_TO_UPDATE, "Trend Normality Indicator", "indicators_daily");
		const int regPos = the_market->getIndicatorPos(the_market, "regime_filter");
		the_market->copyIndicatorQuotes(the_market, "close", "regime_filter");
        ichimoku_tni(the_market->Ind[tenkanPos], the_market->Ind[kijunPos], 
						the_market->Ind[senkouAPos], the_market->Ind[senkouBPos], 
						the_market->Ind[regPos]);
		the_market->Ind[regPos]->db_saveflag = true;				
	}
    
	// set flag so indicators will be written to db if market->saveAllIndicatorsDB(market) is called
	the_market->Ind[hhshortPos]->db_saveflag = true;
	the_market->Ind[hhmidPos]->db_saveflag = true;
	the_market->Ind[hhlongPos]->db_saveflag = true;
	the_market->Ind[llshortPos]->db_saveflag = true;
	the_market->Ind[llmidPos]->db_saveflag = true;
	the_market->Ind[lllongPos]->db_saveflag = true;
	the_market->Ind[tenkanPos]->db_saveflag = true;
	the_market->Ind[kijunPos]->db_saveflag = true;
	the_market->Ind[chikouPos]->db_saveflag = true;
	the_market->Ind[senkouAPos]->db_saveflag = true;
	the_market->Ind[senkouBPos]->db_saveflag = true;

 	return EXIT_SUCCESS; 
}

// eof
