/* readconf.h
 * definition of config parameters, declarations for readconf.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * based on sample code found on 
 * http://www.linuxquestions.org/questions/programming-9/read-parameters-from-config-file-file-parser-362188/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file readconf.h
 * @brief Header file for readconf.c
 *
 * This file contains the declarations for readconf.c and the 
 * configuration parameter struct 'parameters'

 * @author Denis Zetzmann
 */

#ifndef READCONF_H
#define READCONF_H

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "bstrlib.h"

#define MAXLEN 80

// definition of symbolfile layout
#define nr_columns 8
	
#define col_symbol 0
#define col_identifier 1
#define col_csv 2
#define col_tradable 3
#define col_ticks_pips 4
#define col_contract_size 5
#define col_weight 6
#define col_curr 7
#define col_type 8
#define col_comment 9

/**
 * @brief global configuration settings
 * 
 * This struct holds the global configuration settings that are accessible
 * throughout the program, when the function uses 
 * @code
 *  extern parameter parms;
 * @endcode
 * For clarity the members of the struct have the same names as the config 
 * parameters of the configfile(s) do
 */ 
struct parameters
{
	// General settings (overules Major settings because of higher rank)
	bstring		CONFIG_SYMBOLFILE;	/**< file that holds symbols (and filenames of .csv) */
	bstring		ACCOUNT_CURRENCY;                 /**< currency of user`s account */
	unsigned int 	ACCOUNT_CURRENCY_DIGITS;     /**< nr of significant digits */
	float      TRANSACTION_COSTS_FIX;                /**< costs for buying/selling in account currency */
	float      TRANSACTION_COSTS_PERCENTAGE;         /**<  fee as percentage of position´s value */
	 
	/* mysql specific parameters */
	bstring		DB_SYSTEM_HOSTNAME;	/**< hostname of mysql system database */
	bstring		DB_SYSTEM_USERNAME;	/**< username of mysql systemdatabase */
	bstring		DB_SYSTEM_PASSWORD;	/**< password for mysql user */
	bstring		DB_SYSTEM_NAME;		/**< trading system database name */
	unsigned int	DB_SYSTEM_PORT;		/**< port of host */
	bstring		DB_SYSTEM_SOCKET;		/**< socket */
	unsigned int	DB_SYSTEM_FLAGS;		/**< flags for connection to mysql db */
	
	bstring		DB_QUOTES_HOSTNAME;	/**< hostname of mysql quotes database */
	bstring		DB_QUOTES_USERNAME;	/**< username of mysql quotes database */
	bstring		DB_QUOTES_PASSWORD;	/**< password for mysql user */
	bstring		DB_QUOTES_NAME;		/**< central quote repository database name */
	unsigned int	DB_QUOTES_PORT;		/**< port of host */
	bstring		DB_QUOTES_SOCKET;		/**< socket */
	unsigned int	DB_QUOTES_FLAGS;		/**< flags for connection to mysql db */
	
	// terminal output settings
	bstring		TERMINAL_EVAL_RESULT;   /**< output evaluation results in terminal */
						/**< (none, text, html) */
	bstring		TERMINAL_STATISTICS;	/**< do orderbook statistics, print out
						 (none, text) */

	// general indicator settings
	unsigned int	INDI_DAYS_TO_UPDATE; /**< update indicator db: calc how many days?*/
	unsigned int 	INDI_ADX_PERIOD; 	/**< number of periods in ADX' EMAs */
	bool            INDI_TRACK_TREND;  /**< calculate market trends/duration and store them in db */
	
	/* ichimoku specific parameters */
	unsigned int	ICHI_DAYS_TO_ANALYZE;	/**< How many days backwards? Default 60 */
	unsigned int	ICHI_PERIOD_SHORT;	/**< short term period, default 9 days */
	unsigned int 	ICHI_PERIOD_MID;	/**< mid term period, default 26 days */
	unsigned int	ICHI_PERIOD_LONG;	/**< long term period, default 52 days */
	bool 		ICHI_CHIKOU_CONFIRM;	/**< use chikou span as confirmation/filter for trend (default true) */
	bool		ICHI_KUMO_CONFIRM;	//*< check whether Kumo breakout is higher/lower than last reaction high/low */
	bool 		ICHI_EXECUTION_SENKOU_X; /**< execute Signal Senkou Cross, default true */
	bool 		ICHI_EXECUTION_KIJUN_X; /**< execute Signal Kijun Cross, default true */
	bool 		ICHI_EXECUTION_CHIKOU_X; /**< execute Chikou Cross, default true */
	bool		ICHI_EXECUTION_TK_X; 	/**< execute Tenkan/Kijun Cross, default true */
	bool		ICHI_EXECUTION_KUMOBREAK;	/**< execute Kumo breakout, default true */  
	
	// settings related to signal execution
	bstring		SIGNAL_REGIME_FILTER;	/**< Market regime filter: ADX, TNI, none (default) */
	unsigned int 	SIGNAL_REGIME_ADX_THRESH; /**< ADX market filter threshold, default 30 */
	unsigned int	SIGNAL_DAYS_TO_EXECUTE;	/**< how many days backwards should signals in database be considered? */
	bstring		SIGNAL_EXECUTION_DATE;	/**< when should signal be executed: signal_date (date when signal was triggered), real_date (date when program is run), signal_next (the day after signal was triggered) */
	bstring		SIGNAL_EXECUTION_PYRAMID;	/**< pyramid buy/sell signals, e.g. buy more than 1 position in same direction? none: buy max 1 position in short/max 1 in long position, daily: execute only 1 long and 1 short position per day, pyramid on consecutive days, full: do not impose any limits, buy every single signal */
	bool 		SIGNAL_MANUAL_CONFIRM; 	/**< true: confirm execution of signals (default); false: auto buy/sell */
	bool		SIGNAL_EXECUTION_WEAK;	/**< should weak signals be executed? */
	bool		SIGNAL_EXECUTION_NEUTRAL; /**< should neutral signals be executed? */
	bool		SIGNAL_EXECUTION_STRONG;	/**< should strong signals be executed? */
	bool		SIGNAL_EXECUTION_LONG;	/**< should long signals be executed? */
	bool		SIGNAL_EXECUTION_SHORT;	/**< should short signals be executed? */
	bool		SIGNAL_EXECUTION_SUNDAYS; /**< execute signals that occur on sundays? */
	
	// Portfolio related settings
	bstring		PORTFOLIO_ALLOCATION; 	/**< Model to perform portfolio allocation equal/spearman */
	unsigned int PORTFOLIO_RETURNS_PERIOD; /**< period for which mean returns are calulated */
	unsigned int PORTFOLIO_RETURNS_UPDATE; /**< update mean returns every X periods */
		
	// Stop Loss settings
	bstring		SL_TYPE;	/**< percentage (percentage stop), chandelier (Chandelier Stop) */
	float		SL_PERCENTAGE_RISK; 	/**< the SL percentage for SL_TYPE = percentage, default 0.02 */
	float 		SL_ATR_FACTOR;	/**< Place the SL x-times the ATR away (default 3) */
	unsigned int	SL_ATR_PERIOD;	/**< Period for Averaging the True Range (default 26) */
	bstring		SL_ADJUST; 	/**< fixed (use initial SL only), trailing (adjust SL in trend direction only), updown (adjust SL in both directions), default: trailing */
	bool        SL_SAVE_DB; /**< save all SL records in mysql database? (true/false) */
	
	// Risk management settings
	float		STARTING_BALANCE; /**< starting account balance at begin of backtest/live trading */
	float		RISK_PER_POSITION; /**< Risk of a new position in % of current Equity */
	float       POS_SIZE_CAP;    /**< new positions: cap size to x% of current balance + risk free Equity */
	float       RISK_FREE_RATE;    /**rate of returns for 0 risk investment */     
	
	// chart plotting setting
	bool	CHART_PLOT;		/**< should chart be plotted? true/false */
	bool	CHART_PLOT_WEAK;	/**< plot weak signals; default false */
	bool	CHART_PLOT_NEUTRAL;	/**< plot neutral signals; default false */
	bool	CHART_PLOT_STRONG;	/**< plot strong signals; default true */
	bstring	CHART_COLORSCHEME;	/**< color scheme of the plot, (light/contrast) */
	unsigned int  CHART_DAYS_TO_PLOT;	/**< how many days should be plotted */
}
  parameters;

typedef struct parameters parameter;

void init_parameters (parameter *parms);
int parse_config (bstring filename, parameter * parms);
int parse_symbolfile(bstring filename, bstring **symbolnames, bstring **identifiers, bstring **csvfiles, unsigned int **tradeable, float **ticks_pips, float **contract_size, float **weight, bstring **curr, bstring **type);
int check_parameter(parameter *parms, bstring parameter, bstring value);
int destroy_parameters(parameter *parms);

void parse( char *record, char *delim, char arr[][40],int *fldcnt);

bool is_float(const char *s, float *dest);
bool is_uint(const char *s, unsigned int *dest);
#endif		// READCONF_H
/* End of file */
