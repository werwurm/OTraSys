/* class_quote.c
 * Implements a "class"-like struct in C which handles quotes
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file quote_class.c
 * @brief routines that implement a class-like struct which represents 
 * quotes
 *
 * This file contains the definitions of the quote "class methods"
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "class_quote.h"
#include "arrays.h"
#include "database.h"
#include "debug.h"

// declarations for functions that should not be called outside
// of this file, instead as "object method"
static void class_quotes_setMethods(class_quotes* self);
static void class_quotes_destroyImpl(class_quotes *self);
static void class_quotes_printTableImpl(const class_quotes *self);
static void class_quotes_loadQuotesImpl(class_quotes *self);
static class_quotes* class_quotes_cloneImpl(const class_quotes* original);
static void class_quotes_newSymbolImpl(class_quotes *self, char* newSymbolStr, char* newIdentifierStr);
static void class_quotes_copyDataVecImpl(const class_quotes* original, class_quotes* clone);
static long int class_quotes_findDaynrIndexImpl(const class_quotes *self, unsigned int the_daynr);
static unsigned int class_quotes_findDaynrIndex_lBImpl(const class_quotes *self, unsigned int the_daynr);
static void class_quotes_appendQuoteRecordsImpl(class_quotes *self, unsigned int nr_additional_records);
static void class_quotes_appendQuoteTupelImpl(class_quotes *self, bstring *new_date, unsigned int new_daynr, float new_quote);
static void class_quotes_setQuoteTupelImpl(class_quotes *self, unsigned int idx, bstring *new_date, unsigned int new_daynr, float new_quote);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for quotes objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods.
 * 
 * @return pointer to new object of class quotes
*/
///////////////////////////////////////////////////////////////////////////////
class_quotes* class_quotes_init(char* quote_symbol, char* identifier, unsigned int nr,
		    unsigned int nrOfDigits, char* quotetype, char* tablename)
{
    // allocate memory
    class_quotes* self = NULL;
    
    self = (class_quotes*) ZCALLOC(1, sizeof(class_quotes));
    
    self->symbol = init_1d_array_bstring(1);
    self->identifier = init_1d_array_bstring(1);
    self->quotetype = init_1d_array_bstring(1);
    self->tablename = init_1d_array_bstring(1);
    self->quotevec = init_1d_array_float(nr);
    self->daynrvec = init_1d_array_uint(nr);
    self->datevec = init_1d_array_bstring(nr);

    // init data
    self->nr_elements = nr;
    *self->symbol = bfromcstr(quote_symbol);
    *self->identifier = bfromcstr(identifier);
    *self->quotetype = bfromcstr(quotetype);
    *self->tablename = bfromcstr(tablename);
    self->nr_digits = nrOfDigits;
    
    // set methods
    class_quotes_setMethods(self);
    
    return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for quotes objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to quotes struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_quotes_setMethods(class_quotes* self)
{
    self->destroy = class_quotes_destroyImpl;
    self->printTable = class_quotes_printTableImpl;
    self->loadFromDB = class_quotes_loadQuotesImpl;
    self->clone = class_quotes_cloneImpl;
    self->copyDataVec = class_quotes_copyDataVecImpl;
    self->newSymbol = class_quotes_newSymbolImpl;
    self->findDaynrIndex = class_quotes_findDaynrIndexImpl;
    self->findDaynrIndexlB = class_quotes_findDaynrIndex_lBImpl;
    self->appendQuoteRecords = class_quotes_appendQuoteRecordsImpl;
    self->appendQuoteTupel = class_quotes_appendQuoteTupelImpl;
    self->setQuoteTupel = class_quotes_setQuoteTupelImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "destructor" function for quotes objects
 * 
 * This function frees the memory consumed by quotes objects
 * 
 * @param self pointer to quotes object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_destroyImpl(class_quotes *self)
{
    free_1d_array_bstring(self->datevec, self->nr_elements);
    free(self->quotevec);
    free(self->daynrvec);
    free_1d_array_bstring(self->symbol, 1);
    free_1d_array_bstring(self->identifier, 1);
    free_1d_array_bstring(self->quotetype,1);
    free_1d_array_bstring(self->tablename,1);
    free(self);
    self=NULL;
} 

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief prints symbol, daynr, date and quote of the object in tabular form
 * 
 * prints symbol, daynr, date and quote of the object in tabular form
 * 
 * @param self pointer to quotes object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_printTableImpl(const class_quotes* self)
{
    printf("%s (ID: %s) %s quotes, %u elements\n", bdata(*self->symbol), bdata(*self->identifier), bdata(*self->quotetype), self->nr_elements);
    printf("idx\tdaynr\tdate\t\tquote\n");
    for(unsigned int i=0;i<self->nr_elements;i++)
        printf("[%u]\t%u\t%s\t%.*f\n", i, self->daynrvec[i], bdata(self->datevec[i]),self->nr_digits, self->quotevec[i]);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief loads quote information from database
 * 
 * This function loads the information for a quote object (vector with quotes,
 * vector with dates, vector with daynumbers) from myssql database
 * 
 * @param self pointer to quotes object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_loadQuotesImpl(class_quotes *self)
{
	db_mysql_getquotes(*self->symbol, *self->identifier, *self->tablename, *self->quotetype, self->nr_elements, self->quotevec, self->datevec, self->daynrvec);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief clone quote object (deep copy original into clone)
 * 
 * clone quote object (deep copy original into clone), if original object gets
 * modified or destroy()ed, the clone will still live
 * 
 * @param original pointer to quote object with original data
 * @returns pointer to cloned object
*/
///////////////////////////////////////////////////////////////////////////////
static class_quotes* class_quotes_cloneImpl(const class_quotes* original)
{
	class_quotes* clone = NULL;
	clone = class_quotes_init(bdata(*original->symbol), bdata(*original->identifier), original->nr_elements, original->nr_digits, bdata(*original->quotetype), bdata(*original->tablename));
	for(unsigned int i=0; i<clone->nr_elements; i++)
	{
		clone->datevec[i] = bstrcpy(original->datevec[i]);
		clone->daynrvec[i] = original->daynrvec[i];
		clone->quotevec[i] = original->quotevec[i];
	}
	
	return clone;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief changes symbol name and identifier of quotes object
 * 
 * changes the symbol and identifier strings of a quotes object, useful after 
 * cloning quotes
 * 
 * @param self pointer to quote object
 * @param newSymbolStr pointer to char with new name
 * @param newIdentifierStr pointer to char with new identifier
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_newSymbolImpl(class_quotes *self, char* newSymbolStr, char* newIdentifierStr)
{
	bstring tmpstr = NULL;
    bstring tmpstr2 = NULL;
	tmpstr = *self->symbol;
    tmpstr2 = *self->identifier;
	*self->symbol = bfromcstr(newSymbolStr);
    *self->identifier= bfromcstr(newIdentifierStr);
    bdestroy(tmpstr);
    bdestroy(tmpstr2);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief copies quote/date/daynr vectors from original quote obj to clone
 * 
 * copies quote/date/daynr vectors from original quote obj to clone
 * 
 * @param original pointer to quote object with original data
 * @param clone pointer to quote object that will hold the clone
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_copyDataVecImpl(const class_quotes* original, class_quotes* clone)
{
	if(original->nr_elements != clone->nr_elements)
	{
		log_err("quotes_copyDataVecImpl(): cannot copy data, different number of quotes (%s: %i; %s: %i)", bdata(*original->quotetype), original->nr_elements, bdata(*clone->quotetype), clone->nr_elements);
		exit(EXIT_FAILURE);
	}
	
	for(unsigned int i=0; i<clone->nr_elements; i++)
	{
		bstring tmp = NULL;
		tmp = clone->datevec[i];
		clone->datevec[i] = bstrcpy(original->datevec[i]);
		clone->daynrvec[i] = original->daynrvec[i];
		clone->quotevec[i] = original->quotevec[i];
		bdestroy(tmp);
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief finds index of given daynr within Quote Object
 * 
 * This function searches for a given daynr within the quote object and returns
 * its index (=position in Quote Object vectors). It utilizes a binary search.
 * 
 * @note: If the element is not found, -1 is returned
 * 
 * @param self pointer to quote object
 * @param the_daynr uint with the daynr to check
 * @returns if the daynr is found, its index as long int, -1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
static long int class_quotes_findDaynrIndexImpl(const class_quotes *self, unsigned int the_daynr)
{
	unsigned int low = 0;
	unsigned int high = self->nr_elements -1;
	while (low <= high)
	{
        if(high >= self->nr_elements)    // safety net if we loop to long and get ridiculously high high
            return -1;
        
		int mid = low + (high - low)/2;
	
		// check if daynr is at mid
		if (self->daynrvec[mid] == the_daynr) 
			return mid;  
		
		// daynr is greater, look further in right half 
		if (self->daynrvec[mid] < the_daynr) 
			low = mid + 1; 
	
		// daynr is smaller, look further in left half 
		else
			high = mid - 1; 
	}

	// fallthrough if index was not found, return -1
	return -1; 
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief finds index of given daynr within Quote Object
 * 
 * This function searches for a given daynr within the quote object and returns
 * its index (=position in Quote Object vectors). It utilizes a modified binary
 * search. If the daynr is not found, the next smaller existing daynr is 
 * returned. 
 * This can be useful if you are looking for an equivalent quote between two
 * markets (for example doing currency translation), but for some reasons
 * (national holidays) the daynr vectors of both markets are not identical.
 * 
 * @note: If the element is not found, the next smaller element is returned
 * @see quotes_findDaynrIndexImpl
 * @see based on code found at 
 * https://stackoverflow.com/questions/6443569/implementation-of-c-lower-bound
 * 
 * @param self pointer to quote object
 * @param the_daynr uint with the daynr to check
 * @returns if the daynr is found, its index as uint, if not its next smaller
 * daynr that exists
*/
///////////////////////////////////////////////////////////////////////////////
static unsigned int class_quotes_findDaynrIndex_lBImpl(const class_quotes *self, unsigned int the_daynr) 
{
	unsigned int low = 0;
	unsigned int high = self->nr_elements - 1;

    if(self->daynrvec[0] == the_daynr)  // shortcut in case the very first nr is right (don't laugh, I added 
    {
        return 0;                       // this shit after 4hrs debug session)
    }
    
	while (low < high) 
	{
        if(high >= self->nr_elements)    // safety net if we loop to long and get ridiculously high high
            return -1;
            
		unsigned int mid = low + (high - low)/2;
		
		if (self->daynrvec[mid] < the_daynr)
			low = mid + 1;
		else
			high = mid;
	}

	if(self->daynrvec[low]==the_daynr)	// exact daynr was hit
		return low;
	else
		return low-1;	// exact daynr not in array, take next smaller daynr
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief append a nr of empty records to quote object
 * 
 * This method appends a given number of records to an existing quote object.
 * The appended records are initialized with 0
 * @param self pointer to quote object
 * @param nr_additional_records uint with
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_appendQuoteRecordsImpl(class_quotes *self, unsigned int nr_additional_records)
{
	// realloc indicator vector for new element object
    self->datevec = (bstring*) realloc(self->datevec, ((self->nr_elements + nr_additional_records) * sizeof(bstring)));
    self->daynrvec = (unsigned int*) realloc(self->daynrvec, ((self->nr_elements + nr_additional_records)  * sizeof(unsigned int)));
    self->quotevec = (float*) realloc(self->quotevec, ((self->nr_elements + nr_additional_records) * sizeof(float)));

    if(!self->datevec || !self->daynrvec || !self->quotevec)
    {
        printf(ANSI_COLOR_RED"Memory allocation failed!(%s:%i)\n"ANSI_COLOR_RESET, __FILE__, __LINE__);
        exit(EXIT_FAILURE);        
    }
    
    // initialize new entries
    for(unsigned int i = self->nr_elements; i<(self->nr_elements + nr_additional_records); i++)
    {
        self->datevec[i] = NULL;
        self->daynrvec[i] = 0;
        self->quotevec[i] = 0.0;
    }
    
    self->nr_elements = self->nr_elements + nr_additional_records;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief append a (single) {date, daynr, quote} tupel to existing object
 * 
 * This method appends a append a (single) {date, daynr, quote} tupel to 
 * existing object.
 * 
 * @param self pointer to quote object
 * @param new_date bstring ptr with date (as XXXX-YY-ZZ)
 * @param new_daynr uint with nr of days since 1900-01-01
 * @param new_quote float with new quote
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_appendQuoteTupelImpl(class_quotes *self, bstring *new_date, unsigned int new_daynr, float new_quote)
{
    // append a new record
    class_quotes_appendQuoteRecordsImpl(self, 1);
    
    unsigned int last_idx = self->nr_elements - 1;
    
    self->datevec[last_idx] = bstrcpy(*new_date);
    self->daynrvec[last_idx] = new_daynr;
    self->quotevec[last_idx] = new_quote;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief fill an (existing) single {date, daynr, quote} tupel of object
 * 
 * This method fill an (existing) single {date, daynr, quote} tupel of object
 * 
 * @param self pointer to quote object
 * @param idx uint with index to be filled
 * @param new_date bstring ptr with date (as XXXX-YY-ZZ)
 * @param new_daynr uint with nr of days since 1900-01-01
 * @param new_quote float with new quote
*/
///////////////////////////////////////////////////////////////////////////////
static void class_quotes_setQuoteTupelImpl(class_quotes *self, unsigned int idx, bstring *new_date, unsigned int new_daynr, float new_quote)
{
    self->datevec[idx] = bstrcpy(*new_date);
    self->daynrvec[idx] = new_daynr;
    self->quotevec[idx] = new_quote;    
}
// EOF
