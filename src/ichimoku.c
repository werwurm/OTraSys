﻿/* ichimoku.c
 * ichimoku kinko hyo specific functions
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file ichimoku.c
 * @brief Ichimoku Kinko Hyo specific functions
 *
 * This file contains the routines to evaluate price data according
 * to ichimoku rules and generate ichimoku signals
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "debug.h"
#include "constants.h"
#include "bstrlib.h"
#include "datatypes.h"
#include "indicators_general.h"
#include "readconf.h"
#include "arrays.h"
#include "database.h"
#include "export_csv.h"
#include "ichimoku.h"
#include "class_signals.h"
#include "date.h"
#include "p1d_wrapper.h"

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief evaluates indicators for signals
 *
 * reads quotes and indicators stored in db, loops through all 
 * ICHI_DAYS_TO_ANALYZE and checks whether an ichimoku trade signal is 
 * triggered; all found signals are stored in database
 *
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int ichimoku_evaluate_signals(class_market* the_market, class_signal_list* ichimoku_signals)
{
	extern parameter parms; // parms are declared global in main.c
 	unsigned int daynr=0;
	
    // arrays that will store indices of local price extrema
	unsigned int *ind_minima=NULL;
 	unsigned int *ind_maxima=NULL;

	// nr of maxima, note that due to p1d algorith the nr of minima will
	// always be nr_maxima+1
	unsigned int nr_maxima;
 	
	// init extrema arrays, note that they will be resized as needed by
	// get_extrema_indices
	ind_minima = init_1d_array_uint(1);
	ind_maxima = init_1d_array_uint(1);
	
	// get and store Indicator positions for easier access
	const int openPos = the_market->IndPos.open;
	const int highsPos = the_market->IndPos.high;
	const int lowsPos = the_market->IndPos.low;
	const int closePos = the_market->IndPos.close;
	const int kijunPos = the_market->getIndicatorPos(the_market, "kijun");
	const int tenkanPos = the_market->getIndicatorPos(the_market, "tenkan");
	const int chikouPos = the_market->getIndicatorPos(the_market, "chikou");
	const int senkouAPos = the_market->getIndicatorPos(the_market, "senkou_A");
	const int senkouBPos = the_market->getIndicatorPos(the_market, "senkou_B");
    
    // do some sanity checks to prevent out-of-boundary array access
	check(openPos>=0, "Indicator \"open\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(highsPos>=0, "Indicator \"high\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(lowsPos>=0, "Indicator \"low\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(closePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(chikouPos>=0, "Indicator \"chikou\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(kijunPos>=0, "Indicator \"kijun\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(tenkanPos>=0, "Indicator \"tenkan\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(senkouAPos>=0, "Indicator \"Senkou_A\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
	check(senkouBPos>=0, "Indicator \"Senkou_B\" close does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
    int mindays = parms.INDI_DAYS_TO_UPDATE - parms.ICHI_DAYS_TO_ANALYZE - parms.ICHI_PERIOD_MID;
    check(mindays > 0, "\ntrying to access negative index, make sure INDI_DAYS_TO_UPDATE (%i) is GREATER than (ICHI_DAYS_TO_ANALYZE (%i) - ICHI_PERIOD_MID (%i))", 
          error_sanity_check, parms.INDI_DAYS_TO_UPDATE, parms.ICHI_DAYS_TO_ANALYZE, parms.ICHI_PERIOD_MID);

	// if configured to do so, prepare indices for local minima and maxima
	if(parms.ICHI_KUMO_CONFIRM)
	{
		//get nr of extrema and their indices
		get_extrema_indices(the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[closePos]->NR_ELEMENTS, &ind_minima, &ind_maxima, &nr_maxima);

		// sort indices of extrema ascending
 		qsort(ind_minima, nr_maxima+1, sizeof(unsigned int), cmpfunc);
 		qsort(ind_maxima, nr_maxima, sizeof(unsigned int), cmpfunc);
 	}

 	// indicators contain whole dateset of INDI_DAYS_TO_UPDATE days. loop only through ICHI_DAYS_TO_ANALYZE subset
 	// to check for signals
	for (daynr=(parms.INDI_DAYS_TO_UPDATE - parms.ICHI_DAYS_TO_ANALYZE); daynr< parms.INDI_DAYS_TO_UPDATE; daynr++)
  	{
		// check if current close is above, under or in kumo
		trend_type kumo_trend;
		kumo_trend = ichimoku_relation_to_kumo(
			the_market->Ind[closePos]->QUOTEVEC[daynr], 
			the_market->Ind[senkouAPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID],	// substract period to get "today´s kumo"
			the_market->Ind[senkouBPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID]);	// substract period to get "today´s kumo"
	 
		// check if price today is higher or lower than ICHI_PERIOD_MID days ago
		trend_type chikou_trend;
		
		chikou_trend = ichimoku_get_chikou_trend(
			the_market->Ind[closePos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID], 
			the_market->Ind[chikouPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID], 
			the_market->Ind[senkouAPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID],	// substract period to get "today´s kumo"
			the_market->Ind[senkouBPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID]);	// substract period to get "today´s kumo"
		
        // determine market´s current trend according to Ichimoku rules if trend tracking is configured
        if(parms.INDI_TRACK_TREND)
        {
            ichimoku_update_trendStat(the_market, kumo_trend, chikou_trend, daynr- (parms.INDI_DAYS_TO_UPDATE - parms.ICHI_DAYS_TO_ANALYZE));
        }

// ============  CHECK FOR KIJUN SEN CROSS =======================================================
		if(ichimoku_golden_X(daynr, the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[kijunPos]->QUOTEVEC, 0))		
		{
			// yay, long signal
			class_signal* newSignal;
			strength_type strength;
			bool signal_filtered = false;
			
			// we have a golden cross, look if it was above,below or in Kumo
 			switch(kumo_trend)
 			{
 				case uptrend:	// cross above kumo
					strength = strong;
 					break;
 				case downtrend:	// cross below kumo
					strength = weak;
 					break;
 				case sideways:	// cross inside kumo
					strength = neutral;
 					break;
 			}
 			// create temporary signal
 			newSignal = class_signal_init(bdata(the_market->Ind[kijunPos]->DATEVEC[daynr]), 
					the_market->Ind[kijunPos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr],
					the_market->Ind[kijunPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), longsignal, strength, the_market->nr_digits, 
					"Kijun Cross", "Close quote crossed the Kijun- Sen (Standard line)", 
					"Golden X",  "ichimoku_daily_signals");	
			
			// consider config parameter ICHI_CHIKOU_CONFIRM
			// if set, apply Chikou span filter: signals are only
			// valid if the trend status is confirmed by chikou span
 			if(parms.ICHI_CHIKOU_CONFIRM)
  			{	// makes sense only for strong signals
  				if(newSignal->strength == strong)
  				{
  					if(!(kumo_trend == chikou_trend))
						signal_filtered = true;
  				}
  			}
  			if(!(signal_filtered))
				ichimoku_signals->addSignal(ichimoku_signals, newSignal);
			// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
			newSignal->destroy(newSignal);
            
            // update market trend 
 		}
   		else if(ichimoku_death_X(daynr, the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[kijunPos]->QUOTEVEC , 0))
 		{
			// yay, short signal
			class_signal* newSignal;
			strength_type strength;
			bool signal_filtered = false;
			
			// we have a death cross, look if it was above,below or in Kumo
			switch (kumo_trend)
			{
				case uptrend:	// cross above kumo
					strength = weak;
					break;
				case downtrend:	// cross below kumo
					strength = strong;
					break;
				case sideways:	// cross inside kumo
					strength = neutral;
					break;
			}
			// create temporary signal
 			newSignal = class_signal_init(bdata(the_market->Ind[kijunPos]->DATEVEC[daynr]), 
					the_market->Ind[kijunPos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[kijunPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), shortsignal, strength, the_market->nr_digits, 
					"Kijun Cross", "Close quote crossed the Kijun- Sen (Standard line)", 
					"Death X",  "ichimoku_daily_signals");	
			
			// consider config parameter ICHI_CHIKOU_CONFIRM
			// if set, apply Chikou span filter: signals are only
			// valid if the trend status is confirmed by chikou span
 			if(parms.ICHI_CHIKOU_CONFIRM)
  			{	// makes sense only for strong signals
  				if(newSignal->strength == strong)
  				{
  					if(!(kumo_trend == chikou_trend))
 						signal_filtered = true;
  				}
  			}
  			if(!(signal_filtered))
				ichimoku_signals->addSignal(ichimoku_signals, newSignal);
			// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
			newSignal->destroy(newSignal);
 		}
// ============ END CHECK FOR KIJUN SEN CROSS =============================================================
// 
// ============ CHECK FOR TENKAN AND KIJUN CROSS =================================================================
		if(ichimoku_golden_X(daynr, the_market->Ind[tenkanPos]->QUOTEVEC, the_market->Ind[kijunPos]->QUOTEVEC, 0))	
 		{
			// yay, long signal
			trend_type tenk_kij;
			tenk_kij = ichimoku_relation_to_kumo(
					the_market->Ind[tenkanPos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouAPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID],	// substract period to get "today´s kumo"
					the_market->Ind[senkouBPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID]);	// substract period to get "today´s kumo"
 			
			class_signal* newSignal;
			strength_type strength;
			bool signal_filtered = false;
			
			// we have a golden cross, look if it was above,below or in Kumo
			switch (tenk_kij)
			{
				case uptrend:	// cross above kumo
					strength = strong;
					break;
				case downtrend:	// cross below kumo
					strength = weak;
					break;
				case sideways:	// cross inside kumo
					strength = neutral;
					break;
			}
 			
			// create temporary signal
 			newSignal = class_signal_init(bdata(the_market->Ind[tenkanPos]->DATEVEC[daynr]), 
					the_market->Ind[tenkanPos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[tenkanPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), longsignal, strength, the_market->nr_digits, 
					"Tenkan/Kijun Cross", "Tenkan-Sen (Turning line) crossed Kijun-Sen (Standard line)", 
					"Golden X",  "ichimoku_daily_signals");	 			
 			
			// consider config parameter ICHI_CHIKOU_CONFIRM
			// if set, apply Chikou span filter: signals are only
			// valid if the trend status is confirmed by chikou span
 			if(parms.ICHI_CHIKOU_CONFIRM)
  			{	// makes sense only for strong signals
  				if(newSignal->strength == strong)
  				{
  					if(!(kumo_trend == chikou_trend))
 						signal_filtered = true;
  				}
  			}
  			if(!(signal_filtered))
				ichimoku_signals->addSignal(ichimoku_signals, newSignal);
			// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
			newSignal->destroy(newSignal);
 		}
   		else if(ichimoku_death_X(daynr, the_market->Ind[tenkanPos]->QUOTEVEC, the_market->Ind[kijunPos]->QUOTEVEC, 0))
 		{
			// yay, short signal
			trend_type tenk_kij;
			tenk_kij = ichimoku_relation_to_kumo(
					the_market->Ind[tenkanPos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouAPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID],	// substract period to get "today´s kumo"
					the_market->Ind[senkouBPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID]);	// substract period to get "today´s kumo"
 			
			class_signal* newSignal;
			strength_type strength;
			bool signal_filtered = false;
			
			// we have a death cross, look if it was above,below or in Kumo
			switch (tenk_kij)
			{
				case uptrend:	// cross above kumo
					strength = weak;
					break;
				case downtrend:	// cross below kumo
					strength = strong;
					break;
				case sideways:	// cross inside kumo
					strength = neutral;
					break;
			}
			
			// create temporary signal
 			newSignal = class_signal_init(bdata(the_market->Ind[tenkanPos]->DATEVEC[daynr]), 
					the_market->Ind[tenkanPos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[tenkanPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), shortsignal, strength, the_market->nr_digits, 
					"Tenkan/Kijun Cross", "Tenkan-Sen (Turning line) crossed Kijun-Sen (Standard line)", 
					"Death X",  "ichimoku_daily_signals");	 			
 			
			// consider config parameter ICHI_CHIKOU_CONFIRM
			// if set, apply Chikou span filter: signals are only
			// valid if the trend status is confirmed by chikou span
 			if(parms.ICHI_CHIKOU_CONFIRM)
  			{	// makes sense only for strong signals
  				if(newSignal->strength == strong)
  				{
  					if(!(kumo_trend == chikou_trend))
 						signal_filtered = true;
  				}
  			}
  			if(!(signal_filtered))
				ichimoku_signals->addSignal(ichimoku_signals, newSignal);
			// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
			newSignal->destroy(newSignal);	
 		}
// ============ END CHECK FOR TENKAN AND KIJUN CROSS =================================================================
// 
// ============ CHECK FOR KUMO BREAKOUT ==============================================================================
// 		in general: 	rising Kumo --> Senkou Span A is above Senkou Span B
// 				falling Kumo --> Senkou Span A is below Span B
// 				==> Kumo breakout when: -Golden Cross of price with senkou_A in rising kumo
// 							-death cross with senkou B in rising kumo
// 							-Death Cross of price with Senkou_A in falling kumo
// 							-Golden Cross of price with Senkou_B in falling kumo
		// "if(rising kumo)"
		if(the_market->Ind[senkouAPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID] >=
			the_market->Ind[senkouBPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID])
 		{
			if(ichimoku_golden_X(daynr, the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[senkouAPos]->QUOTEVEC, parms.ICHI_PERIOD_MID))
 			{
				class_signal* newSignal;
				bool signal_filtered = false;	

				// create temporary signal
				newSignal = class_signal_init(bdata(the_market->Ind[closePos]->DATEVEC[daynr]), 
					the_market->Ind[closePos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouAPos]->QUOTEVEC[daynr - parms.ICHI_PERIOD_MID],
					bdata(*the_market->symbol), bdata(*the_market->identifier), longsignal, strong, the_market->nr_digits, 
					"Kumo Breakout", "Close quote left or crossed Kumo (Cloud)", 
					"from rising Kumo",  "ichimoku_daily_signals");	 
				
				// consider Chikou filter if configured
				if(parms.ICHI_CHIKOU_CONFIRM)
				{
					if(kumo_trend != chikou_trend)
						signal_filtered = true;
				}
 				
				// consider Kumo filter if configured
				if(parms.ICHI_KUMO_CONFIRM)
 				{
 					if(!check_new_lowhigh(daynr, the_market->Ind[closePos]->QUOTEVEC, ind_minima, ind_maxima, nr_maxima, longsignal))
 						signal_filtered = true;
 				}		
				
				if(!(signal_filtered))
					ichimoku_signals->addSignal(ichimoku_signals, newSignal);
				// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
				newSignal->destroy(newSignal);	
 			}
 			else if(ichimoku_death_X(daynr, the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[senkouBPos]->QUOTEVEC, parms.ICHI_PERIOD_MID))	
 			{
				class_signal* newSignal;
				bool signal_filtered = false;	

				// create temporary signal
				newSignal = class_signal_init(bdata(the_market->Ind[closePos]->DATEVEC[daynr]), 
					the_market->Ind[closePos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouBPos]->QUOTEVEC[daynr - parms.ICHI_PERIOD_MID],
					bdata(*the_market->symbol), bdata(*the_market->identifier), shortsignal, strong, the_market->nr_digits, 
					"Kumo Breakout", "Close quote left or crossed Kumo (Cloud)", 
					"from rising Kumo",  "ichimoku_daily_signals");	 
				
				// consider Chikou filter if configured
				if(parms.ICHI_CHIKOU_CONFIRM)
				{
					if(kumo_trend != chikou_trend)
						signal_filtered = true;
				}
 				
				// consider Kumo filter if configured
				if(parms.ICHI_KUMO_CONFIRM)
 				{
 					if(!check_new_lowhigh(daynr, the_market->Ind[closePos]->QUOTEVEC, ind_minima, ind_maxima, nr_maxima, shortsignal))
 						signal_filtered = true;
 				}		
				
				if(!(signal_filtered))
					ichimoku_signals->addSignal(ichimoku_signals, newSignal);
				// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
				newSignal->destroy(newSignal);					
 			}
 		}
 		// else if("falling kumo")
 		else if(the_market->Ind[senkouAPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID] <
			the_market->Ind[senkouBPos]->QUOTEVEC[daynr-parms.ICHI_PERIOD_MID])		
 		{
			if(ichimoku_golden_X(daynr, the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[senkouBPos]->QUOTEVEC, parms.ICHI_PERIOD_MID))
 			{
				class_signal* newSignal;
				bool signal_filtered = false;	

				// create temporary signal
				newSignal = class_signal_init(bdata(the_market->Ind[closePos]->DATEVEC[daynr]), 
					the_market->Ind[closePos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouBPos]->QUOTEVEC[daynr - parms.ICHI_PERIOD_MID],
					bdata(*the_market->symbol), bdata(*the_market->identifier), longsignal, strong, the_market->nr_digits, 
					"Kumo Breakout", "Close quote left or crossed Kumo (Cloud)", 
					"from falling Kumo",  "ichimoku_daily_signals");	 
				
				// consider Chikou filter if configured
				if(parms.ICHI_CHIKOU_CONFIRM)
				{
					if(kumo_trend != chikou_trend)
						signal_filtered = true;
				}
 				
				// consider Kumo filter if configured
				if(parms.ICHI_KUMO_CONFIRM)
 				{
 					if(!check_new_lowhigh(daynr, the_market->Ind[closePos]->QUOTEVEC, ind_minima, ind_maxima, nr_maxima, longsignal))
 						signal_filtered = true;
 				}		
				
				if(!(signal_filtered))
					ichimoku_signals->addSignal(ichimoku_signals, newSignal);
				// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
				newSignal->destroy(newSignal);	
 			}
 			else if(ichimoku_death_X(daynr, the_market->Ind[closePos]->QUOTEVEC, the_market->Ind[senkouAPos]->QUOTEVEC, parms.ICHI_PERIOD_MID))	
 			{
				class_signal* newSignal;
				bool signal_filtered = false;	

				// create temporary signal
				newSignal = class_signal_init(bdata(the_market->Ind[closePos]->DATEVEC[daynr]), 
					the_market->Ind[closePos]->DAYNRVEC[daynr], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouAPos]->QUOTEVEC[daynr - parms.ICHI_PERIOD_MID],
					bdata(*the_market->symbol), bdata(*the_market->identifier), shortsignal, strong, the_market->nr_digits, 
					"Kumo Breakout", "Close quote left or crossed Kumo (Cloud)", 
					"from falling Kumo",  "ichimoku_daily_signals");	 
				
				// consider Chikou filter if configured
				if(parms.ICHI_CHIKOU_CONFIRM)
				{
					if(kumo_trend != chikou_trend)
						signal_filtered = true;
				}
 				
				// consider Kumo filter if configured
				if(parms.ICHI_KUMO_CONFIRM)
 				{
 					if(!check_new_lowhigh(daynr, the_market->Ind[closePos]->QUOTEVEC, ind_minima, ind_maxima, nr_maxima, shortsignal))
 						signal_filtered = true;
 				}		
				
				if(!(signal_filtered))
					ichimoku_signals->addSignal(ichimoku_signals, newSignal);
				// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
				newSignal->destroy(newSignal);					
 			}
 		}
// ============ END CHECK FOR KUMO BREAKOUT ==========================================================================
// 
// ============  CHECK FOR SENKOU Cross ==============================================================================
// In general: the cross itself is only a trigger and happens shifted into the future. The signal occurs "backwards"
// in time, at the date of the price. The checks of price against Kumo are also done in "price date", not trigger date
		if(ichimoku_golden_X(daynr, the_market->Ind[senkouAPos]->QUOTEVEC, the_market->Ind[senkouBPos]->QUOTEVEC, 0))
 		{
			// yay, long signal
			class_signal* newSignal;
			strength_type strength;
			bool signal_filtered = false;
			
			// we have a golden cross, look if it was above,below or in Kumo
			switch (kumo_trend)
			{
				case uptrend:	// cross above kumo
					strength = strong;
					break;
				case downtrend:	// cross below kumo
					strength = weak;
					break;
				case sideways:	// cross inside kumo
					strength = neutral;
					break;
			}
 			
			// create temporary signal
 			newSignal = class_signal_init(bdata(the_market->Ind[tenkanPos]->DATEVEC[daynr]), 
					the_market->Ind[senkouAPos]->DAYNRVEC[daynr - parms.ICHI_PERIOD_MID], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouAPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), longsignal, strength, the_market->nr_digits, 
					"Senkou Cross", "Senkou A crossed Senkou B", 
					bdata(the_market->Ind[senkouAPos]->DATEVEC[daynr]),  "ichimoku_daily_signals");	 

			// consider config parameter ICHI_CHIKOU_CONFIRM
			// if set, apply Chikou span filter: signals are only
			// valid if the trend status is confirmed by chikou span
 			if(parms.ICHI_CHIKOU_CONFIRM)
  			{	// makes sense only for strong signals
  				if(newSignal->strength == strong)
  				{
  					if(!(kumo_trend == chikou_trend))
 						signal_filtered = true;
  				}
  			}
  			if(!(signal_filtered))
				ichimoku_signals->addSignal(ichimoku_signals, newSignal);
			// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
			newSignal->destroy(newSignal);
		}
		
   		else if(ichimoku_death_X(daynr, the_market->Ind[senkouAPos]->QUOTEVEC, the_market->Ind[senkouBPos]->QUOTEVEC, 0))
 		{
			// yay, short signal
			class_signal* newSignal;
			strength_type strength;
			bool signal_filtered = false;
			
			// we have a death cross, look if it was above,below or in Kumo
			switch (kumo_trend)
			{
				case uptrend:	// cross above kumo
					strength = weak;
					break;
				case downtrend:	// cross below kumo
					strength = strong;
					break;
				case sideways:	// cross inside kumo
					strength = neutral;
					break;
			}
			// create temporary signal
 			newSignal = class_signal_init(bdata(the_market->Ind[kijunPos]->DATEVEC[daynr]), 
					the_market->Ind[senkouAPos]->DAYNRVEC[daynr - parms.ICHI_PERIOD_MID], 
					the_market->Ind[closePos]->QUOTEVEC[daynr], 
					the_market->Ind[senkouAPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), shortsignal, strength, the_market->nr_digits, 
					"Senkou Cross", "Senkou A crossed Senkou B", 
					bdata(the_market->Ind[senkouAPos]->DATEVEC[daynr]),  "ichimoku_daily_signals");	
			
			// consider config parameter ICHI_CHIKOU_CONFIRM
			// if set, apply Chikou span filter: signals are only
			// valid if the trend status is confirmed by chikou span
 			if(parms.ICHI_CHIKOU_CONFIRM)
  			{	// makes sense only for strong signals
  				if(newSignal->strength == strong)
  				{
  					if(!(kumo_trend == chikou_trend))
 						signal_filtered = true;
  				}
  			}
  			if(!(signal_filtered))
				ichimoku_signals->addSignal(ichimoku_signals, newSignal);
			// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
			newSignal->destroy(newSignal);	
		}
// ============  END CHECK FOR SENKOU Cross ==========================================================================
// 
// ============  CHECK FOR Chikou Cross ==============================================================================
// In general: chikou is lagging behind, so do not check for the whole loop as chikou lacks 26 days of data
// attention: signal is only valid if chikou deathcross happens while chikou is rising and deathcross -> chikou
// is falling (so not because price action)
 		if(daynr < (parms.INDI_DAYS_TO_UPDATE - parms.ICHI_PERIOD_MID))
 		{
			if(ichimoku_golden_X(daynr, the_market->Ind[chikouPos]->QUOTEVEC, the_market->Ind[closePos]->QUOTEVEC, 0) &&
				 (indicator_trend(daynr, the_market->Ind[chikouPos])== uptrend))
 			{
				// yay, long signal
				class_signal* newSignal;
				strength_type strength;
				bool signal_filtered = false;
				
				// we have a golden cross, look if it was above,below or in Kumo
				switch (kumo_trend)
				{
					case uptrend:	// cross above kumo
						strength = strong;
						break;
					case downtrend:	// cross below kumo
						strength = weak;
						break;
					case sideways:	// cross inside kumo
						strength = neutral;
						break;
				}
			
				// create temporary signal
				newSignal = class_signal_init(bdata(the_market->Ind[closePos]->DATEVEC[daynr+parms.ICHI_PERIOD_MID]), 
					the_market->Ind[closePos]->DAYNRVEC[daynr+parms.ICHI_PERIOD_MID], 
					the_market->Ind[closePos]->QUOTEVEC[daynr+parms.ICHI_PERIOD_MID], 
					the_market->Ind[chikouPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), longsignal, strength, the_market->nr_digits, 
					"Chikou Cross", "Chikou crossed price", 
					bdata(the_market->Ind[chikouPos]->DATEVEC[daynr]),  "ichimoku_daily_signals");	 				
				
				// consider config parameter ICHI_CHIKOU_CONFIRM
				// if set, apply Chikou span filter: signals are only
				// valid if the trend status is confirmed by chikou span
				if(parms.ICHI_CHIKOU_CONFIRM)
				{	// makes sense only for strong signals
					if(newSignal->strength == strong)
					{
						if(!(kumo_trend == chikou_trend))
							signal_filtered = true;
					}
				}
				if(!(signal_filtered))
					ichimoku_signals->addSignal(ichimoku_signals, newSignal);
				// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
				newSignal->destroy(newSignal);				
 			}
 			else if(ichimoku_death_X(daynr, the_market->Ind[chikouPos]->QUOTEVEC, the_market->Ind[closePos]->QUOTEVEC, 0) &&
				 (indicator_trend(daynr, the_market->Ind[chikouPos])== downtrend))			
 			{
				// yay, short signal
				class_signal* newSignal;
				strength_type strength;
				bool signal_filtered = false;
				
				// we have a death cross, look if it was above,below or in Kumo
				switch (kumo_trend)
				{
					case uptrend:	// cross above kumo
						strength = weak;
						break;
					case downtrend:	// cross below kumo
						strength = strong;
						break;
					case sideways:	// cross inside kumo
						strength = neutral;
						break;
				}
				// create temporary signal
				newSignal = class_signal_init(bdata(the_market->Ind[closePos]->DATEVEC[daynr+parms.ICHI_PERIOD_MID]), 
					the_market->Ind[closePos]->DAYNRVEC[daynr+parms.ICHI_PERIOD_MID], 
					the_market->Ind[closePos]->QUOTEVEC[daynr+parms.ICHI_PERIOD_MID], 
					the_market->Ind[chikouPos]->QUOTEVEC[daynr],
					bdata(*the_market->symbol), bdata(*the_market->identifier), shortsignal, strength, the_market->nr_digits, 
					"Chikou Cross", "Chikou crossed price", 
					bdata(the_market->Ind[chikouPos]->DATEVEC[daynr]),  "ichimoku_daily_signals");
				
				// consider config parameter ICHI_CHIKOU_CONFIRM
				// if set, apply Chikou span filter: signals are only
				// valid if the trend status is confirmed by chikou span
				if(parms.ICHI_CHIKOU_CONFIRM)
				{	// makes sense only for strong signals
					if(newSignal->strength == strong)
					{
						if(!(kumo_trend == chikou_trend))
							signal_filtered = true;
					}
				}
				if(!(signal_filtered))
					ichimoku_signals->addSignal(ichimoku_signals, newSignal);
				// destroy temporary signal, as the filtered signal now is part of ichimoku_signals list
				newSignal->destroy(newSignal);				
 			}
 		}
// ============  END CHECK FOR Chikou Cross ==========================================================================
   	} // end for loop through all days
   	
//     	cleanup
	free_1d_array_uint(ind_minima);
 	free_1d_array_uint(ind_maxima);	
	
	// do terminal output if configured so
	if(!biseqcstr(parms.TERMINAL_EVAL_RESULT, "none"))
		ichimoku_print_eval_summary(the_market, ichimoku_signals);
	
	// if configures so, create plot files
  	if(parms.CHART_PLOT)
		ichimoku_plot_chart(*the_market->symbol, the_market->Ind[openPos], the_market->Ind[highsPos], 
				the_market->Ind[lowsPos], the_market->Ind[closePos], the_market->Ind[tenkanPos], 
				the_market->Ind[kijunPos], the_market->Ind[chikouPos], the_market->Ind[senkouAPos], 
				the_market->Ind[senkouBPos], ichimoku_signals);

	
	return 0;
	
error_sanity_check:
    // cleanup what is possible
    the_market->destroy(the_market);
    ichimoku_signals->destroy(ichimoku_signals);
	free_1d_array_uint(ind_minima);
 	free_1d_array_uint(ind_maxima);	
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief appends the signals in db to the csv file that was already created 
 * 
 * appends the signals in db to the csv file that was already created 
 * @param symbol bstring with market string
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int ichimoku_append_signals_csv(bstring symbol, class_signal_list* the_signals)
{
	extern parameter parms; // parms are declared global in main.c
	FILE *csv_fp;
	FILE *newfile_fp;
	
 	unsigned int i, signal_counter;
	unsigned int warnings;
	unsigned int current_daynr=0, signal_daynr=0;
	bstring path=bfromcstr("data/charts/");
	
	bstring filename_csv=NULL;
	bstring filename_new=NULL;
	bstring current_date=NULL;
	bstring headerline=NULL;
	bstring signalquote=NULL;
	bstring line=NULL;
	
	class_signal_list* filtered_signals= NULL;
	filtered_signals = class_signal_list_init("all current markets symbols");
	
	for(unsigned int i=0; i<the_signals->nr_signals; i++)
	{
		if(biseq(symbol, *the_signals->Sig[i]->THE_SYMBOL))
		{
			filtered_signals->addSignal(filtered_signals, the_signals->Sig[i]);
		}
	}
	
	bconcat(path, symbol);
	filename_csv=bstrcpy(path);
	filename_new=bstrcpy(path);	
	bstring tmp1=bfromcstr("_plotdata.csv");
	bstring tmp2=bfromcstr("_plotdata.tmp.csv");
	bconcat(filename_csv, tmp1);
	bconcat(filename_new, tmp2);

	csv_fp=fopen(bdata(filename_csv),"r");	// open file handler
	newfile_fp=fopen(bdata(filename_new),"w+");
	
	check(csv_fp !=0, "Plotdata file %s not found! Cannot add signal data! \n", error, bdata(filename_csv));
	check(newfile_fp !=0, "Cannot open file %s \n", error, bdata(filename_new));
    
	headerline = bgets((bNgetc) fgetc, csv_fp, (char) '\n');
	
	bdelete (headerline, blength(headerline)-1, 1); //remove '\n' from line
	bstring tmp3 = bfromcstr(";signal1_name;signal1_descr;signal1_quote;signal2_name;signal2_descr;signal2_quote;signal3_name;signal3_descr;signal3_quote;signal4_name;signal4_descr;signal4_quote;signal5_name;signal5_descr;signal5_quote");
	bconcat(headerline, tmp3);
	//printf("\n%s", bdata(headerline));
	fprintf(newfile_fp,"%s\n",bdata(headerline));
	
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);
	
	warnings = 0;
	
	for(warnings = 0; 
		NULL != (line = bgets((bNgetc) fgetc, csv_fp, (char) '\n')); 
		bdestroy(line))
	{
		bdelete (line, blength(line)-1, 1); //remove '\n' from line
		i= bstrchr(line, ';'); 
		bstring tmp1 = bmidstr(line, 0, i); // everything left of the ;
		current_date=bstrcpy(tmp1); 
		bdestroy(tmp1);
		current_daynr = get_daynr_from_bstr(current_date);
		bdestroy(current_date);
		
		signal_counter = 0;
		
		// check if current day has a signal, loop through all signals
		for(i=0; i< filtered_signals->nr_signals; i++)
		{
			// translate current signals date into daynr
			signal_daynr = *filtered_signals->Sig[i]->SIG_DAYNR;
			// compare signals daynr with current daynr
			if(signal_daynr == current_daynr)	// is equal --> current date has a signal
			{
				if(((filtered_signals->Sig[i]->strength == weak) && parms.CHART_PLOT_WEAK) ||
					((filtered_signals->Sig[i]->strength == neutral) && parms.CHART_PLOT_NEUTRAL) ||
					((filtered_signals->Sig[i]->strength == strong) && parms.CHART_PLOT_STRONG))
				{
					signal_counter++;
					bstring tmp1=NULL;
					bstring tmp2 = bfromcstr(";");
					bconcat(line, tmp2);
					//printf("\n%s", bdata(*filtered_signals->Sig[i]->name));
					// shorten the signal names, or the plots will be unreadable
					if (biseqcstr(*filtered_signals->Sig[i]->name, "Kijun Cross")) 
					{
						tmp1 = bfromcstr("K-X");
						bconcat(line, tmp1);
					}
					else if(biseqcstr (*filtered_signals->Sig[i]->name, "Tenkan/Kijun Cross")) 
					{
						tmp1 = bfromcstr("TK-X");
						bconcat(line, tmp1);
					}
					else if(biseqcstr(*filtered_signals->Sig[i]->name, "Chikou Cross"))
					{
						tmp1 = bfromcstr("C-X");
						bconcat(line, tmp1);
					}
					else if(biseqcstr(*filtered_signals->Sig[i]->name, "Senkou Cross"))
					{
						tmp1 = bfromcstr("S-X");
						bconcat(line, tmp1);	
					}
					else if(biseqcstr(*filtered_signals->Sig[i]->name, "Kumo Breakout"))
					{
						tmp1 = bfromcstr("KB");
						bconcat(line, tmp1);	
					}						

					bconcat(line, tmp2);
					bstring tmp3= NULL;
					// also shorten strength description
					switch(filtered_signals->Sig[i]->strength)
					{
						case weak:
							tmp3 = bfromcstr("w");
							bconcat(line, tmp3);
							break;
						case neutral:
							tmp3 =  bfromcstr("n");
							bconcat(line,tmp3);
							break;
						case strong:	
							tmp3 = bfromcstr("S");
							bconcat(line, tmp3);
							break;
					}
					// we also need short signal types for plotting
					bstring tmp4 = NULL;
					switch(filtered_signals->Sig[i]->type)
					{
						case longsignal:
							tmp4 = bfromcstr("L");
							bconcat(line, tmp4);
							break;
						case shortsignal:
							tmp4 = bfromcstr("S");
							bconcat(line, tmp4);
							break;
						case undefined:		// should absolutely not happen here
						default:
							break;	
					}
					bconcat(line, tmp2);
					// convert float quote to bstring 
					bstring tmp5 = bformat("%f",filtered_signals->Sig[i]->indicator_quote);
					signalquote = bstrcpy(tmp5);
					bconcat(line, signalquote);
					
					bdestroy(tmp1);
					bdestroy(tmp2);
					bdestroy(tmp3);
					bdestroy(tmp4);
					bdestroy(tmp5);
					bdestroy(signalquote);
										
				}
				else	// skip signal output to csv, write only 2 colons
				{
					bstring tmp5 = bfromcstr(";;");
					bconcat(line, tmp5);	
					bdestroy(tmp5);
				}
	
				//break;	// do not exit prematurely..in theory we could have 5 signals at the same date
			}
		}
		// well, errm... I know how this looks. That`s the easiest way I came up with to
		// fill the theoretical up to 15 empty indicator columns with ';'
		bstring tmp6 = NULL; 
		switch(signal_counter)
		{
			case 0:
				tmp6 = bfromcstr(";;;;;;;;;;;;;;;");
				bconcat(line, tmp6);
				break;
			case 1:
				tmp6 = bfromcstr(";;;;;;;;;;;;");
				bconcat(line, tmp6);
				break;
			case 2:
				tmp6 = bfromcstr(";;;;;;;;");
				bconcat(line, tmp6);
				break;
			case 3: 
				tmp6 = bfromcstr(";;;");
				bconcat(line, tmp6);
				break;
		}
		bstring tmp7 = bfromcstr("\n");
		bconcat(line,tmp7);
		// write original line to new csv (without signals yet
		fprintf(newfile_fp,"%s", bdata(line)); 	
		bdestroy(tmp6);
		bdestroy(tmp7);
	}
	
	warnings++;
	//fprintf(fp,"%s",bdata(header));	// print header as first row
	
	fclose(csv_fp);
	fclose(newfile_fp);
	
	if(remove(bdata(filename_csv)) != 0)
		fprintf(stdout, "Error deleting %s \n", bdata(filename_csv));
	if(rename(bdata(filename_new), bdata(filename_csv)) != 0)
		fprintf(stdout, "Error renaming %s to %s", bdata(filename_new), bdata(filename_csv));
	
	// cleanup
	bdestroy(path);
	bdestroy(filename_csv);
	bdestroy(filename_new);
//	bdestroy(current_date);
	bdestroy(headerline);
//	bdestroy(signalquote);
	bdestroy(line);
	
	filtered_signals->destroy(filtered_signals);
	
	return 0; 
	
error:
	return 1;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief creates the plot instructions used by GNUPLOT, based on settings of 
 * config file 
 * 
 * creates the plot instructions used by GNUPLOT, based on settings of 
 * config file
 * @param symbol bstring with market string
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int ichimoku_create_plotfile(bstring symbol)
{
	extern parameter parms; // parms are declared global in main.c
	FILE *fp;
 	
	bstring path=bfromcstr("data/charts/");
	bstring filename = NULL;
	bstring datafile=NULL;
	
	bstring linecolor = NULL, backcolor = NULL, candle_down_color = NULL;
	bstring candle_up_color = NULL, fill_style = NULL;
	bstring fill_kumo_up = NULL, fill_kumo_down = NULL, vectorstyle = NULL;
	
	bstring tmp1 = bfromcstr("contrast");
	if (!bstrcmp(parms.CHART_COLORSCHEME,tmp1))
	{
		linecolor = bfromcstr("\"yellow\"");
		backcolor = bfromcstr("\"black\"");
		candle_down_color = bfromcstr("'red'");
		candle_up_color = bfromcstr("'green'");
		fill_style = bfromcstr("solid noborder");
		fill_kumo_up=bfromcstr(" ");
		fill_kumo_down=bfromcstr(" "); 
		vectorstyle=bfromcstr(" lt 5 dashtype 2 ");
	}
	else if (!bstrcmp(parms.CHART_COLORSCHEME,bfromcstr("light")))
	{
		linecolor = bfromcstr("\"black\"");
		backcolor = bfromcstr("\"white\"");
		candle_down_color = bfromcstr("'#CD5C5C'");
		candle_up_color = bfromcstr("'#32CD32'");
		fill_style = bfromcstr("solid 0.25 border");
		fill_kumo_up=bfromcstr("fill pattern 7");
		fill_kumo_down=bfromcstr("fill pattern 6");
		vectorstyle = bfromcstr(" lt 8 dashtype 2 ");
	}
	else
	{
		printf("\nvalue %s for CHART_COLORSCHEME not allowed! Using 'contrast' as default.", bdata(parms.CHART_COLORSCHEME));
		linecolor = bfromcstr("\"yellow\"");
		backcolor = bfromcstr("\"black\"");
		candle_down_color = bfromcstr("'red'");
		candle_up_color = bfromcstr("'green'");
		fill_style = bfromcstr("solid noborder");
		fill_kumo_up=bfromcstr(" ");
		fill_kumo_down=bfromcstr(" "); 
	}
		
	datafile= bstrcpy(symbol);
	bstring tmp2 = bfromcstr("_plotdata.csv");
	bconcat(datafile, tmp2);
	
	bconcat(path,symbol);
	filename=bstrcpy(path);
	bstring tmp3 = bfromcstr(".plot");
	bconcat(filename,tmp3);
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);

	fp=fopen(bdata(filename),"w+");	// open file handler
	
	// construct plot file
	
	fprintf(fp, "set title \"%s daily\" textcolor %s offset char -60,0\n", bdata(symbol), bdata(linecolor));
	fprintf(fp, "set label \"  www.spare-time-trading.de\" at graph 0.021, 0.19 center rotate by 90 textcolor %s front\n", bdata(linecolor));	
	if(parms.CHART_PLOT_WEAK)
	{
		fprintf(fp, "\nset label \"wL: weak Long Signal\" at graph 0.87,0.9 textcolor %s front",bdata(linecolor));
		fprintf(fp, "\nset label \"wS: weak Short Signal\" at graph 0.87, 0.885 textcolor %s front",bdata(linecolor));		
	}
	if(parms.CHART_PLOT_NEUTRAL)
	{
		fprintf(fp, "\nset label \"nL: neutral Long Signal\" at graph 0.87,0.87 textcolor %s front",bdata(linecolor));
		fprintf(fp, "\nset label \"nS: neutral Short Signal\" at graph 0.87, 0.855 textcolor %s front",bdata(linecolor));
	}
	if(parms.CHART_PLOT_STRONG)
	{
		fprintf(fp, "\nset label \"SL: Strong Long Signal\" at graph 0.87, 0.84 textcolor %s front",bdata(linecolor));
		fprintf(fp, "\nset label \"SS: Strong Short Signal\" at graph 0.87, 0.825 textcolor %s front ",bdata(linecolor));	
	}
	fprintf(fp, "\nset label \"K-X: Kijun Cross\" at graph 0.87, 0.81 textcolor %s front\n",bdata(linecolor));
	fprintf(fp, "set label \"TK-X: Tenkan/ Kijun Cross\" at graph 0.87,0.795 textcolor %s\n",bdata(linecolor));
	fprintf(fp, "set label \"C-X: Chikou Cross\" at graph 0.87, 0.78 textcolor %s front\n",bdata(linecolor));
	fprintf(fp, "set label \"S-X: Senkou Cross\" at graph 0.87, 0.765 textcolor %s front\n",bdata(linecolor));
	fprintf(fp, "set label \"KB: Kumo Breakout\" at graph 0.87, 0.75 textcolor %s front\n\n",bdata(linecolor));
		
	fprintf(fp, "set terminal pngcairo size 1600,1200 nocrop enhanced font \"arial,12\"\n");
	fprintf(fp, "set output \"%s_%s.png\"\n\n", bdata(symbol), bdata(parms.CHART_COLORSCHEME));
 	fprintf(fp, "#set terminal wxt size 800,600 enhanced\n\n");

	fprintf(fp, "set border linecolor rgbcolor %s\n", bdata(linecolor));
	fprintf(fp, "set key textcolor rgbcolor %s\n\n", bdata(linecolor));
	
	fprintf(fp, "set obj 1 rectangle behind from screen 0,0 to screen 1,1\n");
	fprintf(fp, "set obj 1 fillstyle solid 1.0 fillcolor rgbcolor %s\n\n", bdata(backcolor));
	
	fprintf(fp, "set palette defined (-1 %s, 1 %s)\n", bdata(candle_down_color), bdata(candle_up_color));
	
	fprintf(fp, "set grid\n");
	fprintf(fp, "set lmargin 9\n");
	fprintf(fp, "set rmargin 2\n\n");
 
	fprintf(fp, "set style fill %s \n\n",bdata(fill_style));
 
	fprintf(fp, "set cbrange [-1:1]\n\n");
	
	fprintf(fp, "unset colorbox\n\n");

	fprintf(fp, "everyNth(countColumn, labelColumnNum, N) = \\");
	fprintf(fp, "\n   ( (int(column(countColumn)) %% N == 0) ? stringcolumn(labelColumnNum) : \"\" )\n\n"); 
   
	fprintf(fp, "set xtics rotate 90\n\n");

	fprintf(fp, "set datafile separator \";\"\n");
	fprintf(fp, "set key autotitle columnhead\n\n");
 
	fprintf(fp, "set multiplot\n");
	fprintf(fp, "plot \"%s\" using 0:9:10:xticlabels(everyNth(0, 1, 5)) with filledcurves above %s lt 2 title 'rising Kumo', \\", bdata(datafile), bdata(fill_kumo_up));
	fprintf(fp, "\n\t\"%s\" using 0:9:10:xticlabels(everyNth(0, 1, 5)) with filledcurves below %s lt 4 title 'falling Kumo', \\", bdata(datafile), bdata(fill_kumo_down));
	// TODO: this plots the signal, make this configurable!
	fprintf(fp, "\n\t\"%s\" using 0:13 with circles lt 3 fs transparent solid 0.3 noborder notitle,\\", bdata(datafile));
	fprintf(fp, "\n\t\"%s\" using 0:13:11 with labels font \"Verdana, 8\" textcolor %s nopoint offset char 0,-1.5 notitle,\\",bdata(datafile), bdata(linecolor));	
	fprintf(fp, "\n\t\"%s\" using 0:6 with lines lt 7 lw 2,\\",bdata(datafile));
	fprintf(fp, "\n\t\"%s\" using 0:7 with lines lt 3 lw 2,\\", bdata(datafile));
	fprintf(fp, "\n\t\"%s\" using 0:8 with lines lt 2 lw 1,\\", bdata(datafile));
	fprintf(fp, "\n\t\"%s\" using 0:2:3:4:5:($5 < $2 ? -1 : 1):xticlabels(everyNth(0, 1, 5)) notitle with candles palette\n", bdata(datafile));

	fprintf(fp, "MIN=GPVAL_Y_MIN\n");
	fprintf(fp, "MAX=GPVAL_Y_MAX\n");
	fprintf(fp, "replot \"%s\" using 0:($13):(0):(MAX-$13) with vectors %s nohead notitle,\\",bdata(datafile), bdata(vectorstyle));
        fprintf(fp, "\n\t\"%s\" using 0:(MAX):12 with labels font \"Verdana, 8\" textcolor %s offset char -1,char 0.8 rotate by 90 notitle\n",bdata(datafile),bdata(linecolor));
	fprintf(fp, "unset multiplot\n");
	
//	fprintf(fp, "\n\t\"%s\" using 0:8 with lines lt 2 lw 1\n\n", bdata(datafile));
 
//	fprintf(fp, "replot \"%s\" using 0:2:3:4:5:($5 < $2 ? -1 : 1):xticlabels(everyNth(0, 1, 5)) notitle with candles palette\n", bdata(datafile));
	
//	fprintf(fp, "pause -1 \"Hit any key to continue\"");
 
	fclose(fp);
	
	// cleanup
	bdestroy(path);
	bdestroy(filename);
	bdestroy(datafile);
	bdestroy(linecolor);
	bdestroy(backcolor);
	bdestroy(candle_down_color);
	bdestroy(candle_up_color);
	bdestroy(fill_style);
	bdestroy(fill_kumo_down);
	bdestroy(fill_kumo_up);
	bdestroy(vectorstyle);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief determines if current price is above, in or below kumo 
 * 
 * determines if current price is above, in or below kumo
 *
 * @param quote_close float with relevant close quote
 * @param quote_senkA float with relevant senkou span A quote
 * @param quote_senkB float with relevant senkou span B quote
 * @return trend_type current trend state (uptrend/downtrend/sideways)
 */
///////////////////////////////////////////////////////////////////////////////
trend_type ichimoku_relation_to_kumo(float quote_close, float quote_senkA, \
                                     float quote_senkB)
{
	trend_type current_trend;
	
	// above both Senkou Spans = above Kumo
	if ((quote_close > quote_senkA) && (quote_close > quote_senkB))
	{
		current_trend = uptrend;
	}
	// below both senkou Spans = below Kumo
 	else if ((quote_close < quote_senkA) && (quote_close < quote_senkB))
	{
 		current_trend = downtrend;
	}
	// if neither it must be inside kumo
 	else
	{
 		current_trend = sideways;	
	}

	return current_trend;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief determines if current chikou is above or below price and kumo 
 * 
 * determines if current chikou is above or below price and kumo
 *
 * @param quote_close float with relevant close quote
 * @param quote_chikou float with relevant chikou span quote
 * @return trend_type current trend state (uptrend/downtrend/sideways)
 */
///////////////////////////////////////////////////////////////////////////////
trend_type ichimoku_get_chikou_trend(float quote_close, float quote_chikou, float quote_senkouA, float quote_senkouB)
{
	trend_type current_trend = sideways;
	
	// is chikou below price?
	if (quote_chikou < quote_close)
	{
		// is chikou below price and kumo?
		if((quote_chikou < quote_senkouA) && (quote_chikou < quote_senkouB))
			current_trend = downtrend;	// yes it is -> downtrend
		else
			current_trend = sideways;	// no it`s not -> sideways	
	}
	//is chikou above price?
	else if(quote_chikou > quote_close)
	{
		// is chikou above price and kumo?
		if((quote_chikou > quote_senkouA) && (quote_chikou > quote_senkouB))
			current_trend = uptrend;	// yes -> uptrend
		else
			current_trend = sideways;	// no -> sideways
	}
	else 
		current_trend = sideways;	// should only be hit rarely 

	return current_trend;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief universal golden cross routine, determines if price 1 crosses price 2
 * from below to above 
 * 
 * universal golden cross routine, determines if price 1 crosses price 2 from
 * below to above. The function can consider shifted or lagging dates between
 * the two indicators, e.g. for ichimoku the senkouA indicator is shifted 26 
 * days into the future, offset_days would be 26. If a hypothetical indicator
 * quotes_2 would have a date 5 days earlier than quotes_1, offset_days would 
 * be -5. Usually this parameter will be zero.
 * 
 * @note as this routine works for every indicator, it might be moved out of
 *       ichimoku.c, which is ichimoku-specific
 * @note the variable daynr that is used througout this file is not equivalent
 * with the date.c daynumber (number of days since 1900-01-01). Instead it is 
 * only a counter between 0 and the PARMS.ICHI_DAYS_TO_ANALYZE
 * @todo change variable names daynr to make clear distinction between date.c`s
 *       daynumbers (days since 1900-01-01) and ichimoku.c´s concept 
 * @param daynr int with daynr of relevant close quote
 * @param quotes_1 pointer to Nx1 matrix with quotes of indicator 1
 * @param quotes_2 pointer to Nx1 matrix with quotes of indicator 2
 * @param offset_days nr. of days that the quotes differ from each other at the
 *                    same position in Nx1 vector
 * @return true if there is a golden X, false if not
 */
///////////////////////////////////////////////////////////////////////////////
bool ichimoku_golden_X(unsigned int daynr, float *quotes_1, float *quotes_2, int offset_days)
{
	extern parameter parms; // parms are declared global in main.c
	unsigned int i;
	// in general: bullish signal if indicator1 crosses indicator2 from below to above
	// check if indicator1 today is above indicator2
	if(quotes_1[daynr] > quotes_2[daynr - offset_days])
	{
		// now check, if yesterdays indicator1 was also above indicator2
		if(quotes_1[daynr-1] > quotes_2[daynr-1 - offset_days])
			return false; // yeah it was, so definitely no golden cross
		else
		{
			// much more interesting, yesterday we were below or equal indicator2
			// so loop backwards until we find a indicator1 not equal to indicator2
			for (i = daynr; i-- > 0 ; )
			{
				if(quotes_1[i] > quotes_2[i - offset_days])
					return false;  // indicator1 was above indicator2, then eqal then above again
				else if(quotes_1[i] < quotes_2[i - offset_days])
					return true;  // ind1 was below ind2, then above (and maybe a couple of days equal)
				// next i will be evaluated only if ind1 and ind2 were equal so far	
			}
		}
		return false;
				
	}
	else 
		return false; //no, indicator1 is below or equal indicator2
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief universal death cross routine, determines if price 1 crosses price 2 
 * from above to below
 * 
 * universal death cross routine, determines if price 1 crosses price 2 from
 * above to below. The function can consider shifted or lagging dates between
 * the two indicators, e.g. for ichimoku the senkouA indicator is shifted 26 
 * days into the future, offset_days would be 26. If a hypothetical indicator
 * quotes_2 would have a date 5 days earlier than quotes_1, offset_days would 
 * be -5. Usually this parameter will be zero.
 * 
 * @note as this routine works for every indicator, it might be moved out of
 *       ichimoku.c, which is ichimoku-specific
 * @note the variable daynr that is used througout this file is not equivalent
 * with the date.c daynumber (number of days since 1900-01-01). Instead it is 
 * only a counter between 0 and the PARMS.ICHI_DAYS_TO_ANALYZE
 * @todo change variable names daynr to make clear distinction between date.c`s
 *       daynumbers (days since 1900-01-01) and ichimoku.c´s concept 
 * @param daynr int with daynr of relevant close quote
 * @param quotes_1 pointer to Nx1 matrix with quotes of indicator 1
 * @param quotes_2 pointer to Nx1 matrix with quotes of indicator 2
 * @param offset_days nr. of days that the quotes differ from each other at the
 *                    same position in Nx1 vector
 * @return true if there is a death X, false if not
 */
///////////////////////////////////////////////////////////////////////////////
bool ichimoku_death_X(unsigned int daynr, float *quotes_1, float *quotes_2, int offset_days)
{
	extern parameter parms; // parms are declared global in main.c
	unsigned int i;
	// in general: bearish signal if indicator1 crosses indicator2 from above to below
	// check if indicator1 today is below indicator2
	// close quotes must be shifted by 26 days, 
	if(quotes_1[daynr] < quotes_2[daynr - offset_days])
	{
		// now check, if yesterdays indicator1 was also below indicator2
		if(quotes_1[daynr-1] < quotes_2[daynr-1 - offset_days])
			return false; // yeah it was, so definitely no death cross
		else
		{
			// much more interesting, yesterday we were above or equal indicator2
			// so loop backwards until we find a indicator1 not equal to indicator2
			for (i = daynr; i-- > 0 ; )
			{
				if(quotes_1[i] < quotes_2[i - offset_days])
					return false;  // indicator1 was below indicator2, then eqal then below again
				else if(quotes_1[i] > quotes_2[i - offset_days])
					return true;  // ind1 was above ind2, then below (and maybe a couple of days equal)
				// next i will be evaluated only if ind1 and ind2 were equal so far	
			}
		}
		return false;
				
	}
	else 
		return false; //no, indicator1 is above or equal indicator2
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prepares .plot file to be used with GNUPLOT
 * 
 * reads database for quotes, indicators and signals, calls routines to write
 * csv file and create a .plot file to be used with GNUPLOT
 * 
 * @param symbol bstring with market string
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int ichimoku_plot_chart(bstring symbol, class_indicators* open, class_indicators* high,
			class_indicators* low, class_indicators* close, class_indicators* tenkan, 
			class_indicators* kijun, class_indicators* chikou, 
			class_indicators* senkouA,class_indicators*  senkouB,  
			class_signal_list* the_signals)
{
	extern parameter parms; // parms are declared global in main.c

 	create_quote_table(symbol, 9, open, high, low, close, tenkan, \
 		 kijun, chikou, senkouA, senkouB);
 	// ============================================
	
 	ichimoku_create_plotfile(symbol);

	// everything is done, XY_plotdata.csv exists, now append the signals to plotdata file
	ichimoku_append_signals_csv(symbol, the_signals);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief checks if specific ichimoku signal is configured to be executed
 * 
 * checks if specific ichimoku signal is configured to be executed
 * 
 * @note as this routine works can be used for every signal, it might be moved 
 * out of ichimoku.c, which is ichimoku-specific
 * @param signalname bstring with signal name
 * @return true if it is configured to be executed, false if not
 */
///////////////////////////////////////////////////////////////////////////////
bool execute_specific_signal(bstring signalname)
{
	bool execute_signal = false;
	extern parameter parms; // parms are declared global in main.c
	
	// check if the execution of each signal is configured
	// if so set return value to true, otherwise return 
	// false
	if(biseqcstr(signalname, "Senkou Cross"))
	{
		if(parms.ICHI_EXECUTION_SENKOU_X == true)
			execute_signal = true;
	}
	
	if(biseqcstr(signalname, "Kijun Cross"))
	{
		if(parms.ICHI_EXECUTION_KIJUN_X == true)
			execute_signal = true;
	}
	
	if(biseqcstr(signalname, "Tenkan/Kijun Cross"))
	{
		if(parms.ICHI_EXECUTION_TK_X == true)
			execute_signal = true;	
	}
	
	if(biseqcstr(signalname, "Chikou Cross"))
	{
		if(parms.ICHI_EXECUTION_CHIKOU_X == true)
			execute_signal = true;
	}
	
	if(biseqcstr(signalname, "Kumo Breakout"))
	{
		if(parms.ICHI_EXECUTION_KUMOBREAK == true)
			execute_signal = true;
	}
	
	return execute_signal;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints the text/html summary to the terminal, if configured to do so
 * 
 * prints the result of the ichimoku evaluation for signals as text/html 
 * summary to the terminal, if configured to do so
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
unsigned int ichimoku_print_eval_summary(class_market* the_market, class_signal_list* the_signals)
{
	extern parameter parms; // parms are declared global in main.c
	
	// get and store Indicator positions for easier access
	const int closePos = the_market->IndPos.close;
	const int chikouPos = the_market->getIndicatorPos(the_market, "chikou");
	const int senkouAPos = the_market->getIndicatorPos(the_market, "senkou_A");
	const int senkouBPos = the_market->getIndicatorPos(the_market, "senkou_B");
	check(closePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error, bdata(*the_market->symbol));
	check(chikouPos>=0, "Indicator \"chikou\" does not exist in market object \"%s\"", error, bdata(*the_market->symbol));
	check(senkouAPos>=0, "Indicator \"Senkou_A\" does not exist in market object \"%s\"", error, bdata(*the_market->symbol));
	check(senkouBPos>=0, "Indicator \"Senkou_B\" close does not exist in market object \"%s\"", error, bdata(*the_market->symbol));
	
	// determine current relation of price to kumo
	trend_type kumo_trend = ichimoku_relation_to_kumo(the_market->Ind[closePos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-1], 
							the_market->Ind[senkouAPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-parms.ICHI_PERIOD_MID-1],   // minus period because same index of senkou 
							the_market->Ind[senkouBPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-parms.ICHI_PERIOD_MID-1]);   // is shifted 1 period(compared to close vector)
 	
	// determine relation of chikou to price and kumo
	trend_type chikou_trend = ichimoku_get_chikou_trend(the_market->Ind[closePos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-parms.ICHI_PERIOD_MID-1], 
							the_market->Ind[chikouPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-parms.ICHI_PERIOD_MID-1], 
							the_market->Ind[senkouAPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1],
							the_market->Ind[senkouBPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1]);

	// determine the kumo top and bottom at the date of last chikou (parms.ICHI_PERIOD_MID lagged)
	float kumo_min = (the_market->Ind[senkouAPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1] <= 	// minus 2 periods because same index of senkou is shifted 1 period
		the_market->Ind[senkouBPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1]) ? 			// and chikou lagging 1 period (so distance = 2 periods)
		the_market->Ind[senkouAPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1] : 
		the_market->Ind[senkouBPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1];
		
 	float kumo_max = (the_market->Ind[senkouAPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1] >= 
		the_market->Ind[senkouBPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1]) ? 
		the_market->Ind[senkouAPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1] : 
		the_market->Ind[senkouBPos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-2*parms.ICHI_PERIOD_MID-1];
   	
 	bstring trendtxt1 = NULL, trendtxt2 = NULL, trendtxt3 = NULL;
 	bstring chiktxt = NULL;
 	bstring kumotxt1 = NULL, kumotxt2 = NULL, kumotxt3 = NULL;
 	bstring html_color1 = NULL, html_color2 = NULL;
 	
 	kumotxt1=bfromcstr("Kumo on ");
 	bconcat(kumotxt1, the_market->Ind[closePos]->DATEVEC[parms.INDI_DAYS_TO_UPDATE-parms.ICHI_PERIOD_MID-1]);
 	
 	kumotxt2=bfromcstr("from ");
	bstring tmp1 = bformat("%.*f", the_market->nr_digits, kumo_min);
 	bconcat(kumotxt2, tmp1);
 	bstring tmp2 = bfromcstr(" to ");
	bconcat(kumotxt2, tmp2);
	bstring tmp3 = bformat("%.*f",the_market->nr_digits, kumo_max);
 	bconcat(kumotxt2, tmp3);

  	kumotxt3=bfromcstr("price ");
	bstring tmp4 = bformat("%.*f", the_market->nr_digits, the_market->Ind[closePos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-parms.ICHI_PERIOD_MID-1]);
 	bconcat(kumotxt3, tmp4);
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);	
	bdestroy(tmp4);
 	
 	switch(kumo_trend)
 	{
 		case downtrend:
 		{
 			trendtxt1=bfromcstr("price below kumo");
 			trendtxt2=bfromcstr("--> Downtrend");
 			trendtxt3=bfromcstr("Signal filter SHORT");
 			html_color1 = HTML_RED_TEXT;
 			
 			if(chikou_trend == downtrend)
 			{
 				chiktxt=bfromcstr("confirmed by Chikou-Span");
 				html_color2 = HTML_RED_TEXT;
 			}
 			else if(chikou_trend == uptrend)
 			{
 				chiktxt=bfromcstr("NOT confirmed by Chikou --> no stable trend");
 				html_color2 = HTML_GREEN_TEXT;
 			}
 			else if(chikou_trend == sideways)
 			{
 				chiktxt=bfromcstr("NOT confirmed by Chikou --> no stable trend");
 				html_color2 = HTML_BLUE_TEXT;
 			}
 			break;
 		}
 		case uptrend:
 		{
 			trendtxt1=bfromcstr("price above kumo");
 			trendtxt2=bfromcstr("--> Uptrend");
 			trendtxt3=bfromcstr("Signal filter LONG");
 			html_color1 = HTML_GREEN_TEXT;
 			if(chikou_trend == downtrend)
 			{
 				chiktxt=bfromcstr("NOT confirmed by Chikou --> no stable trend");
 				html_color2 = HTML_RED_TEXT;
 			}
 			else if(chikou_trend == uptrend)
 			{
 				chiktxt=bfromcstr("confirmed by Chikou-Span");
 				html_color2 = HTML_GREEN_TEXT;
 			}
 			else if(chikou_trend == sideways)
 			{
 				chiktxt=bfromcstr("NOT confirmed by Chikou --> no stable trend");
 				html_color2 = HTML_BLUE_TEXT;
 			}
 			break;
 		}
 		case sideways:
 		{
 			trendtxt1=bfromcstr("price in kumo");
 			trendtxt2=bfromcstr("--> sideways");
 			trendtxt3=bfromcstr("no trades");
 			html_color1 = HTML_BLUE_TEXT;
 			if(chikou_trend == downtrend)
 			{
 				chiktxt=bfromcstr("Chikou-Span below kumo at ");
				bstring tmpstr = bformat("%.*f", the_market->nr_digits, the_market->Ind[chikouPos]->QUOTEVEC[parms.ICHI_DAYS_TO_ANALYZE-parms.ICHI_PERIOD_MID-1]);
 				bconcat(chiktxt,  tmpstr);
 				html_color2 = HTML_RED_TEXT;
				bdestroy(tmpstr);
 			}
 			else if(chikou_trend == uptrend)
 			{
 				chiktxt=bfromcstr("Chikou-Span above kumo at ");
				bstring tmpstr = bformat("%.*f", the_market->nr_digits, the_market->Ind[chikouPos]->QUOTEVEC[parms.ICHI_DAYS_TO_ANALYZE-parms.ICHI_PERIOD_MID-1]);
 				bconcat(chiktxt,  tmpstr);
 				html_color2 = HTML_GREEN_TEXT;
				bdestroy(tmpstr);
 			}
 			else if(chikou_trend == sideways)
 			{
 				chiktxt=bfromcstr("Chikou-Span at ");
				bstring tmpstr = bformat("%.*f", the_market->nr_digits, the_market->Ind[chikouPos]->QUOTEVEC[parms.ICHI_DAYS_TO_ANALYZE-parms.ICHI_PERIOD_MID-1]);
 				bconcat(chiktxt,  tmpstr);
				bstring tmp2str = bfromcstr(" in kumo or between kumo and price");
 				bconcat(chiktxt, tmp2str);
 				html_color2 = HTML_BLUE_TEXT;
				bdestroy(tmp2str);
				bdestroy(tmpstr);
 			}
 			break;
			
 		}
 	}
	
	int count=0;
	int signal_nr[3];
	signal_nr[0]=0;
	signal_nr[1]=0;
	signal_nr[2]=0;
	bstring signaltype0, signaltype1, signaltype2; 
	bstring html_color30 = NULL, html_color31 = NULL, html_color32 = NULL;
	signaltype0=NULL;
	signaltype1=NULL;
	signaltype2=NULL;
	
	// now get the last 3 STRONG signals of current symbol
 	for (unsigned int i = the_signals->nr_signals; i-- > 0 ; )
 	{
 		if((the_signals->Sig[i]->strength == strong) && (count <= 2))
 		{
 			signal_nr[count] = i;
 			count++;
 		}
 		if(count>2)	// ok we have the last 3 strong signals, so exit loop
 		{
 			signal_nr[2] = i;	// without this signal[2] is always 0. wtf..no fucking clue :(
 			break;
 		}
 	}
 	
	// translate the signal type into a string
 	if(the_signals->Sig[signal_nr[0]]->type==longsignal)
 	{
 		signaltype0=bfromcstr("Long");
 		html_color30 = HTML_GREEN_TEXT;
 	}
 	else if(the_signals->Sig[signal_nr[0]]->type==shortsignal)
 	{
 		signaltype0=bfromcstr("Short");
 		html_color30 = HTML_RED_TEXT;
 	}
 	if(the_signals->Sig[signal_nr[1]]->type==longsignal)
 	{
 		signaltype1=bfromcstr("Long");
 		html_color31 = HTML_GREEN_TEXT;
 	}
 	else if(the_signals->Sig[signal_nr[1]]->type==shortsignal)
 	{
 		signaltype1=bfromcstr("Short");
 		html_color31 = HTML_RED_TEXT;
 	}
 	if(the_signals->Sig[signal_nr[2]]->type==longsignal)
 	{
 		signaltype2=bfromcstr("Long");
 		html_color32 = HTML_GREEN_TEXT;
 	}
 	else if(the_signals->Sig[signal_nr[2]]->type==shortsignal)
 	{
 		signaltype2=bfromcstr("Short");
 		html_color32 = HTML_RED_TEXT;
 	}
	// Terminal output depending on settings
	if (biseqcstr(parms.TERMINAL_EVAL_RESULT, "text"))
	// print result as somewhat formatted table in terminal 
	{
	 	printf("\n==========================================================================");
		printf("\n%s \t\t\t\t%s \t\t\t%*f", bdata(*the_market->symbol), 
			bdata(the_market->Ind[closePos]->DATEVEC[parms.INDI_DAYS_TO_UPDATE-1]), 
			the_market->nr_digits, 
			the_market->Ind[closePos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-1]);
	 	printf("\n%s \t\t%s \t\t\t%s",bdata(trendtxt1),bdata(trendtxt2),bdata(trendtxt3));
	 	printf("\n%s", bdata(chiktxt));
	 	printf("\n%s \t\t%s \t\t%s\n", bdata(kumotxt1), bdata(kumotxt2), bdata(kumotxt3));
		printf("----------------------------------------------------------------------------");
		printf("\nLast 3 STRONG signals:");
		printf("\n%s %s at %.*f %s (%s)", bdata(*the_signals->Sig[signal_nr[0]]->SIG_DATE), 
			bdata(signaltype0), the_market->nr_digits,
 			*the_signals->Sig[signal_nr[0]]->SIG_PRICE,
 			bdata(*the_signals->Sig[signal_nr[0]]->name), 
 			bdata(*the_signals->Sig[signal_nr[0]]->amp_info));
		printf("\n%s %s at %.*f %s (%s)", bdata(*the_signals->Sig[signal_nr[1]]->SIG_DATE), 
			bdata(signaltype1), the_market->nr_digits, 
			*the_signals->Sig[signal_nr[1]]->SIG_PRICE, 
			bdata(*the_signals->Sig[signal_nr[1]]->name), 
			bdata(*the_signals->Sig[signal_nr[1]]->amp_info));
		printf("\n%s %s at %.*f %s (%s)", bdata(*the_signals->Sig[signal_nr[2]]->SIG_DATE), 
			bdata(signaltype1), the_market->nr_digits, 
			*the_signals->Sig[signal_nr[2]]->SIG_PRICE, 
			bdata(*the_signals->Sig[signal_nr[2]]->name), 
			bdata(*the_signals->Sig[signal_nr[2]]->amp_info));
		printf("\n==========================================================================\n");
	}
	else if (biseqcstr(parms.TERMINAL_EVAL_RESULT, "html"))
	// print result as html output meant to be c&p'ed to spare-time-trading.de
	{
		printf("\n<table style=\"width: 600px; height: 107px;\" frame=\"box\">");
		printf("\n<tbody>");
		printf("\n<tr style=\"background-color: #cfcfcf;\">");
		printf("\n<td style=\"background-color: #b0e0e6;\"><span style=\"text-decoration: underline;\"><strong>%s</strong></span></td>",bdata(*the_market->symbol));
		printf("\n<td style=\"background-color: #b0e0e6;\"><strong>%s</strong></td>", bdata(the_market->Ind[closePos]->DATEVEC[parms.INDI_DAYS_TO_UPDATE-1]));
		printf("\n<td style=\"background-color: #b0e0e6;\"><strong>%.*f</strong></td>",the_market->nr_digits, the_market->Ind[closePos]->QUOTEVEC[parms.INDI_DAYS_TO_UPDATE-1]);
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color1), bdata(trendtxt1));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color1), bdata(trendtxt2));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color1), bdata(trendtxt3));
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td colspan=\"3\" style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color2), bdata(chiktxt));
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td style=\"border: 1px solid #000000;\">%s</td>",bdata(kumotxt1));
		printf("\n<td style=\"border: 1px solid #000000;\">%s</td>",bdata(kumotxt2));
		printf("\n<td style=\"border: 1px solid #000000;\">%s</td>",bdata(kumotxt3));
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td colspan=\"3\" style=\"border: 1px solid #000000;\">Last 3 STRONG signals:</td>");
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color30), bdata(*the_signals->Sig[signal_nr[0]]->SIG_DATE));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s (%s)</td>",bdata(html_color30), bdata(*the_signals->Sig[signal_nr[0]]->name), bdata(*the_signals->Sig[signal_nr[0]]->amp_info));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s at %.*f</td>",bdata(html_color30), bdata(signaltype0),the_market->nr_digits, *the_signals->Sig[signal_nr[0]]->SIG_PRICE);
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color31), bdata(*the_signals->Sig[signal_nr[1]]->SIG_DATE));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s (%s)</td>",bdata(html_color31), bdata(*the_signals->Sig[signal_nr[1]]->name), bdata(*the_signals->Sig[signal_nr[0]]->amp_info));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s at %.*f</td>",bdata(html_color31), bdata(signaltype1), the_market->nr_digits, *the_signals->Sig[signal_nr[1]]->SIG_PRICE);
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s</td>",bdata(html_color32), bdata(*the_signals->Sig[signal_nr[2]]->SIG_DATE));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s (%s)</td>",bdata(html_color32), bdata(*the_signals->Sig[signal_nr[2]]->name), bdata(*the_signals->Sig[signal_nr[2]]->amp_info));
		printf("\n<td style=\"border: 1px solid #000000;%s\">%s at %.*f</td>",bdata(html_color32), bdata(signaltype2), the_market->nr_digits, *the_signals->Sig[signal_nr[2]]->SIG_PRICE);
		printf("\n</tr>");
		printf("\n<tr>");
		printf("\n<td style=\"border: 1px solid #ffffff;\">&nbsp;</td>");
		printf("\n<td style=\"border: 1px solid #ffffff;\">&nbsp;</td>");
		printf("\n<td style=\"border: 1px solid #ffffff;\">&nbsp;</td>");
		printf("\n</tr>");
		printf("\n</tbody>");
		printf("\n</table>\n");
	}

	// cleanup
	bdestroy(trendtxt1);
	bdestroy(trendtxt2);
	bdestroy(trendtxt3);
	bdestroy(chiktxt);
	bdestroy(kumotxt1);
	bdestroy(kumotxt2);
	bdestroy(kumotxt3);
	bdestroy(html_color1);
	bdestroy(html_color2);
 	
 	bdestroy(signaltype0);
 	bdestroy(signaltype1);
 	bdestroy(signaltype2);
	
	bdestroy(html_color30);
	bdestroy(html_color31);
	bdestroy(html_color32);
		
	return 0;
	
error:
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief updates a market´s trend status according to Ichimoku rules
 * 
 * updates the trend state of a class_market object
 * 
 * @param the_market ptr to market object
 * @param kumo_trend current trend according to kumo
 * @param chikou_trend current trend according to chikou
 * @param daynr_idx uint with current day´s index (refering to market´s close
 * indicator)
 */
///////////////////////////////////////////////////////////////////////////////
void ichimoku_update_trendStat(class_market* the_market, trend_type kumo_trend, trend_type chikou_trend, unsigned int daynr_idx)
{
    extern parameter parms; // parms are declared global in main.c
    
    trend_type currentMarketTrend = sideways;
    
    if(!(daynr_idx == 0))
    {
        currentMarketTrend = the_market->getTrendTypeByIndex(the_market, daynr_idx-1);
    }
    else
    {
        currentMarketTrend = the_market->getTrendTypeByIndex(the_market, 0);
    }

    // if chikou has to confirm a trend, use that rules
    if(parms.ICHI_CHIKOU_CONFIRM)
    {
        if(kumo_trend == chikou_trend)  // trend confirmed
        {
            if((kumo_trend == currentMarketTrend) && (daynr_idx>0)) // if trend continuation, look how long the trend is already running
            {
                unsigned int current_duration = the_market->getTrendDurationByIndex(the_market, daynr_idx - 1);
                the_market->setTrendDurationByIndex(the_market, current_duration + 1, daynr_idx);
            }
            else if ((kumo_trend != currentMarketTrend) && (daynr_idx>0))   // no trend continuation, start duration at 1
            {
                the_market->setTrendDurationByIndex(the_market, 1, daynr_idx);
            }
            else if ((kumo_trend != currentMarketTrend) && (daynr_idx == 0))    
            {
                the_market->setTrendDurationByIndex(the_market, 1, daynr_idx);
            }
            the_market->setTrendTypeByIndex(the_market, kumo_trend, daynr_idx);
        }
        else if (daynr_idx>0)   // trend not confirmed by chikou
        {
            if((currentMarketTrend == sideways) && (daynr_idx>0))   // so count up trend duration in case it is already "sideways"
            {
                unsigned int current_duration = the_market->getTrendDurationByIndex(the_market, daynr_idx - 1);
                the_market->setTrendDurationByIndex(the_market, current_duration + 1, daynr_idx);
            }
            else if ((kumo_trend != currentMarketTrend) && (daynr_idx>0))   // reset duration for all other cases to 1
            {
                the_market->setTrendDurationByIndex(the_market, 1, daynr_idx);
            }
            else if ((kumo_trend != currentMarketTrend) && (daynr_idx == 0))
            {
                the_market->setTrendDurationByIndex(the_market, 1, daynr_idx);
            }
            the_market->setTrendTypeByIndex(the_market, sideways, daynr_idx);
        }
    }
    else    // a bit easier if chikou is not used for trend confirmation
    {
        if((kumo_trend == currentMarketTrend) && (daynr_idx>0)) // trend continuation
        {
            unsigned int current_duration = the_market->getTrendDurationByIndex(the_market, daynr_idx - 1);
            the_market->setTrendDurationByIndex(the_market, current_duration + 1, daynr_idx);
        }
        else if ((kumo_trend != currentMarketTrend) && (daynr_idx>0))   // no trend continuation, reset count
        {
            the_market->setTrendDurationByIndex(the_market, 1, daynr_idx);
        }
        else if ((kumo_trend != currentMarketTrend) && (daynr_idx == 0))
        {
            the_market->setTrendDurationByIndex(the_market, 1, daynr_idx);
        }
        the_market->setTrendTypeByIndex(the_market, kumo_trend, daynr_idx);
    }

    the_market->setTrendPriceByIndex(the_market, daynr_idx, daynr_idx + (parms.INDI_DAYS_TO_UPDATE - parms.ICHI_DAYS_TO_ANALYZE));
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief check if given quote is higher/lower than previous reaction high/low
 * 
 * This function checks if close of given day number is higher than the 
 * previous high (in case of long signal) or lower than the previous low (in 
 * case of short signal). Keep in mind that the minima/maxima indices arrays
 * given as argument contain all extrema of the parms.ICHI_DAYS_TO_ANALYZE
 * period, while the check has to be performed in the left sub-intervall of
 * 0 <= day_index < parms.ICHI_DAYS_TO_ANALYZE.
 * 
 * @param day_index uint with index of given day (=position in quote matrix)
 * @param quotes pointer to float Nx1 matrix with quotes
 * @param indices_minima pointer to uint Nx1 matrix with sorted indices of
 * 			local minima (lows)
 * @param indices_maxima pointer to uint Nx1 matrix with sorted indices of
 * 			local maxima (highs)
 * @param number_maxima nr of maxima (=size of indices_maxima array)
 * @param type determines of looking for a higher high (=longsignal) or a 
 * 			lower lower low (=shortsignal)
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
bool check_new_lowhigh(unsigned int day_index, float *quotes, unsigned int *indices_minima, unsigned int *indices_maxima, unsigned int number_maxima, signal_type type)
{
	bool new_extremum = false;

	unsigned int i=0;
	
	// general comment: un-comment the debug printfs to see the extrema 
	// check algorithm at work in the terminal
	
	// branch according to type of check: new high or new low
	switch(type)
	{
		case longsignal:
			// loop through existing indices in maxima array until
			// we find the one before the current day index
			while((indices_maxima[i] < day_index) && (i < (number_maxima-1)))	// -1 because this is total nr and C array starts at 0
			{
// 				printf("\n   daynr: %i  i:%i, indices_maxima<daynr: %i", day_index, i,indices_maxima[i]);
				i++;
			}
			if(i>0)
				i--; // because of while condition i already has the next index
			// i now has the index of the last maximum before 
			// day_index; if current quote is higher than the quote
			// of i there is a new high
			if(quotes[indices_maxima[i]] < quotes[day_index])
			{
				new_extremum = true; 
// 				printf("\n\ndaynr: %i %.2f < %.2f true", day_index, quotes[indices_maxima[i]] , quotes[day_index]);
			}
//  			else
// 				printf("\n\naynr: %i %.2f < %.2f false", day_index, quotes[indices_maxima[i]] , quotes[day_index]);
			break;

		case shortsignal:
			// loop through existing indices in minima array until
			// we find the one before the current day index
			// note that due to the p1d algorithm the number of 
			// minima is always 1 higher than the number of maxima
			while((indices_minima[i] < day_index) && (i < number_maxima))
			{
			//	printf("\n    i:%i, indices_minima(%i)", i,indices_minima[i]);
				i++;
			}
			if(i>0)
				i--; // because of while condition i already has the next index
			// i now has the index of the last minimum before 
			// day_index; if current quote is lower than the quote
			// of i there is a new low
			if(quotes[indices_minima[i]] > quotes[day_index])
			{
				new_extremum = true; 			
//  				printf("\n\naynr: %i %.2f > %.2f true", day_index, quotes[indices_minima[i]] , quotes[day_index]);
			}
//  			else
// 				printf("\n\naynr: %i %.2f > %.2f false", day_index, quotes[indices_minima[i]] , quotes[day_index]);
			break;
		case undefined:
		default: 
			break;
	}
	
	return new_extremum;
}

// end of file
