/* class_signal_list.h
 * declarations for class_signal_list.c, interface for "signal_list" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file class_signal_list.h
 * @brief Header file for class_signal_list.c
 *
 * This file contains the declarations for class_signal_list.c
 * @author Denis Zetzmann
 */


#ifndef CLASS_SIGNAL_LIST_H
#define CLASS_SIGNAL_LIST_H

#include "bstrlib.h"
#include "datatypes.h"
#include "class_quote.h"
#include "class_signals.h"

struct _class_signal_list;

typedef void (*destroySignalListFunc)(struct _class_signal_list*);
typedef struct _class_signal_list* (*cloneSignalListFunc)(const struct _class_signal_list*);
typedef void (*printSignalListTableFunc)(const struct _class_signal_list*);
typedef void (*addNewSignalFunc)(struct _class_signal_list*, char* datestring, unsigned int daynr, float price, float indicator_quote, char* symbol, char* identifier, signal_type type, strength_type strength, unsigned int nrOfDigits, char* signal_name, char* signal_description, char* amp_info, char* tablename);
typedef void (*addSignalFunc)(struct _class_signal_list*,struct _class_signal*);
typedef void (*removeSignalFunc)(struct _class_signal_list*, unsigned int elementToRemove);
typedef void (*saveAllSignalsFunc)(const struct _class_signal_list*);
typedef void (*loadAllSignalsFunc)(struct _class_signal_list*, char*);
typedef unsigned int (*getNrofSignalsPerDaynrFunc)(const struct _class_signal_list*, unsigned int daynr);
typedef void (*filterSignalsPerDaynrFunc)(struct _class_signal_list*, struct _class_signal_list*, unsigned int);
typedef void (*filterOutSignalsBeforeDaynrFunc)(struct _class_signal_list*, struct _class_signal_list*, unsigned int);
typedef void (*filterSignalsPerSymbolFunc)(struct _class_signal_list*, struct _class_signal_list*, bstring symbol);
typedef void (*filterSignalsPerStrengthFunc)(struct _class_signal_list*,struct _class_signal_list*, strength_type strength);
typedef void (*filterOutSignalsPerStrengthFunc)(struct _class_signal_list*, struct _class_signal_list*, strength_type strength);
typedef void (*filterSignalsPerTrendtypeFunc)(struct _class_signal_list*,struct _class_signal_list*, signal_type type);
typedef void (*filterOutMultipleSignalsADayFunc)(struct _class_signal_list*, struct _class_signal_list*);
typedef struct _class_signal_list*(*getsortedSignalListbyDaynrFunc)(struct _class_signal_list*);

struct _class_signal_list
{
	// public part
	// DATA
	bstring* name;					/**< list of this list of signals */
	class_signal** Sig;					/**< vector to members of signal class */
	unsigned int nr_signals; 			/**< nr of signals contained in object */
	
	// Methods
	destroySignalListFunc destroy;			/**< "destructor" for signal_list object, see class_signalList_destroyImpl() */	
	cloneSignalListFunc clone;			/**< returns a pointer to a clone signal list, see class_signalList_cloneImpl() */
	printSignalListTableFunc printTable; 		/**< prints all signals in list as a table, see class_signalList_printTableImpl() */
	addNewSignalFunc addNewSignal;			/**< create a new signal and add to the list, see class_signalList_addNewSignalImpl() */
	addSignalFunc addSignal;			/**< add existing signal to the list, see class_signalList_addSignalImpl() */
	removeSignalFunc removeSignal;     /**< remove a signal from signal list, see class_signalList_removeSignalImpl() */
	saveAllSignalsFunc saveToDB; 		/**< saves all signals contained in list to database, see class_signalList_saveAllSignalsImpl() */
    loadAllSignalsFunc loadFromDB;     /**< loads all signals currently in database into given struct, see class_signalList_loadAllSignalsImpl() */
	getNrofSignalsPerDaynrFunc getNrofSignalsPerDaynr; /**< returns uint with nr of signals in list on specific daynr, see class_signalList_getNrofSignalsPerDaynrImpl() */
	filterSignalsPerDaynrFunc filterSignalsPerDaynr;	/**< stores all signals within origin that occured on daynr in signal_list dest, see class_signalList_filterSignalsPerDaynrImpl() */
	filterOutSignalsBeforeDaynrFunc filterOutSignalsBeforeDaynr; 	/**< filters out all signals that occured before a specific daynr, see class_signalList_filterOutSignalsBeforeDaynrImpl() */
	filterSignalsPerSymbolFunc filterSignalsPerSymbol; /**< stores all signals within origin that are in a specific market in signal_list dest, see class_signalList_filterSignalsPerSymbolImpl() */
	filterSignalsPerStrengthFunc filterSignalsPerStrength; /**< stores all signals within origin with a specific strength (weak/neutral/strong) in signal_list dest, see class_signalList_filterSignalsPerStrengthImpl() */
	filterOutSignalsPerStrengthFunc filterOutSignalsPerStrength;  /**< filters out signals of specific strength in origin and copy the rest in signal_list dest, see class_signalList_filterOutSignalsPerStrengthImpl() */
	filterSignalsPerTrendtypeFunc filterSignalsPerTrendtype; /**< stores all signals within origin with a specific trendtype (longsignal/shortsignal) in signal_list_dest, see class_signalList_filterSignalsPerTrendtypeImpl() */
	filterOutMultipleSignalsADayFunc filterOutMultipleSignalsADay; 	/**< filters list so that each market has only 1 signal per day, see class_signalList_filterOutMultipleSignalsPerDayImpl() */
	getsortedSignalListbyDaynrFunc getSortedListByDaynr;   /**< returns a list where all signals are sorted by daynr, see class_signalList_getSortedListByDaynrImpl() */
};

typedef struct _class_signal_list class_signal_list;

class_signal_list *class_signal_list_init(char* listname);

#endif // CLASS_SIGNAL_LIST_H
