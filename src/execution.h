/* execution.h
 * declarations for execution.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file execution.h
 * @brief Header file for execution.c
 *
 * This file contains the declarations for execution.c
 * @author Denis Zetzmann
 */

#ifndef EXECUTION_H
#define EXECUTION_H

#include "bstrlib.h"
#include "class_account.h"
#include "class_market.h"
#include "class_market_list.h"
#include "class_signals.h"
#include "class_signal_list.h"
#include "class_portfolio.h"
#include "class_orderbook.h"
#include "class_stoploss_list.h"
#include "datatypes.h"

int execute_stop_loss(class_accounts *account, class_market_list* markets, class_portfolio* portfolio, class_orderbook* orderbook, unsigned int daynr, class_stoploss_list* sl_list);
int execution_manager(class_accounts* the_account, class_market_list* market_list, class_signal_list* the_signals, class_portfolio* the_portfolio, class_orderbook* orderbook);
void skip_signal_print_reason(class_signal *the_signal, bstring the_reason);
float calculate_fee(class_portfolio_element *the_element);

#endif 	// EXECUTION_H

// end of file
