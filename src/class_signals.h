/* class_signals.h
 * declarations for class_signals.c, interface for "signals" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file class_signals.h
 * @brief Header file for class_signals.c
 *
 * This file contains the declarations for class_signals.c
 * @author Denis Zetzmann
 */


#ifndef CLASS_SIGNALS_H
#define CLASS_SIGNALS_H

#include "bstrlib.h"
#include "datatypes.h"
#include "class_quote.h"

struct _class_signal; 

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroySignalsFunc)(struct _class_signal*);
typedef struct _class_signal* (*cloneSignalFunc)(const struct _class_signal*);
typedef void (*printTableFunc)(const struct _class_signal*);
typedef void (*saveSignalFunc)(const struct _class_signal*);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a signal and its describing properties
 * 
 * This struct describes all the properties that are needed to describe a 
 * signal. All values are generated within the program, most are stored in the
 * database and can retrieved from the database. The information of a signal 
 * forms the basis to the information a portfolio entry, if the signal is 
 * executed.
 */ 
struct _class_signal			
{
	// public part
	// DATA
	class_quotes* QuoteObj; 		/**< inherited vector to members of Quote class */
	bstring* name;			/**< name of the signal */
	signal_type type;		/**< long or short signal? @see signal_type */
	strength_type strength; 	/**< weak, neutral or strong signal? @see strength_type */
	bstring* description;		/**< more detailed description of the signal */
	float indicator_quote;		/**< price the indicator had, when the signal occured */
	bstring* amp_info;		/**< amplifying information, for example the trigger date */
	bstring* db_tablename;		/**< name of the mysql db table that holds information */
	bool executed; 			/**< true if signal was executed, false if not (yet) */
	// Methods, call like: foo_obj->bar_method(foo_obj)
	destroySignalsFunc destroy;	/**< "destructor", see class_signal_destroyImpl() */
	cloneSignalFunc clone;		/**< create new object with same data as given one, see class_signal_cloneImpl() */
	printTableFunc printTable;	/**< prints signal info to terminal, see class_signal_printImpl() */
	saveSignalFunc saveToDB;	/**< saves signal to database, see class_signal_saveSignalImpl() */
};

typedef struct _class_signal class_signal; 

class_signal *class_signal_init(char* datestring, unsigned int daynr, float price, float indicator_quote, char* symbol, char* identifier, signal_type type, strength_type strength, unsigned int nrOfDigits, char* signal_name, char* signal_description, char* amp_info, char* tablename);

// Abbreviations of inherited class for ease of use
#ifndef THE_SYMBOL
	#define THE_SYMBOL  QuoteObj->symbol
#endif // THE_SYMBOL
#define SIG_PRICE QuoteObj->quotevec
#define SIG_DAYNR QuoteObj->daynrvec
#define SIG_DATE  QuoteObj->datevec
#define SIG_DIGITS QuoteObj->nr_digits


#endif // CLASS_SIGNALS_H

// End of file
