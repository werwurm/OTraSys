/* class_account.c
 * Implements a "class"-like struct in C which handles accounts
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_account.c
 * @brief routines that implement a class-like struct which represents 
 * an account
 *
 * This file contains the definitions of the functions and the declaration
 * of the non-public data/functions in the account "class".
 * 
 * @author Denis Zetzmann
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "class_account.h"
#include "database.h"
#include "arrays.h"

// declarations for functions that should not be called outside
// of this file, instead as "object method"
static void class_accounts_setMethods(class_accounts* self);
static void class_accounts_destroyImpl(class_accounts* self); // "destructor" for accounts objects
static void class_accounts_setCashImpl(class_accounts *self, float newcash);
static float class_accounts_getCashImpl(const class_accounts *self);
static float class_accounts_getEquityImpl(const class_accounts* self);
static void class_accounts_setEquityImpl(class_accounts *self, float newequity);
static float class_accounts_getRiskFreeEquityImpl(const class_accounts* self);
static void class_accounts_setRiskFreeEquityImpl(class_accounts *self, float newequity);
static float class_accounts_getTotalImpl(const class_accounts *self);
static bool class_accounts_getVirginFlagImpl(const class_accounts* self);
static void class_accounts_setVirginFlagImpl(class_accounts *self, bool virginflag);
static void class_accounts_getAccountfromDB(class_accounts *self);
static void class_accounts_printAccountTable(const class_accounts* self);
static void class_accounts_updateAccountDB(class_accounts *self);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "private" parts of the accounts class.Do not touch your private parts. 
 * 
 * This struct holds the data that is considered to be private, e.g. cannot be
 * accessed outside this file (to do that use the getter/setter functions).
 * The struct is refered by an opaque pointer of "accounts" object
 * @see accounts.h definition of accounts class 
 * 
 */
///////////////////////////////////////////////////////////////////////////////
struct _accounts_private{
	float cash;			/**< account´s cash */
	float equity;			/**< account´s equity */
	float risk_free_equity;		/**< equity considering stop losses */
	bool virginflag;		/**< freshly created untouched db? */
};
typedef struct _accounts_private accounts_private;


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for accounts objects
 * 
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct.
 * 
 * @return pointer to new object of class accounts
*/
///////////////////////////////////////////////////////////////////////////////
class_accounts* class_accounts_init(bstring currency_string, unsigned int digits)
{
	//extern parameter parms; // parms are declared global in main.c
    class_accounts* self = (class_accounts*) ZCALLOC(1, sizeof(class_accounts));
	
	// reserve memory for private parts
	self->accounts_priv = (struct _accounts_private*) ZCALLOC(1, sizeof(accounts_private));
	
	// initialize public variables/methods
	self->currency = init_1d_array_bstring(1);
	*self->currency = bstrcpy(currency_string);
	self->nr_digits = digits;
    self->sum_fees = 0.0;
    self->allTimeHigh = 0.0;
	
    // set methods
    class_accounts_setMethods(self);
	
	// initialize private variables/methods
	self->accounts_priv->cash = 0;
	self->accounts_priv->equity = 0;
	self->accounts_priv->risk_free_equity = 0;
	self->accounts_priv->virginflag = true;
	return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for account objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to account struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_accounts_setMethods(class_accounts* self)
{
    self->setCash = class_accounts_setCashImpl;
	self->getCash = class_accounts_getCashImpl;
	self->getEquity = class_accounts_getEquityImpl;
	self->setEquity = class_accounts_setEquityImpl;
	self->setRiskFreeEquity = class_accounts_setRiskFreeEquityImpl;
	self->getRiskFreeEquity = class_accounts_getRiskFreeEquityImpl;
    self->getTotal = class_accounts_getTotalImpl;
	self->setVirginFlag = class_accounts_setVirginFlagImpl;
	self->getVirginFlag = class_accounts_getVirginFlagImpl;
	self->loadFromDB = class_accounts_getAccountfromDB;
	self->saveToDB = class_accounts_updateAccountDB;
    self->printTable = class_accounts_printAccountTable;
	self->destroy = class_accounts_destroyImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "destructor" function for accounts objects
 * 
 * This function frees the memory consumed by accounts objects
 * 
 * @param self pointer to accounts object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_destroyImpl(class_accounts* self)
{
	free_1d_array_bstring(self->currency, 1);
	free(self->accounts_priv);
	free(self);
    self=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief setter function for account cash
 * 
 * This function sets the cash for objects of the account class
 * 
 * @param self pointer to accounts object
 * @param newcash float value of new cash
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_setCashImpl(class_accounts *self, float newcash)
{
	self->accounts_priv->cash = newcash;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for account cash
 * 
 * This function gets the cash for objects of the account class
 * 
 * @param self pointer to accounts object
 * @return float value of new cash
*/
///////////////////////////////////////////////////////////////////////////////
static float class_accounts_getCashImpl(const class_accounts *self)
{
	return self->accounts_priv->cash;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for account equity
 * 
 * This function gets the equity for objects of the account class
 * 
 * @param self pointer to accounts object
 * @return float value of new equity
*/
///////////////////////////////////////////////////////////////////////////////
static float class_accounts_getEquityImpl(const class_accounts *self)
{
	return self->accounts_priv->equity;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief setter function for account equity
 * 
 * This function sets the equity for objects of the account class
 * 
 * @param self pointer to accounts object
 * @param newequity float value of new equity
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_setEquityImpl(class_accounts *self, float newequity)
{
	self->accounts_priv->equity = newequity;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for account risk free equity
 * 
 * This function gets the risk free equity for objects of the account class
 * 
 * @param self pointer to accounts object
 * @return float value of new risk free equity
*/
///////////////////////////////////////////////////////////////////////////////
static float class_accounts_getRiskFreeEquityImpl(const class_accounts *self)
{
	return self->accounts_priv->risk_free_equity;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief setter function for account risk free equity
 * 
 * This function sets the risk free equity for objects of the account class
 * 
 * @param self pointer to accounts object
 * @param newequity float value of new risk free equity
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_setRiskFreeEquityImpl(class_accounts *self, float newequity)
{
	self->accounts_priv->risk_free_equity = newequity;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for account total
 * 
 * This function gets cash + equity 
 * 
 * @param self pointer to accounts object
 * @return float value of total
*/
///////////////////////////////////////////////////////////////////////////////
static float class_accounts_getTotalImpl(const class_accounts *self)
{
    return (self->accounts_priv->cash + self->accounts_priv->equity);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for account virginity flag
 * 
 * This function gets the value of an accounts` objects virginity flag
 * 
 * @param self pointer to accounts object
 * @return boolean of virginity flag true: untouched, false: has been used
*/
///////////////////////////////////////////////////////////////////////////////
static bool class_accounts_getVirginFlagImpl(const class_accounts *self)
{ 
	return self->accounts_priv->virginflag;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief setter function for account virginity flag
 * 
 * This function sets the value of an accounts` objects virginity flag
 * 
 * @param self pointer to accounts object
 * @param virginity_flag bool virginity flag true: untouched, false: has been used
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_setVirginFlagImpl(class_accounts *self, bool virginity_flag)
{
	self->accounts_priv->virginflag = virginity_flag;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief wrapper for mysql database update of accounts object (pull data)
 * 
 * This function calls the according mysql_get_account_data function to get
 * all account data from the database
 * 
 * @param self pointer to accounts object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_getAccountfromDB(class_accounts *self)
{
	db_mysql_get_account_data(&self->accounts_priv->cash, 
				&self->accounts_priv->equity, 
				&self->accounts_priv->risk_free_equity, 
                &self->sum_fees,
                &self->allTimeHigh,
				&self->accounts_priv->virginflag);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief prints account info to terminal
 * 
 * prints account info to terminal
 * 
 * @param self pointer to account object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_printAccountTable(const class_accounts *self)
{
    printf("\nAccount info:\n Cash: %.*f %s\tEquity: %.*f %s\tRisk free Equity: %.*f %s\n Total: %.*f\tAll time High: %.*f\tPayed fees: %.*f %s",
           self->nr_digits,self->accounts_priv->cash, bdata(*self->currency),
           self->nr_digits,self->accounts_priv->equity, bdata(*self->currency),
           self->nr_digits,self->accounts_priv->risk_free_equity, bdata(*self->currency),
           self->nr_digits, class_accounts_getTotalImpl(self),
           self->nr_digits, self->allTimeHigh,
           self->nr_digits, self->sum_fees, bdata(*self->currency));
    printf("\n"); fflush(stdout);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief wrapper for mysql database update of accounts object (push data)
 * 
 * This function calls the according mysql_update_account function to push the
 * account data to the database
 * 
 * @param self pointer to accounts object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_accounts_updateAccountDB(class_accounts *self)
{
	db_mysql_update_account(&self->accounts_priv->cash, 
	                     &self->accounts_priv->equity, 
	                     &self->accounts_priv->risk_free_equity, 
                         &self->allTimeHigh, &self->sum_fees);
}
// end of file
