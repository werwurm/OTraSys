/* utilities.h
 * declarations for utilities.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file utilities.h
 * @brief Header file for utilities.c
 *
 * This file contains helper routines, which do not fit in other parts of
 * the software and are widely used everywhere
 * @author Denis Zetzmann
 */


#ifndef UTILITIES_H
#define UTILITIES_H

#include "bstrlib.h"

unsigned int get_nrDigitsOfBString(bstring numberstring); 
int get_nr_of_digits(float the_tick);

#endif // UTILITIES_H
