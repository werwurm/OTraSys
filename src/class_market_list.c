/* class_market_list.c
 * Implements a "class"-like struct in C which handles list of markets
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_market_list.c
 * @brief routines that implement a class-like struct which represents 
 * list of markets
 *
 * This file contains the definitions of the "class" market list methods
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "arrays.h"
#include "bstrlib.h"
#include "class_market_list.h"
#include "debug.h"
#include "readconf.h"
#include "statistics.h"

static void class_market_list_setMethods(class_market_list* self);
static void class_market_list_destroyMarketListImpl(class_market_list* self);
static class_market_list* class_market_list_cloneMarketListImpl(const class_market_list* original);
static void class_market_list_printMarketListTableImpl(const class_market_list* self); 
static unsigned int class_market_list_getNrElementsImpl(class_market_list* self);
static void class_market_list_addElementImpl(class_market_list* self, const class_market* the_element);
static void class_market_list_addNewElementImpl(class_market_list* self, char* symbol, char* identifier, const unsigned int nrOfDigits, const bool tradeable_flag, const float contract_size, const float weight, char* currency, char* markettypestr, char* csvfilename, char* comment, char* url);
static void class_market_list_removeElementImpl(class_market_list* self, unsigned int elementToRemove);
static void class_market_list_removeNonTradeableImpl(class_market_list* self);
static void class_market_list_saveAllIndicatorsDBImpl(class_market_list* self);
static int class_market_list_getMarketIndexImpl(const class_market_list* self, char* marketname);
static class_market_list* class_market_list_get_sorted_listByDaynrImpl(class_market_list* self, unsigned int index);
static void class_market_list_UpdateStatisticsImpl(class_market_list *self);
static void class_market_list_printCovarianceMatrixImpl(const class_market_list *self);
static float class_market_list_getMarketWeightImpl(const class_market_list *self, const int index);
static float class_market_list_getKellyLeverageImpl(const class_market_list *self, const int index);
static void class_market_list_saveTrendStateDBImpl(const class_market_list *self);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief statistic measures relevant for the market_list
 * 
 * This struct holds some statisticical measures that are relevant for the 
 * market_list. Those measures will be 'inherited' by the private part of a 
 * market_list object and are thus accessible directly only in this file. From 
 * outside, the setter/getter functions serve as interface for reading/writing 
 * those measures.
 */
///////////////////////////////////////////////////////////////////////////////
struct _statistics
{
	float** CovMatrixInv;	/**< Inverted Covariance Matrix of all market_list elements */
	float* KellyLeverage; /**< Kelly optimal leverages (as defined by Kelly criterion */
    unsigned int statisticsLastUpdate; /**< statistics were updated XX periods ago */	
    bool updateStatisticsNeeded; /**< flag, if statistic measures have to be updated (usually if 
									statisticsLastUpdate == parms.PORTFOLIO_RETURNS_PERIOD */
};

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "private" parts of the orderbook class.Do not touch your private parts. 
 * 
 * This struct holds the data that is considered to be private, e.g. cannot be
 * accessed outside this file (to do that use the getter/setter functions).
 * The struct is refered by an opaque pointer of "orderbook" object
 * @see orderbook_class.h definition of orderbook class 
 * 
 */
///////////////////////////////////////////////////////////////////////////////
struct _market_list_private{
	unsigned int nr_elements;	/**< nr of elements in market list */
	float* market_weights;       /**< weights of markets in list */
	struct _statistics statistics;	/**< statistical measures of portoflio object */
};
typedef struct _market_list_private market_list_private;

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for market_list objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. 
 * 
 * @param name pointer string with market list name
 
 * @return pointer to new object of class market list
*/
////////////////////////////////////////////////////////////////////////////////
class_market_list* class_market_list_init(char* name)
{
	class_market_list *self = NULL;
	
	// allocate memory for object
	self = (class_market_list*) ZCALLOC(1, sizeof(class_market_list));
	
	//bugfix: do not allocate memory for pointer to markets array (as there are none yet)
// 	self->markets = (class_market**)ZCALLOC(1,sizeof(class_market));
    self->markets = NULL;
	
	// reserve memory for private parts
	self->market_list_private = (struct _market_list_private*) ZCALLOC(1, sizeof(market_list_private));
    
    // init dynamic array for Covariance Matrix
    self->market_list_private->statistics.CovMatrixInv = NULL;
	
    // init dynamic 1xN vector for KellyLeverage
    self->market_list_private->statistics.KellyLeverage = NULL;
    
    // init dynamic 1xN vector for individual market´s weights
    self->market_list_private->market_weights = NULL;
    
	//allocate memory for rest of the object
 	self->name = init_1d_array_bstring(1);

	//init data
 	*self->name = bfromcstr(name);
	self->market_list_private->nr_elements = 0;
	
 	// set methods
    class_market_list_setMethods(self);
	
	return self;	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for market_list objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to market_list struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_market_list_setMethods(class_market_list* self)
{
  	self->destroy = class_market_list_destroyMarketListImpl;
 	self->clone = class_market_list_cloneMarketListImpl;
 	self->printTable = class_market_list_printMarketListTableImpl;
	self->getNrElements = class_market_list_getNrElementsImpl;
 	self->addElement = class_market_list_addElementImpl;
 	self->addNewElement = class_market_list_addNewElementImpl;
 	self->removeElement = class_market_list_removeElementImpl;
 	self->removeNonTradeable = class_market_list_removeNonTradeableImpl;
 	self->getElementIndex = class_market_list_getMarketIndexImpl;
    self->saveAllIndicatorsDB = class_market_list_saveAllIndicatorsDBImpl;
    self->getSortedListbyDayNr = class_market_list_get_sorted_listByDaynrImpl;
    self->updateStatistics = class_market_list_UpdateStatisticsImpl;
    self->printCovarianceMatrix = class_market_list_printCovarianceMatrixImpl;
    self->getMarketWeight = class_market_list_getMarketWeightImpl;
    self->getKellyLeverage = class_market_list_getKellyLeverageImpl;
    self->saveTrendStateDB = class_market_list_saveTrendStateDBImpl;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys an market_list object
 *
 * This function destroys an market_list struct by freeing the occupied 
 * memory
 * 
 * @param self pointer to market_list struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_market_list_destroyMarketListImpl(class_market_list* self)
{
    // destroy Covariance Matrix
    free_2d_array_float(self->market_list_private->statistics.CovMatrixInv,self->market_list_private->nr_elements);
    
    // destroy vector for kelly leverages
    free_1d_array_float(self->market_list_private->statistics.KellyLeverage);
    
    // destroy vector for market weights
    free_1d_array_float(self->market_list_private->market_weights);
    
    //destroy all included markets
	for(unsigned int i=0; i<self->market_list_private->nr_elements; i++)
	{
		self->markets[i]->destroy(self->markets[i]);
	}
	free(self->markets);
    self->markets=NULL;
 
	// free private data
	free(self->market_list_private);
    self->market_list_private=NULL;
	
	// free public data
	free_1d_array_bstring(self->name,1);
  
	free(self); 
    self=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone an market_list object
 *
 * clone complete market_list object (deep copy original into clone), if 
 * original object gets modified or destroy()ed, the clone will still live
 * @param original orderbook pointer to original 
 * @returns market_list pointer to object that will hold copy of original
 */
///////////////////////////////////////////////////////////////////////////////
static class_market_list* class_market_list_cloneMarketListImpl(const class_market_list* original)
{
	class_market_list* newclone = NULL;
	
    newclone = class_market_list_init(bdata(*original->name));
    
	for(unsigned int i=0; i<original->market_list_private->nr_elements; i++)
	{
		newclone->addElement(newclone, original->markets[i]);
	}
	
	// copy covariance matrix and Kelly leverages 1xN vector
	for(unsigned int i=0; i<original->market_list_private->nr_elements; i++)
    {
        // copy Kelly leverages and market weights
        newclone->market_list_private->statistics.KellyLeverage[i] = original->market_list_private->statistics.KellyLeverage[i];
        newclone->market_list_private->market_weights[i] = original->market_list_private->market_weights[i];
        for(unsigned int j=0; j<original->market_list_private->nr_elements; j++)
        {
            newclone->market_list_private->statistics.CovMatrixInv[i][j] = original->market_list_private->statistics.CovMatrixInv[i][j];
        }
    }
    return newclone;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints market_list info
 *
 * This function prints info of all markets in market_listk info to terminal
 * 
 * @param self pointer to market_list struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_market_list_printMarketListTableImpl(const class_market_list* self)
{
	printf("\n%s, contains %u markets", bdata(*self->name), self->market_list_private->nr_elements);
	for(unsigned int i=0; i<self->market_list_private->nr_elements; i++)
    {
        printf("\nMarket %s (identifier: %s), tradeable: %i, contract size: %.2f in %s, markettype: %s, current weight: %.2f", 
               bdata(*self->markets[i]->symbol), bdata(*self->markets[i]->identifier),
               self->markets[i]->tradeable, self->markets[i]->contract_size, 
               bdata(*self->markets[i]->currency), bdata(*self->markets[i]->markettype), self->markets[i]->weight);
    }
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds existing market object to market_list
 * 
 * Add an existing market object to the given market_list
 * 
 * @param self pointer to market object
 * @param the_element pointer to market_list object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_list_addElementImpl(class_market_list* self, const class_market* the_element)
{
    // increase nr of elements in market_list object
	self->market_list_private->nr_elements = self->market_list_private->nr_elements + 1;
    
    // realloc space for covariance matrix
    self->market_list_private->statistics.CovMatrixInv = resize_2d_array_float(
                                    self->market_list_private->statistics.CovMatrixInv, self->market_list_private->nr_elements-1,
                                    self->market_list_private->nr_elements-1, self->market_list_private->nr_elements,self->market_list_private->nr_elements);
	
    // realloc 1xN vector for kelly leverages
    self->market_list_private->statistics.KellyLeverage = resize_1d_array_float(
                                    self->market_list_private->statistics.KellyLeverage, 
                                    self->market_list_private->nr_elements);
    
    // the same for 1xN market weights
    self->market_list_private->market_weights = resize_1d_array_float(
                                    self->market_list_private->market_weights,
                                    self->market_list_private->nr_elements);
    
	// realloc indicator vector for new element object
	self->markets = (class_market**)realloc(self->markets, (self->market_list_private->nr_elements) * sizeof(class_market));
    if(self->markets == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    }

    
    
	// clone given element within portfolio object
	self->markets[self->market_list_private->nr_elements - 1] = the_element->clone(the_element);
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds new market object to market_list
 * 
 * Add a new market object to the given market_list
 * 
 * @param self pointer to market_list object
 * @param symbol char pointer with markets name
 * @param nrOfDigits uint with significant nr of digits for market
 * @param tradeable_flag boolean true/false if market shouldbe traded
 * @param contract_size float with size of 1 contract
 * @param weight weight factor of market (in a list of markets and/or portfolio
 * @param currency char ptr with market´s currency
 * @param markettypestr char ptr with type of market (CFDFUT/CFDCURR/...)
 * @param csvfilename name of the file with quote data
 * @param comment char ptr whatever you want to put here
 * @param url char ptr for urls
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_list_addNewElementImpl(class_market_list* self, char* symbol, char* identifier,
                                    const unsigned int nrOfDigits, const bool tradeable_flag,
                                    const float contract_size, const float weight, 
                                    char* currency, char* markettypestr, 
                                    char* csvfilename, char* comment, char* url)
{
	// increase nr of elements in portfolio object
	self->market_list_private->nr_elements = self->market_list_private->nr_elements + 1;

    // realloc space for covariance matrix
    self->market_list_private->statistics.CovMatrixInv = resize_2d_array_float(
                                    self->market_list_private->statistics.CovMatrixInv, self->market_list_private->nr_elements-1,
                                    self->market_list_private->nr_elements-1, self->market_list_private->nr_elements,self->market_list_private->nr_elements);
    
    // realloc 1xN vector for kelly leverages
    self->market_list_private->statistics.KellyLeverage = resize_1d_array_float(
                                    self->market_list_private->statistics.KellyLeverage, 
                                    self->market_list_private->nr_elements);
    
     // the same for 1xN market weights
    self->market_list_private->market_weights = resize_1d_array_float(
                                    self->market_list_private->market_weights,
                                    self->market_list_private->nr_elements);   
    
	// realloc markets vector for new element object
	self->markets = (class_market**)realloc(self->markets, (self->market_list_private->nr_elements) * sizeof(class_market));

    if(self->markets == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    }
    
	// create temporary order object from portfolio portfolio_entry
	class_market* tmp_market = NULL;
	tmp_market = class_market_init(symbol, identifier, nrOfDigits,tradeable_flag, contract_size, weight, 
                                   currency, markettypestr, csvfilename, comment, url);
	
	// clone given element within market_list object
	self->markets[self->market_list_private->nr_elements - 1] = tmp_market->clone(tmp_market);
	
	// destroy temporary market
	tmp_market->destroy(tmp_market);	
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief removes an existing market from market_list
 * 
 * This method removes an existing market from the list, elements
 * after the removed one move up by 1 index nr (leaving no holes)
 * 
 * @param self pointer to market_list object
 * @param elementToRemove uint with index of market to remove
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_list_removeElementImpl(class_market_list* self, unsigned int elementToRemove)
{
	// check if elementToRemove really is within array limits
	check(elementToRemove<self->market_list_private->nr_elements, "Trying to remove element with index nr %u failed, \nmarket list \"%s\" has only %u elements (starting with index 0)!", errorlabel,
	     elementToRemove,  bdata(*self->name), self->market_list_private->nr_elements);
	
	// create a temporary vector of markets
	class_market **tempElements = NULL;
	
	// alloc memory for 1 element less than original list
	tempElements = (class_market**)ZCALLOC((self->market_list_private->nr_elements-1), sizeof(class_market));
	
	// copy everything before the elementToRemove
	if(elementToRemove != 0)
	{
		for(unsigned int i=0; i<elementToRemove; i++)
			tempElements[i] = self->markets[i]->clone(self->markets[i]);
	}
	
	// copy everything after the elementToRemove
	if(elementToRemove != (self->market_list_private->nr_elements - 1))
	{
		for(unsigned int i=elementToRemove + 1; (i<self->market_list_private->nr_elements); i++)
			tempElements[i-1] = self->markets[i]->clone(self->markets[i]);
	}
	
	//destroy all originally included market objects
	for(unsigned int i=0; i<self->market_list_private->nr_elements; i++)
	{
		self->markets[i]->destroy(self->markets[i]);
	}
	free(self->markets);
	
	// redirect pointer to portfolio_elements to freshly copied list
	self->markets = tempElements;
	
	// decrease element counter
	self->market_list_private->nr_elements = self->market_list_private->nr_elements - 1;
    
    // realloc space for covariance matrix
    self->market_list_private->statistics.CovMatrixInv = resize_2d_array_float(
                                    self->market_list_private->statistics.CovMatrixInv, self->market_list_private->nr_elements,
                                    self->market_list_private->nr_elements, self->market_list_private->nr_elements,self->market_list_private->nr_elements);
    
    // realloc space for Kelly leverages 1xN vector
    self->market_list_private->statistics.KellyLeverage = resize_1d_array_float(
                                    self->market_list_private->statistics.KellyLeverage, 
                                    self->market_list_private->nr_elements);
    
    // the same for 1xN market weights
    self->market_list_private->market_weights = resize_1d_array_float(
                                    self->market_list_private->market_weights,
                                    self->market_list_private->nr_elements);
	
    // statistics now have to be recalculated, set flag
    self->market_list_private->statistics.updateStatisticsNeeded = true;
    
	return;

errorlabel:
	self->destroy(self);
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief removes all non-tradeable markets from market_list
 * 
 * This method removes all markets within market_list, that have the 
 * tradeable flag set to false. This includes markets you need for currency
 * translation, but do not want to trade them. In order to save memory
 * and time, the specific markets can be removed.
 * 
 * @NOTE: while it should be safe to call this function after class_market´s
 * assignTranslationCurrency(), make sure you really don't need those 
 * markets any more,.
 * 
 * @param self pointer to market_list object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_list_removeNonTradeableImpl(class_market_list* self)
{
	for(unsigned int i=0; i< self->market_list_private->nr_elements; i++)
		{
			if(!self->markets[i]->tradeable)
				{
					self->removeElement(self, i);
					i--;
				}
		}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief return the nr of market_list entries
 * 
 * This method returns the nr of entries within market_list object
 * 
 * @param self pointer to market_list object
 * @returns nr of elements as uint
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_market_list_getNrElementsImpl(class_market_list* self)
{
	return self->market_list_private->nr_elements;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief return index of a specific market in market_list
 * 
 * Checks if a market with the given symbol and returns it´s index. Returns -1 
 * otherwise
 *
 * @note if indicator does not exist, a warning will be sent to terminal, the 
 * program will continue
 *
 * @param self pointer to market_list object
 * @param symbol bstring ptr with market symbol
 * @returns uint with position within class_market_list->markets
*/
////////////////////////////////////////////////////////////////////////////
static int class_market_list_getMarketIndexImpl(const class_market_list* self, char* marketname)
{
	int position = -1;
	
	// loop through all elements of market_list
	for(unsigned int i = 0; i < self->market_list_private->nr_elements; i++)
	{	
		if(biseqcstr(*self->markets[i]->symbol, marketname))
        {
            position = i;
            break;
        }

	}

	if(position == -1)
    {
        log_warn("market %s does not exist in market_list %s:", marketname, bdata(*self->name));
        self->printTable(self);
    }

	return position;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves all indicators currently existing in all market objects
 * into database
 * 
 * This method is basically a wrapper around the saveAllIndicatorsDB method
 * of class_market (which in turn is again a wrapper around saveQuotes method
 * of indicator class). it takes a market_list object and saves the quote data 
 * from from all indicators within all markets into the db  * 
 * @param self pointer to market_list object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_list_saveAllIndicatorsDBImpl(class_market_list* self)
{
    for(unsigned int i=0; i<self->market_list_private->nr_elements; i++)
		self->markets[i]->saveAllIndicatorsDB(self->markets[i]);
    return;   
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief sorts markets within market_list by the lowest daynr of a specific 
 * index using quicksort algorithm.
 *
 * This function sorts the markets by included daynumbers of a given index
 * This is needed because the last x trading days are not neccessarily the same
 * dates for all symbols, due to national holidays. So we sort for the lowest 
 * daynrs and bring symbols and dates into the same order, so that execution
 * manager can loop through all days chronologically
 * 
 * @TODO: check why quicksort implementation fails, remove bubblesort
 * @see based on a quicksort implementation found at: 
 *      http://alienryderflex.com/quicksort/
 * 
 * 
 * @param self pointer to market list object
 * @param index uint with index of quoteObj->daynrs
 * @return nothing
 */
///////////////////////////////////////////////////////////////////////////////
static class_market_list* class_market_list_get_sorted_listByDaynrImpl(class_market_list* self, unsigned int index)
{
    class_market_list* list_sorted = NULL;
    
    list_sorted = self->clone(self);
    
    // shortcut for special condition nr_markets < 2
    if(list_sorted->market_list_private->nr_elements < 2)
		return list_sorted;
    
    int closePos[list_sorted->market_list_private->nr_elements];   // stores the position of the close indicator for each market
    
    for(unsigned j=0; j< list_sorted->market_list_private->nr_elements; j++)
    {
        closePos[j] = list_sorted->markets[j]->getIndicatorPos(list_sorted->markets[j], "close");
        check(closePos[j] >=0, "Indicator \"close\" does not exist in market object \"%s\"", quote_error, bdata(*list_sorted->markets[j]->symbol));
    }
	
	// bubblesort implementation
	// TODO: look again why quicksort (commented out below) didn't work
	for(unsigned int i=1; i<list_sorted->market_list_private->nr_elements; i++)
	{
		for(unsigned int j=0; j< list_sorted->market_list_private->nr_elements - i; j++)
		{
			if(list_sorted->markets[j]->Ind[closePos[j]]->DAYNRVEC[index] > list_sorted->markets[j+1]->Ind[closePos[j+1]]->DAYNRVEC[index])
				{
					class_market* tmp_market = NULL;
					tmp_market = list_sorted->markets[j]->clone(list_sorted->markets[j]);
					list_sorted->markets[j]->destroy(list_sorted->markets[j]);
					list_sorted->markets[j] = list_sorted->markets[j+1]->clone(list_sorted->markets[j+1]);
					list_sorted->markets[j+1]->destroy(list_sorted->markets[j+1]);
					list_sorted->markets[j+1] = tmp_market->clone(tmp_market);
					tmp_market->destroy(tmp_market);
				}
		}
	}
    
    //#define MAX_LEVELS 300
    //unsigned int pivot, low, high, swap;
    //int i=0;
    //unsigned int beg[MAX_LEVELS], end[MAX_LEVELS];
    
    //beg[0] = 0;
    //end[0] = list_sorted->market_list_private->nr_elements-1;
    
    //while (i >= 0)
    //{
        //low = beg[i];
        //high = end[i] - 1;
        
        //if(low < high)
        //{
            //class_market* tmp_market = NULL;
            //pivot = low;
            //tmp_market = list_sorted->markets[pivot]->clone(list_sorted->markets[pivot]);
            
            //while (low < high)
            //{
                //while ( list_sorted->markets[high]->Ind[closePos[high]]->DAYNRVEC[index] >= list_sorted->markets[pivot]->Ind[closePos[pivot]]->DAYNRVEC[index] && low < high)
                    //high--;
                //if (low < high)
                //{
                    //class_market* tmp_market2 = NULL;
                    //tmp_market2 = list_sorted->markets[high]->clone(list_sorted->markets[high]);
                    //list_sorted->markets[low]->destroy(list_sorted->markets[low]);
                    //list_sorted->markets[low] = tmp_market2->clone(tmp_market2);
                    //low = low + 1;
                    //tmp_market2->destroy(tmp_market2);
                //}
                //while (list_sorted->markets[low]->Ind[closePos[low]]->DAYNRVEC[index] <= list_sorted->markets[pivot]->Ind[closePos[pivot]]->DAYNRVEC[index] && low < high)
                    //low++;
                //if (low < high)
                //{
                    //class_market* tmp_market3 = NULL;
                    //tmp_market3 = list_sorted->markets[low]->clone(list_sorted->markets[low]);
                    //list_sorted->markets[high]->destroy(list_sorted->markets[high]);
                    //list_sorted->markets[high] = tmp_market3->clone(tmp_market3);
                    //high = high - 1;
                    //tmp_market3->destroy(tmp_market3);
                //}
           //}
           //list_sorted->markets[low]->destroy(list_sorted->markets[low]);
           //list_sorted->markets[low] = tmp_market->clone(tmp_market);
           //tmp_market->destroy(tmp_market);
           //beg[i+1] = low+1;
           //end[i+1] = end[i];
           //end[i++] = low;
           //if (end[i] - beg[i] > end[i-1] - beg[i-1])
           //{
                //swap = beg[i];
                //beg[i] = beg[i-1];
                //beg[i-1] = swap;
                //swap = end[i];
                //end[i] = end[i-1];
                //end[i-1] = swap;
           //}
        //}
        //else
            //i--;
    //}
    return list_sorted; 
    
quote_error:
    self->destroy(self);
    exit(EXIT_FAILURE);    
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates market_list´s Covariance Matrix (of returns) and Kelly
 * leverages
 *
 * This function calculates the Covariance Matrix and optimal Kelly leverages
 * of all market´s returns (all markets within given market_list). 
 * 
 * @param self pointer to market list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_list_UpdateStatisticsImpl(class_market_list *self)
{    
    extern parameter parms; // parms are declared global in main.c
	extern bool verbose_flag; // declared global in main.c
	
	if(biseqcstr(parms.PORTFOLIO_ALLOCATION, "equal"))
    {
        for(unsigned int i = 0; i<self->market_list_private->nr_elements; i++)
            self->markets[i]->weight = 1;
    }
    else if(biseqcstr(parms.PORTFOLIO_ALLOCATION, "fixed"))
        return;
	
    else if(biseqcstr(parms.PORTFOLIO_ALLOCATION, "kelly"))
    {
        unsigned int nr_quotes = parms.PORTFOLIO_RETURNS_PERIOD;
        unsigned int nr_markets = self->market_list_private->nr_elements;
	
        float** quoteMatrix = init_2d_array_float(nr_markets, nr_quotes);
        float* means = init_1d_array_float(nr_markets);
        float* variances = init_1d_array_float(nr_markets);
    
        // build a MxN matrix of returns (M: nr of markets, N number of quotes aka parms.PORTFOLIO_RETURNS_PERIOD)
        for(unsigned int market = 0; market < nr_markets; market++)
        {
            unsigned int returnsPos = self->markets[market]->IndPos.returns;
            unsigned int quote_idx = self->markets[market]->getLastUpdateIndex(self->markets[market]);  // get quote idx of last CovMat update
            if(quote_idx < parms.PORTFOLIO_RETURNS_PERIOD)     // exit loop if there is not enough data yet
                break;
            for(unsigned int quote = 0; quote < nr_quotes; quote++)
                quoteMatrix[market][quote] = self->markets[market]->Ind[returnsPos]->QUOTEVEC[quote_idx - parms.PORTFOLIO_RETURNS_PERIOD + 1 + quote];
            means[market] = self->markets[market]->getMeanReturns(self->markets[market]); 
            variances[market] = self->markets[market]->getVariance(self->markets[market]);
        }
    
        calc_covariance_matrix(nr_markets, parms.PORTFOLIO_RETURNS_PERIOD, self->market_list_private->statistics.CovMatrixInv, quoteMatrix, means, variances, parms.PORTFOLIO_RETURNS_PERIOD);
    
        free_2d_array_float(quoteMatrix, nr_markets);
        free_1d_array_float(means);
        free_1d_array_float(variances);
    
        // Original Code that did the calculation of the covariance matrix here instead of using
        // statistics.c´ calc_covariance_matrix
        // Although the old version is faster, the code was sourced out for maintainability and flexibility
        // loop through all markets
    // 	for(unsigned int i=0; i< nr_markets; i++)
    //     {
    //         unsigned int market_x_idx = self->markets[i]->getLastUpdateIndex(self->markets[i]);
    //         if(market_x_idx < parms.PORTFOLIO_RETURNS_PERIOD)
    //             break;
    //         for(unsigned int j=0; j<=i; j++)
    //         {
    //             // main diagonal entries of Covariance Matrix is variance 
    //             if(i==j)
    //             {
    //                 float variance = self->markets[i]->getVariance(self->markets[i]);
    //                 self->market_list_private->statistics.CovMatrixInv[i][j] = variance;
    //             }
    //             else
    //             {
    //                 unsigned int market_y_idx = self->markets[j]->getLastUpdateIndex(self->markets[j]);
    //                 if(market_y_idx < parms.PORTFOLIO_RETURNS_PERIOD)
    //                     break;
    //                 
    //                 int x_returnsPos = self->markets[i]->IndPos.returns;
    //                 int y_returnsPos = self->markets[j]->IndPos.returns;
    //                 float x_mean = self->markets[i]->getMeanReturns(self->markets[i]);
    //                 float y_mean = self->markets[j]->getMeanReturns(self->markets[j]);
    //                 
    //                 float cov = calc_covariance(&self->markets[i]->Ind[x_returnsPos]->QUOTEVEC[market_x_idx-parms.PORTFOLIO_RETURNS_PERIOD+1], 
    //                                        &self->markets[j]->Ind[y_returnsPos]->QUOTEVEC[market_y_idx-parms.PORTFOLIO_RETURNS_PERIOD+1],
    //                                        x_mean, y_mean, nr_quotes);
    //                 
    //                 // set cov entries within the period (matrix is symmetric)
    //                 cov = cov * parms.PORTFOLIO_RETURNS_PERIOD;
    //                 self->market_list_private->statistics.CovMatrixInv[i][j] = cov;
    //                 self->market_list_private->statistics.CovMatrixInv[j][i] = cov;
    //             }
    //         }
    //     }    

        // finally invert the Covariance Matrix, check if invertion was successfull
        bool invert_success = invert_Matrix(self->market_list_private->statistics.CovMatrixInv, nr_markets);
        
        if(!invert_success)    // print a warning if not (that may happen)
        {
            if(verbose_flag)
            {
                log_warn("Covariance matrix inversion failed!");
                printf("\n");
            }
        }

        // calculate optimal Kelly leverages as F = C^-1 * M  
        // (F kelly leverage vector, C^-1 inverted Covariance Matrix and M mean excess returns)
        float kelly_sum_pos = 0; // sum of all positive kelly leverages
        float kelly_sum_neg = 0; // sum of all negative kelly leverages
    
        for(unsigned int i=0; i<nr_markets; i++)
        {
            float kelly_leverage = 0;
    
            for(unsigned int j=0; j<nr_markets; j++)
            {
                kelly_leverage = kelly_leverage + self->market_list_private->statistics.CovMatrixInv[i][j] * self->markets[i]->getMeanExcessReturns(self->markets[i]);
            }
        
            self->market_list_private->statistics.KellyLeverage[i] = kelly_leverage; 
            
            if(kelly_leverage > 0)
                kelly_sum_pos = kelly_sum_pos + kelly_leverage;
            else if(kelly_leverage < 0)
                kelly_sum_neg = kelly_sum_neg - kelly_leverage;
            
            self->market_list_private->statistics.KellyLeverage[i] = kelly_leverage;
        }
       
        // finally calculate weights normalized Kelly leverages
        // TODO: quick n dirty solution was to hardcode resulting weights to 0, 0.25, 0.5, 0.75 and 1
        // make it somehow configurable
        for(unsigned int i=0; i<self->market_list_private->nr_elements; i++)
        {
            float market_weight = 0;
            if(self->market_list_private->statistics.KellyLeverage[i] > 0)
            {
                market_weight = self->market_list_private->statistics.KellyLeverage[i] / kelly_sum_pos;
                market_weight = market_weight * 4;
                market_weight = roundf(market_weight) / 4.0;
            }
            else if(self->market_list_private->statistics.KellyLeverage[i] < 0)
            {
                market_weight = self->market_list_private->statistics.KellyLeverage[i] / kelly_sum_neg;
                market_weight = market_weight * 4;
                market_weight = roundf(market_weight) / 4.0;
            }
            
            self->market_list_private->market_weights[i] = fabs(market_weight);
            self->markets[i]->weight = self->market_list_private->market_weights[i];
        }   
       
        if(verbose_flag)
        {
            fflush(stdout);
            log_info("Updated optimal Kelly leverages of market list \"%s\"", bdata(*self->name));
    //         self->printTable(self);
            fflush(stdout);
        }
    }

}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints out market_lists Covariance Matrix to terminal
 *
 * This function prints out market_lists Covariance Matrix to terminal
 * 
 * @param self pointer to market list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_list_printCovarianceMatrixImpl(const class_market_list *self)
{
    fflush(stdout); 
    log_info("Covariance Matrix of market list %s\n=====================================================", bdata(*self->name));
    fflush(stdout); 
    for(unsigned int i=0; i<self->market_list_private->nr_elements; i++)
        printf("%s\t\t", bdata(*self->markets[i]->symbol));
    printf("\n");
    print_2d_array_float(self->market_list_private->statistics.CovMatrixInv, self->market_list_private->nr_elements,self->market_list_private->nr_elements);
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief returns the weight of a specific market in list
 *
 * All markets in a list are weighted against each other. This method returns
 * the weight of a specific market within market_list. The return value will
 * be within [0,1]
 * 
 * @param self pointer to market list object
 * @param index int with index number of market
 * @return weight of specific market
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_list_getMarketWeightImpl(const class_market_list* self, const int index)
{
//     return self->market_list_private->market_weights[index];
    return self->markets[index]->weight;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief returns the suggested kelly leverage for specific market
 *
 * The suggested leverages according to Kelly criterion are constantly being
 * updated. This method returns the leverage for a given market
 * 
 * @param self pointer to market list object
 * @param index int with index number of market
 * @return leverage of specific market
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_list_getKellyLeverageImpl(const class_market_list *self, const int index)
{
    return self->market_list_private->statistics.KellyLeverage[index];
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief saves all market´s trend states to database
 *
 * This method saves all market´s trend states to database
 * 
 * @param self pointer to market list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_list_saveTrendStateDBImpl(const class_market_list *self)
{
    for(unsigned int i = 0; i< self->market_list_private->nr_elements; i++)
    {
        self->markets[i]->saveTrendStateDB(self->markets[i]);
    }
}

// eof
