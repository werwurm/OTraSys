/* indicators_general.h
 * declarations for indicators_general.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file indicators_general.h
 * @brief Header file for indicators_general.c
 * 
 * This file contains the declarations for indicators_general.c
 * @author Denis Zetzmann
 */

#ifndef INDICATORS_GENERAL_H
#define INDICATORS_GENERAL_H

#include <stdlib.h>

#include "bstrlib.h"
#include "debug.h"
#include "class_indicators.h"
#include "class_market.h"

// macro to check if 2 indicators have same nr. of quotes/dates/daynrs
#define CHECK_EQUAL_LENGTH(k,l) {																\
	if(k->NR_ELEMENTS != l->NR_ELEMENTS) 															\
	{																			\
		log_err(" indicators have different nr of quotes (%s: %i; %s: %i)!",bdata(*k->name), k->NR_ELEMENTS, bdata(*l->name), l->NR_ELEMENTS);	\
		exit(0);																	\
	}																			\
}

int cmpfunc (const void * a, const void * b);
void highest_high(unsigned int period, const class_indicators* highs, class_indicators* highest_highs);
void lowest_low(unsigned int period, const class_indicators* lows, class_indicators* lowest_lows);
void calc_true_range(const class_indicators* highs, const class_indicators* lows, const class_indicators* closes, class_indicators* true_range);
void calc_average_true_range(const unsigned int period, const class_indicators *true_range, class_indicators *average_true_range);
int calc_directional_movement_index(const unsigned int period, const class_indicators* highs, const class_indicators* lows, const class_indicators* tr, class_indicators* dmi, class_indicators* adx);
int exponential_moving_average(const unsigned int period, const class_indicators* input, class_indicators* output);
int ADX_regime_filter(const class_indicators *adx, const float adx_threshhold, class_indicators *regime_filter);
trend_type indicator_trend(const unsigned int daynr, const class_indicators *theIndicator);
int update_indicators(class_market* the_market);
#endif // INDICATORS_GENERAL_H
