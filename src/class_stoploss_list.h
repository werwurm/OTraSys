/* class_stoploss_list.h
 * declarations for class_stoploss_list.c, interface for 
 * "stoploss_list" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file class_stoploss_list.h
 * @brief Header file for class_stoploss_list.c
 *
 * This file contains the declarations for class_stoploss_list.c
 * @author Denis Zetzmann
 */

#ifndef CLASS_STOPLOSS_LIST_H
#define CLASS_STOPLOSS_LIST_H

#include "bstrlib.h"
#include "datatypes.h"
#include "class_stoploss_record.h"

struct _class_stoploss_list;

typedef void (*destroyStopLossListFunc)(struct _class_stoploss_list*);
typedef struct _class_stoploss_list* (*cloneStopLossListFunc)(const struct _class_stoploss_list*);
typedef void (*printStopLossListTableFunc)(const struct _class_stoploss_list*);
typedef void (*addNewSLFunc)(struct _class_stoploss_list*, char* quote_symbol, char* identifier, unsigned int nr_records, unsigned int nrOfDigits, char* buydate, char* stoploss_type);
typedef void (*addSLFunc)(struct _class_stoploss_list*, const struct _class_sl_record*);
typedef void (*saveAllSLFunc)(const struct _class_stoploss_list*);

typedef void (*removeSLFunc)(struct _class_signal_list*, unsigned int elementToRemove);


struct _class_stoploss_list
{
	// public part
	// DATA
    bstring* name;					/**< name of this list of stop losses */
	class_sl_record** SL;					/**< vector to members of stoploss_record class */
	unsigned int nr_records; 			/**< nr of SL contained in object */
	
	// Methods
	destroyStopLossListFunc destroy;			/**< "destructor" for stoploss_list object, see class_stoploss_list_destroyImpl() */	
	printStopLossListTableFunc printTable; 		/**< prints info about list as a table, see class_stoploss_list_PrintTableImpl() */
	addNewSLFunc addNewSL;			/**< create a new stoploss record and add to the list, see  class_stoploss_list_addNewSLImpl() */
	addSLFunc addSL;			/**< add existing stoploss to the list, see class_stoploss_list_addSLImpl() */
	saveAllSLFunc saveToDB; 		/**< saves all signals contained in list to database, see class_stoploss_list_saveToDBImpl() */
};

typedef struct _class_stoploss_list class_stoploss_list;

class_stoploss_list *class_stoploss_list_init(char* listname);

#endif // CLASS_STOPLOSS_LIST_H 
