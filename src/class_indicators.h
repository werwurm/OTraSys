/* class_indicators.h
 * declarations for class_indicators.c, interface for "indicators" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_indicators.h
 * @brief Header file for class_indicators.c, public member declarations
 *
 * This file contains the "public" available data and functions of the 
 * indicators "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_INDICATORS_H
#define CLASS_INDICATORS_H

#include <stdbool.h>

#include "class_quote.h"      // indicators inherit from quotes

typedef struct _class_indicators class_indicators;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyIndicatorFunc)(struct _class_indicators*);
typedef struct _class_indicators*(*cloneIndicatorFunc)(const struct _class_indicators*);
typedef void (*printIndicatorTableFunc)(const struct _class_indicators*);
typedef void (*loadIndicatorQuotesFunc)(struct _class_indicators*);
typedef void (*saveIndicatorQuotesFunc)(const struct _class_indicators*);
				
struct _class_indicators
{
    //public part
    // DATA
    class_quotes* QuoteObj; /**< inherited members of quotes-"class" */
    bstring* name;          /**< name of the indicator*/
    bstring* description;   /**< more detailed description of the indicator */
    bool db_saveflag;	    /**< should indicator be saved to database? */
    bstring* db_tablename;  /**< name of the mysql db table that holds information */
    
    // METHODS, call like: foo_obj->bar_method(foo_obj)
    destroyIndicatorFunc destroy; /**< "destructor", see class_indicators_destroyImpl() */
    cloneIndicatorFunc	clone;	/**< create new object with same data as given one, see class_indicators_cloneImpl() */
    printIndicatorTableFunc printTable;	/**<  prints out quotes in tabular format, see  class_indicators_printTableImpl() */
    loadIndicatorQuotesFunc loadFromDB; /**< load quotes from db, see class_indicators_loadQuotesImpl() */
    saveIndicatorQuotesFunc saveToDB; /**< save quotes to db, see class_indicators_saveQuotesImpl() */
    //private part
};

class_indicators* class_indicators_init(char* quote_symbol, char* identifier, unsigned int nr_quotes, unsigned int nrOfDigits, char* indicator_name, char* ind_descr, char* tablename);     // "constructor" for indicator "objects"

// Abbreviations of inherited class for ease of use
#define QUOTEVEC QuoteObj->quotevec
#define DAYNRVEC QuoteObj->daynrvec
#define DATEVEC  QuoteObj->datevec
#define NR_ELEMENTS  QuoteObj->nr_elements
#define THE_SYMBOL  QuoteObj->symbol
#define NR_DIGITS  QuoteObj->nr_digits

#endif // CLASS_INDICATORS_H
// eof
