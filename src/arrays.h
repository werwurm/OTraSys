/* arrays.h
 * declarations for arrays.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file arrays.h
 * @brief Header file for arrays.c
 *
 * This file contains the declarations for arrays.c

 * @author Denis Zetzmann
 */

#ifndef ARRAYS_H
#define ARRAYS_H

#include "bstrlib.h"

// macro that wraps around zcalloc function and passes file name/line number
// to it ==> in event of failure this information makes it easier
#define ZCALLOC(N, theSize) zcalloc(__FILE__, __LINE__, N, theSize)
void *zcalloc(const char *file, int line, int N, int size);

float **init_2d_array_float(unsigned int N, unsigned int M);
unsigned int **init_2d_array_uint(unsigned int N, unsigned int M);
bstring **init_2d_array_bstring(unsigned int N, unsigned int M);
float *init_1d_array_float(unsigned int N);
unsigned int *init_1d_array_uint(unsigned int N);
char **init_1d_array_string(unsigned int N);
bstring *init_1d_array_bstring(unsigned int N);

float **resize_2d_array_float(float **array, unsigned int oldN, unsigned int oldM, unsigned int newN, unsigned int newM);
float *resize_1d_array_float(float *array, int newsize);
bstring *resize_1d_array_bstring(bstring *array, int newsize);
unsigned int *resize_1d_array_uint(unsigned int *array, int newsize);

void print_2d_array_float(float **array, unsigned int N, unsigned int M);
void print_1d_array_char(char **array, unsigned int N);
void print_quote_table(bstring *dates,bstring symbol, float **quotes, unsigned int rows);
void print_indicator_table(bstring *dates, bstring symbol, float *indicator, \
                        	unsigned int rows);

void free_2d_array_float(float **array, unsigned int N);
void free_2d_array_uint(unsigned int **array, unsigned int N);
void free_2d_array_bstring(bstring **array, unsigned int N, unsigned int M);
void free_1d_array_float(float *array);
void free_1d_array_uint(unsigned int *array);
void free_1d_array_char(char **array, unsigned int N);
void free_1d_array_bstring(bstring *array, unsigned int N);

#endif //ARRAYS_H

/* End of file */
