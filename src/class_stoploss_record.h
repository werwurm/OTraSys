/* class_stoploss_record.h
 * declarations for class_stoploss_record.c, interface for 
 * "stoploss_record" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file class_stoploss_record.h
 * @brief Header file for class_stoploss_record.c
 *
 * This file contains the declarations for class_stoploss_recod.c
 * @author Denis Zetzmann
 */


#ifndef CLASS_SL_RECORD_H
#define CLASS_SL_RECORD_H 

#include "bstrlib.h"
#include "class_quote.h"

struct _class_sl_record; 

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroySL_recordFunc)(struct _class_sl_record*);
typedef struct _class_sl_record* (*cloneSL_RecordFunc)(const struct _class_sl_record*);
typedef void (*printSL_RecordTableFunc)(const struct _class_sl_record*);
typedef void (*save_sl_record)(const struct _class_sl_record*);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a stop loss record and its describing properties
 * 
 * This struct describes all the properties that are needed to describe a 
 * stop loss record. All values are generated within the program, stored 
 * in the database and retrieved from the database. The information of a sl 
 * record forms the basis to the stop loss record list, as defined in 
 * @see class_stoploss_list.h
 */ 
struct _class_sl_record			
{
	// public part
	// DATA
	class_quotes* QuoteObj; 		/**< inherited vector to members of Quote class */
	bstring* buydate;       /**< the date this entry was bought */
	bstring* selldate;      /**< the date this entry was sold (if transaction already complete) */
	bstring* stoploss_type;       /**< as defined in configuration/ parms.SL_TYPE */
	
	// Methods, call like: foo_obj->bar_method(foo_obj)
	destroySL_recordFunc destroy;	/**< "destructor", see class_sl_record_destroyImpl() */
	printSL_RecordTableFunc printTable;	    /**< prints signal info to terminal, see class_sl_record_printTableImpl() */
	save_sl_record saveToDB;       /**< save this stop loss record to database, see class_sl_record_saveToDBImpl()  */
};

typedef struct _class_sl_record class_sl_record; 

class_sl_record *class_sl_record_init(char* quote_symbol, char* identifier, unsigned int nr_records, unsigned int nrOfDigits, char* buydate, char* stoploss_type);

#endif // CLASS_SL_RECORD_H

// End of file
