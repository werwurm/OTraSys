/* database.h
 * declarations for database.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file database.h
 * @brief Header file for database.c
 * 
 * This file contains the declarations for database.c
 *
 * @see for the documentation of the database structure, see 
 * /docs/databse_structure.md
 * @author Denis Zetzmann
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <stdbool.h>
#include </usr/include/mysql/mysql.h>

#include "bstrlib.h"
#include "class_quote.h"
#include "class_portfolio_element.h"
#include "class_signal_list.h"
#include "class_portfolio.h"
#include "class_orderbook.h"
#include "class_market_list.h"

// wrap a check to prevent ATOF/ATOI failing because of NULL argument
#define CHECK_ATOF_NULL(k,l){ 		\
		if(l != NULL)		\
			k = atof(l);	\
		else k = 0.0; }
#define CHECK_ATOI_NULL(k,l){		\
		if(l != NULL)		\
			k = atoi(l);	\
		else k = 0; }

void check_mysql_error(bstring functionname, bstring querystring);
void connect_mysql_database(MYSQL *db_handle, char *host, char *username, char *password,\
	char *dbname, int port, char *socket, int flags);
void mysql_create_db(char *host, char *username, char *password,\
	char *dbname, int port, char *socket, int flags);
void db_close_mysql_connection(MYSQL *handle);

void db_mysql_getquotes(bstring symbol, bstring identifier, bstring db_table, bstring quotetype, int nr_quotes, float *quotes, \
		bstring *dates, unsigned int *daynrs);
unsigned int db_mysql_getNrOfQuotes(bstring symbol, bstring identifier,  bstring quotetype, bstring db_table);

void mysql_updatequotes(bstring symbol, bstring filename);

void db_mysql_update_indicator(bstring symbol, bstring identifier, int digits, bstring indicatorname, bstring db_table, unsigned int nr_quotes, float* quotes, bstring* dates, unsigned int* daynrs);

void db_mysql_update_signal(bstring symbol, bstring identifier, bstring db_table, bstring datestring, float price, 
			     float indicator_quote, signal_type type, strength_type strength, unsigned int nrOfDigits,
			     bstring signal_name, bstring signal_description, bstring amp_info, bool executed);
	
int db_mysql_get_signals(class_signal_list *the_list, bstring* table_name);
int db_mysql_get_signals_subset(class_signal_list *the_list, bstring* table_name, bstring* searchstring);

int db_mysql_get_portfolio(class_portfolio* the_portfolio);
int db_mysql_update_portfolio(class_portfolio_element* the_portfolio_element);
int db_mysql_clear_portfolio_table();

int db_mysql_update_orderbook(class_orderbook* orderbook);
int db_mysql_get_orderbook(class_orderbook* orderbook, class_market_list* the_markets);

int db_mysql_get_nr_trades(bstring winlose, signal_type type);
float db_mysql_get_PL_sum(bstring winlose, signal_type type);
int db_mysql_get_PL(bstring PL_type, bstring *symbols, float *PL, bstring *dates);
int db_mysql_get_hold_days_ordered(unsigned int *hold_days);

int db_mysql_remove_null_rows(void);

int db_mysql_get_account_data(float *cash, float *equity, float *risk_free_equity, float *sum_fees, float *high, bool *virgin_flag);
int db_mysql_update_account(float *cash, float *equity, float *risk_free_equity, float *fees, float *high);

int db_mysql_update_sl_record(class_quotes *the_quotes, bstring *buydate, bstring *selldate, bstring *sl_type);

int db_mysql_update_market_trend(class_market *the_market);
int db_mysql_get_market_trend(class_market *the_market, unsigned int index);

#endif 	// DATABASE_H
/* End of file */
