/* class_market.c
 * Implements a "class"-like struct in C which handles markets
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_market.c
 * @brief routines that implement a class-like struct which represents 
 * markets
 *
 * This file contains the definitions of the market "class methods"
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "arrays.h"
#include "class_market.h"
#include "constants.h"
#include "database.h"
#include "debug.h"
#include "readconf.h"
#include "statistics.h"

// declarations for functions that should not be called outside
// of this file, instead as "object method"
static void class_market_destroyImpl(class_market *self);
static class_market* class_market_cloneMarketImpl(const class_market *self);
static void class_market_printTableImpl(const class_market *self);
static void class_market_printTrendStatusImpl(const class_market *self);
static void class_market_printTrendStatusbyIdxImpl(const class_market *self, unsigned int idx);
static void class_market_printTranslCurTableImpl(const class_market* self);
static unsigned int class_market_getNrIndicatorsImpl(const class_market* self);
static void class_market_addNewIndicatorImpl(class_market* self, char* indicator_name, unsigned int nr_quotes, char* ind_descr, char* tablename);
static void class_market_addIndicatorImpl(class_market* self, class_indicators* the_indicator);
static bool class_market_IndicatorExistsImpl(const class_market* self, char* indicator_name);
static void class_market_loadIndicatorDBImpl(class_market* self, char* indicator_name);
static void class_market_saveAllIndicatorsDBImpl(const class_market* self);
static int class_market_getIndicatorPosImpl(const class_market *self, char* indicator_name);
static void class_market_copyIndicatorQuotesImpl(class_market *self, char* SrcIndicatorName, char* DestIndicatorName);
static void class_market_ERRORcopyIndicatorQuotesImpl(class_market *self, char* src, char* dest);
static void class_market_assignTranslationCurrencyImpl(class_market** the_markets, unsigned int nr_markets, class_market* self);
static float class_market_getTranslationCurrencyQuotebyIndexImpl(const class_market* self, unsigned int index);
static unsigned int class_market_getTranslationCurrencyDaynrbyIndexImpl(const class_market *self, unsigned int index);
static bstring* class_market_getTranslationCurrencyDatebyIndexImpl(const class_market* self, unsigned int index);
static void class_market_initIndicatorPositionsImpl(class_market* self);

static float class_market_getMeanReturnsImpl(const class_market* self);
static float class_market_getMeanExcessReturnsImpl(const class_market* self);
static float class_market_getVarianceImpl(const class_market* self);
static unsigned int class_market_getLastUpdateIndexImpl(const class_market* self);

static bool class_market_updateMarketStatisticsImpl(class_market* self, unsigned int quote_idx);

static trend_type class_market_getTrendTypeByIndexImpl(class_market* self, unsigned int idx);
static void class_market_setTrendTypeByIndexImpl(class_market* self, trend_type currTrend, unsigned int idx);
static unsigned int class_market_getTrendDurationByIndexImpl(class_market* self, unsigned int idx);
static void class_market_setTrendDurationByIndexImpl(class_market* self, unsigned int duration, unsigned int idx);
static void class_market_setTrendPriceByIndexImpl(class_market* self, unsigned int idx, unsigned int quote_idx);
static bstring* class_market_getTrendDateByIndexImpl(class_market* self, unsigned int idx);
static unsigned int class_market_getTrendDaynrByIndexImpl(class_market *self, unsigned int idx);
static float class_market_getTrendPricebyIndexImpl(class_market *self, unsigned int idx);
static void class_market_loadTrendStateDBbyIndexImpl(class_market *self, unsigned int idx);
static void class_market_saveTrendStateDBImpl(class_market *self);

// following functions won`t be accessible outside this file and are not 
// set as "object method"
static void class_market_setMethods(class_market* self);
static bstring* class_market_determineTranslationCurrencyString(class_market* self, bstring account_currency);
static int class_market_checkTranslationCurrency(class_market** the_markets, unsigned int nr_markets, bstring account_currency, class_market* self, bool* inverted_flag);


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief statistic measures relevant for a market
 * 
 * This struct holds some statisticical measures that are relevant for the 
 * market. Those measures will be 'inherited' by the private part of a market
 * object and are thus accessible directly only in this file. From outside the
 * setter/getter functions serve as interface for reading/writing those 
 * measures.
 */
///////////////////////////////////////////////////////////////////////////////
struct _statistics
{
    float periodMeanReturns;    /**< mean returns in last parms.PORTFOLIO_RETURNS_PERIOD periods */
    float periodMeanExcessReturns;  /**< mean excess returns (compared to risk free rate */
    float periodVariance; 		/**< variance in last parms.PORTFOLIO_RETURNS_PERIOD periods */
    unsigned int statisticsLastUpdate; /**< statistics were updated XX periods ago */	
    unsigned int lastUpdate_idx; /**< index of QuoteObj, when last update took place */
    bool updateStatisticsNeeded; /**< flag, if statistic measures have to be updated (usually if 
									statisticsLastUpdate == parms.PORTFOLIO_RETURNS_PERIOD */
};


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief trend information for a market
 * 
 * This struct holds information aber this market´s currend trend. Those 
 * measures will be 'inherited' by the private part of a market
 * object and are thus accessible directly only in this file. From outside the
 * setter/getter functions serve as interface for reading/writing those 
 * measures.
 */
///////////////////////////////////////////////////////////////////////////////
struct _trend_stats
{
    unsigned int quote_idx;         /**< index for last quote (refering to close indicator as part of every market object) */
    trend_type currTrendType;       /**< current trend for market */
    unsigned int trendDuration;     /**< duration of current trend */
};

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "private" parts of the markets class.Do not touch.
 * 
 * This struct holds the data that is considered to be private, e.g. cannot be
 * accessed outside this file (to do that use the getter/setter functions).
 * The struct is refered by an opaque pointer of "market" object
 * @see market.h definition of market class 
 * 
 */
///////////////////////////////////////////////////////////////////////////////
struct _market_private
{
	unsigned int nr_indicators;	/**< number of indicators that are part of current market object */
	class_quotes* translation_currency; 	/**< inherited object with translation currency */
	struct _statistics statistics;			/**< statistical measures of market object */
	struct _trend_stats* trend_stats;        /**< trend information for market */
};
typedef struct _market_private market_private;


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for market objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods.
 * 
 * @param symbol char pointer with markets name
 * @param identifier char ptr to identifier (like ISIN) of market
 * @param nrOfDigits uint with significant nr of digits for market
 * @param tradeable_flag boolean true/false if market shouldbe traded
 * @param contract_size float with size of 1 contract
 * @param weight float weight factor of market (in a list of markets and/or portfolio
 * @param currency char ptr with market´s currency
 * @param markettypestr char ptr with type of market (CFDFUT/CFDCURR/...)
 * @param csvfilename name of the file with quote data
 * @param comment char ptr whatever you want to put here
 * @param url char ptr for urls
 * @return pointer to new object of class market
*/
///////////////////////////////////////////////////////////////////////////////
class_market* class_market_init(char* symbol, char* identifier, unsigned int nrOfDigits,  bool tradeable_flag,
		    float contract_size, float weight, char* currency, char* markettypestr, 
		    char* csvfilename, char* comment, char* url)
{
	extern parameter parms; // parms are declared global in main.c

	class_market *self = NULL;
	
	// allocate memory for object
	self = ZCALLOC(1, sizeof(class_market));
	
	// Bugfix: do not allocate memory for included indicator objects- there are none yet
// 	self->Ind = (class_indicators**)ZCALLOC(1,sizeof(class_indicators));
    self->Ind = NULL;
	
	// reserve memory for private parts
	self->market_private = (struct _market_private*) ZCALLOC(1, sizeof(market_private));
    self->market_private->trend_stats = (struct _trend_stats *) ZCALLOC(1, parms.ICHI_DAYS_TO_ANALYZE * sizeof(struct _trend_stats));
    
	//allocate memory for rest of the object
	self->symbol = init_1d_array_bstring(1);
    self->identifier = init_1d_array_bstring(1);
	self->currency = init_1d_array_bstring(1);
	self->markettype = init_1d_array_bstring(1);
	self->csvfile = init_1d_array_bstring(1);
	self->comment = init_1d_array_bstring(1);
	self->url =init_1d_array_bstring(1);
	
	// init data   
 	*self->symbol = bfromcstr(symbol);
    *self->identifier = bfromcstr(identifier);
	self->tradeable = tradeable_flag;
	self->nr_digits = nrOfDigits;
	self->contract_size = contract_size;
    self->weight = weight;
	*self->currency = bfromcstr(currency);
 	*self->markettype = bfromcstr(markettypestr);
	*self->csvfile = bfromcstr(csvfilename);
 	*self->comment = bfromcstr(comment);
 	*self->url=bfromcstr(url);
    
	self->market_private->nr_indicators = 0;
    self->market_private->statistics.periodMeanReturns = 0.0;
    self->market_private->statistics.periodVariance = 0.0;
    self->market_private->statistics.statisticsLastUpdate = 0;
    self->market_private->statistics.lastUpdate_idx = 0; 
    self->market_private->statistics.updateStatisticsNeeded = true;
    
	self->IndPos.open = -1;		// after init there are no indicators yet
	self->IndPos.close = -1;
	self->IndPos.high = -1;
	self->IndPos.low = -1;
    self->IndPos.returns = -1;
	
    for(unsigned int i= 0; i<parms.ICHI_DAYS_TO_ANALYZE; i++)
    {
        self->market_private->trend_stats[i].quote_idx = parms.INDI_DAYS_TO_UPDATE - parms.ICHI_DAYS_TO_ANALYZE + i; // the index position can be calculated, even if the indicator "close" is not yet loaded
        self->market_private->trend_stats[i].currTrendType = sideways;
        self->market_private->trend_stats[i].trendDuration = 0;
    }
    
	// set methods
    class_market_setMethods(self);

	// create quote obj for translation currency
	bstring* transl_currency;
	transl_currency = class_market_determineTranslationCurrencyString(self, parms.ACCOUNT_CURRENCY);
	self->market_private->translation_currency = class_quotes_init(bdata(*transl_currency), bdata(*transl_currency), parms.INDI_DAYS_TO_UPDATE, 10, "translation currency", "quotes_daily");  // arbitrarily set nr_digits=10, transl_currency needs much more accuracy than underlying market
	free_1d_array_bstring(transl_currency,1);
	return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for market objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to market struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_market_setMethods(class_market* self)
{
	self->destroy = class_market_destroyImpl;
	self->clone = class_market_cloneMarketImpl;
	self->printTable = class_market_printTableImpl;
    self->printMarketTrend = class_market_printTrendStatusImpl;
    self->printMarketTrendByIdx = class_market_printTrendStatusbyIdxImpl;
    self->printTranslCurrTable = class_market_printTranslCurTableImpl;
	self->getNrIndicators = class_market_getNrIndicatorsImpl;    
	self->addNewIndicator = class_market_addNewIndicatorImpl;
	self->addIndicator = class_market_addIndicatorImpl;
	self->IndicatorExists = class_market_IndicatorExistsImpl;
	self->loadIndicatorDB = class_market_loadIndicatorDBImpl;
	self->saveAllIndicatorsDB = class_market_saveAllIndicatorsDBImpl;
	self->getIndicatorPos = class_market_getIndicatorPosImpl;
	self->copyIndicatorQuotes = class_market_ERRORcopyIndicatorQuotesImpl;	// init with dummy because at init no indicator is inside this object. the method will be set later by addNewIndicator, when at least 2 indicators are present
	self->assignTranslationCurrency = class_market_assignTranslationCurrencyImpl;
	self->getTranslationCurrencyQuotebyIndex = class_market_getTranslationCurrencyQuotebyIndexImpl;
	self->getTranslationCurrencyDaynrbyIndex = class_market_getTranslationCurrencyDaynrbyIndexImpl;
	self->getTranslationCurrencyDatebyIndex = class_market_getTranslationCurrencyDatebyIndexImpl;    
	self->initIndicatorPositions = class_market_initIndicatorPositionsImpl;
    
	self->getMeanReturns = class_market_getMeanReturnsImpl;
    self->getMeanExcessReturns = class_market_getMeanExcessReturnsImpl;
	self->getVariance = class_market_getVarianceImpl;
    self->getLastUpdateIndex = class_market_getLastUpdateIndexImpl;
	self->updateStatistics = class_market_updateMarketStatisticsImpl;
    
    self->getTrendTypeByIndex = class_market_getTrendTypeByIndexImpl;
    self->setTrendTypeByIndex = class_market_setTrendTypeByIndexImpl;
    self->setTrendDurationByIndex = class_market_setTrendDurationByIndexImpl;
    self->getTrendDurationByIndex = class_market_getTrendDurationByIndexImpl;
    self->setTrendPriceByIndex = class_market_setTrendPriceByIndexImpl;
    self->getTrendDateByIndex = class_market_getTrendDateByIndexImpl;
    self->getTrendDaynrByIndex = class_market_getTrendDaynrByIndexImpl;
    self->getTrendPriceByIndex = class_market_getTrendPricebyIndexImpl;
    self->loadTrendStateDBbyIndex = class_market_loadTrendStateDBbyIndexImpl;
    self->saveTrendStateDB = class_market_saveTrendStateDBImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "destructor" function for market objects
 * 
 * This function frees the memory consumed by market objects
 * 
 * @param self pointer to market object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_destroyImpl(class_market *self)
{
	//destroy all included indicators
	for(unsigned int i=0; i<self->market_private->nr_indicators; i++)
	{
		self->Ind[i]->destroy(self->Ind[i]);
	}
	free(self->Ind);
    self->Ind=NULL;
	
	// free private data
    free(self->market_private->trend_stats);
    self->market_private->trend_stats=NULL;
    
	self->market_private->translation_currency->destroy(self->market_private->translation_currency);
	free(self->market_private);
    self->market_private=NULL;
    
	// free public data
	free_1d_array_bstring(self->symbol,1);
    free_1d_array_bstring(self->identifier, 1);
	free_1d_array_bstring(self->currency, 1);
	free_1d_array_bstring(self->markettype,1);
	free_1d_array_bstring(self->csvfile,1);
	free_1d_array_bstring(self->comment,1);
	free_1d_array_bstring(self->url,1);

	free(self);
    self=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief clone market object (deep copy original into clone)
 * 
 * clone market object (deep copy original into clone), if original object gets
 * modified or destroy()ed, the clone will still live
 * 
 * @param self pointer to market object with original data
 * @returns pointer to cloned object
*/
////////////////////////////////////////////////////////////////////////////
class_market* class_market_cloneMarketImpl(const class_market* self)
{
    extern parameter parms; // parms are declared global in main.c
    
	class_market* clone = NULL;
	// allocate memory and fill with basic data
	clone = class_market_init(bdata(*self->symbol), bdata(*self->identifier), self->nr_digits, self->tradeable, self->contract_size, self->weight, bdata(*self->currency), bdata(*self->markettype), bdata(*self->csvfile), bdata(*self->comment), bdata(*self->url));
	
	// copy indicators too
 	for(unsigned int i = 0; i < self->getNrIndicators(self); i++)
 	{
		// create a temp indicator
		class_indicators* tmp_indi=NULL;
		// clone to i-th indicator to temp indicator
		tmp_indi = self->Ind[i]->clone(self->Ind[i]);
		// and add that to clone of market object
 		clone->addIndicator(clone, tmp_indi);
		// finally destroy the temp indicator
		tmp_indi->destroy(tmp_indi);
 	}
 	
 	// Uuugly bug, hereby fixed: when calling class_market_init() above, an QuoteObj already is created for 
 	// translation currency. So this has to be destroy()ed first, before the translation currency of self
 	// can be cloned below
 	clone->market_private->translation_currency->destroy(clone->market_private->translation_currency);
    
  	clone->market_private->translation_currency = self->market_private->translation_currency->clone(self->market_private->translation_currency);
    clone->market_private->statistics.periodMeanReturns = self->market_private->statistics.periodMeanReturns;
    clone->market_private->statistics.periodMeanExcessReturns = self->market_private->statistics.periodMeanExcessReturns;
    clone->market_private->statistics.periodVariance = self->market_private->statistics.periodVariance;
    clone->market_private->statistics.statisticsLastUpdate = self->market_private->statistics.statisticsLastUpdate;
    clone->market_private->statistics.lastUpdate_idx = self->market_private->statistics.lastUpdate_idx;
    clone->market_private->statistics.updateStatisticsNeeded = self->market_private->statistics.updateStatisticsNeeded;
 	
 	clone->IndPos.open = self->IndPos.open;
 	clone->IndPos.close = self->IndPos.close;
 	clone->IndPos.high = self->IndPos.high;
 	clone->IndPos.low = self->IndPos.low;
    clone->IndPos.returns = self->IndPos.returns;
 	
    for(unsigned int i=0; i<parms.ICHI_DAYS_TO_ANALYZE; i++)
    {
        clone->market_private->trend_stats[i].quote_idx = self->market_private->trend_stats[i].quote_idx;
        clone->market_private->trend_stats[i].trendDuration = self->market_private->trend_stats[i].trendDuration;
        clone->market_private->trend_stats[i].currTrendType = self->market_private->trend_stats[i].currTrendType;
    }
    
 	return clone;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief print market information to terminal
 * 
 * prints summary of given market to terminal, including nr and
 * names of indicators
 * 
 * @param self pointer to market object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_printTableImpl(const class_market* self)
{
	printf("\nMarket %s (%s), tradeable: %i, contract size: %.2f in %s (translating currency pair: %s), markettype: %s", 
                    bdata(*self->symbol), bdata(*self->identifier), self->tradeable, self->contract_size, 
                    bdata(*self->currency), bdata(*self->market_private->translation_currency->symbol), bdata(*self->markettype));
	printf("\ndata by %s, nr. of digits: %i, comment: %s, url: %s", bdata(*self->csvfile), self->nr_digits, bdata(*self->comment), bdata(*self->comment));
	printf("\ncurrent weight: %.2f, mean returns in period: %.8f, mean excs ret. %.8f, variance in period: %.8f", self->weight, self->market_private->statistics.periodMeanReturns, self->market_private->statistics.periodMeanExcessReturns, self->market_private->statistics.periodVariance);
	unsigned int nr_indicators = class_market_getNrIndicatorsImpl(self);
	
	printf("\nNr. of indicators: %u (", nr_indicators);
	
	if(nr_indicators > 0)
	{
		for(unsigned int i=0; i< nr_indicators-1; i++)
			printf("%s, ",bdata(*self->Ind[i]->name));
		printf("%s)\n", bdata(*self->Ind[nr_indicators-1]->name));
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief print trend info of market to terminal
 * 
 * print trend info of market to terminal
 * 
 * @param self pointer to market object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_printTrendStatusImpl(const class_market *self)
{
    extern parameter parms; // parms are declared global in main.c    
    
    for(unsigned int i=0; i < parms.ICHI_DAYS_TO_ANALYZE; i++)
    {
        bstring trendtypestr = NULL;
        switch(self->market_private->trend_stats[i].currTrendType)
        {
            case sideways:
                trendtypestr = bfromcstr("trending sideways");
                break;
            case uptrend:
                trendtypestr = bfromcstr("Uptrend");
                break;
            case downtrend:
                trendtypestr = bfromcstr("Downtrend");
                break;
            default:
                break;
        }
    
        bstring lastQuoteDate = bstrcpy(self->Ind[self->IndPos.close]->QuoteObj->datevec[self->market_private->trend_stats[i].quote_idx]);
        float lastQuote = self->Ind[self->IndPos.close]->QuoteObj->quotevec[self->market_private->trend_stats[i].quote_idx];
      
        printf("\nMarket %s (%s) %s since %i days @%*f (last quote: %s)", bdata(*self->symbol), bdata(*self->identifier), 
                        bdata(trendtypestr), self->market_private->trend_stats[i].trendDuration, self->nr_digits, lastQuote, bdata(lastQuoteDate));
        
        bdestroy(trendtypestr);
        bdestroy(lastQuoteDate);
    }
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief print trend info of market for a specific date to terminal
 * 
 * print trend info of market for a specific date to terminal
 * 
 * @param self pointer to market object
 * @param idx index as uint
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_printTrendStatusbyIdxImpl(const class_market *self, unsigned int idx)
{
    bstring trendtypestr = NULL;
    switch(self->market_private->trend_stats[idx].currTrendType)
    {
    case sideways:
        trendtypestr = bfromcstr("trending sideways");
        break;
    case uptrend:
        trendtypestr = bfromcstr("Uptrend");
        break;
    case downtrend:
        trendtypestr = bfromcstr("Downtrend");
        break;
    default:
        break;
    }

    bstring lastQuoteDate = bstrcpy(self->Ind[self->IndPos.close]->QuoteObj->datevec[self->market_private->trend_stats[idx].quote_idx]);
    float lastQuote = self->Ind[self->IndPos.close]->QuoteObj->quotevec[self->market_private->trend_stats[idx].quote_idx];

    printf("\nMarket %s (%s) %s since %i days @%*f (last quote: %s)", bdata(*self->symbol), bdata(*self->identifier), 
                        bdata(trendtypestr), self->market_private->trend_stats[idx].trendDuration, self->nr_digits, lastQuote, bdata(lastQuoteDate));

    bdestroy(trendtypestr);
    bdestroy(lastQuoteDate);   
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief print markets translation currency table to terminal
 * 
 * print markets translation currency table to terminal, mainly used for
 * developing/debugging
 * 
 * @param self pointer to market object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_printTranslCurTableImpl(const class_market* self)
{
    self->market_private->translation_currency->printTable(self->market_private->translation_currency);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns nr. of indicators embedded in market object
 * 
 * getter function for (private data) nr of indicators, which are
 * currently living within the given object
 * 
 * @param self pointer to market object
 * @returns uint with nr of indicators
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_market_getNrIndicatorsImpl(const class_market* self)
{
	return self->market_private->nr_indicators;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds new indicator object to market 
 * 
 * Add a new indicator to the given market, initialize with arguments
 * 
 * @param self pointer to market object
 * @param indicator_name char* name of the new indicator
 * @param nr_quotes uint wih nr of qoutes within indicator
 * @param ind_descr char* with description of new indicator
 * @param tablename char* with db tablename which stores indicator
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_addNewIndicatorImpl(class_market* self, char* indicator_name, unsigned int nr_quotes, char* ind_descr, char* tablename)
{
	// increase nr of Indicators in market object
	self->market_private->nr_indicators = self->market_private->nr_indicators + 1;
	// realloc indicator vector for new indicator object
	self->Ind = (class_indicators**)realloc(self->Ind, (self->market_private->nr_indicators) * sizeof(class_indicators));	
    if(self->Ind == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    } 
        
	// create new indicator object in indicator vector (which is part of market object)
	self->Ind[self->market_private->nr_indicators - 1] = class_indicators_init(bdata(*self->symbol), bdata(*self->identifier),  nr_quotes, self->nr_digits, indicator_name, ind_descr, tablename);

	// set method for copying an indicator from dummy to real one
	if((self->copyIndicatorQuotes==&class_market_ERRORcopyIndicatorQuotesImpl) && (self->market_private->nr_indicators > 1))
		self->copyIndicatorQuotes = class_market_copyIndicatorQuotesImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds existing indicator object to market 
 * 
 * Add an existing indicator to the given market
 * 
 * @param self pointer to market object
 * @param the_indicator pointer to indicator object
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_addIndicatorImpl(class_market* self, class_indicators* the_indicator)
{
	// increase nr of Indicators in market object
	self->market_private->nr_indicators = self->market_private->nr_indicators + 1;
    
	// realloc indicator vector for new indicator object
	self->Ind = (class_indicators**)realloc(self->Ind, (self->market_private->nr_indicators) * sizeof(class_indicators));	
    if(self->Ind == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    } 
    
	// clone given indicator within market object
	self->Ind[self->market_private->nr_indicators - 1] = the_indicator->clone(the_indicator);
	
	// set method for copying an indicator from dummy to real one
	if((self->copyIndicatorQuotes==&class_market_ERRORcopyIndicatorQuotesImpl) && (self->market_private->nr_indicators > 1))
		self->copyIndicatorQuotes = class_market_copyIndicatorQuotesImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief checks if indicator is present in market object
 * 
 * checks if a given indicator name does exist within a specific market object
 * 
 * @param self pointer to market object
 * @param indicator_name char* name of the indicator
 * @returns true if indicator exists, false otherwise
*/
////////////////////////////////////////////////////////////////////////////
static bool class_market_IndicatorExistsImpl(const class_market* self, char* indicator_name)
{
	bool indicator_exists = false;
	
	for(unsigned int i=0; i<self->market_private->nr_indicators; i++)
	{
		if(biseqcstr(*self->Ind[i]->name, indicator_name))
		{
			indicator_exists = true;
			return indicator_exists;
		}
	}
	
	return indicator_exists;		
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief loads data from database directly into into indicator
 * living within market object
 * 
 * This method is basically a wrapper around the loadQuotes method
 * of indicator class. it takes a market object and an indicator name as arguments,
 * and loads the quote data from db into the market object.
 *@note note that the indicator must already exist within the market object,
 * otherwise this method will spit out an error and exit
 * 
 * @param self pointer to market object
 * @param indicator_name char* name of the new indicator
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_loadIndicatorDBImpl(class_market* self, char* indicator_name)
{
	if(!self->IndicatorExists(self, indicator_name))
	{
		log_err("Cannot load indicator data from db: indicator %s not found in market object %s!\n"ANSI_COLOR_RESET, indicator_name, bdata(*self->symbol));
 		exit(EXIT_FAILURE);
	}
	self->Ind[self->getIndicatorPos(self, indicator_name)]->loadFromDB(self->Ind[self->getIndicatorPos(self, indicator_name)]);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves all indicators currently existing in market object
 * into database
 * 
 * This method is basically a wrapper around the saveQuotes method
 * of indicator class. it takes a market object and saves the quote data 
 * from from all indicators within the market into the db  * 
 * @param self pointer to market object
*/
////////////////////////////////////////////////////////////////////////////
void class_market_saveAllIndicatorsDBImpl(const class_market* self)
{
	for(unsigned int i=0; i<self->market_private->nr_indicators; i++)
	{
		self->Ind[i]->saveToDB(self->Ind[i]);
	}
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns indicator position in market object
 * 
 * indicators are stored in an array within market objects. This method
 * returns the position of a given indicator. 
 * @note if indicator does not exist, a warning will be sent to terminal, the program
 * will continue
 * @note when implementing indicators, use 
 *              #define BarIndiPos(k) k->getIndicatorPos(k, "FoobarIndicator")
 *             so instead of using  
 *             market_foo->getIndicatorPos(market_foo,"FoobarIndicator") you can abbreviate:
 *             market_foo->BarIndiPos(market_foo);
 * @note or, even better, save position in an const int:
 *              const unsigned int bar_pos = market_bar->getIndicatorPos(market_bar, "FoobarIndicator");
 * @param self pointer to market object
 * @param indicator_name char* name of the indicator
 * @returns uint with position of indicator in matrix
*/
////////////////////////////////////////////////////////////////////////////
static int class_market_getIndicatorPosImpl(const class_market *self, char* indicator_name)
{
	int position = -1;
	for(unsigned int i=0; i<self->market_private->nr_indicators; i++)
	{
		if(biseqcstr(*self->Ind[i]->name, indicator_name))
		{
			position = i;
			break;
		}
	}
	if(position == -1)
	{
		log_warn("indicator %s does not exist in object %s:", indicator_name, bdata(*self->symbol));
		self->printTable(self);
	}
	return position;	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief copies data vectors from one indicator within a market
 * object into another
 * 
 * This method copies the data from one indicator into another. Both
 * indicators must exist within a market object and be of equal length
 * (equal nr of quotes)
 * @param self pointer to market object
 * @param SrcIndicatorName char ptr with name of source indicator
 * @param DestIndicatorName char ptr with name of destination
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_copyIndicatorQuotesImpl(class_market *self, char* SrcIndicatorName, char* DestIndicatorName)
{
	if(!self->IndicatorExists(self, SrcIndicatorName))
 	{
 		log_err("Cannot copy indicator data: indicator %s not found in market object %s:", SrcIndicatorName, bdata(*self->symbol));
		self->printTable(self);
 		exit(EXIT_FAILURE);
	}
	
	const unsigned int srcIndicatorPos = self->getIndicatorPos(self, SrcIndicatorName);
	
	if(!self->IndicatorExists(self, DestIndicatorName))
	{
		log_err("Cannot copy indicator data: indicator %s not found in market object %s:", DestIndicatorName, bdata(*self->symbol));
		self->printTable(self);
 		exit(EXIT_FAILURE);
	}
	
	const unsigned int destIndicatorPos = self->getIndicatorPos(self, DestIndicatorName);
	
	if(self->Ind[srcIndicatorPos]->NR_ELEMENTS != self->Ind[destIndicatorPos]->NR_ELEMENTS)
	{
		log_err("Different number of elements (%s: %i, %s, %i)", 
			bdata(*self->Ind[srcIndicatorPos]->name), self->Ind[srcIndicatorPos]->NR_ELEMENTS,
			bdata(*self->Ind[destIndicatorPos]->name), self->Ind[destIndicatorPos]->NR_ELEMENTS);
		exit(EXIT_FAILURE);
	}
	
	if(!(srcIndicatorPos == destIndicatorPos))	// do not copy if both indicators are identical
		self->Ind[srcIndicatorPos]->QuoteObj->copyDataVec(self->Ind[srcIndicatorPos]->QuoteObj, self->Ind[destIndicatorPos]->QuoteObj);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief dummy function that is set for copy method as long
 * as less than 2 indicators sre present within a market object
 * 
 * dummy function that is set for copy method as long
 * as less than 2 indicators sre present within a market object,
 * so copying an indicstor won't make sense
 * note: this method will be set for copy initially by the constructor, and 
 *           "reassiged" to market_copyIndicatorQuotesImpl as soon as the
 *            2nd indicator is added to an market object
 * @param self pointer to market object
 * @param Src char ptr with name of source indicator
 * @param Dest char ptr with name of destination
*/
////////////////////////////////////////////////////////////////////////////
static void class_market_ERRORcopyIndicatorQuotesImpl(class_market* self, char* src, char* dest)
{
	if(!(*src==*dest) && self->market_private->nr_indicators==1)	// if there is one indicator in market, do nothing as the function call is stupid, but does no harm
	{
		log_err("Cannot copy indicator data as market object for %s contains less than 2 indicators\n", bdata(*self->symbol));
		exit(EXIT_FAILURE); // in all other cases terminate program
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief checks if for current market a translation currency is needed (and
 * available)
 * 
 * This function checks, if the translation currency for the current market is
 * available. It returns the position of the translation currency within the
 * vector of given markets. 
 * @note The routine is smart enough to invert currency pairs if needed. 
 * Example: your account currency is EUR, the market is SPX in US Dollar. The
 * needed translation currency is USD/EUR. If one of the given markets contains
 * USD/EUR, perfect. If not, but the data for EUR/USD is present, the function
 * will recognize that and set the "inverted" flag.
 * 
 * @param the_markets vector with all available market object
 * @param self market for which a translation currency is needed
 * @return int with the position of the translation currency in the market 
 * vector; 0 if no translation currency is needed (market currency equals 
 * account currency; -1 if no translation currency was found
*/
///////////////////////////////////////////////////////////////////////////////
static int class_market_checkTranslationCurrency(class_market** the_markets, unsigned int nr_markets, bstring account_currency, class_market* self, bool* inverted_flag)
{
	int position = -1;	// -1 will equal as "no translation currency found"

	// if symbols currency is the account currency, take function´s shortcut and use table position 0 (by convention)
	if(biseq(*self->currency, account_currency))
	{
		position = 0;
		return position;
	}
	
	for(unsigned int i=0; i< nr_markets; i++)
	{
		// check if wanted translation currency is within given markets
		if(biseq(*self->market_private->translation_currency->symbol, *the_markets[i]->symbol))
		{
//			printf("\n%s für %s (in %s) in markt %s gefunden!", bdata(*self->market_private->translation_currency->symbol), bdata(*self->symbol), bdata(*self->currency), bdata(*the_markets[i]->symbol));
			position = i;
			break;
		}
		else // if not found, try with reverse pair
		{
			// reverse the symbol´s name (of course this makes only
			// sense for FOREX pairs, the string length filter tries to 
			// avoid too much overhead)
			if(blength(*the_markets[i]->symbol) > 5)
			{
				bstring reverse = NULL;
				reverse = bmidstr(*the_markets[i]->symbol, 3, 5);
				bstring tmp1 = bmidstr(*the_markets[i]->symbol, 0, 3);
				bconcat(reverse, tmp1);
				btrimws(reverse);
				bdestroy(tmp1);
				if(biseq(*self->market_private->translation_currency->symbol, reverse))
				{
					*inverted_flag=true;
//					printf("\n%s für %s (in %s) in markt %s gefunden! (invertiert)", bdata(*self->market_private->translation_currency->symbol), bdata(*self->symbol), bdata(*self->currency), bdata(*the_markets[i]->symbol));
					position = i;
					bdestroy(reverse);
					break;
				}
				bdestroy(reverse);
			}
			else	// length of current market symbol>5
				continue;
		}
	}

	return position;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief creates a bstring for the translation currency of the given market
 * 
 * This function takes the account currency and a market as argument as creates
 * a bstring for the translation currency, e.g. the currency that would
 * translate the market into account currency
 * @note: Make sure that the pointer that is returned by this function is freed
 * by the caller, using free_1d_array_bstring. Not doing so will result in a 
 * memory leak.
 * @param self market for which a translation currency is needed
 * @param account_currency bstring with the account currency
 * @return pointer to bstring with translation currency string
*/
///////////////////////////////////////////////////////////////////////////////
static bstring* class_market_determineTranslationCurrencyString(class_market* self, bstring account_currency)
{
	bstring *transl_string = NULL;
	
	transl_string = init_1d_array_bstring(1);
	*transl_string = bstrcpy(account_currency);
	bconcat(*transl_string, *self->currency);
	// set that string to translation currency
	return transl_string;	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief assigns the corresponding translation currency to a given market
 * 
 * This function checks all markets if an fitting translation currency for the
 * given market is available, and copies the quote data of this currency into
 * the private data object self->market_private->translation_currency
 * @note: The routine is smart enough to invert currency pairs if needed. 
 * Example: your account currency is EUR, the market is SPX in US Dollar. The
 * needed translation currency is USD/EUR. If one of the given markets contains
 * USD/EUR, perfect. If not, but the data for EUR/USD is present, the function
 * will recognize that by the inverted flag and invert the quote
 * 
 * @param the_markets 1xN vector with pointers to all markets
 * @param nr_markets uint with total number of markets
 * @param self market for which a translation currency is needed
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_assignTranslationCurrencyImpl(class_market **the_markets, unsigned int nr_markets, class_market *self)
{
	extern parameter parms; // parms are declared global in main.c
	
	int currency_position=-1;
	
	bool inverted_flag = false;
	
	// check in all markets if translation currency pair (or its reverse) for market self exists
	currency_position = class_market_checkTranslationCurrency(the_markets, nr_markets, parms.ACCOUNT_CURRENCY, self, &inverted_flag);

	// if not (indicated by position==-1) exit 
	check(currency_position>=0, "Account currency is %s; market %s is in %s, needed translation pair %s (or reverse) is not found within configured markets. Please check symbol config file!", error,
	      bdata(parms.ACCOUNT_CURRENCY), bdata(*self->symbol), bdata(*self->currency), bdata(*self->market_private->translation_currency->symbol));
	
	// get and store Indicator positions for easier access
 	const int closePos = self->getIndicatorPos(self, "close");	// position of own markets closes
	check(closePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error, bdata(*self->symbol));
	const int CurClosePos = the_markets[currency_position]->getIndicatorPos(the_markets[currency_position], "close");	// position of translation currencies` closes
	check(CurClosePos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error, bdata(*the_markets[currency_position]->symbol));

	// copy the quote data from indicator "close" to translation currency (to get correct dates and daynrs)
	self->market_private->translation_currency->copyDataVec(self->Ind[closePos]->QuoteObj, self->market_private->translation_currency);
    
	// copy translation currency quotes into self->market_private->translation_currency
	if(currency_position == 0)	// which means market´s currency equals account currency
	{
		// substitute the quote of the "translation currency" with 1 (contains close quotes from the copy above)
		for(unsigned i= 0; i<self->market_private->translation_currency->nr_elements; i++)
		{
			self->market_private->translation_currency->quotevec[i] = 1.0;
		}
	}
	else // position > 0 ==> current market has its translation currency in the_markets[position]
	{
		// unfortunately this is much more complicated, because current market and it´s translation currency market 
		// can have different data sets (due to national holidays there can be dates where a quote for one exists,
		// but not for the other). So loop through every day of current market and copy the up to this date´s 
		// latest translation quote to this day (ideally it is the same date, but for above reason does not have to be).
		// To make the situation even more complicated, FOREX markets tend to have much more trading days between 2 dates compared to 
		// national stock markets (For example you get quotes for a lot of FOREX markets on Sunday). because of that the idea 
		// mentioned above will fail if the quotes vectors of the translation currency and the current market have the 
		// same length (believe me, it WILL. Took me 2 days to find that problem). Solution is to create a temporary 
		// quotes object with all available currency data from the database		
        
        *self->market_private->translation_currency->identifier = bstrcpy(*the_markets[currency_position]->identifier);
        
        bstring tablename = bfromcstr("quotes_daily");
        bstring indicatorname = bfromcstr("close");
        unsigned int tq_nr_elements = db_mysql_getNrOfQuotes(*the_markets[currency_position]->symbol, *the_markets[currency_position]->identifier, indicatorname, tablename) -1;  // get nr. of datasets for specific currency
        bdestroy(tablename);
        bdestroy(indicatorname);
        
        // create temporary quote object that will hold all available quotes
		class_quotes* transl_quote = class_quotes_init(bdata(*self->market_private->translation_currency->symbol), bdata(*self->market_private->translation_currency->identifier),  tq_nr_elements, 6, "close", "quotes_daily");  // heads up: arbitrarily set nr_digits to 6!
		transl_quote->loadFromDB(transl_quote);

        // now loop through all elements of current market and find the nearest translation currency quote in the temporary quote obj
		for(unsigned int i=0; i<self->market_private->translation_currency->nr_elements; i++)
		{
			unsigned int market_daynr = self->market_private->translation_currency->daynrvec[i];
			int idx_nearest_tq_daynr = -1;
            idx_nearest_tq_daynr = transl_quote->findDaynrIndexlB(transl_quote, market_daynr);
            check(idx_nearest_tq_daynr >=0, "no quote for daynr %i found in translational quote object %s within market %s!", 
                      error_market_daynr_idx, market_daynr, 
                      bdata(*transl_quote->symbol), bdata(*self->symbol));
            
            if(!inverted_flag)
				self->market_private->translation_currency->quotevec[i] = transl_quote->quotevec[idx_nearest_tq_daynr];
			else if(inverted_flag)
				self->market_private->translation_currency->quotevec[i] = (1.0 / transl_quote->quotevec[idx_nearest_tq_daynr]);
		}
        transl_quote->destroy(transl_quote);
	}
	return;
	
error:
	exit(EXIT_FAILURE);

error_market_daynr_idx:
    // cleanup whats possible
    self->destroy(self);
    exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function of translation quote for given market and index
 * 
 * getter function of translation quote for given market and index
 * @param self market for which a translation currency is needed
 * @param index unsigned int with index nr
 * @return float with translation quote
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_getTranslationCurrencyQuotebyIndexImpl(const class_market *self, unsigned int index)
{
	// prevent out-of-boundary access of Quote Vector
	check(index <= self->market_private->translation_currency->nr_elements, "trying to access element %i of market´s %s translation currency %s: only %i elements found!", error,
	      index, bdata(*self->symbol), bdata(*self->market_private->translation_currency->symbol), 
	      self->market_private->translation_currency->nr_elements);
	
	float the_quote;
	the_quote = self->market_private->translation_currency->quotevec[index];
	return the_quote;

error:
	exit(EXIT_FAILURE);	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function of translation quote daynr for given market and index
 * 
 * getter function of translation quote daynr for given market and index
 * @param self market for which a translation currency is needed
 * @param index unsigned int with index nr
 * @return uint with translation quote daynr
*/
///////////////////////////////////////////////////////////////////////////////
static unsigned int class_market_getTranslationCurrencyDaynrbyIndexImpl(const class_market* self, unsigned int index)
{
	// prevent out-of-boundary access of Quote Vector
	check(index <= self->market_private->translation_currency->nr_elements, "trying to access element %i of market´s %s translation currency %s: only %i elements found!", error,
	      index, bdata(*self->symbol), bdata(*self->market_private->translation_currency->symbol), 
	      self->market_private->translation_currency->nr_elements);
	
	unsigned int the_daynr;
	the_daynr = self->market_private->translation_currency->daynrvec[index];
	return the_daynr;
	
error:
	exit(EXIT_FAILURE);		
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function of translation quote date for given market and index
 * 
 * getter function of translation quote date for given market and index
 * @param self market for which a translation currency is needed
 * @param index unsigned int with index nr
 * @return bstring ptr with translation quote date
*/
///////////////////////////////////////////////////////////////////////////////
static bstring* class_market_getTranslationCurrencyDatebyIndexImpl(const class_market* self, unsigned int index)
{
	bstring* the_date=NULL;
	the_date = init_1d_array_bstring(1);
	
	// prevent out-of-boundary access of Quote Vector
	check(index <= self->market_private->translation_currency->nr_elements, "trying to access element %i of market´s %s translation currency %s: only %i elements found!", error,
	      index, bdata(*self->symbol), bdata(*self->market_private->translation_currency->symbol), 
	      self->market_private->translation_currency->nr_elements);

	*the_date = bstrcpy(self->market_private->translation_currency->datevec[index]);
	return the_date;
	
error:
	exit(EXIT_FAILURE);		
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief fill the "short-cut" struct within market object with basic indicator
 *        positions
 * 
 * Within each market object is an embedded struct which stores the basic 
 * indicators of each market: open/close/low/high/returns. This enables a 
 * quicker access than the relatively slow getIndicatorPos method
 * @param self ptr to market object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_initIndicatorPositionsImpl(class_market* self)
{
	self->IndPos.open = class_market_getIndicatorPosImpl(self, "open");
	self->IndPos.close = class_market_getIndicatorPosImpl(self, "close");
	self->IndPos.low = class_market_getIndicatorPosImpl(self, "low");
	self->IndPos.high = class_market_getIndicatorPosImpl(self, "high");
    self->IndPos.returns = class_market_getIndicatorPosImpl(self, "returns"); 
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for market´s mean returns
 * 
 * getter function for market´s mean returns
 * @param self ptr to market object
 * @returns mean returns as float
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_getMeanReturnsImpl(const class_market* self)
{
	return self->market_private->statistics.periodMeanReturns;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for market´s mean excess returns
 * 
 * getter function for market´s mean excess returns
 * @param self ptr to market object
 * @returns mean returns as float
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_getMeanExcessReturnsImpl(const class_market* self)
{
	return self->market_private->statistics.periodMeanExcessReturns;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for market return´s standard deviation
 * 
 * getter function for market returns standard deviation
 * @param self ptr to market object
 * @returns sigma as float
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_getVarianceImpl(const class_market* self)
{
	return self->market_private->statistics.periodVariance;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for index in QuoteObj, when last update took place
 * 
 * getter function for index in QuoteObj, when last update took place
 * @param self ptr to market object
 * @returns index as uint
*/
///////////////////////////////////////////////////////////////////////////////
static unsigned int class_market_getLastUpdateIndexImpl(const class_market* self)
{
	return self->market_private->statistics.lastUpdate_idx;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief update market´s statistical measures if needed
 * 
 * This method checks if an update of a market´s statistical measures is needed
 * and performs it. For the check both the internal update flag and the number
 * of periods since the last update are evaluated.
 * @TODO: atm this method relies on external/global parameters, which is
 * 		  somewhat ugly
 * @param self ptr to market object
 * @param update_period unit with the desired update period
 * @return updateFlag bool flag, if anything was updated (false if not)
*/
///////////////////////////////////////////////////////////////////////////////
static bool class_market_updateMarketStatisticsImpl(class_market* self, unsigned int quote_idx)
{
    bool updateFlag = false;
    
	extern parameter parms; // parms are declared global in main.c
	extern bool verbose_flag; // declared global in main.c
	
	// increase update counter
	self->market_private->statistics.statisticsLastUpdate++;
	
	// check if the update flag is set or if the internal counter of periods
	// since last update has reached the update period
	if((self->market_private->statistics.updateStatisticsNeeded) ||
		(self->market_private->statistics.statisticsLastUpdate == parms.PORTFOLIO_RETURNS_UPDATE))
	{
		float meanReturns = 0.0;
		float variance = 0.0;
		int returnsPos = self->IndPos.returns;

		// calculate the starting position within quote vector for statistics calculations
		int quoteVec_start = quote_idx - parms.PORTFOLIO_RETURNS_PERIOD + 1;
		// usually the number of quotes used for statistical calulations is
		// as configured...
		int nr_quotes = parms.PORTFOLIO_RETURNS_PERIOD;
		
		// ...except when the current quote index is smaller than that number
		// (that would result in an negative array index and thus a segfault
		if(quoteVec_start < 0)
		{
			log_warn("statistics: not enough quotes for %s- quote index is %i, PORTFOLIO_RETURNS_PERIOD is %i\nContinuing with less data (You could increase INDI_DAYS_TO_UPDATE by %i periods to prevent this warning)", bdata(*self->symbol), quote_idx, parms.PORTFOLIO_RETURNS_PERIOD, (quoteVec_start *(-1)));
			// in this case limit the number of quotes to what data is currently available
			quoteVec_start = 0;
			nr_quotes = quote_idx;			
		}
		
		// calculate mean returns and standard deviation, using a subset of the quote data
		meanReturns = calc_arithmeticMean(&self->Ind[returnsPos]->QUOTEVEC[quoteVec_start], nr_quotes);
//         printf("\n%s quotevec[%i]: %.7f quotevec[%i]: %.7f", bdata(*self->symbol), quoteVec_start, self->Ind[returnsPos]->QUOTEVEC[quoteVec_start], quoteVec_start + nr_quotes-1, self->Ind[returnsPos]->QUOTEVEC[quoteVec_start+nr_quotes-1]);
        variance = calc_variance(&self->Ind[returnsPos]->QUOTEVEC[quoteVec_start], meanReturns, nr_quotes);
        
        // mean returns in period is calculated by multipling the mean return with the number of periods
        self->market_private->statistics.periodMeanReturns = meanReturns * parms.PORTFOLIO_RETURNS_PERIOD;
        
        // similar to that the variance within the period
        self->market_private->statistics.periodVariance = variance * parms.PORTFOLIO_RETURNS_PERIOD;
        
        // mean excess returns is risk free rate substracted from mean returns
        self->market_private->statistics.periodMeanExcessReturns = self->market_private->statistics.periodMeanReturns - parms.RISK_FREE_RATE;
        
		// finally set internal counter and flag back to zero
		self->market_private->statistics.updateStatisticsNeeded = false;
		self->market_private->statistics.statisticsLastUpdate = 0;

		//log_info(ANSI_COLOR_RESET"%s %s Updated statistics at index %i: mean returns %.8f, sigma: %.8f", bdata(self->Ind[returnsPos]->DATEVEC[quote_idx]), bdata(*self->symbol), quote_idx, meanReturns, sigma);
        if(verbose_flag)
        {
            log_info("%s %s Updated statistics at index %i", bdata(self->Ind[returnsPos]->DATEVEC[quote_idx]), bdata(*self->symbol), quote_idx);
        }
        
        updateFlag = true;
        self->market_private->statistics.lastUpdate_idx = quote_idx;
        
		return updateFlag;
	}	
	// no update needed, jump back
	else
		return updateFlag;		
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for current trend type
 * 
 * getter function for current trend type
 * @param self ptr to market object
 * @param idx index as uint
 * @returns trend_type
*/
///////////////////////////////////////////////////////////////////////////////
static trend_type class_market_getTrendTypeByIndexImpl(class_market* self, unsigned int idx)
{
    return self->market_private->trend_stats[idx].currTrendType;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief setter function for current trend type
 * 
 * setter function for current trend type
 * @param self ptr to market object
 * @param idx index as uint
 * @param currTrend current trend as trend_type
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_setTrendTypeByIndexImpl(class_market* self, trend_type currTrend, unsigned int idx)
{
    self->market_private->trend_stats[idx].currTrendType = currTrend;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for current trend duration
 * 
 * getter function for current trend duration
 * @param self ptr to market object
 * @param idx index as uint
 * @returns duration as unsigned int
*/
///////////////////////////////////////////////////////////////////////////////
static unsigned int class_market_getTrendDurationByIndexImpl(class_market* self, unsigned int idx)
{
    return self->market_private->trend_stats[idx].trendDuration;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief setter function for current trend duration
 * 
 * setter function for current trend duration
 * @param self ptr to market object
 * @param idx index as uint
 * @param duration as uint
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_setTrendDurationByIndexImpl(class_market* self, unsigned int duration, unsigned int idx)
{
    self->market_private->trend_stats[idx].trendDuration = duration;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief set date, daynr and quote in market->market_privat->trend_stats
 * 
 * setter function for market->market_privat->trend_stats.lastQuote,
 * market->market_private->trend_stats.lastQuoteDate and 
 * market->market_private->trend_stats.lastQuoteDayNr
 * 
 * @param self ptr to market object
 * @param idx index as uint
 * @param idx unsigned int with quote index (refering to the close quote 
 * indicator embedded in each market object)
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_setTrendPriceByIndexImpl(class_market* self, unsigned int idx, unsigned int quote_idx)
{
     self->market_private->trend_stats[idx].quote_idx = quote_idx;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for date of a specific trend record
 * 
 * getter function for date of a specific trend record
 * 
 * @param self ptr to market object
 * @param idx index as uint
 * @returns date as ptr to bstring
*/
///////////////////////////////////////////////////////////////////////////////
static bstring* class_market_getTrendDateByIndexImpl(class_market* self, unsigned int idx)
{
 	bstring* the_date=NULL;
	the_date = init_1d_array_bstring(1);
	
	*the_date = bstrcpy(self->Ind[self->IndPos.close]->QuoteObj->datevec[self->market_private->trend_stats[idx].quote_idx]);
	return the_date;   
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for daynr of a specific trend record
 * 
 * getter function for daynr of a specific trend record
 * 
 * @param self ptr to market object
 * @param idx index as uint
 * @returns daynr as uint
*/
///////////////////////////////////////////////////////////////////////////////
static unsigned int class_market_getTrendDaynrByIndexImpl(class_market *self, unsigned int idx)
{
    return self->Ind[self->IndPos.close]->QuoteObj->daynrvec[self->market_private->trend_stats[idx].quote_idx];
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter function for price of a specific trend record
 * 
 * getter function for price of a specific trend record
 * 
 * @param self ptr to market object
 * @param idx index as uint
 * @returns price as float
*/
///////////////////////////////////////////////////////////////////////////////
static float class_market_getTrendPricebyIndexImpl(class_market *self, unsigned int idx)
{
    return self->Ind[self->IndPos.close]->QuoteObj->quotevec[self->market_private->trend_stats[idx].quote_idx];
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief loads trend record for a given index from database
 * 
 * this method loads a trend record for a given index from database
 * 
 * @param self ptr to market object
 * @param idx index as uint
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_loadTrendStateDBbyIndexImpl(class_market *self, unsigned int idx)
{
    db_mysql_get_market_trend(self, idx);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves trend records to database
 * 
 * this method saves all trend records to database
 * 
 * @param self ptr to market object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_market_saveTrendStateDBImpl(class_market *self)
{
    if(self->tradeable)
    {   
        db_mysql_update_market_trend(self);
    }
}

// EOF
