/* indicators_general.c
 * routines to calculate various indicators
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file indicators_general.c
 * @brief calculation of various indicators
 *
 * This file implements various general indicators
 * 
 * @author Denis Zetzmann
 */

#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

#include "datatypes.h"
#include "constants.h"
#include "readconf.h"
#include "date.h"
#include "arrays.h"
#include "database.h"
#include "debug.h"

#include "indicators_general.h"

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief comparison function for qsort()
 *
 * comparison function for qsort()
 */
///////////////////////////////////////////////////////////////////////////////
int cmpfunc(const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the "highest highs" indicator 
 *
 * determines the highest highs within a given period for number of days
 * can be used for arbitrary period (period < nr_days). Result is stored in 
 * parameter indicator *highest_highs
 * 
 * @todo check if days > period!
 * 
 * @param period search for high in given period days
 * @param highs pointer to indicator struct with daily highs
 * @param highest_highs pointer to indicator struct with highest highs
 */
///////////////////////////////////////////////////////////////////////////////
void highest_high(unsigned int period, const class_indicators *highs, class_indicators *highest_highs)
{
	// check if all arguments have same nr. of quotes/dates/daynrs
	CHECK_EQUAL_LENGTH(highs, highest_highs);
	// sanity check if nr of elements in indicator is sufficient for given period
	if(highs->NR_ELEMENTS < period)
	{
		log_err("computing highest high of a %i day period with only %i days of high data not possible", period, highs->NR_ELEMENTS);
		exit(EXIT_FAILURE);
	}
	
	unsigned int day=period-1;		/* start at the end of 1st period */
	int i=0;
	float tmp_maximum=0;
	
	/* Outer Loop: End of 1st period until end */
	while (day < (highs->NR_ELEMENTS))		
	{
		tmp_maximum = highs->QUOTEVEC[day];
		i=period-1;
		/* Inner loop: from actual day until (actual day- period)   */
		while (i>=0)
		{
			if(highs->QUOTEVEC[day-i] > tmp_maximum)
				tmp_maximum = highs->QUOTEVEC[day-i];
			i=i-1;	
		}
		highest_highs->QUOTEVEC[day] = tmp_maximum;
		day++;
	}

	/* fill out beginning of array (from 0 to 1st period) with 0 */
	for (i=0; i< ( (int) (period-1)); i++)
		highest_highs->QUOTEVEC[i]=0;	
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the "lowest lows" indicator 
 *
 * determines the lowest lows within a given period for number of days
 * can be used for arbitrary period (period < nr_days). Result is stored in 
 * parameter indicator *lowest_lows
 * 
 * @todo check if days > period!
 * 
 * @param period search for low in given period days
 * @param highs pointer to indicator struct with daily lows
 * @param highest_highs pointer to indicator struct with lowest lows
 */
///////////////////////////////////////////////////////////////////////////////
void lowest_low(unsigned int period, const class_indicators *lows, class_indicators *lowest_lows)
{
	// check if all arguments have same nr. of quotes/dates/daynrs
	CHECK_EQUAL_LENGTH(lows, lowest_lows);
	// sanity check if nr of elements in indicator is sufficient for given period
	if(lows->NR_ELEMENTS < period)
	{
		log_err("computing lowest low of a %i day period with only %i days of low data not possible", period, lows->NR_ELEMENTS);
		exit(EXIT_FAILURE);
	}
	
	unsigned int day=period-1;		/* start at the end of 1st period */
	int i=0;
	float tmp_minimum=0;
	
	/* Outer Loop: End of 1st period until end */
	while (day < (lows->NR_ELEMENTS))		// removed: <= (valgrind: leak)
	{
		tmp_minimum = lows->QUOTEVEC[day];
		i=period-1;
		/* Inner loop: from actual day until (actual day- period)   */
		while (i>=0)
		{
			if(lows->QUOTEVEC[day-i] < tmp_minimum)
				tmp_minimum = lows->QUOTEVEC[day-i];
			i=i-1;	
		}
		lowest_lows->QUOTEVEC[day] = tmp_minimum;
		day++;
	}

	/* fill out beginning of array (from 0 to 1st period) with 0 */
	for (i=0; i<( (int) (period-1)); i++)
		lowest_lows->QUOTEVEC[i]=0;		
}
 
///////////////////////////////////////////////////////////////////////////////
/**
 * @brief determine the current direction of the indicator: up, down, sideways
 *
 * determine the current direction of the indicator: up, down, sideways
 * this is down by linear regression. The regression function is estimated
 * using a least square estimator which is fed with the daynumbers and the
 * corresponding quotes.
 * slope of regression function:
 * b= \frac{ \sum [(x_i - \overline{x})*(y_i - \overline{y})] } 
 *			{\sum (x_i - \overline{x})^2 }
 * 
 * @todo: recheck if parameter daynr is really needed --> is stored
 *        within indicator
 * @todo: make the currently hardcoded 5 day period for linear regression
 *        configurable
 * @param daynr number of days in indicator
 * @param theIndicator pointer to indicator struct
 * @return trend_type current trend type of indicator
 */
///////////////////////////////////////////////////////////////////////////////
trend_type indicator_trend(const unsigned int daynr, const class_indicators *theIndicator)
{
	unsigned int i=0;
	unsigned int daysum=0;
	float quotesum=0;
	float avg_daynr=0;
	float avg_quote=0;
	float numerator=0;
	float denominator=0;
	float slope=0;
	
	for(i=daynr-5; i< daynr; i++)	// TODO: make 5 days configurable in config file
	{
		daysum = daysum + theIndicator->DAYNRVEC[i];
		quotesum = quotesum + theIndicator->QUOTEVEC[i];
	}
	avg_daynr = (float) daysum / 5; // TODO: make 5 days configurable in config file
	avg_quote = quotesum / 5;  // TODO: make 5 days configurable in config file
	
	for(i=daynr-5; i< daynr; i++)	// TODO: make 5 days configurable in config file
	{
		numerator= numerator + \
			( ((float) theIndicator->DAYNRVEC[i]- avg_daynr)* \
			(theIndicator->QUOTEVEC[i] - avg_quote));
		denominator = denominator + \
			(((float) theIndicator->DAYNRVEC[i] - avg_daynr)* \
			((float) theIndicator->DAYNRVEC[i] - avg_daynr));
	}
	slope = numerator / denominator;
	if(slope > 0.01) // TODO: make the "tolerance level" configurable
		return uptrend;
	else if (slope < -0.01)
		return downtrend;
	else 
		return sideways;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the true range indicator 
 *
 * Calculates the true range indicator. Result is stored in parameter 
 * indicator *true_range
 * 
 * @param highs pointer to indicator with highs
 * @param lows pointer to indicator with lows
 * @param closes pointer to indicator with close quotes
 * @param true_range pointer to indicator struct with true_range
 */
///////////////////////////////////////////////////////////////////////////////
void calc_true_range(const class_indicators *highs, const class_indicators *lows, const class_indicators *closes, 
		class_indicators *true_range)
{
	// check if all arguments have same nr. of quotes/dates/daynrs
	CHECK_EQUAL_LENGTH(highs, lows);
	CHECK_EQUAL_LENGTH(lows, closes);
	CHECK_EQUAL_LENGTH(closes, true_range);
	
	unsigned int i=0;
	float tmp_max = 0;
	float val1, val2,val3;
	
	true_range->QUOTEVEC[0] = 0.0;
	
	for(i=1; i<(closes->NR_ELEMENTS); i++)
	{
		//           val1       val2               val3
		// TR = MAX( H – L ; H – C(yesterday) ; C(yesterday) – L )
		val1 = highs->QUOTEVEC[i] - lows->QUOTEVEC[i];
		val2 = highs->QUOTEVEC[i] - closes->QUOTEVEC[i-1];
		val3 = closes->QUOTEVEC[i-1] - lows->QUOTEVEC[i];
		//maxvalue = ( m >= n ) ? m : n; do it twice for 3 values
		tmp_max = (val1 >= val2) ? val1 : val2;
		true_range->QUOTEVEC[i] = (tmp_max >= val3) ? tmp_max : val3;
	}
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of the average true range (ATR) indicator 
 *
 * Calculates the average true range indicator. Result is stored in parameter 
 * indicator *average_true_range
 * 
 * @param period unsigned int averaging/smoothing period
 * @param true_range pointer to indicator struct with true range
 * @param average_true_range pointer to indicator struct with ATR
 */
///////////////////////////////////////////////////////////////////////////////
void calc_average_true_range(const unsigned int period, const class_indicators *true_range, class_indicators *average_true_range)
{
	// check if all arguments have same nr. of quotes/dates/daynrs
	CHECK_EQUAL_LENGTH(true_range, average_true_range);
	
	unsigned int i=0;
	
	average_true_range->QUOTEVEC[0] = 0.0;
	
	// first ATR value is arithmetic mean of TRs (of first ATR period)
	for(i=0;i < period; i++)
	{
		average_true_range->QUOTEVEC[0] = average_true_range->QUOTEVEC[0] + true_range->QUOTEVEC[i];
	}
	average_true_range->QUOTEVEC[0] = average_true_range->QUOTEVEC[0] / period;
	
	for(i=1; i<average_true_range->NR_ELEMENTS; i++)
	{
		average_true_range->QUOTEVEC[i] = (average_true_range->QUOTEVEC[i-1] * (period - 1) + true_range->QUOTEVEC[i]) / period;
	}
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of directional movement index (dmi) and average 
 * directional movement index indicators
 *
 * Calculates directional movement index (dmi) and average directional movement
 * index (adx) indicators. Result is stored in parameters indicator *dmi 
 * and *adx
 * 
 * @note This implementation uses Welles Wilder Average, an exponential
 * moving average for smoothing is commented out
 * 
 * @see http://www.dinosaurtech.com/2007/average-directional-movement-index-adx-formula-in-c-2/
 * @see http://http://www.stockcharts.com/school/doku.php?id=chart_school:technical_indicators:average_directional_index_adx#indicator_calculation
 * @param highs pointer to indicator struct containing highs of the period
 * @param lows pointer to indicator struct containing lows of the period
 * @param tr pointer to indicator struct containing true range
 * @param dmi pointer to indicator struct that will store dmi
 * @param adx pointer to indicator struct that will store adx
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int calc_directional_movement_index(const unsigned int period, const class_indicators *highs, const class_indicators *lows, const class_indicators *tr, class_indicators *dmi, class_indicators *adx)
{
	//extern parameter parms; // parms are declared global in main.c

	float delta_high = 0, delta_low = 0;

	// check if all arguments have same nr. of quotes/dates/daynrs
	CHECK_EQUAL_LENGTH(highs, lows);
	CHECK_EQUAL_LENGTH(lows, tr);
	CHECK_EQUAL_LENGTH(tr, dmi);
	CHECK_EQUAL_LENGTH(dmi, adx);
	
	float DM_plus = 0 , DM_minus = 0;
	
	// following indicators are only temporary, we can avoid the overhead 
	// of creating indicator objects and use only the quote information
	class_quotes* ADM_plus=NULL;
	class_quotes* ADM_minus= NULL;
	class_quotes* DI_plus= NULL;
	class_quotes* DI_minus=NULL;
	
	// ATR has to be indicators object though, to be able to use calc_average_true_range()
	class_indicators* ATR=NULL;
	
	ADM_plus = class_quotes_init(bdata(*highs->THE_SYMBOL), "indicator", highs->NR_ELEMENTS, highs->NR_DIGITS, "adm_plus", "none");
	ADM_minus= class_quotes_init(bdata(*highs->THE_SYMBOL), "indicator",highs->NR_ELEMENTS, highs->NR_DIGITS, "adm_minus", "none");
	DI_minus = class_quotes_init(bdata(*highs->THE_SYMBOL), "indicator",highs->NR_ELEMENTS, highs->NR_DIGITS, "DI_minus", "none");
	DI_plus  = class_quotes_init(bdata(*highs->THE_SYMBOL), "indicator",highs->NR_ELEMENTS, highs->NR_DIGITS, "DI_plus", "none");
	
	ATR	 = class_indicators_init(bdata(*highs->THE_SYMBOL), bdata(*highs->QuoteObj->identifier), highs->NR_ELEMENTS, highs->NR_DIGITS, "ATR", "average true range", "indicators_daily");

 	//float exp_factor = (2.0 / ( (float) period + 1.0));
	
	// calc ATR by averaging True Range
	calc_average_true_range(period, tr, ATR);
		
 	for(unsigned int i=1; i< highs->NR_ELEMENTS; i++)
	{
		delta_high = highs->QUOTEVEC[i] - highs->QUOTEVEC[i-1];
		delta_low = lows->QUOTEVEC[i-1] - lows->QUOTEVEC[i];
		
		// calculate directional move indicators
		if(((delta_high < 0) && (delta_low < 0)) || (delta_high == delta_low))
		{
			DM_plus = 0;
			DM_minus = 0;
		}
		else if(delta_high > delta_low)
		{
			DM_plus = delta_high;
			DM_minus = 0;
		}
		else if(delta_high < delta_low)
		{
			DM_plus = 0;
			DM_minus = delta_low;
		}
		
		// original Welles Wilder Average:
		ADM_plus->quotevec[i]  = (ADM_plus->quotevec[i-1]  * ((float) period - 1.0) + DM_plus * 1.0) / (float) period;
		ADM_minus->quotevec[i] = (ADM_minus->quotevec[i-1] * ((float) period - 1.0) + DM_minus * 1.0) / (float) period;
		
		// alternative: exponential smoothing
// 		ADM_plus->quotevec[i]  = DM_plus * exp_factor + (1 - exp_factor) * ADM_plus->quotevec[i-1];
// 		ADM_minus->quotevec[i] = DM_minus* exp_factor + (1 - exp_factor) * ADM_minus->quotevec[i-1];
		
		// calculate directional index
		if(!(ATR->QUOTEVEC[i]==0))
		{
			DI_plus->quotevec[i]  = ADM_plus->quotevec[i] / ATR->QUOTEVEC[i] * 100;
			DI_minus->quotevec[i] = ADM_minus->quotevec[i] / ATR->QUOTEVEC[i] * 100;
		}
		else 
		{
			DI_plus->quotevec[i] = DI_plus->quotevec[i-1];
			DI_minus->quotevec[i] = DI_minus->quotevec[i-1];
		}
		
		// finally calculate directional movement index
		if(!((DI_plus->quotevec[i] + DI_minus->quotevec[i])==0))
			dmi->QUOTEVEC[i] = fabs(DI_plus->quotevec[i] - DI_minus->quotevec[i]) / (DI_plus->quotevec[i] + DI_minus->quotevec[i]) * 100;
		else
			dmi->QUOTEVEC[i] = dmi->QUOTEVEC[i-1];
	
		delta_high = 0;
		delta_low = 0;
		DM_minus = 0;
		DM_plus = 0;
	}

 	// first adx element is arithmetic mean of first period DMI
 	for(unsigned int i=0; i < period; i++)
		adx->QUOTEVEC[0]= adx->QUOTEVEC[0] + dmi->QUOTEVEC[i];
	adx->QUOTEVEC[0] = adx->QUOTEVEC[0] / (float) period;
	
	 // now average dmi to get adx
	for(unsigned int i=1; i< adx->NR_ELEMENTS; i++)
	{
		adx->QUOTEVEC[i] = (adx->QUOTEVEC[i-1] * (period - 1) + dmi->QUOTEVEC[i]) / (float) period;
	}
	
	// again the alternative: exponential smoothing
	// exponential_moving_average_2(period, dmi, adx);
 	
 	
 	// cleanup
	ADM_plus->destroy(ADM_plus);
	ADM_minus->destroy(ADM_minus);
	DI_minus->destroy(DI_minus);
	DI_plus->destroy(DI_plus);
	ATR->destroy(ATR);
	
	return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates exponential moving average for any given data set
 *
 * Generalized routine that will calculate an exponential moving average for
 * the input data, based on the time period given. EMA will be stored in 
 * parameter output. Note that since this can be used to calulate "classic"
 * EMAs as EMA20/EMA40 of daily closes, it is not limited to. Also possible
 * is adx (from dmi), atr (from tr) and so on.
 * 
 * @param input float pointer to matrix containing input data
 * @param period int number of days over which the data should be averaged
 * @param nrdays unsigned int total number of data in dataset
 * @param output float pointer to matrix that will contain EMA data
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int exponential_moving_average(const unsigned int period, const class_indicators *input, class_indicators *output)
{
	// check if all arguments have same nr. of quotes/dates/daynrs
	CHECK_EQUAL_LENGTH(input, output);
	unsigned int i=0;
	
	float exp_factor = (2.0 / ( (float)period + 1.0));
	
	output->QUOTEVEC[0] = 0.0;
	
	for(i=1; i< input->NR_ELEMENTS; i++)
		output->QUOTEVEC[i] = (input->QUOTEVEC[i] - output->QUOTEVEC[i-1]) * exp_factor + output->QUOTEVEC[i-1];
	
	return EXIT_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates the ADX-based market regime filter
 *
 * This routine calculates a binary filter (0/false; 1/true) for a given adx.
 * This is used to determine whether a certain market is trending or not. If 
 * adx is below the given threshhold, the filter will be set to 1, so that 
 * the execution routines can filter particular signals out.
 * 
 * @param adx pointer adx indicator struct
 * @param adx_threshhold float threshhold for adx, below that will be filtered
 * @param regime_filter pointer to regime_filter indicator struct
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int ADX_regime_filter(const class_indicators *adx, const float adx_threshhold, class_indicators *regime_filter)
{
	unsigned int i=0;
	for(i=0; i< adx->NR_ELEMENTS; i++)
	{
		if(adx->QUOTEVEC[i] < adx_threshhold)
			regime_filter->QUOTEVEC[i] = 1;
		else
			regime_filter->QUOTEVEC[i] = 0;		
	}
	return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Updates some generally used indicators in database
 *
 * Updates some generally used indicators (like TR/ATR, HH/LL in database 
 * (and database only, no data exchange outside of function). Only commonly used
 * indicators are treated here, for specific ones (like ichimoku's Kijun-Sen)
 * there are specific routines.First this function gets the lates quotes from db
 * (quotes in db should be updated outside this function), calculates 
 * indicators and stores them in database again. 
 * 
 * @param the_market class_market object
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int  update_indicators(class_market* the_market)
 {
	extern parameter parms; // parms are declared global in main.c
	
 	the_market->addNewIndicator(the_market, "open", parms.INDI_DAYS_TO_UPDATE, "open quotes", "quotes_daily");
  	the_market->addNewIndicator(the_market, "close", parms.INDI_DAYS_TO_UPDATE, "close quotes", "quotes_daily");
	the_market->addNewIndicator(the_market, "high", parms.INDI_DAYS_TO_UPDATE, "high quotes", "quotes_daily");
	the_market->addNewIndicator(the_market, "low", parms.INDI_DAYS_TO_UPDATE, "low quotes", "quotes_daily");
 
	// get and store Indicator positions for easier access
 	const int closePos = the_market->getIndicatorPos(the_market, "close");
 	// const unsigned int openPos = the_market->getIndicatorPos(the_market, "open"); // nowhere used
	const int highPos = the_market->getIndicatorPos(the_market, "high");
	const int lowPos = the_market->getIndicatorPos(the_market, "low");	
    
    // check if enough data is available
    unsigned int nr_quotes_in_db = db_mysql_getNrOfQuotes(*the_market->symbol, *the_market->identifier, *the_market->Ind[closePos]->name, *the_market->Ind[closePos]->db_tablename);
    check(nr_quotes_in_db >= parms.INDI_DAYS_TO_UPDATE, "not enough quotes (%i) for market %s (%s) (vs. %i INDI_DAYS_TO_UPDATE)), check data/%s", error_nr_quotes, nr_quotes_in_db, bdata(*the_market->symbol), bdata(*the_market->identifier), parms.INDI_DAYS_TO_UPDATE, bdata(*the_market->csvfile));

	// query db for last symbol quotes
  	the_market->loadIndicatorDB(the_market, "open");
  	the_market->loadIndicatorDB(the_market, "close");
	the_market->loadIndicatorDB(the_market, "high");
	the_market->loadIndicatorDB(the_market, "low");
      
    // add and calculate indicator for returns
	the_market->addNewIndicator(the_market, "returns", parms.INDI_DAYS_TO_UPDATE, "returns in percent of 2 consequtive periods", "quotes_daily");
    the_market->copyIndicatorQuotes(the_market, "close", "returns");
    const int returnsPos = the_market->getIndicatorPos(the_market, "returns");
    the_market->Ind[returnsPos]->db_saveflag = true;
    the_market->Ind[returnsPos]->QuoteObj->nr_digits = 8;
    the_market->Ind[returnsPos]->QUOTEVEC[0] = 0.0;
    for(unsigned int i = 1; i < the_market->Ind[returnsPos]->NR_ELEMENTS; i++)
    {
        the_market->Ind[returnsPos]->QUOTEVEC[i] = (the_market->Ind[closePos]->QUOTEVEC[i] - the_market->Ind[closePos]->QUOTEVEC[i-1]) / the_market->Ind[closePos]->QUOTEVEC[i-1];
    }
	
    // set shortcuts for above indicators
    the_market->initIndicatorPositions(the_market);
	
	//  ATR/TR is needed by chandelier stops, TR also by ADX indicator
 	if(biseqcstr(parms.SL_TYPE, "chandelier") || 
		biseqcstr(parms.SIGNAL_REGIME_FILTER, "ADX") ||
        biseqcstr(parms.SL_TYPE, "atr"))
 	{
		the_market->addNewIndicator(the_market, "HH_atr_period", parms.INDI_DAYS_TO_UPDATE, "lowest low for INDI_ADX_PERIOD days", "indicators_daily");
		the_market->addNewIndicator(the_market, "LL_atr_period", parms.INDI_DAYS_TO_UPDATE, "lowest low INDI_ADX_PERIOD days", "indicators_daily");
		the_market->addNewIndicator(the_market, "TR", parms.INDI_DAYS_TO_UPDATE, "true range", "indicators_daily");
		the_market->addNewIndicator(the_market, "ATR", parms.INDI_DAYS_TO_UPDATE, "average true range", "indicators_daily");
		
		const int hh_atrPos = the_market->getIndicatorPos(the_market, "HH_atr_period");
		const int ll_atrPos = the_market->getIndicatorPos(the_market, "LL_atr_period");
		const int trPos = the_market->getIndicatorPos(the_market, "TR");
		const int atrPos = the_market->getIndicatorPos(the_market, "ATR");
		
		// fill indicators with some data
		the_market->copyIndicatorQuotes(the_market, "high", "HH_atr_period");
		the_market->copyIndicatorQuotes(the_market, "low", "LL_atr_period");
		the_market->copyIndicatorQuotes(the_market, "close", "TR");
		the_market->copyIndicatorQuotes(the_market, "close", "ATR");
		
		// calculate indicators
		highest_high(parms.SL_ATR_PERIOD, the_market->Ind[highPos], the_market->Ind[hh_atrPos]);
		lowest_low(parms.SL_ATR_PERIOD, the_market->Ind[lowPos], the_market->Ind[ll_atrPos]);
 		calc_true_range(the_market->Ind[highPos],the_market->Ind[lowPos], the_market->Ind[closePos], the_market->Ind[trPos]);

 		calc_average_true_range(parms.INDI_ADX_PERIOD, the_market->Ind[trPos], the_market->Ind[atrPos]);
		
		// set flag so indicators will be written to db if market->saveAllIndicatorsDB(market) is called
		the_market->Ind[hh_atrPos]->db_saveflag = true;
		the_market->Ind[ll_atrPos]->db_saveflag = true;
		the_market->Ind[trPos]->db_saveflag = true;
		the_market->Ind[atrPos]->db_saveflag = true;
	}
	
	if(biseqcstr(parms.SIGNAL_REGIME_FILTER, "ADX"))
 	{	
		the_market->addNewIndicator(the_market, "DMI", parms.INDI_DAYS_TO_UPDATE, "directional movement indicator", "none");
		the_market->addNewIndicator(the_market, "ADX", parms.INDI_DAYS_TO_UPDATE, "average directional index", "indicators_daily");
		the_market->addNewIndicator(the_market, "regime_filter", parms.INDI_DAYS_TO_UPDATE, "market regime filter", "indicators_daily");
		
		const int dmiPos = the_market->getIndicatorPos(the_market, "DMI");
		const int adxPos = the_market->getIndicatorPos(the_market, "ADX");
		const int regPos = the_market->getIndicatorPos(the_market, "regime_filter");
		const int trPos = the_market->getIndicatorPos(the_market, "TR");
		
		the_market->copyIndicatorQuotes(the_market, "close", "DMI");
		the_market->copyIndicatorQuotes(the_market, "close", "ADX");
		the_market->copyIndicatorQuotes(the_market, "close", "regime_filter");

		// calculate and store ADX
		calc_directional_movement_index(parms.INDI_ADX_PERIOD, 
			the_market->Ind[highPos], the_market->Ind[lowPos], 
			the_market->Ind[trPos], the_market->Ind[dmiPos], 
			the_market->Ind[adxPos]);
		
 		// now calculate and store market regime filter
 		ADX_regime_filter(the_market->Ind[adxPos], parms.SIGNAL_REGIME_ADX_THRESH, the_market->Ind[regPos]);
		
		// set flag so indicators will be written to db if market->saveAllIndicatorsDB(market) is called
		the_market->Ind[adxPos]->db_saveflag = true;
		the_market->Ind[regPos]->db_saveflag = true;
	}
	
	// load the first trend record if the market is configured as tradeable and trend tracking is configured
	if((the_market->tradeable) && parms.INDI_TRACK_TREND)
    {
        the_market->loadTrendStateDBbyIndex(the_market, 0);
    }
	
	return EXIT_SUCCESS;
    
error_nr_quotes:
    exit(1);
}

/* End of file */
