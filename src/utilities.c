/* utilities.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file utilities.c
 * @brief various helper routines that do not fit elsewhere
 *
 * This file contains helper routines, which do not fit in other parts of
 * the software and are widely used everywhere
 * @author Denis Zetzmann
 */

#include "utilities.h"

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns the number of digits of a string that contains a floating 
 * point number
 * 
 * This function returns the number of digits of a string that contains a floating 
 * point number
 * 
 * @param numberstring bstring that contains the number, e.g. "123.456"
 * @return unit with the nr. of digits (in above example 3)
*/
///////////////////////////////////////////////////////////////////////////////
unsigned int get_nrDigitsOfBString(bstring numberstring)
{
    unsigned int nr_digits = 0;   
    int digit_pos;

   if (0 > (digit_pos = bstrchr(numberstring, '.')))  // digit_pos stores position of '.'
    { 
    // Search for = character
        if (blength (numberstring)) 
            return 0;   // no decimal point found in string
    }
    
   bstring fraction = bmidstr(numberstring, digit_pos+1, blength(numberstring)); // copy everything right of the ". "
    
   unsigned int fraction_length = blength(fraction);
   nr_digits = fraction_length;
    
   for(int i=fraction_length-1; i>=0; i--)      // loop backwards through fraction string
   {
        bstring tmp_str = bmidstr(fraction, i, 1);  // copy current number in tmp_str
        if(biseqcstr(tmp_str, "0"))                 // is it a 0?
            nr_digits = nr_digits -1;               // yep, reduce nr_digits
        else
        {   
            bdestroy(tmp_str);                      // no, current i equals number of digits in fraction
            break;
        }
            
        bdestroy(tmp_str);
    }
    
    bdestroy(fraction);
    
    return (unsigned int) nr_digits;                // cast result to uint and return
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns nr. of digits of a tick/pip value
 * 
 * @return nr of digits as int
*/
///////////////////////////////////////////////////////////////////////////////
int get_nr_of_digits(float the_tick)
{
	if(the_tick==0)
		return 0;
	else if((the_tick*10)>= 1)
		return 1;
	else if((the_tick*100)>=1)
		return 2;
	else if((the_tick*1000)>=1)
		return 3;
	else if((the_tick*10000)>=1)
		return 4;
	else if((the_tick*100000)>=1)
		return 5;
	else return 0;
}

// end of file

