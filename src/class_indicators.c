/* class_indicators.c
 * Implements a "class"-like struct in C which handles indicators
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_indicators.c
 * @brief routines that implement a class-like struct which represents 
 * indicators
 *
 * This file contains the definitions of the indicator "class methods"
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "debug.h"
#include "class_indicators.h"
#include "bstrlib.h"
#include "arrays.h"
#include "database.h"

// declarations for functions that should not be called outside
// of this file, instead as "object method"
static void class_indicators_setMethods(class_indicators* self);
static void class_indicators_destroyImpl(class_indicators *self);
static class_indicators* class_indicators_cloneImpl(const class_indicators* original);
static void class_indicators_printTableImpl(const class_indicators *self);
static void class_indicators_loadQuotesImpl(class_indicators *self);
static void class_indicators_saveQuotesImpl(const class_indicators* self);


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for indicator objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods.
 * 
 * @return pointer to new object of class indicators
*/
///////////////////////////////////////////////////////////////////////////////
class_indicators* class_indicators_init(char* quote_symbol, char* identifier, unsigned int nr_quotes,  
				unsigned int nrOfDigits, char* indicator_name, 
				char* ind_descr, char* tablename)
{
    class_indicators* self;
    
    // allocate memory for object
    self = (class_indicators*) ZCALLOC(1, sizeof(class_indicators));
   
    //call base class constructor
    self->QuoteObj = class_quotes_init(quote_symbol, identifier, nr_quotes, nrOfDigits, "indicator", "none");
    
    //allocate memory for rest of the object
    self->name = init_1d_array_bstring(1);
    self->description = init_1d_array_bstring(1);
    self->db_tablename = init_1d_array_bstring(1);

    // init data   
    *self->name = bfromcstr(indicator_name);
    *self->description = bfromcstr(ind_descr);
    self->db_saveflag = false;
    *self->db_tablename = bfromcstr(tablename);
    
    // set methods
    class_indicators_setMethods(self);
    
    return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for indicator objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to indicator struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_indicators_setMethods(class_indicators* self)
{
    self->printTable = class_indicators_printTableImpl;
    self->destroy = class_indicators_destroyImpl;
    self->loadFromDB = class_indicators_loadQuotesImpl;
    self->saveToDB = class_indicators_saveQuotesImpl;
    self->clone = class_indicators_cloneImpl;
}
///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "destructor" function for indicator objects
 * 
 * This function frees the memory consumed by indicator objects
 * 
 * @param self pointer to indicator object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_indicators_destroyImpl(class_indicators *self)
{
   self->QuoteObj->destroy(self->QuoteObj);
   free_1d_array_bstring(self->name,1);
   free_1d_array_bstring(self->description, 1);
   free_1d_array_bstring(self->db_tablename,1);
   free(self);    
   self=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief prints symbol, daynr, date and quote of the object in tabular form
 * 
 * prints symbol, daynr, date and quote of the object in tabular form
 * 
 * @param self pointer to indicator object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_indicators_printTableImpl(const class_indicators* self)
{
    printf("\nIndicator %s for %s", bdata(*self->name), bdata(*self->QuoteObj->symbol));
	printf("\n(%s), %i elements",bdata(*self->description), self->QuoteObj->nr_elements);
	printf("\nidx\tdaynr\tdate\t\tquote\n");
	for(unsigned int i=0;i<self->QuoteObj->nr_elements;i++)
		printf("[%u]\t%u\t%s\t%.*f\n",i, self->QuoteObj->daynrvec[i], bdata(self->QuoteObj->datevec[i]),self->QuoteObj->nr_digits, self->QuoteObj->quotevec[i]);
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief wrapper for database access function to pull in indicator quotes
 * 
 * wrapper for database access function to pull in indicator quotes
 * 
 * @param self pointer to indicator object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_indicators_loadQuotesImpl(class_indicators* self)
{
    db_mysql_getquotes(*self->QuoteObj->symbol, *self->QuoteObj->identifier, *self->db_tablename, *self->name, self->QuoteObj->nr_elements, self->QuoteObj->quotevec, self->QuoteObj->datevec, self->QuoteObj->daynrvec);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief wrapper for database access function save indicator quotes
 * 
 * wrapper for database access function to save indicator quotes
 * 
 * @param self pointer to indicator object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_indicators_saveQuotesImpl(const class_indicators* self)
{
	if(!self->db_saveflag)
	{
	//	log_debug("won't save indicator %s, db_saveflag is set to 'false'!", bdata(*self->name));
	}
	else
		db_mysql_update_indicator(*self->QuoteObj->symbol, *self->QuoteObj->identifier,  self->QuoteObj->nr_digits, *self->name, *self->db_tablename, self->QuoteObj->nr_elements, self->QuoteObj->quotevec, self->QuoteObj->datevec, self->QuoteObj->daynrvec);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief clone indicator object (deep copy original into clone)
 * 
 * clone indicator object (deep copy original into clone), if original object gets
 * modified or destroy()ed, the clone will still live
 * 
 * @param original pointer to indicator object with original data
 * @returns pointer to cloned object
*/
////////////////////////////////////////////////////////////////////////////
static class_indicators* class_indicators_cloneImpl(const class_indicators* original)
{
	class_indicators* clone = NULL;
 	clone = class_indicators_init(bdata(*original->QuoteObj->symbol), bdata(*original->QuoteObj->identifier), original->QuoteObj->nr_elements, original->QuoteObj->nr_digits, bdata(*original->name), bdata(*original->description), bdata(*original->db_tablename));
	
	// free memory occupied by QuoteObj after init
	clone->QuoteObj->destroy(clone->QuoteObj);
	
	// clone the QuoteObj from the original indicator
	clone->QuoteObj = original->QuoteObj->clone(original->QuoteObj);
	
	return clone;
}

// eof
