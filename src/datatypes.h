/* datatypes.h
 * datatype definitions for ichinscratchy
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DATATYPES_H
#define DATATYPES_H

#include <stdbool.h>

#include "bstrlib.h"

///////////////////////////////////////////////////////////////////////////////
/**
 * @file datatypes.h
 * @brief datatype definitions
 *
 * This file defines the commonly used datatypes within ichinscratchy 
 * @author Denis Zetzmann
 */

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Stores the type of trend for Indicators
 * 
 * As markets can be in an up- or downtrend, or alternatively move sideways
 * the Indicators..eh..indicate that.
 */
enum _trend_type{
	uptrend,
	downtrend,
	sideways,
};
typedef enum _trend_type trend_type;
	
///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief Direction of signals and portfolio entries
 * 
 * This enum stores wheter a signal/portfolio entry is long or short. The value
 * undefined should occur only, if signals/portfolio entries are queried from
 * database without specifying which direction should be returned (this is used
 * only if direction does not matter for this particular use case
 */
enum _longshort{
	longsignal,	/**< signal/portfolio entry is "long" */
	shortsignal,	/**< signal/portfolio entry is "short" */
	undefined,	/**< should not occur unless specifically queried from db */
};
typedef enum _longshort signal_type;


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	strength of signal
 * 
 * This enum defines the strenth of a signal. Allowed values are
 * weak, neutral, strong. Generally it is assumed a strong signal has better 
 * signal quality than a neutral one, which in turn is better than weak.
 * The concept of signal strength was introduced implementing the ichimoku 
 * kinko hyo system but may be applied to other trading systems, too
 */
enum _strength_type{
	weak,		/**< weak signal */
	neutral,	/**< neutral signal */
	strong,		/**< strong */
};
typedef enum _strength_type strength_type;

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a signal and its describing properties
 * 
 * This struct describes all the properties that are needed to describe a 
 * signal. All values are generated within the program, most are stored in the
 * database and can retrieved from the database. The information of a signal 
 * forms the basis to the information a portfolio entry, if the signal is 
 * executed.
 */ 
struct _signal			
{
	bstring name;		/**< name of the signal */
	bstring date;		/**< date the signal occured */
	bstring symbol;		/**< market where the signal occured */
	signal_type type;	/**< long or short signal? @see signal_type */
	strength_type strength; /**< weak, neutral or strong signal? @see strength_type */
	bstring description;	/**< more detailed description of the signal */
	float price;		/**< price at which the signal occured */
	float indicator_quote;	/**< price the indicator had, when the signal occured */
	bstring amp_info;	/**< amplifying information, for example the trigger date */
	bstring db_tablename;	/**< name of the mysql db table that holds information */
	bool executed; 		/**< true if signal was executed, false if not (yet) */
};
typedef struct _signal signal; 

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief 	datetype of an indicator: standard, lagged or shifted
 * 
 * In the ichimoku system, there are 3 different types of indicators: the 
 * standard type which is identical to the "real world" dates, the lagging
 * type which is projected into the past and the shifted which is projected
 * into the future. * 
 */
enum _date_type{
	standard,	/**< standard (=equal to real world) dates */
	lagged,		/**< dates are projected into past */
	shifted,	/**< dates are projected into the future */
};
typedef enum _date_type date_type;

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief definition of an indicator and its describing properties
 * 
 * An indicator basicly is a combination of dates with corresponding values,
 * mostly prices (but not restricted to). Additional metadata is stored within
 * the struct to allow the better handling for ichinscratchy's routines.
 */
struct _indicator
{
	unsigned int days;	/**< number of entries (=days) in the date/price series */
	bstring name;		/**< name of the indicator*/
	bstring description;	/**< more detailed description of the indicator */
	bstring db_tablename;	/**< name of the mysql db table that holds information */
	date_type datetype;	/**< date type of the indicator, @see date_type */
	bstring *dates;		/**< bstring vector with the dates in format YYYY-MM-DD */
	unsigned int *daynrs; 	/**< uint vector with dates as nr of days since 01.01.1900 */
	float *quotes;		/**< float vector with the price/value of the indicator for the dates */
};
typedef struct _indicator indicator;

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Definition of an portfolio entry
 * 
 * This struct defines the information that constitutes a portfolio entry.
 * All elements are generated within the program, stored in the database and 
 * can be retrieved from it.
 */
struct _portfolio		
{
	bstring symbol;		/**< market symbol of the portfolio entry */
	bstring buydate;	/**< date that specific entry was bought */
	bstring signaldate;	/**< date the corresponding signal occured (can differ from buydate) */
	bstring signalname;	/**< name of the signal that triggered the buy */
	signal_type type;	/**< long or short entry, @see signal_type */
	float price_buy; 	/**< buying price in market´s currency */
	float price_last; 	/**< last price in market´s currency */
	float stoploss;		/**< current stop loss, will be updated regularly */
	bool isl;		/**< flag, if current stop loss is the initial one */
	float cost_per_item;	/**< price for one unit that was bought in account currency */
	float pos_size;		/**< weight factor that was determined during allocation process for this portfolio entry */
	float quantity;		/**< number of units that were bought */
	float p_l;		/**< current value of position after last program run (in account currency) */
	float sl_p_l;		/**< value of sl after last program run (in account currency)  */
	int trading_days;	/**< number of days the position is already active */
};
typedef struct _portfolio portfolio_entry;

#endif // DATATYPES_H

// end of file
