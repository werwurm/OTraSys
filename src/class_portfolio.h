/* class_portfolio.h
 * declarations for class_portfolio.c, interface for "portfolio_list" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_portfolio.h
 * @brief Header file for class_portfolio.c, public member declarations
 *
 * This file contains the "public" available data and functions of the 
 * portfolio_list "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_PORTFOLIO_H
#define CLASS_PORTFOLIO_H

#include <stdbool.h>

#include "class_account.h"
#include "class_quote.h"      // indicators inherit from quotes
#include "datatypes.h"
#include "class_portfolio_element.h"
#include "class_market_list.h"

struct _portfolio_private; 	/**< opaque forward declaration, this structs contains
				non-public/ private data/functions */
				
typedef struct _class_portfolio class_portfolio;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyPortfolioFunc)(struct _class_portfolio*);
typedef struct _class_portfolio*(*clonePortfolioFunc)(const struct _class_portfolio*);
typedef void (*printPortfolioTableFunc)(const struct _class_portfolio*); 
typedef void (*addPortfolioElementFunc)(struct _class_portfolio*, const void*);
typedef void (*addNewPortfolioElementFunc)(struct _class_portfolio*, const struct _class_market*, const enum _longshort, const float, const unsigned int, const unsigned int, const char*, const struct _class_accounts*, const int, const int);
typedef void (*removePortfolioElementFunc)(struct _class_portfolio*, unsigned int);
typedef unsigned int (*getNrPortfolioElementsFunc)(const struct _class_portfolio*);
typedef unsigned int (*getElementIndexFunc)(const struct _class_portfolio*, bstring*, bstring*, bstring*);
typedef void (*savePortfolioDBFunc)(const struct _class_portfolio*);
typedef void (*loadPortfolioDBFunc)(struct _class_portfolio*);
typedef bool (*containsSymbolFunc)(const struct _class_portfolio*, bstring*);
typedef unsigned int (*getNrElementsBySymbolFunc)(const struct _class_portfolio*, bstring*);
typedef void (*updatePortfolioEquityFunc)(struct _class_portfolio*);
typedef float (*getPortfolioEquityFunc)(const struct _class_portfolio*);
typedef float (*getPortfolioRiskFreeEquityFunc)(const struct _class_portfolio*);
typedef void (*setMarketListPositionsFunc)(struct _class_portfolio*, struct _class_market_list*);
//typedef float (*getWeightFactorFunc)(const struct _portfolio*, const struct _portfolio_element_2);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a portfolio and its describing properties
 * 
 * This struct describes the properties that form a portfolio. Mainly, a 
 * portfolio is a list of portfolio_elements, combined with some additional 
 * information (like nr of elements in portfolio) and methods to deal with the
 * portfolio.
 */
struct _class_portfolio
{
	//public part
	// DATA
	class_portfolio_element** elements; 	/**< inherited vector to members of portfolio elements */
	bstring* name;				/**< identifier of portfolio */ 
	
	// METHODS, call like: foo_obj->bar_method(foo_obj)
	destroyPortfolioFunc destroy; /**< "destructor", see class_portfolio_destroyImpl() */
	clonePortfolioFunc clone;	/**< create new object with same data as given one, see class_portfolio_clonePortfolioImpl() */
	printPortfolioTableFunc printTable;	/**< prints out quotes in tabular format, see class_portfolio_printTableImpl() */
	addPortfolioElementFunc addElement;		/**< add existing portfolio_element to portfolio, see class_portfolio_addElementImpl() */
	addNewPortfolioElementFunc addNewElement; 	/**< create and add new portfolio_element to portfolio, see class_portfolio_addNewElementImpl() */
	removePortfolioElementFunc removeElement;	/**< remove existing portfolio_element from portfolio, see class_portfolio_removeElementImpl() */
	getNrPortfolioElementsFunc getNrElements;	/**< returns nr. of portfolio entries, see class_portfolio_getNrElementsImpl() */
	getElementIndexFunc getElementIndex;	/**< returns index of specfied portfolio element, see  class_portfolio_getElementIndexImpl() */
	savePortfolioDBFunc saveToDB; 	/**< saves portfolio in database, see class_portfolio_savePortfolioDBImpl() */
	loadPortfolioDBFunc loadFromDB;    /**< loads portfolio from database, see class_portfolio_loadPortfolioDBImpl() */
	containsSymbolFunc containsSymbol; 		/**< true if Portfolio contains given symbol, false if not, see class_portfolio_containsSymbolImpl() */
	getNrElementsBySymbolFunc getNrElementsBySymbol; 	/** return nr of elements of a given symbol, see class_portfolio_getNrElementsPerSymbolImpl() */
	updatePortfolioEquityFunc updatePortfolioEquity;   /** calulate equity and risk free equity based on current portfolio entries, see class_portfolio_updatePortfolioImpl() */
	getPortfolioEquityFunc getPortfolioEquity;  /** get current value of portfolio, see class_portfolio_getPortfolioEquityImpl() */
	getPortfolioRiskFreeEquityFunc getPortfolioRiskFreeEquity; /** get value of portfolio if sold at current SL´s, see class_portfolio_getPortfolioRiskFreeEquityImpl() */
	setMarketListPositionsFunc	setMarketListPositions;		/**< determines and sets all positions of portfolio_elements markets within a given market_list, see class_portfolio_setMarketListPositionsImpl() */
	
//	getWeightFactorFunc getWeightFactor; /**< calculate weight factor of a new portfolio_element for existing elements in portfolio */
	//private part
	struct _portfolio_private *portfolio_private;	/**< opaque pointer to private data and functions */
	// ... add methods for "update XY for all portfolio_elements
};

// "constructor" for portfolio "objects"
class_portfolio* class_portfolio_init(char* name);

#endif // CLASS_PORTFOLIO_H

// eof
