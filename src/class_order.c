/* class_order.c
 * Implements a "class"-like struct in C which handles orders
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_order.c
 * @brief routines that implement a class-like struct which represents 
 * orders
 *
 * This file contains the definitions of the "class" portfolio_elements
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "arrays.h"
#include "bstrlib.h"
#include "debug.h"

#include "constants.h"
#include "class_account.h"
#include "class_order.h"
#include "class_quote.h"
#include "datatypes.h"

static void class_order_setMethods(class_order* self);
static void class_order_destroyOrderImpl(class_order* self);
static class_order* class_order_cloneOrderImpl(const class_order* original);
static void class_order_printOrderTableImpl(const class_order* self); 

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief automatic "constructor" function for order objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. Basis for the order will be an existing
 * portfolio entry
 * 
 * @param portfolio_entry ptr to class_portfolio_element object
 * @param ordertype buy/sell
 
 * @return pointer to new object of class order
*/
////////////////////////////////////////////////////////////////////////////////
class_order* class_order_init(const class_portfolio_element* portfolio_entry, order_type ordertype, float fee)
{
	class_order *self = NULL;
	
	// allocate memory for object
	self = (class_order*) ZCALLOC(1, sizeof(class_order));

	// clone quote objects from portfolio element, depending on ordertype
	self->Buyquote_signal = portfolio_entry->Buyquote_signal->clone(portfolio_entry->Buyquote_signal);
	switch(ordertype)
	{
		case buyorder:			
			self->quote_market = portfolio_entry->Buyquote_market->clone(portfolio_entry->Buyquote_market);
			self->quote_account = portfolio_entry->Buyquote_account->clone(portfolio_entry->Buyquote_account);
			break;
		case sellorder:
			self->quote_market = portfolio_entry->currQuote_market->clone(portfolio_entry->currQuote_market);
			self->quote_account = portfolio_entry->currQuote_account->clone(portfolio_entry->currQuote_account);
			break;
		default:
			break;
	}
	
 	//allocate memory for rest of the object
	self->buydate = init_1d_array_bstring(1);
	self->selldate = init_1d_array_bstring(1);
  	self->symbol = init_1d_array_bstring(1);
    self->identifier = init_1d_array_bstring(1);
 	self->markettype = init_1d_array_bstring(1);
  	self->signalname = init_1d_array_bstring(1);
 
 	//init data
  	*self->symbol = bstrcpy(*portfolio_entry->symbol);
    *self->identifier = bstrcpy(*portfolio_entry->identifier);
 	*self->markettype = bstrcpy(*portfolio_entry->markettype);
  	*self->signalname = bstrcpy(*portfolio_entry->signalname);
	self->signaltype = portfolio_entry->type;
	self->stoploss = portfolio_entry->stoploss;
	self->pos_size = portfolio_entry->pos_size;
	self->quantity = portfolio_entry->quantity;
    self->fee = fee;
    
	// init data that depends on type of order
	switch(ordertype)
	{
		case buyorder:
			*self->buydate = bstrcpy(*portfolio_entry->Buyquote_account->datevec);
			self->ordertype = buyorder;
			self->trading_days = 1;
			self->p_l_total = 0.0;
			self->p_l_piece = 0.0;
			self->p_l_percent = 0.0;
			break;
		case sellorder:
			*self->buydate = bstrcpy(*portfolio_entry->Buyquote_account->datevec);
			*self->selldate = bstrcpy(*portfolio_entry->currQuote_account->datevec);
			self->ordertype = sellorder;
			self->trading_days = portfolio_entry->trading_days;
			self->p_l_total = portfolio_entry->p_l;
			self->p_l_piece = portfolio_entry->p_l / portfolio_entry->quantity;
			self->p_l_percent = portfolio_entry->p_l_percent;
	}
 	// set methods
    class_order_setMethods(self);
 	
	return self;	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief manual "constructor" function for order objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. 
 * 
 * @return pointer to new object of class order
*/
////////////////////////////////////////////////////////////////////////////////
class_order* class_order_init_manually(char* buydate, char* selldate, char* symbol, char* identifier, char* markettype, char* signalname, order_type ordertype, 
                                       class_quotes* quote_market, class_quotes* quote_account, class_quotes* Buyquote_signal, signal_type signaltype,
                                       unsigned int trading_days, float stoploss, float pos_size, float quantity, float p_l_total, float p_l_piece, float p_l_percent, float fee)
{
    class_order *self = NULL;
	
	// allocate memory for object
	self = (class_order*) ZCALLOC(1, sizeof(class_order));
    
    // clone inherited objects
    self->Buyquote_signal = Buyquote_signal->clone(Buyquote_signal);
    self->quote_account = quote_account->clone(quote_account);
    self->quote_market = quote_market->clone(quote_market);
    
    // allocate memory for strings in object 
 	self->buydate = init_1d_array_bstring(1);
	self->selldate = init_1d_array_bstring(1);
  	self->symbol = init_1d_array_bstring(1);
    self->identifier = init_1d_array_bstring(1);
 	self->markettype = init_1d_array_bstring(1);
  	self->signalname = init_1d_array_bstring(1);
    
    // init data
    *self->buydate = bfromcstr(buydate);
    *self->selldate = bfromcstr(selldate);
    *self->symbol = bfromcstr(symbol);
    *self->identifier = bfromcstr(identifier);
    *self->markettype = bfromcstr(markettype);
    *self->signalname = bfromcstr(signalname);
    self->ordertype = ordertype;
    self->signaltype = signaltype;
    self->trading_days = trading_days;
    self->stoploss = stoploss;
    self->pos_size = pos_size;
    self->quantity = quantity;
    self->p_l_total = p_l_total;
    self->p_l_piece = p_l_piece;
    self->p_l_percent = p_l_percent;
    self->fee = fee; 
    
  	// set methods
    class_order_setMethods(self);
    
    return self;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for order objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to order struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_order_setMethods(class_order* self)
{
 	self->destroy = class_order_destroyOrderImpl;
 	self->clone = class_order_cloneOrderImpl;
 	self->printTable = class_order_printOrderTableImpl;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys an order object
 *
 * This function destroys an order struct by freeing the occupied 
 * memory
 * 
 * @param self pointer to order struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_order_destroyOrderImpl(class_order* self)
{
 	self->quote_market->destroy(self->quote_market);
 	self->quote_account->destroy(self->quote_account);
 	self->Buyquote_signal->destroy(self->Buyquote_signal);

	free_1d_array_bstring(self->buydate, 1);
	free_1d_array_bstring(self->selldate,1);
 	free_1d_array_bstring(self->symbol, 1);
    free_1d_array_bstring(self->identifier, 1);
 	free_1d_array_bstring(self->signalname, 1);
 	free_1d_array_bstring(self->markettype, 1);
	free(self); 
    self=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone an order object
 *
 * clone complete order object (deep copy original into clone), if 
 * original object gets modified or destroy()ed, the clone will still live
 * @param original order pointer to original 
 * @returns order pointer to object that will hold copy of original
 */
///////////////////////////////////////////////////////////////////////////////
static class_order* class_order_cloneOrderImpl(const class_order* original)
{
	class_order* newclone = NULL;
	
	// allocate memory for object
	newclone = (class_order*) ZCALLOC(1, sizeof(class_order));

	// clone quote objects from portfolio element, depending on ordertype
	newclone->Buyquote_signal = original->Buyquote_signal->clone(original->Buyquote_signal);
	newclone->quote_account = original->quote_account->clone(original->quote_account);
	newclone->quote_market = original->quote_market->clone(original->quote_market);
	
 	//allocate memory for rest of the object
	newclone->buydate = init_1d_array_bstring(1);
	newclone->selldate = init_1d_array_bstring(1);
  	newclone->symbol = init_1d_array_bstring(1);
    newclone->identifier = init_1d_array_bstring(1);
 	newclone->markettype = init_1d_array_bstring(1);
  	newclone->signalname = init_1d_array_bstring(1);
	
	// copy rest of the data
	*newclone->buydate = bstrcpy(*original->buydate);
	*newclone->selldate = bstrcpy(*original->selldate);
  	*newclone->symbol = bstrcpy(*original->symbol);
    *newclone->identifier = bstrcpy(*original->identifier);
 	*newclone->markettype = bstrcpy(*original->markettype);
  	*newclone->signalname = bstrcpy(*original->signalname);
	newclone->signaltype = original->signaltype;
	newclone->stoploss = original->stoploss;
	newclone->pos_size = original->pos_size;
	newclone->quantity = original->quantity;
	newclone->ordertype = original->ordertype;
	newclone->trading_days = original->trading_days;
	newclone->p_l_total = original->p_l_total;
	newclone->p_l_piece = original->p_l_piece;
	newclone->p_l_percent = original->p_l_percent;
	newclone->fee = original->fee;
    
 	// set methods
    class_order_setMethods(newclone);
	
	return newclone;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints order info
 *
 * This function prints order info to terminal
 * 
 * @param self pointer to order struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_order_printOrderTableImpl(const class_order* self)
{
    bstring typestring=NULL;
	switch(self->signaltype)
	{
		case longsignal:
			typestring=bfromcstr("long");
			break;
		case shortsignal:
			typestring=bfromcstr("short");
			break;
		case undefined:
		default: //should not happen
			typestring=bfromcstr("undefined");			
			break;
	}
	
	bstring orderstring=NULL;
	switch(self->ordertype)
	{
		case buyorder:
			orderstring=bfromcstr("buy ");
			break;
		case sellorder:
			orderstring=bfromcstr("sell");
			break;
		default:	//should not happen
			typestring=bfromcstr("undefined");			
			break;
	}
 	
 	// different terminal output depending on buy- or sellorder
 	switch(self->ordertype)
	{
		case buyorder:
			printf("\n[%s] %s %s (%s) %s@%.*f (%s@%.*f) initialSL: %.*f, quant: %.2f (fee: %.2f)",
				bdata(orderstring), bdata(*self->buydate),
				bdata(*self->symbol), bdata(*self->identifier), bdata(typestring),
				self->quote_market->nr_digits, *self->quote_market->quotevec,
				bdata(*self->signalname), self->Buyquote_signal->nr_digits, *self->Buyquote_signal->quotevec,
				self->quote_market->nr_digits, self->stoploss,
				self->quantity,
                self->fee);
			break;
		case sellorder:
        {
            if(self->p_l_total < 0)
                printf("\n[%s] %s %s (%s) %s@%.*f (buydate %s) SL: %.*f, quant: %.2f, P/L: "ANSI_COLOR_RED"%.*f"ANSI_COLOR_RESET" (fee: %.2f)",
                    bdata(orderstring), bdata(*self->selldate),
                    bdata(*self->symbol), bdata(*self->identifier),  bdata(typestring),
                    self->quote_market->nr_digits, *self->quote_market->quotevec,
                    bdata(*self->buydate), 
                    self->quote_market->nr_digits, self->stoploss,
                    self->quantity,
                    self->quote_account->nr_digits, self->p_l_total, self->fee);
            else   
                    printf("\n[%s] %s %s (%s) %s@%.*f (buydate %s) SL: %.*f, quant: %.2f, P/L: "ANSI_COLOR_YELLOW"%.*f"ANSI_COLOR_RESET" (fee: %.2f)",
                    bdata(orderstring), bdata(*self->selldate),
                    bdata(*self->symbol), bdata(*self->identifier), bdata(typestring),
                    self->quote_market->nr_digits, *self->quote_market->quotevec,
                    bdata(*self->buydate), 
                    self->quote_market->nr_digits, self->stoploss,
                    self->quantity,
                    self->quote_account->nr_digits, self->p_l_total, self->fee);
			break;
        }
		default:
			break;
	}

	bdestroy(typestring);
	bdestroy(orderstring);
}

// eof
