/* database.c
 * Collection of routines to access a mysql database
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file database.c
 * @brief routines for accessing the mysql database
 *
 * This file defines the functions that access the mysql database for storing
 * and retrieving data. In future releases this might be abstracted to allow 
 * using different database systems. 
 * @see for the documentation of the database structure, see 
 * /docs/databse_structure.md
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "debug.h"
#include "constants.h"
#include "database.h"
#include "readconf.h"
#include "date.h"
#include "datatypes.h"
#include "utilities.h"
#include "class_portfolio_element_cfdcur.h"
#include "class_portfolio_element_cfdfut.h"
#include "class_portfolio_element_stocks.h"

// global handler for mysql connection
MYSQL *mysql;
MYSQL *quote_db;

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief check for errors after any interaction with myqsl server
 * 
 * Check for errors after any interaction with myqsl server. This routine is
 * called with information of the calling function as parameter, to allow
 * easier debugging in case of error.
 * @param functionname bstring containing the name of the calling function
 * @param querystring bstring containing the full querystring
*/
///////////////////////////////////////////////////////////////////////////////
void check_mysql_error(bstring functionname, bstring querystring)
{
	if (mysql_errno(mysql) != 0)
	{
		fprintf(stdout, ANSI_COLOR_RED"\n\nMySQL error %u: %s", \
			mysql_errno(mysql), mysql_error(mysql));
		fprintf(stdout, "\noccured in %s using querystring: "ANSI_COLOR_RESET, 
		        bdata(functionname));
		fprintf(stdout, "\n%s\n\n", bdata(querystring));
		exit(1);
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief establishes connection to mysql server
 * 
 * establishes connection to mysql server
 * @todo change function and callers to utilize bstrings
 * 
 * @param host string with the hostname/ IP adress
 * @param username string with user of database
 * @param password string with password of given user
 * @param dbname string with database name
 * @param port string with port number
 * @param socket string with socket number
 * @param flags string with additional/special  server flags
*/
///////////////////////////////////////////////////////////////////////////////
void connect_mysql_database(MYSQL *db_handle, char *host, char *username, char *password,\
	char *dbname, int port, char *socket, int flags)
{
	bstring errormsg= bfromcstr("connect_mysql_database()");
	bstring functionname= bfromcstr("mysql_real_connect()");
	check_mysql_error(errormsg, functionname);
	
	/* make sure import of local csv files is allowed, done via
	   MYSQL_OPT_LOCAL_INFILE 
	   this is equivalent to commandline option --local-infile
	*/
	mysql_options(db_handle,MYSQL_OPT_LOCAL_INFILE,0);

	mysql_real_connect(
	    db_handle,
	    host,
	    username,
	    password,
	    dbname,
	    port, 
	    socket,
		flags);
	
	bdestroy(errormsg);
	bdestroy(functionname);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief creates database for program
 * 
 * creates database and user with access to it (both are specified in
 * file. Then all neccessary tables are created. This function is called
 * if program is started with -s/--setup_db command line option
 * 
 * @todo change function and callers to utilize bstrings
 * 
 * @param host string with the hostname/ IP adress
 * @param username string with user of database
 * @param password string with password of given user
 * @param dbname string with database name
 * @param port string with port number
 * @param socket string with socket number
 * @param flags string with additional/special  server flags
*/
///////////////////////////////////////////////////////////////////////////////
void mysql_create_db(char *host, char *username, char *password,\
	char *dbname, int port, char *socket, int flags)
{
	extern parameter parms; /* parms are declared global in main.c */
	mysql=mysql_init(mysql);
	check_mysql_error(bfromcstr("mysql_create_db()"), 
	                  bfromcstr("mysql_init()"));
	
	bstring querystring;
	bstring db_name;
	
	db_name = bfromcstr(dbname);

	mysql_real_connect(
		mysql,   /* Pointer to MYSQL-handler */
		host,
		username,
		password,
		NULL,
		port, 
		socket,
		flags);

    bstring funcname = bfromcstr("connect_mysql_database()");
    bstring tmpstring = bfromcstr("mysql_real_connect()");
    
	check_mysql_error(funcname, tmpstring);
    bdestroy(tmpstring);
    
	
	// Delete database
	querystring=bfromcstr("DROP DATABASE IF EXISTS ");
	bconcat(querystring, db_name);
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
	
	// recreate database
	querystring=bfromcstr("CREATE DATABASE ");
	bconcat(querystring, db_name);
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
	
	// create user if not exists
	querystring=bfromcstr("CREATE USER '");
	bconcat(querystring, parms.DB_SYSTEM_USERNAME);
	bconcat(querystring, bfromcstr("'@'"));
	bconcat(querystring, bfromcstr(host));
	bconcat(querystring, bfromcstr("' IDENTIFIED BY '"));
	bconcat(querystring, parms.DB_SYSTEM_PASSWORD);
	bconcat(querystring, bfromcstr("';"));
	// printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
	// handle this errorcheck different from the rest
	// we do not want to quit programm if user already exists
	if (mysql_errno(mysql) != 0)
	{
		fprintf(stdout, ANSI_COLOR_YELLOW"error %u: %s\n", \
			mysql_errno(mysql), mysql_error(mysql));
		fprintf(stdout, "most likely this is not an issue as the user seems to exist already\n"ANSI_COLOR_RESET);
	}
		
	// grant privileges to user
	querystring=bfromcstr("GRANT ALL PRIVILEGES ON ");
 	bconcat(querystring, db_name);
 	bconcat(querystring, bfromcstr(".* TO "));
 	bconcat(querystring, parms.DB_SYSTEM_USERNAME);
	// printf("\n'%s'", bdata(querystring));
	bconcat(querystring, bfromcstr(" IDENTIFIED BY '"));
	bconcat(querystring, parms.DB_SYSTEM_PASSWORD);
	bconcat(querystring, bfromcstr("';"));
 	mysql_query(mysql, bdata(querystring));
	check_mysql_error(funcname, querystring); 
    
	querystring=bfromcstr("USE ");
	bconcat(querystring, db_name);
	mysql_query(mysql, bdata(querystring));
    check_mysql_error(funcname, querystring); 
    bdestroy(querystring);
    
	// Create account table
	querystring=bfromcstr("CREATE TABLE `account` (`variable_name` char(30) NOT NULL, `value` decimal(16,");
    bconcat(querystring, bformat("%i", parms.ACCOUNT_CURRENCY_DIGITS));
    bconcat(querystring, bfromcstr("), PRIMARY KEY (`variable_name`));"));
//	printf("\n%s", bdata(querystring));
 	mysql_query(mysql, bdata(querystring));
    check_mysql_error(funcname, querystring); 
 	querystring=bfromcstr("INSERT INTO account (variable_name, value) VALUES ('cash',");
 	bconcat(querystring, bformat("%.2f", parms.STARTING_BALANCE));
 	bconcat(querystring, bfromcstr(") , ('equity', "));
	bconcat(querystring, bformat("%.2f", 0));			       
	bconcat(querystring, bfromcstr(") , ('risk_free_equity', "));
	bconcat(querystring, bformat("%.2f", 0));	
    bconcat(querystring, bfromcstr(") , ('sum_fees', ")); 
    bconcat(querystring, bformat("%.*f", parms.ACCOUNT_CURRENCY_DIGITS,0));
    bconcat(querystring, bfromcstr(") , ('all_time_high', ")); 
    bconcat(querystring, bformat("%.*f", parms.ACCOUNT_CURRENCY_DIGITS,0));
	bconcat(querystring, bfromcstr(") , ('virgin_flag', 1.0);"));
    
// 	printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
    bdestroy(querystring);
	
	// Create table for indicators
	querystring=bfromcstr("CREATE TABLE `indicators_daily` ( `date` date NOT NULL, `daynr` int, `symbol` char(40) NOT NULL, `identifier` char(20), `HH_short` decimal(16,6), `LL_short` decimal(16,6), `HH_mid` decimal(16,6),`LL_mid` decimal(16,6),`HH_long` decimal(16,6),`LL_long` decimal(16,6), `HH_atr_period` decimal(16,6), `LL_atr_period` decimal(16,6), `TR` decimal(16,6),`ATR` decimal(16,6), `ADX` decimal(16,6), `regime_filter` decimal(16,6), PRIMARY KEY (`date`,`symbol`));");
	//printf("\n%s", bdata(querystring));
 	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
    bdestroy(querystring);
    
	// Create table for ichimoku indicators
	querystring=bfromcstr("CREATE TABLE `ichimoku_daily` (`date` date NOT NULL, `daynr` int, `symbol` char(40) NOT NULL, `identifier` char(20), `tenkan` decimal(16,6),`kijun` decimal(16,6), `chikou` decimal(16,6), `senkou_A` decimal(16,6), `senkou_B` decimal(16,6), `tni` decimal(16,6), PRIMARY KEY (`date`,`symbol`));");
	//printf("\n%s", bdata(querystring));
 	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
    bdestroy(querystring);
	
	// Create table for signals
	querystring=bfromcstr("CREATE TABLE `ichimoku_daily_signals` (`date` date NOT NULL, `daynr` int, `symbol` char(40) NOT NULL, `identifier` char(20), `name` char(30), `type` char(5), `strength` char(10), `description` char(60), `amp_info` char(30), `signal_quote` decimal(16,6), `price_quote` decimal(16,6), `executed` bool, PRIMARY KEY (`date`,`symbol`,`name`));");
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
    bdestroy(querystring);
    
    // create table for portfolio
	querystring=bfromcstr("CREATE TABLE `portfolio` (`symbol` char(40) NOT NULL, `identifier` char(20), `markettype` char(6) NOT NULL, "
			"`currency_market` char(3) NOT NULL, `buyquote_market` decimal(16,6) NOT NULL, "
			"`buydate_market` date NOT NULL, `buydaynr_market` int NOT NULL, `buyquote_account` decimal(16,6) NOT NULL,"
			"`buydate_account` date NOT NULL, `buydaynr_account` int NOT NULL, `buyquote_signal` decimal(16,6) NOT NULL,"
			"`buydate_signal` date NOT NULL, `buydaynr_signal` int NOT NULL, `currquote_market` decimal(16,6) NOT NULL,"
			"`currdate_market` date NOT NULL, `currdaynr_market` int NOT NULL,"
			"`currquote_account` decimal(16,6) NOT NULL, `currdate_account` date NOT NULL,"
			"`currdaynr_account` int NOT NULL, `type` char(5) NOT NULL, `signalname` char(30) NOT NULL,"
			"`stoploss` decimal(16,6) NOT NULL, `initial_sl` int NOT NULL, `pos_size` decimal(10,2) NOT NULL,"
			"`quantity` decimal(10,2) NOT NULL, `trading_days` int NOT NULL,"
			"`current_value` decimal(16,6), `invested_value` decimal(16,6), `risk_free_value` decimal(16,6),"
			"`p_l` decimal(16,6), `risk_free_p_l` decimal(16,6), `p_l_percent` decimal(10,2), `equity` decimal(16,6), `rf_equity` decimal(16,6),"
			"PRIMARY KEY (`symbol`, `buydate_market`,`buydate_signal`,`signalname`,`type`,`quantity`));");
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
		
	// create table for orderbook
	querystring=bfromcstr("CREATE TABLE `orderbook_daily` (`date` date NOT NULL, `symbol` char(40) NOT NULL, `identifier` char(20), `type` char(5) NOT NULL, `buy_sell` char(4) NOT NULL, `price` decimal(16,6), `signalname` char(30) NOT NULL, `cost_per_item` decimal(16,2), `pos_size` decimal(16,2),`quantity` decimal(16,2), `buydate` date, `signaldate` date NOT NULL, `hold_days` int, `comission` decimal(16,2), `stoploss` decimal(16,6), `P_L_total` decimal(16,2), `P_L_piece` decimal(16,2), `P_L_percent` decimal(16,2), `fee` decimal(12,6), PRIMARY KEY (`date`,`signaldate`,`symbol`, `price`, `type`,`buy_sell`, `signalname`));");
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
	
	//create table for stop loss records
	querystring=bfromcstr("CREATE TABLE `stoploss_record` (`date` date NOT NULL, `daynr` int NOT NULL, `symbol` CHAR(10) NOT NULL, `identifier` char(20), `buydate` date NOT NULL, `selldate` date, `sl_type` CHAR(10) NOT NULL, `sl` decimal(16,6) NOT NULL, PRIMARY KEY (`symbol`, `buydate`, `sl_type`, `date`));");
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
    
    // create table for performance records
    querystring = bfromcstr("CREATE TABLE `performance_record` (`date` date NOT NULL, `symbol` CHAR(11), `identifier` char(20), `daynr` int, `cash` decimal(16,");
    bconcat(querystring, bformat("%i", parms.ACCOUNT_CURRENCY_DIGITS));
    bconcat(querystring, bfromcstr("), `equity` decimal(16,"));
    bconcat(querystring, bformat("%i", parms.ACCOUNT_CURRENCY_DIGITS));
    bconcat(querystring, bfromcstr("), `risk_free_equity` decimal(16,"));
    bconcat(querystring, bformat("%i", parms.ACCOUNT_CURRENCY_DIGITS));
    bconcat(querystring, bfromcstr("), `total` decimal(16,"));
    bconcat(querystring, bformat("%i", parms.ACCOUNT_CURRENCY_DIGITS));
    bconcat(querystring, bfromcstr("), `total_high` decimal(16,"));
    bconcat(querystring, bformat("%i", parms.ACCOUNT_CURRENCY_DIGITS));
    bconcat(querystring, bfromcstr("), PRIMARY KEY (`date`));"));
	//printf("\n%s", bdata(querystring));
	mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring);    
    
    // create table for market trend records
    querystring = bfromcstr("CREATE TABLE `market_trend_record` (`date` date NOT NULL, `symbol` CHAR(40), `identifier` CHAR(20), `trend` CHAR(9), `duration` int, `daynr` int, `price_quote` decimal(16,6), PRIMARY KEY (`date` , `symbol` , `identifier`));");
   //printf("\n%s", bdata(querystring));
    mysql_query(mysql, bdata(querystring));
 	check_mysql_error(funcname, querystring); 
    
	bdestroy(querystring);
	bdestroy(db_name);
    bdestroy(funcname);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief close connection to myqsl server
 * 
 * check for errors after any interaction with myqsl server
 * 
*/
///////////////////////////////////////////////////////////////////////////////
void db_close_mysql_connection(MYSQL *handle)
{
	mysql_close(handle);
	mysql_library_end();
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief Retrieves quotes from database
 * 
 * Queries the database for the quotes of a specific indicator and loads them
 * into float array
 * 
 * @code
 *	   SELECT daynr,date,quotetype FROM (SELECT * FROM db_table 
 *        WHERE symbol='XYZ' AND identifier='ZYX' ORDER BY date DESC LIMIT daynrs)     
 *        sub ORDER BY date ASC 
 * @endcode
 * 
 * @param symbol bstring with symbol to be queried
 * @param db_table bstring with the db table name
 * @param quotetype bstring with the type of quote (e.g. open/low/high/close)
 * @param nr_quotes integer with the number to be queried/stored
 * @param quotes pointer to float* array where the quotes will be stored
 * @param dates ptr to bstring* array which stores dates (YYYY-MM-DD) 
 * @param daynrs ptr to uint array which stores daynrs
*/
///////////////////////////////////////////////////////////////////////////////
void db_mysql_getquotes(bstring symbol, bstring identifier, bstring db_table, bstring quotetype, int nr_quotes, float *quotes, \
		bstring *dates, unsigned int *daynrs) // new implementation, leakfree
{
    extern MYSQL *system_db_handle;
    extern MYSQL *quote_db_handle;
    
	unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;

     /* prepare statement in query:   
	   SELECT daynr,date,quotetype FROM (SELECT * FROM db_table 
         WHERE symbol='XYZ' ORDER BY date DESC LIMIT daynrs)     
         sub ORDER BY date ASC 
     */
	
    // select db based on the type of quotes:
    // open, close, high, low are stored in PARMS.DB_QUOTES_NAME
    // everything else in PARMS.DB_SYSTEM_NAME
    MYSQL *the_handle = NULL;
    if(biseqcstr(quotetype, "open") ||
        biseqcstr(quotetype, "close") ||
        biseqcstr(quotetype, "high") ||
        biseqcstr(quotetype, "low"))
    {
        the_handle = quote_db_handle;
    }
    else
    {
        the_handle = system_db_handle;
    }
     
	bstring query1,query2,query3,query4;
    bstring query2_5; 

// 	// create query string with query
	bstring querystring=bfromcstr("SELECT daynr, date, ");
	bconcat(querystring, quotetype);
	query1 = bfromcstr(" FROM (SELECT * from ");
	bconcat(querystring, query1);
	bconcat(querystring, db_table);
	query2 = bfromcstr(" WHERE symbol='");
	bconcat(querystring, query2);
	bconcat(querystring, symbol);
    query2_5 = bfromcstr("' AND identifier='");
    bconcat(querystring, query2_5);
    bconcat(querystring, identifier);
	query3 = bfromcstr("' ORDER BY DATE DESC LIMIT ");
 	bconcat(querystring, query3);
	
	/* convert int ICHI_DAYS_TO_ANALYZE to bstring
	   (mysql_query in mysql_getquotes expects string) */
 	bstring nrdays=bformat("%d",nr_quotes);

// 	// append nrdays to query
 	bconcat(querystring, nrdays);
	
// 	// append last part of query
	query4 = bfromcstr(") sub ORDER BY date ASC");
 	bconcat(querystring, query4);	
	
	// Query the server
	// uncomment to see query statement
//  	fprintf(stdout, "%s\n",bdata(querystring));
 	mysql_query(the_handle, bdata(querystring));
	bstring functionname = bfromcstr("db_mysql_getquotes");
 	check_mysql_error(functionname, querystring);
	
// 	/* get results */
 	result = mysql_store_result(the_handle);
	
	bstring errormsg = bfromcstr("none");
 	check_mysql_error(functionname, errormsg);
	bdestroy(functionname);
	bdestroy(errormsg);
// 
// 	/* loop through all rows of result*/
 	while((row = mysql_fetch_row(result)) !=0)
 	{
 	/* for each row extract field results (change from string to float) */
 		daynrs[row_nr] = atoi(row[0]);
		dates[row_nr] = bfromcstr(row[1]);
		CHECK_ATOF_NULL(quotes[row_nr], row[2]);
		row_nr++;
 	}

 	mysql_free_result(result);
	bdestroy(querystring);
	bdestroy(query1);
	bdestroy(query2);
    bdestroy(query2_5);
	bdestroy(query3);
	bdestroy(query4);
	bdestroy(nrdays);
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// returns the total nr. of quotes within a given table for a specific symbol
unsigned int db_mysql_getNrOfQuotes(bstring symbol, bstring identifier, bstring quotetype, bstring db_table)
{
    extern MYSQL *system_db_handle;
    extern MYSQL *quote_db_handle;
    
    // select db based on the type of quotes:
    // open, close, high, low are stored in PARMS.DB_QUOTES_NAME
    // everything else in PARMS.DB_SYSTEM_NAME
    MYSQL *the_handle = NULL;
    if(biseqcstr(quotetype, "open") ||
        biseqcstr(quotetype, "close") ||
        biseqcstr(quotetype, "high") ||
        biseqcstr(quotetype, "low"))
    {
        the_handle = quote_db_handle;
    }
    else
    {
        the_handle = system_db_handle;
    }
    
    MYSQL_ROW	row;
    MYSQL_RES	*result;
	unsigned int number=0;
	
	/* prepare query statement:
        select count(*) from 
        db_table where symbol="xyz"; */
    
    bstring tmp1, tmp2;
    
	bstring querystring;
	querystring = bfromcstr("SELECT COUNT(*) from ");
    bconcat(querystring, db_table);
    tmp1 = bfromcstr(" where symbol='");
    bconcat(querystring, tmp1);
    bconcat(querystring, symbol);
    bstring tmp3 = bfromcstr("' AND identifier='");
    bconcat(querystring, tmp3);
    bconcat(querystring, identifier);
    tmp2 = bfromcstr("';");
    bconcat(querystring, tmp2);
	
	// uncomment to see query statement
// 	fflush(stdout);  printf("\n%s", bdata(querystring));	
	
	mysql_query(the_handle, bdata(querystring));
    bstring functionname = bfromcstr("db_mysql_getNrOfQuotes()");
    bstring errormsg = bfromcstr("none");
 	check_mysql_error(functionname, errormsg);
	bdestroy(functionname);
	bdestroy(errormsg);
	
	// store the result (we don't really need the result itself)
	result = mysql_store_result(the_handle);
    
    // loop through all rows of result
	while((row = mysql_fetch_row(result)) !=0)
	{
		if (row[0]!=0)	/* row[0] holds daily open */
			number = atoi(row[0]);
	}

	mysql_free_result(result);
	bdestroy(querystring);
    bdestroy(tmp1);
    bdestroy(tmp2);
	
	return number;    
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief updates database with latest quotes, stored in csv file.
 * 
 * This function uses mySQLs build-in csv reading capabilities to feed new quote
 * data into the database.
 * @note The function expects the csv data to be in the following order:
 *	 date, open, high, low, close, volume, returns
 * @note After pulling the data in, it uses MySQLs build in DATEDIFF() to
 *       calculate the daynr for each date
 * 
 * @code
 *	 LOAD DATA LOCAL INFILE 'data/symbol.csv' INTO TABLE quotes_daily 
 *	   FIELDS TERMINATED BY ',' ENCLOSED BY ',' 
 *	   LINES TERMINATED BY '\n'
 *	   (date, open, high, low, close, volume, returns) 
 *     set symbol=symbol;
 * @endcode
 * 
 * @param symbol bstring with symbol to be updated
 * @param filename bstring with the name of the csv file
*/
///////////////////////////////////////////////////////////////////////////////
void mysql_updatequotes(bstring symbol, bstring filename)
{
    extern MYSQL *system_db_handle;  // declared in main.c
    extern MYSQL *quote_db_handle; 
    
	/* First delete last 2 database entries
	   useful if data is pulled in more than once a day, in this case
	   the primary key {date,symbol} already exists and won't be 
	   updated.	
	
		DELETE FROM quotes_daily WHERE symbol='DOW' 
			ORDER BY date DESC LIMIT 2;
	*/
	
	bstring query1,query2,query3,query4;
	
	// create query string with first part of query
	bstring querystring=bfromcstr(
	  "DELETE FROM quotes_daily WHERE symbol='");
	// add symbol from argument
	bconcat(querystring, symbol);
	
	// add 2nd part of statement
	query1 = bfromcstr("' ORDER BY date DESC LIMIT 2");
	bconcat(querystring, query1);

	// uncomment to see query string
	// printf("\n%s\n",bdata(querystring));
	
	mysql_query(quote_db_handle, bdata(querystring));
	bstring functionname = bfromcstr("db_mysql_updatequotes()");
	check_mysql_error(functionname, querystring);

	// Removal of last 2 lines done, now do the quote update

	/* prepare statement in query:  
	   LOAD DATA LOCAL INFILE 'data/symbol.csv' INTO TABLE quotes_daily 
	   FIELDS TERMINATED BY ',' ENCLOSED BY ',' 
	   LINES TERMINATED BY '\n'
   	   (date, open, high, low, close, volume, returns) 
   	   set symbol=symbol; 
	*/

	//first part of query statement
	bstring querystring2 = bfromcstr("LOAD DATA LOCAL INFILE 'data/");
	
	// add filename
	bconcat(querystring2, filename);
	
	// second part of query statement after symbol
	query2 = bfromcstr("' INTO TABLE quotes_daily "
		"FIELDS TERMINATED BY ',' ENCLOSED BY ',' "
		"LINES TERMINATED by '\n' "
		"(date, open, high, low, close, volume, returns) set symbol='");
	bconcat(querystring2, query2);
		
	// add symbol again
	bconcat(querystring2, symbol);	
	query3 = bfromcstr("'");
	bconcat(querystring2, query3);
	
	// uncomment to see query statement
	//fprintf(stdout, "%s\n",bdata(querystring2));
	
	// Query the server 
	mysql_query(quote_db_handle, bdata(querystring2));
	check_mysql_error(functionname, querystring2);
	
	// now let MySQL calculate the daynr for each date, (+1 because the
	// last day should also count
	query4 = bfromcstr("UPDATE quotes_daily SET daynr = (SELECT datediff(date, '1900-01-01'))+1");
	//fprintf(stdout, "%s\n",bdata(querystring2));
	
	// query server
	mysql_query(quote_db_handle, bdata(query4));
	check_mysql_error(functionname, query4);
	
	// clean up
	bdestroy(querystring);
	bdestroy(querystring2);
	bdestroy(query1);
	bdestroy(query2);
	bdestroy(query3);
	bdestroy(query4);
	bdestroy(functionname);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief update indicator in mysql database
 * 
 * update indicator in mysql database. The struct holds all neccessary
 * information (which table, which column). Only non-zero quotes are
 * updated. If primary key {date,symbol} already exists, it is updated.
 * 
 * @code
 *	  INSERT INTO tablename
 *		(date, daynr, symbol, indicatorname)
 *		VALUES
 *		(dates[0], daynrs[0], symbol, quotes[0],
 *		...
 *		ON DUPLICATE KEY UPDATE indicatorname=VALUES(indicatorname);
 * @endcode		
 *		everything after VALUES will be stored in sequential string 
 *		'datequote'
 * 
 * @note After pushing the data to the db it uses MySQLs build in DATEDIFF() to
 *       calculate the daynr for each date
 * 
 * @param symbol bstring with symbol to be updated
 * @param identifier bstring with unique identifier of symbol (e.g. ISIN)
 * @param digits nr of digits for the indicator quote
 * @param indicatorname bstring 
 * @param db_table bstring with table name
 * @param nr_quotes int with nr. of datasets (date, daynr, quote)
 * @param quotes float ptr to quote 1xN matrix
 * @param dates bstring ptr to dates 1xN Matrix (format YYYY-MM-DD)
 * @param daynrs uint ptr to daynumber 1xN matrix
*/
///////////////////////////////////////////////////////////////////////////////
void db_mysql_update_indicator(bstring symbol, bstring identifier, int digits, bstring indicatorname, bstring db_table, unsigned int nr_quotes, float *quotes, bstring *dates, unsigned int *daynrs)
{
    extern MYSQL *system_db_handle;
	/* prepare query statement: 
	  INSERT INTO tablename
		(date, daynr, symbol, identifier, indicatorname)
		VALUES
		(dates[0], daynrs[0], symbol, quotes[0],
		(dates[1], daynrs[0], symbol, quotes[1],
		...
		ON DUPLICATE KEY UPDATE indicatorname=VALUES(indicatorname);
		
		everything after VALUES will be stored in sequential string 
		'datequote'
	*/
	bstring tmp1,tmp2,tmp3, tmp4,tmp5, tmp6, tmp8, tmp9, tmp10;	
	// prepare first part of query
	bstring querystring=bfromcstr("INSERT INTO ");
 	// insert tablename
 	bconcat(querystring, db_table);
 	// insert 2nd part
	tmp1 = bfromcstr(" (date, daynr, symbol, identifier, ");
 	bconcat(querystring, tmp1);
 	// insert indicator name
 	bconcat(querystring, indicatorname);
 	// insert  3rd part
	tmp2 = bfromcstr(") VALUES ");
	bconcat(querystring, tmp2);

	tmp3 = bfromcstr(" ('");
	tmp4 = bfromcstr("', '");
	tmp5 = bfromcstr("') ");
	tmp6 = bfromcstr(",");	
	
    bool firstflag = true;
    
	// append all values that are to be inserted	
	for (unsigned int i=0; i< nr_quotes; i++)
	{
		// (dates[i], daynrs[i], symbol, identifier, quotes[i])
		// do that only if quotes[0] != 0
		// ugly exception for regime_filter and performance records: here we need 0 to be stored
		if(quotes[i] || biseqcstr(indicatorname, "regime_filter") || biseqcstr(indicatorname, "equity") || biseqcstr(indicatorname, "risk_free_equity"))
		{
			if(!isnan(quotes[i]))
			{
                if(!firstflag)     // add a colon if there already was a quote tuple
                    bconcat(querystring, tmp6);
                
				bconcat(querystring, tmp3);
				bconcat(querystring, dates[i]);
				bconcat(querystring, tmp4);
				bstring inttmp = bformat("%i", daynrs[i]);
				bconcat(querystring, inttmp);
				bconcat(querystring, tmp4);
				bconcat(querystring, symbol);
				bconcat(querystring, tmp4);
                bconcat(querystring, identifier);
                bconcat(querystring, tmp4);
				bstring floattmp; 
				floattmp= bformat("%.*f",digits, quotes[i]);
				bconcat(querystring, floattmp);
				bconcat(querystring, tmp5);

				bdestroy(inttmp);
				bdestroy(floattmp);
                firstflag = false;
			}
		}
	}
	
	// Tell mysql to update indicator in case primary key {date,symbol} 
	// already exists	
	tmp8 = bfromcstr("ON DUPLICATE KEY UPDATE ");
 	bconcat(querystring, tmp8);
 	bconcat(querystring, indicatorname);
	tmp9 = bfromcstr("=VALUES(");
	bconcat(querystring, tmp9);
	bconcat(querystring, indicatorname);
	tmp10 = bfromcstr(")");
	bconcat(querystring, tmp10);
	
	//Pheew thats it
	
	// uncomment to see query statement printed out
// 	fprintf(stdout, "\n%s",bdata(querystring));
	
	//query server
	mysql_query(system_db_handle, bdata(querystring));
 	bstring funcname = bfromcstr("db_mysql_update_indicator()");
	check_mysql_error(funcname, querystring);
	
	// cleanup
	bdestroy(querystring);
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);
	bdestroy(tmp4);
	bdestroy(tmp5);
	bdestroy(tmp6);
	bdestroy(tmp8);
	bdestroy(tmp9);
	bdestroy(tmp10);
	bdestroy(funcname);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief update signal table in mysql database.
 * 
 * update signal table in mysql database.
 * 
 * @code
 *	  INSERT INTO tablename
 *		(date, symbol, identifier, name, type, strength, amp_info, signal_quote, price_quote, executed)
 *		VALUES
 *		(..., ..., ..., ..., ..., ..., ...);
 *		ON DUPLICATE KEY UPDATE name=VALUES(name);
 * @endcode
 * 
 * @note After pushing the data to the db it uses MySQLs build in DATEDIFF() to
 *       calculate the daynr for each date
 * 
 * @param symbol bstring with underlying market
 * @param identifier bstring with unique ID of underlying market (e.g. ISIN)
 * @param db_table bstring with tablename the signal should be stored within
 * @param datestring bstring with date (YYYY-MM-DD) the signal occured
 * @param price float with price at which signal occured
 * @param indicator_quote float with value of indicator when signal occured
 * @param type signal_type (longsignal/shortsignal)
 * @param strength strength_type (weak/neutral/strong)
 * @param nrOfDigits uint with significant nr of digits of underlying market´s units
 * @param signal_name bstring with name of signal
 * @param signal_description bstring description of signal
 * @param amp_info bstring with additional/amplifying information
 * @param executed bool flag (true/false) if signal was already executed
*/
///////////////////////////////////////////////////////////////////////////////
void db_mysql_update_signal(bstring symbol, bstring identifier, bstring db_table, bstring datestring, float price, 
			     float indicator_quote, signal_type type, strength_type strength, unsigned int nrOfDigits,
			     bstring signal_name, bstring signal_description, bstring amp_info, bool executed)
{
    extern MYSQL *system_db_handle;
	bstring signaltype, signalstrength;

	signalstrength = NULL;
	signaltype = NULL;
	
	// prepare first part of query
	bstring querystring=bfromcstr("INSERT INTO ");
	// add tablename
	bconcat(querystring, db_table);
	// continue with query
	bstring tmp0 = bfromcstr(" (date, symbol, identifier, name, type, strength, description, amp_info, signal_quote, price_quote, executed) VALUES ('");
	bconcat(querystring, tmp0);
	bconcat(querystring, datestring);
	bstring tmp1 = bfromcstr("', '");
	bconcat(querystring, tmp1);
	bconcat(querystring, symbol);
	bconcat(querystring, tmp1);
    bconcat(querystring, identifier);
    bconcat(querystring, tmp1);
	bconcat(querystring, signal_name);
	bconcat(querystring, tmp1);
	switch (type)
	{
		case longsignal:
			signaltype = bfromcstr("long");
			break;
		case shortsignal:
			signaltype = bfromcstr("short");
			break;
		case undefined:		// should absolutely not happen here
		default:
			break;
	}
	bconcat(querystring, signaltype);
	bconcat(querystring, tmp1);
	switch (strength)
	{
		case weak:
			signalstrength = bfromcstr("weak");
			break;
		case neutral:
			signalstrength = bfromcstr("neutral");
			break;
		case strong:
			signalstrength = bfromcstr("strong");
			break;
	}
	bconcat(querystring, signalstrength);
	bconcat(querystring, tmp1);
	bconcat(querystring, signal_description);
	bconcat(querystring, tmp1);
	bconcat(querystring, amp_info);
	bconcat(querystring, tmp1);
	bstring tmp2 = bformat("%.*f", nrOfDigits, indicator_quote);
	bconcat(querystring, tmp2);
	bconcat(querystring, tmp1);
	bstring tmp3 = bformat("%.*f", nrOfDigits, price);
	bconcat(querystring, tmp3);
	bconcat(querystring, tmp1);
	bstring tmp4 = bformat("%i", executed);
	bconcat(querystring, tmp4);
	bstring tmp5 = bfromcstr("')");
	bconcat(querystring, tmp5);
	bstring tmp6 = bfromcstr(" ON DUPLICATE KEY UPDATE name=VALUES(name)");
	bconcat(querystring, tmp6);
	if(executed == true)		// update the executed flag only from false to true (this means when function is called by execution manager, because signal is executed
	{
		bstring tmp7 = bfromcstr(", executed=VALUES(executed);");
		bconcat(querystring, tmp7);	// this way mysql_update_signal remains universal
		bdestroy(tmp7);
	}
	else
	{
		bstring tmp8 = bfromcstr(";");
		bconcat(querystring, tmp8);	// do not update if executed flag is false, for example during signal evaluation (before buying)
		bdestroy(tmp8);
	}
	
	// uncomment to see query statement printed out
	// fprintf(stdout, "\n%s",bdata(querystring));
	
	//query server
	mysql_query(system_db_handle, bdata(querystring));
	bstring funcname = bfromcstr("db_mysql_update_signal()");
	check_mysql_error(funcname, querystring);
	
	// now let MySQL calculate the daynr for each date
	bstring querystring2 = bfromcstr("UPDATE ");
	bconcat(querystring2, db_table);
	bstring tmp9 = bfromcstr(" SET daynr = (SELECT datediff(date, '1900-01-01'))+1");
	bconcat(querystring2, tmp9);
	//fprintf(stdout, "%s\n", bdata(querystring));
	
	// query server
	mysql_query(system_db_handle, bdata(querystring2));
	check_mysql_error(funcname, querystring2);
	
	// cleanup
	bdestroy(tmp0);
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);
	bdestroy(tmp4);
	bdestroy(tmp5);
	bdestroy(tmp6);
	bdestroy(tmp9);
	bdestroy(funcname);
	
	bdestroy(querystring);
	bdestroy(querystring2);
	if(signalstrength != NULL)
		bdestroy(signalstrength);
	if(signaltype != NULL)
		bdestroy(signaltype);		
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief remove indicator dates with no quote data within from db
 * 
 * This is very ichimoku-specific...as the future dates for senkouA and B
 * are only guessed (future workdays) it will happen that those dates won't
 * be real workdates (like holidays). Over time the database (specifically the
 * ichimoku_daily table will get "holes" as there will be "past" days for which
 * senkouA/B values exist, but no  other indicators (as there are no quotes
 * to calculate them. This routine looks for those dates and deletes them. 
 * 
 * @code
 * 	DELETE FROM ichimoku_daily 
 * 		where tenkan IS NULL AND senkou_A is NOT NULL;
 * @endcode
 * 
 * @return 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_remove_null_rows(void)
{
    extern MYSQL *system_db_handle;
	bstring querystring=bfromcstr("DELETE FROM ichimoku_daily where tenkan IS NULL AND senkou_A is NOT NULL;");
	
	// uncomment to see query statement printed out
	// fprintf(stdout, "\n%s",bdata(querystring));
	
	//query server
	mysql_query(system_db_handle, bdata(querystring));
	bstring funcname = bfromcstr("db_mysql_remove_null_rows()");
	check_mysql_error(funcname,querystring);
	
	// cleanup
	bdestroy(querystring);
	bdestroy(funcname);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief gets the content of signals in db
 * 
 * gets the content of signals in db, stores them in signal_list
 * Note that ALL signals are fetched and stored, regardless of their strength
 * or type
 * 
 * @code
 * 	select date, daynr, symbol, name, type, strength, description, amp_info, 
 *  signal_quote, price_quote,executed 
 *		from (select * from ichimoku_daily_signals order by date) 
 *      sub order by symbol ASC;
 * @endcode
 * @param the_list pointer class_signal_list struct
 * @param tablename bstring ptr with db table name
 * @return 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_signals(class_signal_list *the_list, bstring* table_name)
{
    extern MYSQL *system_db_handle;
 	//unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;
	unsigned int row_nr=0;
	
	//  prepare query statement:
	bstring querystring;
	querystring = bfromcstr("select date, daynr, symbol, identifier, name, type, strength, description, amp_info, signal_quote, price_quote, executed from (select * from ichimoku_daily_signals order by date) sub order by daynr ASC");
	
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));	
	
	// query server
	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("db_mysql_get_signals()");
	check_mysql_error(funcname, querystring);
	
	// get results
	result = mysql_store_result(system_db_handle);
    bstring tmp = bfromcstr("none");

	check_mysql_error(funcname, tmp);
    bdestroy(tmp);
    bdestroy(funcname);
	
	// Loop through result rows, create signal struct
	while((row = mysql_fetch_row(result)) !=0)
	{
        unsigned int the_daynr = 0;
        float the_price = 0.0;
        float the_ind_quote = 0.0;
        CHECK_ATOI_NULL(the_daynr, row[1]);
        CHECK_ATOF_NULL(the_ind_quote, row[9]);
        CHECK_ATOF_NULL(the_price, row[10]);
        
        signal_type the_type = undefined;
        bstring tmp_type = bfromcstr(row[5]);
        if(biseqcstr(tmp_type, "long"))
            the_type = longsignal;
        else if(biseqcstr(tmp_type, "short"))
            the_type = shortsignal;
        bdestroy(tmp_type);
       
        strength_type the_strength = weak;
        bstring tmp_strength = bfromcstr(row[6]);
        if(biseqcstr(tmp_strength, "weak"))
            the_strength = weak;
        else if(biseqcstr(tmp_strength, "neutral"))
            the_strength = neutral;
        else if(biseqcstr(tmp_strength, "strong"))
            the_strength = strong;
        bdestroy(tmp_strength);
        
        unsigned int nr_digits = 0;
        bstring price_string = bfromcstr(row[10]);
        nr_digits = get_nrDigitsOfBString(price_string);
        bdestroy(price_string);
        the_list->addNewSignal(the_list, row[0], the_daynr, the_price, the_ind_quote, row[2], row[3], the_type, the_strength, nr_digits, row[4], row[7], row[8], bdata(*table_name));
        unsigned int exec_flag = 0;
        CHECK_ATOI_NULL(exec_flag, row[11]);
        the_list->Sig[the_list->nr_signals-1]->executed = exec_flag;
        row_nr++;
    }
	
	mysql_free_result(result);
	
	bdestroy(querystring);
  
    return EXIT_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief gets the content of signals in db depending on a specific search
 * 
 * gets the content of signals in db, stores them in signal_list
 * In contrary to mysql_get_signals this function expects a search string as
 * parameter. This way specific subsets of signals may be fetched
 * 
 * @param the_list pointer class_signal_list struct
 * @param tablename bstring ptr with db table name
 * @param searchstring bstring ptr with mysql search to execute
 * @return 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_signals_subset(class_signal_list *the_list, bstring* table_name, bstring* searchstring)
{
    extern MYSQL *system_db_handle;
    
 	//unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;
	unsigned int row_nr=0;

	// query server
	mysql_query(system_db_handle, bdata(*searchstring));
    bstring funcname = bfromcstr("db_mysql_get_signals_subset()");
	check_mysql_error(funcname, *searchstring);
	
	// get results
	result = mysql_store_result(system_db_handle);
    bstring tmp = bfromcstr("none");

	check_mysql_error(funcname, tmp);
    bdestroy(tmp);
    bdestroy(funcname);
	
	// Loop through result rows, create signal struct
	while((row = mysql_fetch_row(result)) !=0)
	{
        unsigned int the_daynr = 0;
        float the_price = 0.0;
        float the_ind_quote = 0.0;
        CHECK_ATOI_NULL(the_daynr, row[1]);
        CHECK_ATOF_NULL(the_ind_quote, row[8]);
        CHECK_ATOF_NULL(the_price, row[9]);
        
        signal_type the_type = undefined;
        bstring tmp_type = bfromcstr(row[4]);
        if(biseqcstr(tmp_type, "long"))
            the_type = longsignal;
        else if(biseqcstr(tmp_type, "short"))
            the_type = shortsignal;
        bdestroy(tmp_type);
       
        strength_type the_strength = weak;
        bstring tmp_strength = bfromcstr(row[5]);
        if(biseqcstr(tmp_strength, "weak"))
            the_strength = weak;
        else if(biseqcstr(tmp_strength, "neutral"))
            the_strength = neutral;
        else if(biseqcstr(tmp_strength, "strong"))
            the_strength = strong;
        bdestroy(tmp_strength);
        
        unsigned int nr_digits = 0;
        bstring price_string = bfromcstr(row[9]);
        nr_digits = get_nrDigitsOfBString(price_string);
        bdestroy(price_string);
        
        the_list->addNewSignal(the_list, row[0], the_daynr, the_price, the_ind_quote, row[2], row[3], the_type, the_strength, nr_digits, row[4], row[7], row[8], bdata(*table_name));
        unsigned int exec_flag = 0;
        CHECK_ATOI_NULL(exec_flag, row[10]);
        the_list->Sig[the_list->nr_signals-1]->executed = exec_flag;
        row_nr++;
    }
	
	mysql_free_result(result);
  
    return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief gets all entries from portfolio 
 * 
 * Queries the database for portfolio entries and stores them in portfolio
 * object
 * 
 * @code
	SELECT symbol, markettype, currency_market,
		buyquote_market, buydate_market, buydaynr_market, 
		buyquote_account, buydate_account, buydaynr_account,
		buyquote_signal, buydate_signal, buydaynr_signal, 
		currquote_market, currdate_market, currdaynr_market, 
		currquote_account, currdate_account, currdaynr_account,
		type, signalname, stoploss, initial_sl, pos_size, 
		quantity, trading_days, current_value, invested_value, 
		risk_free_value, p_l, risk_free_p_l, p_l_percent,
        equity, rf_equity from portfolio 
	ORDER BY buydate_account;");
 * @endcode
 * @param the_portfolio pointer to portfolio object
 * @return 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_portfolio(class_portfolio *the_portfolio)
{
    extern MYSQL *system_db_handle;
    
	//unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;
	
	bstring querystring;
	querystring = bfromcstr("SELECT symbol, identifier, markettype, currency_market, "			// 2
				"buyquote_market, buydate_market, buydaynr_market, "		// 5
				"buyquote_account, buydate_account, buydaynr_account, "		// 8
				"buyquote_signal, buydate_signal, buydaynr_signal, "		// 11
				"currquote_market, currdate_market, currdaynr_market, " 	// 14
				"currquote_account, currdate_account, currdaynr_account, "	// 17
				"type, signalname, stoploss, initial_sl, pos_size, "		// 22
				"quantity, trading_days, current_value, invested_value, "	// 26
				"risk_free_value, p_l, risk_free_p_l, p_l_percent , equity, rf_equity from portfolio ORDER BY buydate_account;");
	
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));	
	
	// query server
	mysql_query(system_db_handle, bdata(querystring));
	bstring funcname = bfromcstr("db_mysql_get_portfolio()");
	check_mysql_error(funcname, querystring);
	
	// get results
	result = mysql_store_result(system_db_handle);
	bstring tmpstr = bfromcstr("none");
	check_mysql_error(funcname, tmpstr);
	
	// Loop through result rows, create signal struct
	while((row = mysql_fetch_row(result)) !=0)
	{
		// for each row extract field results

		// get nr of digits by converting results to bstring and use helper function
		bstring buyquote_market_str = bfromcstr(row[4]);
		bstring buyquote_account_str = bfromcstr(row[7]);
		bstring buyquote_signal_str = bfromcstr(row[10]);
		
		unsigned int buyquote_market_digits = get_nrDigitsOfBString(buyquote_market_str);
		unsigned int buyquote_account_digits = get_nrDigitsOfBString(buyquote_account_str);
		unsigned int buyquote_signal_digits = get_nrDigitsOfBString(buyquote_signal_str);
		
        if(buyquote_market_digits < 2)
            buyquote_market_digits = 2;
        if(buyquote_account_digits < 2)
            buyquote_account_digits = 2;
        if(buyquote_signal_digits <2 )
            buyquote_signal_digits = 2;
        
		float buyquote_market, buyquote_account, buyquote_signal, quantity;
		float currquote_market, currquote_account, stoploss, pos_size;
		float current_value, invested_value, risk_free_value, p_l;
		float risk_free_p_l, p_l_percent, equity, rf_equity;
		
		unsigned int currdaynr_market, currdaynr_account, initial_sl, trading_days;
		unsigned int buydaynr_market, buydaynr_account, buydaynr_signal;
		
		CHECK_ATOF_NULL(buyquote_market, row[4]);
		CHECK_ATOI_NULL(buydaynr_market, row[6]);
		CHECK_ATOF_NULL(buyquote_account, row[7]);
		CHECK_ATOI_NULL(buydaynr_account, row[9]);
		CHECK_ATOF_NULL(buyquote_signal, row[10]);
		CHECK_ATOI_NULL(buydaynr_signal, row[12]);

		signal_type element_type;
		bstring typestring = bfromcstr(row[19]);
		//bstring longstring = bfromcstr("long");
		if(biseqcstr(typestring, "long"))		// translate the strings from db entry back to enums as
			element_type = longsignal;			// defined in datatypes.h
		else if(biseqcstr(typestring, "short"))
			element_type = shortsignal;
		else 
			element_type = undefined;	// should not happen
		
		CHECK_ATOF_NULL(quantity, row[24]);

        // update additional available data from db
		CHECK_ATOF_NULL(currquote_market, row[13]);
		CHECK_ATOI_NULL(currdaynr_market, row[15]);
		CHECK_ATOF_NULL(currquote_account, row[16]);
		CHECK_ATOI_NULL(currdaynr_account, row[18]);
		CHECK_ATOF_NULL(stoploss, row[21]);
		CHECK_ATOI_NULL(initial_sl, row[22]);
		CHECK_ATOF_NULL(pos_size, row[23]);
		CHECK_ATOI_NULL(trading_days, row[25]);
		CHECK_ATOF_NULL(current_value, row[26]);
		CHECK_ATOF_NULL(invested_value, row[27]);
		CHECK_ATOF_NULL(risk_free_value, row[28]);
		CHECK_ATOF_NULL(p_l, row[29]);
		CHECK_ATOF_NULL(risk_free_p_l, row[30]);
		CHECK_ATOF_NULL(p_l_percent, row[31]);
        CHECK_ATOF_NULL(equity, row[32]);
        CHECK_ATOF_NULL(rf_equity, row[33]);
        
        // create temporary portfolio element
        class_portfolio_element *tmp_element;
        
        bstring tmp1 = bfromcstr(row[2]);
        if(biseqcstr(tmp1, "STOCK"))
            tmp_element = (class_portfolio_element*) class_portfolio_element_stocks_init_manually(row[0], row[1], row[2], row[3], element_type,
					quantity, buyquote_market, buyquote_market_digits, row[5], buydaynr_market,
					buyquote_account, buyquote_account_digits, row[8], buydaynr_account, 
					buyquote_signal, buyquote_signal_digits, row[11], buydaynr_signal,
					row[20], -1, -1);  // note that last parameters are dummy ones which have to be properly set within a function that has an idea of market_lists and stop losses
        else if(biseqcstr(tmp1, "CFDCUR"))
                    tmp_element = (class_portfolio_element*) class_portfolio_element_cfdcur_init_manually(row[0], row[1], row[2], row[3], element_type,
					quantity, buyquote_market, buyquote_market_digits, row[5], buydaynr_market,
					buyquote_account, buyquote_account_digits, row[8], buydaynr_account, 
					buyquote_signal, buyquote_signal_digits, row[11], buydaynr_signal,
					row[20], -1, -1);
        else if(biseqcstr(tmp1, "CFDFUT"))
                    tmp_element = (class_portfolio_element*) class_portfolio_element_cfdfut_init_manually(row[0], row[1], row[2], row[3], element_type,
					quantity, buyquote_market, buyquote_market_digits, row[5], buydaynr_market,
					buyquote_account, buyquote_account_digits, row[8], buydaynr_account, 
					buyquote_signal, buyquote_signal_digits, row[11], buydaynr_signal,
					row[20], -1, -1);
        else
        {
            log_err("found unknown market type %s for market %s in database... Aborting!", row[1], row[0]);
            bdestroy(buyquote_market_str);
            bdestroy(buyquote_account_str);
            bdestroy(buyquote_signal_str);
            bdestroy(typestring);
            mysql_free_result(result);
	
            bdestroy(querystring);
            bdestroy(funcname);
            bdestroy(tmpstr);
            exit(EXIT_FAILURE);
        }
        
        bdestroy(tmp1);
        
		// free bstring memory (alloc'ed by constructor) to prevent leaks
		bdestroy(*tmp_element->currQuote_market->datevec);
		bdestroy(*tmp_element->currQuote_account->datevec);
		
 		*tmp_element->currQuote_market->quotevec = currquote_market;
 		*tmp_element->currQuote_market->datevec = bfromcstr(row[14]);
 		*tmp_element->currQuote_market->daynrvec = currdaynr_market;
 		*tmp_element->currQuote_account->quotevec = currquote_account;
 		*tmp_element->currQuote_account->datevec = bfromcstr(row[17]);
 		*tmp_element->currQuote_account->daynrvec = currdaynr_account;
		tmp_element->stoploss = stoploss;
		tmp_element->isl_flag = initial_sl;
		tmp_element->pos_size = pos_size;
		tmp_element->trading_days = trading_days;
		tmp_element->current_value = current_value;
		tmp_element->invested_value = invested_value;
		tmp_element->risk_free_value = risk_free_value;
		tmp_element->p_l = p_l;
		tmp_element->risk_free_p_l = risk_free_p_l;
		tmp_element->p_l_percent = p_l_percent;		
        tmp_element->equity = equity;
        tmp_element->rf_equity = rf_equity;
		
		bdestroy(buyquote_market_str);
		bdestroy(buyquote_account_str);
		bdestroy(buyquote_signal_str);
		bdestroy(typestring);
		
		the_portfolio->addElement(the_portfolio, tmp_element);
		tmp_element->destroy(tmp_element);
	}
	
	mysql_free_result(result);
	
	bdestroy(querystring);
	bdestroy(funcname);
	bdestroy(tmpstr);
	return 0; 
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int db_mysql_update_portfolio(class_portfolio_element *the_portfolio_element)
{
    extern MYSQL *system_db_handle;
    
	bstring querystring=bfromcstr("INSERT INTO portfolio (symbol, identifier, markettype, currency_market, "
				"buyquote_market, buydate_market, buydaynr_market, "
				"buyquote_account, buydate_account, buydaynr_account, "
				"buyquote_signal, buydate_signal, buydaynr_signal, "
				"currquote_market, currdate_market, currdaynr_market, " 
				"currquote_account, currdate_account, currdaynr_account, "
				"type, signalname, stoploss, initial_sl, pos_size, "
				"quantity, trading_days, current_value, invested_value, "
				"risk_free_value, p_l, risk_free_p_l, p_l_percent, equity, rf_equity) VALUES ('");
	
	bconcat(querystring, *the_portfolio_element->symbol);
	bstring tmp0 = bfromcstr("', '");
	bconcat(querystring, tmp0);
    bconcat(querystring, *the_portfolio_element->identifier); bconcat(querystring, tmp0);
	bconcat(querystring, *the_portfolio_element->markettype); bconcat(querystring, tmp0);
	bconcat(querystring, *the_portfolio_element->market_currency); bconcat(querystring, tmp0);
	bstring tmp1 = bformat("%.*f", the_portfolio_element->Buyquote_market->nr_digits, the_portfolio_element->Buyquote_market->quotevec[0]);
	bconcat(querystring, tmp1); bconcat(querystring, tmp0);
	bconcat(querystring, the_portfolio_element->Buyquote_market->datevec[0]); bconcat(querystring, tmp0);
	bstring tmp2 = bformat("%i", the_portfolio_element->Buyquote_market->daynrvec[0]); 
	bconcat (querystring, tmp2); bconcat(querystring, tmp0);
	
	bstring tmp3 = bformat("%.*f", the_portfolio_element->Buyquote_account->nr_digits, the_portfolio_element->Buyquote_account->quotevec[0]);
	bconcat(querystring, tmp3); bconcat(querystring, tmp0);
	bconcat(querystring, the_portfolio_element->Buyquote_account->datevec[0]); bconcat(querystring, tmp0);
	bstring tmp4 = bformat("%i", the_portfolio_element->Buyquote_account->daynrvec[0]); 
	bconcat (querystring, tmp4); bconcat(querystring, tmp0);
	
	bstring tmp5 = bformat("%.*f", the_portfolio_element->Buyquote_signal->nr_digits, the_portfolio_element->Buyquote_signal->quotevec[0]);
	bconcat(querystring, tmp5); bconcat(querystring, tmp0);
	bconcat(querystring, the_portfolio_element->Buyquote_signal->datevec[0]); bconcat(querystring, tmp0);
	bstring tmp6 = bformat("%i", the_portfolio_element->Buyquote_signal->daynrvec[0]); 
	bconcat (querystring, tmp6); bconcat(querystring, tmp0);
	
	bstring tmp7 = bformat("%.*f", the_portfolio_element->currQuote_market->nr_digits, the_portfolio_element->currQuote_market->quotevec[0]);
	bconcat(querystring, tmp7); bconcat(querystring, tmp0);
	bconcat(querystring, the_portfolio_element->currQuote_market->datevec[0]); bconcat(querystring, tmp0);
	bstring tmp8 = bformat("%i", the_portfolio_element->currQuote_market->daynrvec[0]); 
	bconcat (querystring, tmp8); bconcat(querystring, tmp0);
	
	bstring tmp9 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->currQuote_account->quotevec[0]);
	bconcat(querystring, tmp9); bconcat(querystring, tmp0);
	bconcat(querystring, the_portfolio_element->currQuote_account->datevec[0]); bconcat(querystring, tmp0);
	bstring tmp10 = bformat("%i", the_portfolio_element->currQuote_account->daynrvec[0]); 
	bconcat (querystring, tmp10); bconcat(querystring, tmp0);
	
	bstring signaltype = NULL;
	switch (the_portfolio_element->type)
	{
		case longsignal:
			signaltype = bfromcstr("long");
			break;
		case shortsignal:
			signaltype = bfromcstr("short");
			break;
		case undefined:		// should absolutely not happen here
		default:
			break;
	}
	bconcat(querystring, signaltype); bconcat(querystring, tmp0);
	bconcat(querystring, *the_portfolio_element->signalname); bconcat(querystring, tmp0);
	
	bstring tmp11 = bformat("%.*f", the_portfolio_element->Buyquote_market->nr_digits, the_portfolio_element->stoploss); 
	bconcat(querystring, tmp11); bconcat(querystring, tmp0);
	bstring tmp12 = bformat("%i", the_portfolio_element->isl_flag); 
	bconcat(querystring, tmp12); bconcat(querystring, tmp0);
	bstring tmp13 = bformat("%.2f", the_portfolio_element->pos_size);
	bconcat(querystring, tmp13); bconcat(querystring, tmp0);
	bstring tmp14 = bformat("%.2f", the_portfolio_element->quantity);
	bconcat(querystring, tmp14); bconcat(querystring, tmp0);
	bstring tmp15 = bformat("%i", the_portfolio_element->trading_days);
	bconcat(querystring, tmp15); bconcat(querystring, tmp0);
	bstring tmp16 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->current_value);
	bconcat(querystring, tmp16); bconcat(querystring, tmp0);
	bstring tmp17 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->invested_value);
	bconcat(querystring, tmp17); bconcat(querystring, tmp0);
	bstring tmp18 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->risk_free_value);
	bconcat(querystring, tmp18); bconcat(querystring, tmp0);
	bstring tmp19 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->p_l);
	bconcat(querystring, tmp19); bconcat(querystring, tmp0);
	bstring tmp20 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->risk_free_p_l);
	bconcat(querystring, tmp20); bconcat(querystring, tmp0);
	bstring tmp21 = bformat("%.2f", the_portfolio_element->p_l_percent);
	bconcat(querystring, tmp21); bconcat(querystring, tmp0);
    bstring tmp211 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->equity);
    bconcat(querystring, tmp211); bconcat(querystring, tmp0);
    bstring tmp212 = bformat("%.*f", the_portfolio_element->currQuote_account->nr_digits, the_portfolio_element->rf_equity);
	bconcat(querystring, tmp212);
    
	bstring tmp22 = bfromcstr("') "); 
	
	bstring tmp23 = bfromcstr("ON DUPLICATE KEY UPDATE currquote_market=VALUES(currquote_market), currdate_market=VALUES(currdate_market), currdaynr_market=VALUES(currdaynr_market), "
				"currquote_account=VALUES(currquote_market), currdate_account=VALUES(currdate_account), currdaynr_account=VALUES(currdaynr_account), "
				"stoploss=VALUES(stoploss), initial_sl=VALUES(initial_sl), current_value=VALUES(current_value), invested_value=VALUES(invested_value), "
				"risk_free_value=VALUES(risk_free_value), p_l=VALUES(p_l), risk_free_p_l=VALUES(risk_free_p_l), p_l_percent=VALUES(p_l_percent), trading_days=VALUES(trading_days), equity=VALUES(equity), rf_equity=VALUES(rf_equity);");
	
	bconcat(querystring, tmp22); bconcat(querystring, tmp23);

	// uncomment to see query statement
	// printf("\n%s\n", bdata(querystring));
	
	// query server
	bstring funcname = bfromcstr("db_mysql_update_portfolio()");
	mysql_query(system_db_handle, bdata(querystring));
	check_mysql_error(funcname, querystring);
	
	bdestroy(querystring);
	bdestroy(tmp0);
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);
	bdestroy(tmp4);
	bdestroy(tmp5);
	bdestroy(tmp6);
	bdestroy(tmp7);
	bdestroy(tmp8);
	bdestroy(tmp9);
	bdestroy(tmp10);
	bdestroy(tmp11);
	bdestroy(tmp12);
	bdestroy(tmp13);
	bdestroy(tmp14);
	bdestroy(tmp15);
	bdestroy(tmp16);
	bdestroy(tmp17);
	bdestroy(tmp18);
	bdestroy(tmp19);
	bdestroy(tmp20);
	bdestroy(tmp21);
    bdestroy(tmp211);
    bdestroy(tmp212);
	bdestroy(tmp22);
	bdestroy(tmp23);	
	bdestroy(signaltype);
	bdestroy(funcname);
	
	return EXIT_SUCCESS;
}

int db_mysql_clear_portfolio_table()
{
    extern MYSQL *system_db_handle;
	bstring querystring = bfromcstr("TRUNCATE portfolio");
	mysql_query(system_db_handle, bdata(querystring));
	bstring funcname = bfromcstr("db_mysql_clear_portfolio_table");
	check_mysql_error(funcname, querystring);	
	
	bdestroy(querystring);
	bdestroy(funcname);
	return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief Updates orderbook entries
 * 
 * saves the content of the object orderbook to mysql database
 * 
 * @code
 * INSERT INTO orderbook_daily
 *		( date, symbol, type, buy_sell, price, cost_per_item, signalname,  quantity, buydate, signaldate, hold_days, comission, stoploss, P_L_total, P_L_piece, P_L_percent, fee)
 *		VALUES
 *		('...','...','...',`...`, '...','...','...','...','...','...','...','...',);
 * @endcode
 * @param orderbook ptr to class_orderbook object
 * @return 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_update_orderbook(class_orderbook* orderbook)
{
    extern MYSQL *system_db_handle;
    unsigned int nrElements = 0;
    nrElements = orderbook->getNrElements(orderbook);
    
    if(nrElements < 1)
    {
        return EXIT_SUCCESS;
    }
    
	/* prepare query statement:
	  INSERT INTO orderbook_daily
		( date, symbol, type, buy_sell, price, cost_per_item, signalname,  quantity, buydate, signaldate, hold_days, comission, stoploss, P_L_total, P_L_piece, P_L_percent)
		VALUES
		('...','...','...',`...`, '...','...','...','...','...','...','...',);
	*/
	// prepare first part of query
	bstring querystring=bfromcstr("INSERT INTO orderbook_daily ( date, symbol, identifier, type, buy_sell, price, signalname, cost_per_item, pos_size, quantity, buydate, signaldate, hold_days, comission, stoploss, P_L_total, P_L_piece, P_L_percent, fee) VALUES ");
	bstring tmpstr14 = bfromcstr(";");
    
    for(unsigned int i = 0; i < nrElements; i++)
    {
        // add parameters
        bstring tmpstr0 = bfromcstr("('");
        bstring tmpstr1 = bfromcstr("', '");
        bstring tmpstr2 = bfromcstr("') ");
        bstring tmpstr13 = bfromcstr(",");
        
        bstring signaltype = NULL;
        bstring order = NULL;
        bstring date = NULL;
        bstring buydate = NULL;
        switch(orderbook->orders[i]->ordertype)
        {
            case buyorder:
            {
                date = bstrcpy(*orderbook->orders[i]->buydate);
                buydate = bstrcpy(date);
                order = bfromcstr("buy");
                break;
            }
            case sellorder:
            {
                date = bstrcpy(*orderbook->orders[i]->selldate);
                buydate = bstrcpy(*orderbook->orders[i]->buydate);
                order = bfromcstr("sell");
                break;
            }
            default:
                break;	
        }        
        
        bconcat(querystring, tmpstr0);
        bconcat(querystring, date);
        bconcat(querystring, tmpstr1);
        bconcat(querystring, *orderbook->orders[i]->symbol);
        bconcat(querystring, tmpstr1);
        bconcat(querystring, *orderbook->orders[i]->identifier);
        
        switch (orderbook->orders[i]->signaltype)
        {
            case longsignal:
                signaltype = bfromcstr("long");
                break;
            case shortsignal:
                signaltype = bfromcstr("short");
                break;
            case undefined:		// should absolutely not happen here
            default:
                break;	
        }      
        bconcat(querystring, tmpstr1);
        bconcat(querystring, signaltype);
        
        bconcat(querystring, tmpstr1);
        bconcat(querystring, order);
        
        unsigned int nrDigits1 = orderbook->orders[i]->quote_market->nr_digits;
        bstring tmpstr3 = bformat("%.*f", nrDigits1, orderbook->orders[i]->quote_market->quotevec[0]);
        bconcat(querystring, tmpstr1);
        bconcat(querystring, tmpstr3);
        
        bconcat(querystring, tmpstr1);
        bconcat(querystring, *orderbook->orders[i]->signalname);
        
        bconcat(querystring, tmpstr1);
        unsigned int nrDigits2 = orderbook->orders[i]->quote_account->nr_digits;
        bstring tmpstr4 = bformat("%.*f", nrDigits2, orderbook->orders[i]->quote_account->quotevec[0]);
        bconcat(querystring, tmpstr4);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr5 = bformat("%.2f", orderbook->orders[i]->pos_size);
        bconcat(querystring, tmpstr5);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr6 = bformat("%.2f", orderbook->orders[i]->quantity);
        bconcat(querystring, tmpstr6);
        
        bconcat(querystring, tmpstr1);
        bconcat(querystring, buydate);
        
        bconcat(querystring, tmpstr1);
        bconcat(querystring, orderbook->orders[i]->Buyquote_signal->datevec[0]);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr7 = bformat("%i", orderbook->orders[i]->trading_days);
        bconcat(querystring, tmpstr7);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr8 = bformat("%.*f", nrDigits2, 0.0);
        bconcat(querystring, tmpstr8);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr9 = bformat("%.*f", nrDigits1, orderbook->orders[i]->stoploss);
        bconcat(querystring, tmpstr9);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr10 = bformat("%.*f", nrDigits1, orderbook->orders[i]->p_l_total);
        bconcat(querystring, tmpstr10);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr11 = bformat("%.*f", nrDigits1, orderbook->orders[i]->p_l_piece);
        bconcat(querystring, tmpstr11);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr12 = bformat("%.*f", nrDigits1, orderbook->orders[i]->p_l_percent);
        bconcat(querystring, tmpstr12);
        
        bconcat(querystring, tmpstr1);
        bstring tmpstr16 = bformat("%.6f", orderbook->orders[i]->fee);
        bconcat(querystring, tmpstr16);
        
        bconcat(querystring, tmpstr2);
        
        if(i<nrElements - 1)
            bconcat(querystring, tmpstr13);
        
        bdestroy(date);
        bdestroy(buydate);
        bdestroy(signaltype);
        bdestroy(order);
        
        bdestroy(tmpstr0);
        bdestroy(tmpstr1);
        bdestroy(tmpstr2);
        bdestroy(tmpstr3);
        bdestroy(tmpstr4);
        bdestroy(tmpstr5);
        bdestroy(tmpstr6);
        bdestroy(tmpstr7);
        bdestroy(tmpstr8);
        bdestroy(tmpstr9);
        bdestroy(tmpstr10);
        bdestroy(tmpstr11);
        bdestroy(tmpstr12);        
        bdestroy(tmpstr13);
        bdestroy(tmpstr16);
    }
    
    bstring tmpstr15 = bfromcstr(" ON DUPLICATE KEY UPDATE date=VALUES(date)");
    bconcat(querystring, tmpstr15);
    
    bconcat(querystring, tmpstr14);
	// uncomment to see query statement
// 	printf("\n%s", bdata(querystring));
    
   	// query server
	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("db_mysql_update_orderbook()");
	check_mysql_error(funcname, querystring);
    bdestroy(funcname);
    
    bdestroy(querystring);
    bdestroy(tmpstr14);
    bdestroy(tmpstr15);
    
    
    return EXIT_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief loads orderbook entries
 * 
 * loads the content of table orderbook in mysql db into the object orderbook
 * 
 * @code
 * SELECT * from orderbook_daily ORDER BY date
 * @endcode
 * @param orderbook ptr to class_orderbook object
 * @param the_markets prt to class_market_list object that holds all markets
 * @return 0 if successful, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_orderbook(class_orderbook* orderbook, class_market_list* the_markets)
{
    extern parameter parms; // parms are declared global in main.c
    extern MYSQL *system_db_handle;
    
	MYSQL_RES	*result;
	MYSQL_ROW	row;
    
    bstring querystring = bfromcstr("SELECT * from orderbook_daily ORDER BY date;");
  
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));    

	// query server
	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("db_mysql_get_orderbook()");
	check_mysql_error(funcname, querystring);
    
	// get results
	result = mysql_store_result(system_db_handle);
    bstring tmp = bfromcstr("none");
	check_mysql_error(funcname, tmp);    
    bdestroy(funcname);
    bdestroy(tmp);
    
    // loop through all rows of result
 	while((row = mysql_fetch_row(result)) !=0)
 	{
        // for each row extract field results
        bstring date = bfromcstr(row[0]);
        bstring symbol = bfromcstr(row[1]);
        bstring identifier = bfromcstr(row[2]);
        
        // get the index of the market with that symbol
        int market_idx = the_markets->getElementIndex(the_markets, bdata(symbol));
        check(market_idx>=0, "symbol \"%s\" (from database orderbook) does not exist in market list \"%s\"", error_markets, bdata(symbol), bdata(*the_markets->name));
        
        bstring typestring = bfromcstr(row[3]);
        signal_type type = undefined;
        
        if(biseqcstr(typestring, "long"))
            type = longsignal;
        else if(biseqcstr(typestring, "short"))
            type = shortsignal;
        
        bstring orderstring = bfromcstr(row[4]);
        order_type ordertype = buyorder;
        if(biseqcstr(orderstring, "buy"))
            ordertype = buyorder;
        else if(biseqcstr(orderstring, "sell"))
            ordertype = sellorder;
        
        float price = 0.0;
        CHECK_ATOF_NULL(price, row[5]);
        
        bstring signalname = bfromcstr(row[6]);
        
        float costperitem = 0.0;
        CHECK_ATOF_NULL(costperitem, row[7]);
        
        float possize = 0.0;
        CHECK_ATOF_NULL(possize, row[8]);
        
        float quant = 0.0;
        CHECK_ATOF_NULL(quant, row[9]);
        
        bstring buydate = bfromcstr(row[10]);
        bstring sigdate = bfromcstr(row[11]);
        
        unsigned int holddays = 0;
        CHECK_ATOI_NULL(holddays, row[12]);
        
//         float comission = 0.0;
//         CHECK_ATOF_NULL(comission, row[12]);
        
        float stoploss = 0.0;
        CHECK_ATOF_NULL(stoploss, row[14]);
        
        float pl_total = 0.0;
        CHECK_ATOF_NULL(pl_total, row[15]);
        
        float pl_piece = 0.0;
        CHECK_ATOF_NULL(pl_piece, row[16]);
        
//         float pl_percent = 0.0;
//         CHECK_ATOF_NULL(pl_percent, row[16]);
        
        float fee = 0.0;
        CHECK_ATOF_NULL(fee, row[18]);
        
        // create temporary quote objects needed for quote obj
        class_quotes* quote_market_tmp = NULL;
        class_quotes* quote_account_tmp = NULL;
        class_quotes* quote_buy_tmp = NULL;
        quote_market_tmp = class_quotes_init(bdata(symbol), "nothing", 1, the_markets->markets[market_idx]->nr_digits, "tmp quote", "none");
        quote_account_tmp = class_quotes_init(bdata(symbol),"nothing", 1, parms.ACCOUNT_CURRENCY_DIGITS, "tmp account quote", "none");
        quote_buy_tmp = class_quotes_init(bdata(symbol), "nothing", 1, the_markets->markets[market_idx]->nr_digits, "tmp buy quote", "none");
        //fill quote obj with neccessary data
        quote_market_tmp->quotevec[0] = price;
        quote_market_tmp->datevec[0] = bstrcpy(buydate); 
        quote_market_tmp->daynrvec[0] = 1; // fake daynr, just to make sure something is in there
        quote_account_tmp->quotevec[0] = costperitem;
        quote_account_tmp->datevec[0] = bstrcpy(buydate);
        quote_account_tmp->daynrvec[0] = 1; // fake daynr, just to make sure something is in there
        quote_buy_tmp->quotevec[0] = price;
        quote_buy_tmp->datevec[0] = bstrcpy(buydate); 
        quote_buy_tmp->daynrvec[0] = 1; // fake daynr, just to make sure something is in there
        
        // create temp order object, feed with all tmp data
        class_order* tmp_order = NULL;
        tmp_order = class_order_init_manually(bdata(buydate), bdata(date), bdata(symbol), bdata(identifier), bdata(*the_markets->markets[market_idx]->markettype), bdata(signalname), ordertype,
                                              quote_market_tmp, quote_account_tmp, quote_buy_tmp, type, holddays, stoploss, possize, quant, pl_total, pl_piece, pl_total, fee);
        // add that to orderbook
        orderbook->addElement(orderbook, tmp_order);

        // cleanup for this row
        bdestroy(date);
        bdestroy(symbol);
        bdestroy(identifier);
        bdestroy(typestring);
        bdestroy(orderstring);
        bdestroy(signalname);
        bdestroy(buydate);
        bdestroy(sigdate);
        quote_market_tmp->destroy(quote_market_tmp);
        quote_account_tmp->destroy(quote_account_tmp);
        quote_buy_tmp->destroy(quote_buy_tmp);
        tmp_order->destroy(tmp_order);
 	}
 	
    // cleanup
    bdestroy(querystring);
	mysql_free_result(result);    
    return EXIT_SUCCESS;

error_markets:
    // cleanup what´s possible
    orderbook->destroy(orderbook);
    the_markets->destroy(the_markets);
    exit(EXIT_FAILURE);
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_nr_trades(bstring winlose, signal_type type)
{
    extern MYSQL *system_db_handle;
    
	MYSQL_RES	*result;
	MYSQL_ROW	row;
	unsigned int number=0;
	
	/* prepare query statement:
	 * select count(*) from orderbook_daily where buy_sell ='sell' 
	 * {optional: and P_L_percent </> 0} 
	 * {optional: and type = 'long'/'short'} */
	bstring querystring = bfromcstr("SELECT COUNT(*) FROM orderbook_daily WHERE buy_sell ='sell' ");
	if(biseqcstr(winlose, "win"))
		bconcat(querystring, bfromcstr("AND P_L_total > 0 "));
	else if(biseqcstr(winlose, "lose"))
		bconcat(querystring, bfromcstr("AND P_L_total < 0 "));
	else
		bconcat(querystring, bfromcstr(" "));
	
	switch (type)
	{
		case longsignal:
			bconcat(querystring, bfromcstr("AND type='long';"));
			break;
		case shortsignal:
			bconcat(querystring, bfromcstr("AND type='short';"));
			break;
		case undefined:
			bconcat(querystring, bfromcstr(";"));
			break;
		default:
			break;
	}
	
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));
	
	// query server
	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("db_mysql_get_nr_trade()");
	check_mysql_error(funcname, querystring);
	
	// get results
	result = mysql_store_result(system_db_handle);
	check_mysql_error(funcname, bfromcstr("none"));

	// loop through all rows of result
	while((row = mysql_fetch_row(result)) !=0)
	{
		if (row[0]!=0)	/* row[0] holds daily open */
			number = atoi(row[0]);
	}
	
	/* free memory */
	mysql_free_result(result);
	
	bdestroy(querystring);
    bdestroy(funcname);
	
	return number;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
float db_mysql_get_PL_sum(bstring winlose, signal_type type)
{
    extern MYSQL *system_db_handle;
    
	MYSQL_RES	*result;
	MYSQL_ROW	row;
	float PL_sum=0;
	
	/* prepare query statement:
	 * select SUM(P_L_percent) from orderbook_daily where buy_sell ='sell' 
	 * {optional: and P_L_percent </> 0} 
	 * {optional: and type = 'long'/'short'} */
	bstring querystring = bfromcstr("SELECT SUM(P_L_total) FROM orderbook_daily WHERE buy_sell ='sell' ");
	if(biseqcstr(winlose, "win"))
		bconcat(querystring, bfromcstr("AND P_L_total > 0 "));
	else if(biseqcstr(winlose, "lose"))
		bconcat(querystring, bfromcstr("AND P_L_total < 0 "));
	else
		bconcat(querystring, bfromcstr(" "));
	
	switch (type)
	{
		case longsignal:
			bconcat(querystring, bfromcstr("AND type='long';"));
			break;
		case shortsignal:
			bconcat(querystring, bfromcstr("AND type='short';"));
			break;
		case undefined:	
			bconcat(querystring, bfromcstr(";"));
			break;
		default:
			break;
	}
	
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));
	
	// query server
	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("db_mysql_get_PL_sum()");
	check_mysql_error(funcname, querystring);
	
	// get results
	result = mysql_store_result(system_db_handle);
	check_mysql_error(funcname, bfromcstr("none"));

	// loop through all rows of result
	while((row = mysql_fetch_row(result)) !=0)
	{
		if (row[0]!=0)	/* row[0] holds daily open */
		{
			CHECK_ATOF_NULL(PL_sum, row[0]);
		}
	}
	
	/* free memory */
	mysql_free_result(result);
	
	bdestroy(querystring);
    bdestroy(funcname);
	
	return PL_sum;
}	

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_PL(bstring PL_type, bstring *symbols, float *PL, bstring *dates)	
{
    extern MYSQL *system_db_handle;
    
	unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;
	
	/* prepare query statement:
	 * SELECT date, symbol, P_L_percent FROM orderbook_daily WHERE buy_sell='sell'; */
	
	bstring querystring=bfromcstr("SELECT date, symbol, ");
	bconcat(querystring, PL_type);
	bconcat(querystring, bfromcstr(" FROM orderbook_daily WHERE buy_sell='sell' ORDER BY date;"));
	
	// Query the server
	// uncomment to see query statement
 	// fprintf(stdout, "%s\n",bdata(querystring);  
	
 	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("db_mysql_get_PL()");
 	check_mysql_error(funcname, querystring);
 	
 	// get results
 	result= mysql_store_result(system_db_handle);
 	check_mysql_error(funcname, bfromcstr("none"));
 
 	// loop through all rows of result
 	while((row = mysql_fetch_row(result)) !=0)
	{
 	// for each row extract field results 
		dates[row_nr] = bfromcstr(row[0]);
		symbols[row_nr] = bfromcstr(row[1]);
 		CHECK_ATOF_NULL(PL[row_nr], row[2]);
 		// printout for debug:	
 		// printf("%.2f\n",PL[row_nr]));
 		row_nr++;
 	}
 	
 	// free memory
	mysql_free_result(result);
	bdestroy(querystring);	
    bdestroy(funcname);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief get hold days for all past trades (already sold) in ordered form
 * 
 * This queries the database to get all hold days of past trades, order 
 * ascending for further statistical processing.
 * 
 * @code
 *  SELECT hold_days FROM orderbook_daily WHERE buy_sell ='sell' ORDER BY hold_days ASC;
 * @endcode
 * @param hold_days unsigned int Nx1 vector that will store holddays
 * @return 0 if successfull, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_hold_days_ordered(unsigned int *hold_days)
{
    extern MYSQL *system_db_handle;
    
	unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;
	
	// prepare qurey statement
	bstring querystring = bfromcstr("SELECT hold_days FROM orderbook_daily WHERE buy_sell ='sell' ORDER BY hold_days ASC;");
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));	
	
	mysql_query(system_db_handle, bdata(querystring));
    bstring funcname = bfromcstr("mysql_getquotes");
	check_mysql_error(funcname,  querystring);
	
	/* get results */
	result = mysql_store_result(system_db_handle);
	check_mysql_error(funcname,bfromcstr("none"));

	/* loop through all rows of result*/
	while((row = mysql_fetch_row(result)) !=0)
	{
	/* for each row extract field results (change from string to float) */
		hold_days[row_nr] = atoi(row[0]);
		row_nr++;
	}
	
	// free memory 
	mysql_free_result(result);
	
	bdestroy(querystring);
	bdestroy(funcname);
    
	return 0;
	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief reads account data from db
 * 
 * This function reads the variables of the account table from db
 * 
 * @code
 *	SELECT variable_name,value 
 * 		FROM account 
 * 	WHERE 
 * 		variable_name= 'cash' 
 * 		OR variable_name='equity' 
 * 		OR variable_name='risk_free_equity' 
 *      OR variable_name='all_time_high'
 * 		OR variable_name='virgin_flag';
 * @endcode
 * @param cash float pointer to cash
 * @param equity float pointer to equity
 * @param risk_free_equity float pointer to risk free quity
 * @param sum_fees float pointer to sum of transaction costs
 * @param virgin_flag bool pointer which marks untouched accounts
 * @return 0 if successfull, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_account_data(float *cash, float *equity, float *risk_free_equity, float *sum_fees, float *high, bool *virgin_flag)
{
    extern MYSQL *system_db_handle;  // declared in main.c
    
	unsigned long row_nr=0;
	MYSQL_ROW	row;
	MYSQL_RES	*result;
	
	// prepare qurey statement
	bstring querystring = bfromcstr("SELECT variable_name,value FROM account WHERE variable_name='cash' OR variable_name='equity' OR variable_name='risk_free_equity' OR variable_name='sum_fees' OR variable_name='all_time_high' OR variable_name='virgin_flag';");
	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));	
	
	mysql_query(system_db_handle, bdata(querystring));
	bstring functionname = bfromcstr("db_mysql_get_account_data");
	check_mysql_error(functionname, querystring);
	
	/* get results */
	result = mysql_store_result(system_db_handle);
	bstring querystring2 = bfromcstr("none");
	check_mysql_error(functionname, querystring2);
	bdestroy(functionname);
	bdestroy(querystring2);

	/* loop through all rows of result*/
	bstring column1,column2,column3,column4, column5, column6;
	column1 = bfromcstr("cash");
	column2 = bfromcstr("equity");
	column3 = bfromcstr("risk_free_equity");
    column4 = bfromcstr("sum_fees");
    column5 = bfromcstr("all_time_high");
	column6 = bfromcstr("virgin_flag");
 	while((row = mysql_fetch_row(result)) !=0)
 	{
 	/* for each row extract field results (change from string to float) */
		if(biseqcstr(column1,row[0]))
		{
			CHECK_ATOF_NULL(*cash, row[1]);
		}
		else if(biseqcstr(column2,row[0]))
		{
			CHECK_ATOF_NULL(*equity, row[1]);
		}
		else if(biseqcstr(column3,row[0]))
		{
			CHECK_ATOF_NULL(*risk_free_equity, row[1]);
		}
		else if(biseqcstr(column4, row[0]))
        {
            CHECK_ATOF_NULL(*sum_fees, row[1]);
        }
        else if(biseqcstr(column5, row[0]))
        {
            CHECK_ATOF_NULL(*high, row[1]);
        }
        else if(biseqcstr(column6,row[0]))
		{
			CHECK_ATOI_NULL(*virgin_flag, row[1]);
		}
 		row_nr++;
 	}
 	
	// free memory 
	mysql_free_result(result);
	
	bdestroy(querystring);
	bdestroy(column1);
	bdestroy(column2);
	bdestroy(column3);
	bdestroy(column4);
    bdestroy(column5);
    bdestroy(column6);
	return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief writes account data from db
 * 
 * This function updates the variables of the account table into db
 * 
 * @code
 * 	INSERT INTO account (variable_name, value) 
 * 		VALUES 
 *              ('cash',60),('equity',80), 
 * 		('risk_free_equity',100),
 *      ('all_time_high', 50000),
 * 		('virgin_flag', 0)
 * 	ON DUPLICATE KEY UPDATE value=VALUES(value);
 * @endcode
 * @param cash float pointer to cash
 * @param equity float pointer to equity
 * @param risk_free_equity float pointer to risk free quity
 * @param sum_fees float pointer to sum of transaction costs
 * @return 0 if successfull, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_update_account(float *cash, float *equity, float *risk_free_equity, float *high, float *sum_fees)
{
    extern MYSQL *system_db_handle;
    
	// prepare first part of query
	bstring querystring=bfromcstr("INSERT INTO account (variable_name, value) VALUES ('cash',");
	bstring query0,query1,query2,query3,query4, query8, query9, query10, query11;
	bstring query5, query7;
	query0 = bformat("%f", *cash);
	bconcat(querystring, query0);
	query1 = bfromcstr(") , ('equity',");
	bconcat(querystring, query1);
	query2 = bformat("%f", *equity);
	bconcat(querystring, query2);
	query3 = bfromcstr(") , ('risk_free_equity',");
	bconcat(querystring, query3);
	query4 = bformat("%f", *risk_free_equity);
	bconcat(querystring, query4);
    query8 = bfromcstr(") , ('sum_fees' , ");
    bconcat(querystring, query8);
    query9 = bformat("%f", *sum_fees);
    bconcat(querystring, query9);
    query10 = bfromcstr("), ('all_time_high' , ");
    bconcat(querystring, query10);
    query11 = bformat("%f", *high);
    bconcat(querystring, query11);
    query5 = bfromcstr(") , ('virgin_flag',");
	bconcat(querystring, query5);
	query7 = bfromcstr("0) ON DUPLICATE KEY UPDATE value=VALUES(value);");
	bconcat(querystring, query7);

	// uncomment to see query statement
	// printf("\n%s", bdata(querystring));	
	
	// query server
	mysql_query(system_db_handle, bdata(querystring));
	bstring functionname = bfromcstr("db_mysql_update_account");
	check_mysql_error(functionname, querystring);
	
	bdestroy(querystring);
	bdestroy(query0);
	bdestroy(query1);
	bdestroy(query2);
	bdestroy(query3);
	bdestroy(query4);
	bdestroy(query5);
	bdestroy(query7);
    bdestroy(query8);
    bdestroy(query9);
    bdestroy(query10);
    bdestroy(query11);
	bdestroy(functionname);
	
	return EXIT_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief writes a complete stop loss record to database
 * 
 * This function updates a specific stop loss record (dates, daynrs, sl and 
 * metadata like buydate, selldate and type of sl) into the database
 *  
 * @code
 * INSERT INTO stoploss_record (date, daynr, symbol, identifier, buydate, selldate, sl_type, sl) 
 *      VALUES 
 *      ('2021-03-01', '44000', 'DUX', '2021-02-29', '0000-00-00', 'percentage', '666') , 
 *      ('2021-03-02', '44001', 'DUX', '2021-02-29', '0000-00-00', 'percentage', '666.2') , 
 *      ('2021-03-03', '44002', 'DUX', '2021-02-29', '0000-00-00', 'percentage', '666.3') 
 * ON DUPLICATE KEY UPDATE selldate= VALUES(selldate), buydate=VALUES(buydate)
 * @endcode
 *
 * @param *the_quotes ptr to quotes object (with dates, daynrs and sl)
 * @param *buydate bstring pointer
 * @param *selldate bstring pointer (will be 0000-00-00 if position is not sold yet)
 * @param *sl_type bstring ptr with information about what kind of sl it is
 * @return 0 if successfull, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_update_sl_record(class_quotes *the_quotes, bstring *buydate, bstring *selldate, bstring *sl_type)
{
    extern MYSQL *system_db_handle;
    
    bstring querystring = bfromcstr("INSERT INTO stoploss_record (date, daynr, symbol, identifier, buydate, selldate, sl_type, sl) VALUES ");
  
    bool firstflag = true;

    bstring tmp1, tmp2, tmp3, tmp4;
    tmp1 = bfromcstr(" ('");
	tmp2 = bfromcstr("', '");
	tmp3 = bfromcstr("') ");
	tmp4 = bfromcstr(",");
   
    for(unsigned int i=0; i< the_quotes->nr_elements; i++)
    {
        if(!firstflag)
            bconcat(querystring, tmp4);
            
        bconcat(querystring, tmp1);                  // ('
        bconcat(querystring, the_quotes->datevec[i]);
        bconcat(querystring, tmp2);                  // ' , '
        bstring inttmp = bformat("%i", the_quotes->daynrvec[i]);
        bconcat(querystring, inttmp);
        bconcat(querystring, tmp2); 
        bconcat(querystring, *the_quotes->symbol);
        bconcat(querystring, tmp2);
        bconcat(querystring, *the_quotes->identifier);
        bconcat(querystring, tmp2);
        bconcat(querystring, *buydate);
        bconcat(querystring, tmp2);
        bconcat(querystring, *selldate);
        bconcat(querystring, tmp2);
        bconcat(querystring, *sl_type);
        bconcat(querystring, tmp2);
        bstring floattmp = bformat("%.*f", the_quotes->nr_digits, the_quotes->quotevec[i]);
        bconcat(querystring, floattmp);
        bconcat(querystring, tmp3);                 // ')
        
        bdestroy(inttmp);
        bdestroy(floattmp);
        firstflag = false;
    }
    
    bstring tmp5 = bfromcstr("ON DUPLICATE KEY UPDATE selldate=VALUES(selldate), buydate=VALUES(buydate);");
    bconcat(querystring, tmp5);
 
   	// uncomment to see query statement
// 	printf("\n%s", bdata(querystring));
   
    // query server
	mysql_query(system_db_handle, bdata(querystring));
	bstring functionname = bfromcstr("db_mysql_update_sl_record");
	check_mysql_error(functionname, querystring);
    
    bdestroy(querystring);
    bdestroy(tmp1); bdestroy(tmp2); bdestroy(tmp3), bdestroy(tmp4); bdestroy(tmp5);
    return EXIT_SUCCESS; 
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves a markte´s trend states to database
 * 
 * This function saves a markte´s trend states to database
 * 
 *  @code
 * INSERT INTO market_trend_record (date, symbol, identifier, trend, duration, daynr, price_quote) 
 *      VALUES 
 *      ('2021-03-01', 'DUX', 'GDUXI',  'uptrend', '20', '44000', '14000') , 
 *      ('2021-03-02', 'DUX', 'GDUXI' , 'uptrend', '21', '44001', '14666') ,  
 *      ('2021-03-03', 'DUX', 'GDUXI' , 'sideways', '1', '44002', '13999') 
 * ON DUPLICATE KEY UPDATE date = VALUES(date), symbol=VALUES(symbol), identifier=VALUES(identifier);
 * @endcode
 * @param *the_market ptr to class_market object
 * @return 0 if successfull, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_update_market_trend(class_market *the_market)
{
    extern MYSQL *system_db_handle;
    extern parameter parms; // parms are declared global in main.c
    
    bool firstflag = true;
    
    bstring tmp1, tmp2, tmp3, tmp4;
    tmp1 = bfromcstr(" ('");
	tmp2 = bfromcstr("', '");
	tmp3 = bfromcstr("') ");
	tmp4 = bfromcstr(",");
    
    bstring querystring = bfromcstr("INSERT INTO market_trend_record (date, symbol, identifier, trend, duration, daynr, price_quote)  VALUES ");
    
    for(unsigned int i=0; i< parms.ICHI_DAYS_TO_ANALYZE; i++)
        {
            if(!firstflag)
                bconcat(querystring, tmp4);
            
            bconcat(querystring, tmp1);                  // ('
            bconcat(querystring, *the_market->getTrendDateByIndex(the_market, i));
            bconcat(querystring, tmp2);                  // ' , '    
            bconcat(querystring, *the_market->symbol);
            bconcat(querystring, tmp2);                  // ' , '  
            bconcat(querystring, *the_market->identifier);
            bconcat(querystring, tmp2);                  // ' , '  
            
            bstring trendstring = NULL;
            if(the_market->getTrendTypeByIndex(the_market, i)  == uptrend)
            {
                trendstring = bfromcstr("uptrend");
            }
            else if(the_market->getTrendTypeByIndex(the_market, i)  == downtrend)
            {
                trendstring = bfromcstr("downtrend");
            }
            else if(the_market->getTrendTypeByIndex(the_market, i) == sideways)
            {
                trendstring = bfromcstr("sideways");
            }
            bconcat(querystring, trendstring);
            
            bconcat(querystring, tmp2);
            bstring durationstr = bformat("%i", the_market->getTrendDurationByIndex(the_market, i));
            bconcat(querystring, durationstr);
            bconcat(querystring, tmp2);
            bstring daynrstr = bformat("%i", the_market->getTrendDaynrByIndex(the_market, i));
            bconcat(querystring, daynrstr);
            bconcat(querystring, tmp2);
            bstring quotestr = bformat("%*f", the_market->nr_digits,  the_market->getTrendPriceByIndex(the_market, i));
            bconcat(querystring, quotestr);
            bconcat(querystring, tmp3);                 // ')
            
            firstflag = false;
            
            bdestroy(trendstring);
            bdestroy(durationstr);
            bdestroy(quotestr);
        }
        
    bstring tmp5 = bfromcstr("ON DUPLICATE KEY UPDATE trend = VALUES(trend), duration=VALUES(duration);");
    bconcat(querystring, tmp5);
    
    // uncomment to see query statement
// 	printf("\n%s", bdata(querystring));
    
    // query server
	mysql_query(system_db_handle, bdata(querystring));
	bstring functionname = bfromcstr("db_mysql_update_market_trend");
	check_mysql_error(functionname, querystring);
    
    bdestroy(tmp1);
    bdestroy(tmp2);
    bdestroy(tmp3);
    bdestroy(tmp4);
    bdestroy(tmp5);
    bdestroy(querystring);
    bdestroy(functionname);
    
    return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief loads a specific trend state for a market from database
 * 
 * This function loads a specific trend state for a market from database, based
 * on the given index. The index relates to the market´s internal trend_stats
 * struct, which starts at 0 and goes to parms.ICHI_DAYS_TO_ANALYZE
 * Date, daynr and close quote is taken from the market´s internal `close`
 * indicator, the same daynr is used to query the database.
 * 
 *  @code SELECT trend, duration FROM market_trend_record 
 *          WHERE daynr=44108 AND identifier='AAPL';
 * @endcode
 * @param *the_market ptr to class_market object
 * @param index uint which record shall be loaded
 * @return 0 if successfull, 1 otherwise
*/
///////////////////////////////////////////////////////////////////////////////
int db_mysql_get_market_trend(class_market *the_market, unsigned int index)
{
    extern MYSQL *system_db_handle;
    extern parameter parms; // parms are declared global in main.c
    unsigned int daynr = parms.INDI_DAYS_TO_UPDATE - parms.ICHI_DAYS_TO_ANALYZE + index;

    MYSQL_ROW	row;
	MYSQL_RES	*result;
    
    int close_pos = -1;
    close_pos = the_market->getIndicatorPos(the_market, "close");
    
    check(close_pos>=0, "Indicator \"close\" does not exist in market object \"%s\"", error_sanity_check, bdata(*the_market->symbol));
    
    daynr = the_market->Ind[close_pos]->QuoteObj->daynrvec[daynr];

    bstring querystring = bfromcstr("SELECT trend, duration FROM market_trend_record WHERE daynr= ");
    bstring daynrstr = bformat("%i", daynr);
    bconcat(querystring, daynrstr);
    bstring tmp1 = bfromcstr(" AND identifier='");
    bconcat(querystring, tmp1);
    bconcat(querystring, *the_market->identifier);
    bstring tmp2 = bfromcstr("';");
    bconcat(querystring, tmp2);
    
    // uncomment to see query statement
// 	printf("\n%s", bdata(querystring));    

    mysql_query(system_db_handle, bdata(querystring));
 	bstring functionname = bfromcstr("db_mysql_get_market_trend");
	check_mysql_error(functionname, querystring);

    // 	 get results
	result = mysql_store_result(system_db_handle);
	bstring querystring2 = bfromcstr("none");
	check_mysql_error(functionname, querystring2);
	bdestroy(functionname);
	bdestroy(querystring2);
    
    row = mysql_fetch_row(result);
    bstring trendstr = NULL;
    unsigned int trendDuration = 0;

    if(row != NULL)
    {
        trendstr = bfromcstr(row[0]);
        CHECK_ATOI_NULL(trendDuration, row[1]); 
    }
    else
    {
        trendstr = bfromcstr("sideways");
    }

    if(biseqcstr(trendstr, "uptrend"))
    {
        the_market->setTrendTypeByIndex(the_market, uptrend, index);
    }
    else if(biseqcstr(trendstr, "downtrend"))
    {
        the_market->setTrendTypeByIndex(the_market, downtrend, index);
    }
    else 
    {
        the_market->setTrendTypeByIndex(the_market, sideways, index);
    }
    
    the_market->setTrendDurationByIndex(the_market, trendDuration, index);
    
    bdestroy(querystring);
    bdestroy(daynrstr);
    bdestroy(tmp1);
    bdestroy(tmp2);
    bdestroy(trendstr);

    return EXIT_SUCCESS;
    
error_sanity_check:
    the_market->destroy(the_market);
    exit(EXIT_FAILURE);
}

/* End of file */
