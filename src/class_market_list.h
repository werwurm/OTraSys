/* class_market_list.h
 * declarations for class_market_list.c, interface for "market_list" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_market_list.h
 * @brief Header file for class_market_list.c, public member declarations
 *
 * This file contains the "public" available data and functions of the 
 * market_list "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_MARKET_LIST_H
#define CLASS_MARKET_LIST_H

#include <stdbool.h>

#include "class_market.h"

struct _market_list_private; 	/**< opaque forward declaration, this structs contains
                                non-public/ private data/functions */
				
typedef struct _class_market_list class_market_list;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyMarket_listFunc)(struct _class_market_list*);
typedef struct _class_market_list*(*cloneMarketListFunc)(const struct _class_market_list*);
typedef void (*printMarketListTableFunc)(const struct _class_market_list*); 
typedef unsigned int (*getNrElementsFunc)(struct _class_market_list*);
typedef void (*addMarketFunc)(struct _class_market_list*, const struct _class_market*);
typedef void (*removeMarketFunc)(struct _class_market_list*, unsigned int);
typedef void(*removeNonTradeableMarketsFunc)(struct _class_market_list*);
typedef void (*addNewMarketFunc)(struct _class_market_list*, char*, char* , const unsigned int, const bool, const float, const float, char*, char*, char*, char*, char*);
typedef int (*getMarketIndexFunc)(const struct _class_market_list*, char*);
typedef void (*saveAllListIndicatorsDBFunc)(struct _class_market_list*);
typedef struct _class_market_list*(*getsortedMarketListbyDaynrFunc)(struct _class_market_list*, unsigned int);
typedef void (*updateMarketListStatisticsFunc)(struct _class_market_list*);
typedef void (*printMarketListCovarianceMatrixFunc)(const struct _class_market_list*);
typedef float (*getMarketWeightFunc)(const struct _class_market_list*, const int index);
typedef float (*getKellyLeverageFunc)(const struct _class_market_list*, const int index);
typedef void (*saveTrendStateListDBFunc)(const struct _class_market_list *); 

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a market_list and its describing properties
 * 
 * This struct describes the properties that form a market_list. Not 
 * surprisingly, a market_list is a list of markets, bundled with some methods
 * to deal with a whole list of markets
 */
struct _class_market_list
{
	//public part
	// DATA
	class_market** markets; 		/**< vector to markets in market_list */
	bstring* name;			/**< identifier of market_list */ 
	
	// METHODS, call like: foo_obj->bar_method(foo_obj)
	destroyMarket_listFunc destroy; /**< "destructor", see class_market_list_destroyMarketListImpl() */
	cloneMarketListFunc clone;	/**< create new object with same data as given one, see class_market_list_cloneMarketListImpl() */
	printMarketListTableFunc printTable;	/**< prints out market_list in tabular format, see class_market_list_printMarketListTableImpl() */
	getNrElementsFunc getNrElements;  /**< gets nr of markets in list, see class_market_list_getNrElementsImpl() */
	addMarketFunc addElement;	/**< adds existing market to list, see class_market_list_addElementImpl() */
	addNewMarketFunc addNewElement; 	/**< create and add new market to list, see class_market_list_addNewElementImpl() */
	removeMarketFunc removeElement; 	/**< remove a market from market list, see class_market_list_removeElementImpl() */
	removeNonTradeableMarketsFunc removeNonTradeable; /**< removes all markets that were loaded and have the non tradeable flag, see class_market_list_removeNonTradeableImpl() */
	getMarketIndexFunc getElementIndex;  /**< return index of specified market in market_list, see class_market_list_getMarketIndexImpl() */
	saveAllListIndicatorsDBFunc saveAllIndicatorsDB;   /**< save all indicators within list of market object to database, see class_market_list_saveAllIndicatorsDBImpl() */
	getsortedMarketListbyDaynrFunc getSortedListbyDayNr; /**< sorts markets in list by lowest daynr within given index, see class_market_list_get_sorted_listByDaynrImpl() */
	updateMarketListStatisticsFunc updateStatistics; /**< update the market_lists inverted covariance matrix and kelly leverages, see class_market_list_UpdateStatisticsImpl() */
	printMarketListCovarianceMatrixFunc printCovarianceMatrix; /**< prints out the current covariance Matrix, see class_market_list_printCovarianceMatrixImpl() */
	getMarketWeightFunc getMarketWeight;   /** get the weight [0,1] of specific market in list, see class_market_list_getMarketWeightImpl() */
	getKellyLeverageFunc getKellyLeverage; /** get the leverage for specific market suggested by kelly criterion, see class_market_list_getKellyLeverageImpl() */
    saveTrendStateListDBFunc saveTrendStateDB;  /**< saves all markets trend states to database */
	//private part
	struct _market_list_private *market_list_private;	/*<< opaque pointer to private data and functions */
	// ... add methods
};

// "constructor" for market_list "objects"
class_market_list* class_market_list_init(char* name);

#endif // CLASS_MARKET_LIST_H

// eof
