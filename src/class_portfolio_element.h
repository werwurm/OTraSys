/* class_portfolio_element.h
 * declarations for class_portfolio_element.h, interface for "portfolio_element" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_portfolio_element.h
 * @brief Header file for class_portfolio_element.h, public member declarations
 *
 * This file contains the "public" available data and functions of the 
 * portfolio_element "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_PORTFOLIO_ELEMENT_H
#define CLASS_PORTFOLIO_ELEMENT_H

#include <stdbool.h>

#include "class_quote.h"      // indicators inherit from quotes
#include "datatypes.h"
#include "class_market.h"
#include "class_account.h"

struct _portfolio_element_private;    /**< opaque forward declaration, this structs contains
					non-public/ private data/functions */

typedef struct _class_portfolio_element class_portfolio_element;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyPortfolioElementFunc)(void*);
typedef struct _class_portfolio_element*(*clonePortfolioElementFunc)(const void*);
typedef void (*printPortfolioElementTableFunc)(const void*); 
typedef void (*updatePortfolioElementbyMarketFunc)(void*, float, bool, unsigned int, const struct _class_market*, unsigned int, float);
typedef int (*getMarketListPositionFunc)(const void*);
typedef void (*setMarketListPositionFunc)(void*, const int);
typedef int (*getSL_list_idxFunc)(const void*);
typedef void (*setSL_list_idxFunc)(void*, const int);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of a portfolio element and its describing properties
 * 
 * This struct describes all the properties that are needed to describe an 
 * element of the portfolio. All values are generated within the program, most
 * are stored in the database and can retrieved from the database. The 
 * information of a portfolio element is based of the information of a signal,
 * if the signal is executed.
 */ 
struct _class_portfolio_element
{
	//public part
	// DATA
	bstring* symbol;		/**< market symbol of the portfolio entry */
	bstring* identifier;    /**< identifier (like ISIN) of the entry */
	bstring* markettype;		/**< type of market */
	bstring* market_currency;	/**< market currency */
	class_quotes* Buyquote_market;	/**< Quote Object with buydate/price in market´s currency (1 unit) */
	class_quotes* Buyquote_account;	/**< Quote Object with buydate/price in  account currency (1 unit) */
	class_quotes* Buyquote_signal;	/**< Quote Object with signal date/price (can differ from buydate/price) */
	class_quotes* currQuote_market;	/**< Quote Object with current price in market´s currency (1 unit) */
	class_quotes* currQuote_account;	/**< Quote Object with current price in account´s currency (1 unit) */
	signal_type type;	/**< long or short entry, @see signal_type */
	bstring* signalname;	/**< name of the signal that triggered the buy */
	float stoploss;		/**< current stop loss, will be updated regularly */
	bool isl_flag;		/**< flag, if current stop loss is the initial one */
	float pos_size;		/**< weight factor that was determined during allocation process for this portfolio entry */
	float quantity;		/**< number of units that were bought */
	unsigned int trading_days;	/**< number of days the position is already active */
	float current_value;	/**< current value of position after last program run (in account currency) */
	float invested_value; 	/**< amount of account currency that was initially invested */
	float risk_free_value;  /**< value of position if sold at current stop loss */
	float p_l;		/**< current profit/loss of position */
	float risk_free_p_l;	/**< current p/l if sold at current stop loss */
	float p_l_percent;	/**< current p/l in percent (of invested money) */
	float equity;  /**< current equity contribution to portfolio */
	float rf_equity; /**< current risk free equity contribution to portfolio */
  
	// METHODS, call like: foo_obj->bar_method(foo_obj)
	destroyPortfolioElementFunc destroy; /**< "destructor", see class_portfolio_element_destroyPortfolioElementImpl() */
	clonePortfolioElementFunc clone;	/**< create new object with same data as given one, see class_portfolio_element_clonePortfolioElementImpl() */
	printPortfolioElementTableFunc printTable;	/**< prints out quotes in tabular format, see class_portfolio_element_printPortfolioElementTableImpl() */
	updatePortfolioElementbyMarketFunc update; /**< update portfolio element "automatically" by market information, see class_portfolio_element_update_byMarketImpl() */
	getMarketListPositionFunc	getMarketListPosition; /**< returns the position of element´s market within a market list, see class_portfolio_element_getMarketListPositionImpl() */ 
	setMarketListPositionFunc	setMarketListPosition; /**< sets the position of element´s market within a market list, see class_portfolio_element_setMarketListPositionImpl() */
	getSL_list_idxFunc          getSL_list_idx;        /**< get the position of this element within a sl_record_list, see class_portfolio_element_getSL_list_idxImpl() */
	setSL_list_idxFunc          setSL_list_idx;        /**< set the position of this element within a sl_record_list, see class_portfolio_element_setSL_list_idxImpl() */
	
	//private part
	struct _portfolio_element_private *portfolio_element_private;	/**< opaque pointer to private data and functions */
};

// "constructor" for portfolio_element "objects"
class_portfolio_element* class_portfolio_element_init(const class_market* the_market, const signal_type longorshort, 
						const float quant, const unsigned int quotenr, const unsigned int indicatorpos, 
						const char* signalstring, const class_accounts* the_account, const int marketListPosition, const int sl_list_idx);
class_portfolio_element* class_portfolio_element_init_manually(char* symbol, char* identifier, char* markettype, char* market_currency, 
						signal_type longorshort, float quant, float market_quote, unsigned int mq_digits, char* mq_date, 
						unsigned int mq_daynr, float account_quote, unsigned int aq_digits, char* aq_date, 
						unsigned int aq_daynr, float signal_quote, unsigned int sq_digits, char* sq_date, 
						unsigned int sq_daynr, char* signalstring, const int marketListPosition, const int sl_list_idx);
#endif // CLASS_PORTFOLIO_ELEMENT_H
// eof
