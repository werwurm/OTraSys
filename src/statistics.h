/* statistics.h
 * header file for statistics.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file statistics.h
 * @brief Header file for statistics.c
 *
 * This file contains the declarations for statistics.c 
 * @author Denis Zetzmann
 */

#ifndef STATISTICS_H
#define STATISTICS_H

#include "bstrlib.h"
#include "class_account.h"

int statistics_manager(class_accounts *the_account);
float calc_arithmeticMean(float *values, int number);
float calc_standardDeviation(float *values, float mean, int number);
float calc_variance(float *values, float mean, int number);
float calc_covariance(float *values_x, float *values_y, float x_mean, float y_mean, int number);
void calc_covariance_matrix(unsigned int row_cols, unsigned int nr_values, float **CovMat, float **variables, float* means, float* variances, unsigned int period);
bool invert_Matrix(float **the_matrix, unsigned int row_cols);

#endif 	// STATISTICS_H

// end of file 
