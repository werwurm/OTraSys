/* stoploss.c
 * implements various stop loss strategies/calculations
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/  

/**
 * @file stoploss.c
 * @brief implements various stop loss strategies/calculations
 *
 * This file implements various types of stop losses and their helper functions
 * @author Denis Zetzmann
 */

#include <stdlib.h>
#include "datatypes.h"
#include "stoploss.h"
#include "database.h"
#include "readconf.h"
#include "bstrlib.h"
#include "class_market.h"
#include "debug.h"

float new_stop_loss(class_market* the_market, unsigned int quote_idx, signal_type the_type)
{
    float new_sl = 0.0;
    
	extern parameter parms; // parms are declared global in main.c
	
    // get current close quote
    float the_quote = the_market->Ind[the_market->getIndicatorPos(the_market, "close")]->QUOTEVEC[quote_idx];
	
    // percentage stop is straightforward
	if(biseqcstr(parms.SL_TYPE, "percentage"))
    {
        new_sl = percentage_stop(parms.SL_PERCENTAGE_RISK, the_quote, the_type);
    }
    
    // perform chandelier stop; NOTE that in extreme momentum conditions (or for long periods) it might be
    // that a chandelier sl will be above current quote (for long, short below). In that cases fall back to 
    // percentage stop
    else if(biseqcstr(parms.SL_TYPE, "chandelier"))
    {
        float hh_ll = 0;
        float atr = 0;
        int atr_idx = quote_idx - parms.SL_ATR_PERIOD;   //TODO: make a sanity check for proper configuration
        check(atr_idx >= 0, "not enough quotes (%i) for market %s (vs. %i SL_ATR_PERIOD), check data/%s", error_nr_quotes, the_market->Ind[the_market->IndPos.close]->NR_ELEMENTS, bdata(*the_market->symbol), parms.SL_ATR_PERIOD, bdata(*the_market->csvfile));
        
        atr =  the_market->Ind[the_market->getIndicatorPos(the_market, "ATR")]->QUOTEVEC[atr_idx];       
                        
        switch(the_type)
        {
            case longsignal:
                hh_ll = the_market->Ind[the_market->getIndicatorPos(the_market, "HH_atr_period")]->QUOTEVEC[atr_idx];
                new_sl = chandelier_stop(hh_ll, atr, parms.SL_ATR_FACTOR, the_type);
                if(new_sl > the_quote)
                    new_sl = percentage_stop(parms.SL_PERCENTAGE_RISK, the_quote, the_type);    // fallback
                break;
            case shortsignal:
                hh_ll = the_market->Ind[the_market->getIndicatorPos(the_market, "LL_atr_period")]->QUOTEVEC[atr_idx];
                new_sl = chandelier_stop(hh_ll, atr, parms.SL_ATR_FACTOR, the_type);
                if(new_sl < the_quote)
                    new_sl = percentage_stop(parms.SL_PERCENTAGE_RISK, the_quote, the_type);    // fallback
                break;
            case undefined:
            default:
                new_sl = 0;  
                break;
        }
    }
    else if(biseqcstr(parms.SL_TYPE, "atr"))
    {
        float atr = 0;
        int atr_idx = quote_idx - parms.SL_ATR_PERIOD;   //TODO: make a sanity check for proper configuration
        atr =  the_market->Ind[the_market->getIndicatorPos(the_market, "ATR")]->QUOTEVEC[atr_idx];
        check(atr_idx >= 0, "not enough quotes (%i) for market %s (vs. %i SL_ATR_PERIOD), check data/%s", error_nr_quotes, the_market->Ind[the_market->IndPos.close]->NR_ELEMENTS, bdata(*the_market->symbol), parms.SL_ATR_PERIOD, bdata(*the_market->csvfile));
   
        new_sl = atr_stop(the_quote, atr, parms.SL_ATR_FACTOR, the_type);
    }
    
    return new_sl;

error_nr_quotes:
    the_market->destroy(the_market);
    exit(EXIT_FAILURE);
    
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of simple percentage stop loss 
 *
 * This function implements a simple percentage stop loss: given a price and a 
 * configured percentage it calculates the SL for this price
 * 
 * @param percentage_at_risk float value, how much percent should be risked
 * @param price float value with price
 * @param type signal_type (SL will be above price for down-, and below price
 *        for uptrends)
 * @return float with the new stop loss
 */
///////////////////////////////////////////////////////////////////////////////
float percentage_stop(float percentage_at_risk, float price, signal_type type)
{
	float stoploss=0.0;
	switch(type)
	{
		case longsignal:
			stoploss = (1 - percentage_at_risk) * price;
			break;
		case shortsignal:
			stoploss = (1 + percentage_at_risk) * price;
			break;
		case undefined:		// should absolutely not happen here
		default:
			break;		
	}
	
	return stoploss;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of chandelier stop loss 
 *
 * This function implements a volatility-based, so called "chandelier SL". It
 * utilizes the average true range and the high/low of a given date to 
 * calculate a stop loss.
 * 
 * @see http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:chandelier_exit
 * 
 * @param hh_ll highest high or lowest low as float (depending on signal_type)
 * @param atr float with average true range
 * @param atr_factor float with weight factor of ATR 
 * @param type signal_type (SL will be above price for down-, and below price
 *        for uptrends)
 * @return float with the new stop loss
 */
///////////////////////////////////////////////////////////////////////////////
float chandelier_stop(float hh_ll, float atr, float atr_factor, signal_type type)
{
    float stoploss = 0;
    switch(type)
    {
        case longsignal:
            stoploss = hh_ll - atr * atr_factor;
            break;
            
        case shortsignal:
            stoploss = hh_ll + atr * atr_factor;
            break;  
        case undefined:
        default:
            stoploss = 0;
            break;
    }
    
    return stoploss;    
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief implementation of a simple ATR based stop
 *
 * This function implements a simple volatility-based SL. It utilizes the ATR
 * to calculate a stop loss.
 * 
 * @param price float with latest quote
 * @param atr float with average true range
 * @param atr_factor float with weight factor of ATR 
 * @param type signal_type (SL will be above price for down-, and below price
 *        for uptrends)
 * @return float with the new stop loss
 */
///////////////////////////////////////////////////////////////////////////////
float atr_stop(float price, float atr, float atr_factor, signal_type type)
{
    float stoploss = 0;
    
    switch(type)
    {
        case longsignal:
            stoploss = price - atr * atr_factor;
            break;
            
        case shortsignal:
            stoploss = price + atr * atr_factor;
            break;  
        case undefined:
        default:
            stoploss = 0;
            break;
    }

    return stoploss;    
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief checks if a given SL is needed 
 *
 * This function checks if a given SL has to be updated
 * 
 */
///////////////////////////////////////////////////////////////////////////////
bool new_sl_needed(float newstop, float oldstop, signal_type type)
{
	extern parameter parms; // parms are declared global in main.c
	
	// decide if a new SL is needed, based on setting in config file
	if(biseqcstr(parms.SL_ADJUST, "fixed"))	// for fixed SL only
		return false;		// the initial SL is used
	
	// a trailing SL is moving only in trend direction
	else if(biseqcstr(parms.SL_ADJUST, "trailing"))	
	{
		switch(type)
		{
			case longsignal:
				if(newstop > oldstop)
					return true;
				else
					return false;
				break;
			case shortsignal:
				if(newstop < oldstop)
					return true;
				else
					return false;	
				break;
			case undefined:		// should absolutely not happen here
			default:
                    return false;
			break;	
		}
	}
	// if SL is configured to move up and down, each day a new SL is used
	// does not make sense in combination with a percentage stop, but is
	// useful when using a volatility based stop (eg ATR in chandelier SL)
	else if(biseqcstr(parms.SL_ADJUST, "updown"))
		return true;
	
	return false;
}

// End of File
