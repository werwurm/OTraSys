/* class_order.h
 * declarations for class_order.c, interface for "order class"
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_order.h
 * @brief Header file for class_order.c, public member declarations
 *
 * This file contains the "public" available data and functions of the 
 * order "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_ORDER_H
#define CLASS_ORDER_H

#include <stdbool.h>

#include "bstrlib.h"
#include "class_quote.h"      // indicators inherit from quotes
#include "datatypes.h"
#include "class_portfolio_element.h"

struct _order_private;   	 /**< opaque forward declaration, this structs contains
					non-public/ private data/functions */

typedef struct _class_order class_order;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyOrderFunc)(struct _class_order*);
typedef struct _class_order*(*cloneOrderFunc)(const struct _class_order*);
typedef void (*printOrderTableFunc)(const struct _class_order*); 

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Stores the type of an order (buy/sell)
 * 
 * Stores the type of an order (buy/sell).
 * 
 * @note Please note that the logic used in this software is as follows: you
 * can sell only a position you already have (=you bought first). Even a short 
 * position has to be bought first, and then sold to make a profit. 
 * The notion of a sell order in the sense of opening a new short position is
 * not applicable here
 */
enum _order_type{
	buyorder,
	sellorder,
};
typedef enum _order_type order_type;


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of an order and its describing properties
 * 
 * This struct describes all the properties that are needed to describe an 
 * order. Orders can later be stored as elements of the orderbook
 * 
 */ 
struct _class_order
{
	//public part
	// DATA
	bstring* buydate;		/**< buydate */
	bstring* selldate;		/**< selldate */
	bstring* symbol;		/**< market symbol of the portfolio entry */
	bstring* identifier;    /**< unique ID of market e.g. ISIN */
	bstring* markettype;		/**< type of market */
	bstring* signalname;		/**< name of the signal that triggered the buy */
	order_type ordertype; 		/**< type of order (buy/sell) */
	class_quotes* quote_market;	/**< Quote Object with buydate/price in market´s currency (1 unit) */
	class_quotes* quote_account;	/**< Quote Object with buydate/price in  account currency (1 unit) */
	class_quotes* Buyquote_signal;	/**< Quote Object with signal date/price (can differ from buydate/price) */
	signal_type signaltype;		/**< long or short entry, @see signal_type */
	unsigned int trading_days;	/**< number of days the position is already active */
	float stoploss;		/**< current stop loss, will be updated regularly */
	float pos_size;		/**< weight factor that was determined during allocation process for this portfolio entry */
	float quantity;		/**< number of units that were bought */
	float p_l_total;	/**< profit/loss of position */
	float p_l_piece;	/**< profit/loss per unit */
	float p_l_percent;	/**< p/l in percent (of invested money) */
	float fee;        /**< transaction costs for this transaction */
  
	// METHODS, call like: foo_obj->bar_method(foo_obj)
	destroyOrderFunc destroy; /**< "destructor", see class_order_destroyOrderImpl() */
	cloneOrderFunc clone;	/**< create new object with same data as given one, see class_order_cloneOrderImpl() */
	printOrderTableFunc printTable;	/**< prints out quotes in tabular format, see  class_order_printOrderTableImpl() */
	
	//private part
};

// "constructor" for portfolio_element "objects"
class_order* class_order_init(const class_portfolio_element* portfolio_entry, order_type ordertype, float fee);
class_order* class_order_init_manually(char* buydate, char* selldate, char* symbol, char* identifier, char* markettype, char* signalname, order_type ordertype, 
                                       class_quotes* quote_market, class_quotes* quote_account, class_quotes* Buyquote_signal, signal_type signaltype,
                                       unsigned int trading_days, float stoploss, float pos_size, float quantity, float p_l_total, float p_l_piece, float p_l_percent, float fee);

#endif // CLASS_ORDER_H
// eof
