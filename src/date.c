/* date.c
 * routines to handle date bstrings (format yyyy-mm-dd), get weekdays etc
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file date.c
 * @brief routines for handling date bstrings
 *
 * routines to handle date bstrings (format yyyy-mm-dd), get weekdays etc
 * @author Denis Zetzmann d1z@gmx.de
 */

#include <stdlib.h>
#include <time.h>

#include "arrays.h"
#include "bstrlib.h"
#include "date.h"
#include "database.h"

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns year as int (1900-...) from date bstring (YYYY-MM-DD)
 * 
 * returns year as int (1900-...) from date bstring (YYYY-MM-DD)
 * @param date bstring with date (YYYY-MM-DD)
 * @return year as integer
*/
///////////////////////////////////////////////////////////////////////////////
int get_yearnr_from_bstr(const bstring date)
{
	unsigned int year=0;
	unsigned int i=0;
	bstring yearstr;
	
	i = bstrchr(date, '-');	// search for first -, store position in i
	yearstr = bmidstr(date, 0, i);	// cut off date string after first -
	year = atoi(bdatae(yearstr,"-1"));		// cast string -> integer
	bdestroy(yearstr);
	return year;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns month number as int (1-12) from date bstring (YYYY-MM-DD)
 * 
 * returns month number as int (1-12) from date bstring (YYYY-MM-DD)
 *
 * @param date bstring with date (YYYY-MM-DD)
 * @return month as integer
*/
///////////////////////////////////////////////////////////////////////////////
int get_monthnr_from_bstr(const bstring date)
{
	unsigned int month=0;
	unsigned int i=0, j=0;
	i = bstrchr(date, '-');		// find first '-' in date string
	j = bstrrchr(date, '-');	// find 2nd '-' in date string
	bstring tmp1 = bmidstr(date, i+1,  j+1);
	//date = bstrcpy(tmp1);	// cut out month
	month = atoi(bdatae(tmp1,"-1"));
	bdestroy(tmp1);
	return month;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns the day of the month (1-31) as int from date bstring 
 *	 (YYYY-MM-DD)
 * 
 * returns the day of the month (1-31) as int from date bstring (YYYY-MM-DD)
 * @param date bstring with date (YYYY-MM-DD)
 * @return day as integer
*/
///////////////////////////////////////////////////////////////////////////////
int get_dayofmonth_from_bstr(const bstring date)
{
	unsigned int day=0;
	unsigned int i=0;

	i = bstrrchr(date, '-');	// find 2nd '-' in date string
	bstring tmp1= bmidstr(date, i+1, blength(date)); // cut out day nr
	bstring tmp2 = bstrcpy(tmp1);
	day = atoi(bdatae(tmp2,"-1"));
	bdestroy(tmp1);
	bdestroy(tmp2);
	return day;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief determines if int year is a leap year
 * 
 * determines if int year is a leap year
 * @param year integer yearnumber (1900-...)
 * @return 0 -> no leap year, 1 -> is leapyear
*/
///////////////////////////////////////////////////////////////////////////////
int isleapyear(int year)
{
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns the day number of the current year
 * 
 * returns the day number of the current year 01-01 -> 01, 
 * 01-02 -> 02, ... 12-31 -> 365 (or 366, leap years are considered)
 * @see found on stackoverflow.com/questions/19377396/c-get-day-of-year-from-date
 * @param year integer yearnumber (1900-...)
 * @param month integer monthnumber (01,02,...12)
 * @param monthday integer day of month (01,02,...31)
 * @return daynr of current year as integer
*/
///////////////////////////////////////////////////////////////////////////////
int day_of_year(int year, int month, int monthday)
{
    static const int days[2][13] = {
        {0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334},
        {0, 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335}
    };
    int leap = isleapyear(year);

    return days[leap][month] + monthday;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief calculates the number of days since 1900-01-01
 * 
 * calculates the number of days since 1900-01-01
 * @note: 1900-01-01 is considered the "reference point" and has daynr = 1 
 * @param year integer yearnumber (1900-...)
 * @param dayofyear integer day of current year (1,2,...366)
 * @return daynumber, starting from 1900-01-01 as integer
*/
///////////////////////////////////////////////////////////////////////////////
int daynumber(int year,int dayofyear)
{
	int i=0;
	int day_nr=0;
	for (i=1900; (i<(year)); i++)
	{
		if (isleapyear(i))
			day_nr=day_nr+366;
		else
			day_nr=day_nr+365;
	}
	day_nr=day_nr+dayofyear;
	return day_nr;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  translates the date stored in a bstring (YYYY-MM-DD) into the number
 * of days since 01.01.1900
 * 
 * translates the date stored in a bstring (YYYY-MM-DD) into the number
 * of days since 01.01.1900. This allows comparison between different dates 
 * (like bigger than, smaller than), in general the integer translation of the
 * date is by far easier to handle than the strings
 *
 * @param date bstring with date (YYYY-MM-DD)
 * @return daynumber, starting from 1900-01-01 as integer
*/
///////////////////////////////////////////////////////////////////////////////
unsigned int get_daynr_from_bstr(bstring date)
{
	unsigned int year=0, month=0, dayofmonth=0, dayofyear=0, daynr=0;
	
	year=get_yearnr_from_bstr(date);
	month=get_monthnr_from_bstr(date);
	dayofmonth=get_dayofmonth_from_bstr(date);
	dayofyear=day_of_year(year, month, dayofmonth);
	daynr=daynumber(year, dayofyear);
	
	return daynr;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  determines the weekday of current daynr
 * 
 * determines the weekday of current daynr, using modulo of daynr
 * (1900-01-01 was a Monday)
 *
 * @param daynr daynumber starting from 1900-01-01 as integer
 * @return weekday as bstring* ptr
*/
///////////////////////////////////////////////////////////////////////////////
void get_weekday(bstring* weekday, int daynr)
{
    bstring tmp = NULL;
    
	switch ((daynr-1) % 7)
	{
		case MONDAY:
            tmp = bfromcstr("Monday");
			break;
		case TUESDAY:
            tmp = bfromcstr("Tuesday");
			break;
		case WEDNESDAY:
            tmp = bfromcstr("Wednesday");
			break;
		case THURSDAY:
            tmp = bfromcstr("Thursday");
			break;
		case FRIDAY:
            tmp = bfromcstr("Friday");
			break;
		case SATURDAY:
            tmp = bfromcstr("Saturday");
			break;
		case SUNDAY:
            tmp = bfromcstr("Sunday");
			break;
	}	
    *weekday = bstrcpy(tmp);
    bdestroy(tmp);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  determines the year from the daynr 
 * 
 * This function returns the year of the given daynr
 * 
 * @note This uses a very ugly and time-consuming algorithm
 * @todo find a better (and faster!) algorithm for that *
 * @param daynr unsigned int number of days since 1900-01-01
 * @return year as int
*/
///////////////////////////////////////////////////////////////////////////////
int get_year_from_daynr(int daynr)
{
	int yearnr=1900;
	// errm... again sorry to my future self...
	// couldn't come up with an elegant algorithm for that
	// a brute force loop would be too time consuming as this will be
	// called VERY often
	// what follows is kind of a ugly mutated quicksort for the yearnumber
	
	// today is 2016-08-06. Dunno why anyone should use dates beyond 2021... 
	// but if he does he/she should wait for this brute force loop :-P
	if(daynr > 43833)	// > 01.01.2020
	{
		int i=0;
		int daycount=1;
		int currentyear = 2020;
		for(i=43830; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;		
	}		
	else if(daynr > 42004)	// > 01.01.2015
	{
		if(daynr > 43465)	// 01.01.2019
			yearnr = 2019;
		else if(daynr > 43100)	// 01.01.2018
			yearnr = 2018;
		else if(daynr > 42735)	// 01.01.2017
			yearnr = 2017;
		else if(daynr > 42369)  // 01.01.2016
			yearnr = 2016;
		else if(daynr > 42004)	// 01.01.2015
			yearnr = 2015;
	}
	else if(daynr > 40178)	// > 01.01.2010
	{
		if(daynr > 41639)	// 01.01.2014
			yearnr = 2014;
		else if(daynr > 41274)	// 01.01.2013
			yearnr = 2013;
		else if(daynr > 40908)	// 01.01.2012
			yearnr = 2012;
		else if(daynr > 40543)	// 01.01.2011
			yearnr = 2011;
		else if(daynr > 40178)
			yearnr = 2010;
	}
	else if(daynr > 36525)	// > 01.01.2000
	{
		if(daynr > 39813)	// 01.01.2009
			yearnr = 2009;
		else if(daynr > 39447)	// 01.01.2008
			yearnr = 2008;
		else if(daynr > 39082)	// 01.01.2007
			yearnr = 2007;
		else if(daynr > 38717)	// 01.01.2006
			yearnr = 2006;
		else if(daynr > 38352)	// 01.01.2005
			yearnr = 2005;
		else if(daynr > 37986) 	// 01.01.2004
			yearnr = 2004;
		else if(daynr > 37621)	// 01.01.2003
			yearnr = 2003;
		else if(daynr > 37265)	// 01.01.2002
			yearnr = 2002;
		else if(daynr > 36891)	// 01.01.2001
			yearnr = 2001;
		else if(daynr > 36525) // 01.01.2000
			yearnr = 2000;
	}
	else if(daynr > 32873)	// > 01.01.1990
	{
		if(daynr > 36160)	// 01.01.1999
			yearnr = 1999;
		else if(daynr > 35795)	// 01.01.1998
			yearnr = 1998;
		else if(daynr > 35430)	// 01.01.1997
			yearnr = 1997;
		else if(daynr > 35064)	// 01.01.1996
			yearnr = 1996;
		else if(daynr > 34699)	// 01.01.1995
			yearnr = 1995;
		else if(daynr > 34334) 	// 01.01.1994
			yearnr = 1994;
		else if(daynr > 33969)	// 01.01.1993
			yearnr = 1993;
		else if(daynr > 33603)	// 01.01.1992
			yearnr = 1992;
		else if(daynr > 33238)	// 01.01.1991
			yearnr = 1991;
		else if(daynr > 32873) // 01.01.1990
			yearnr = 1990;
	}	
	// ok F**k it. Mini-Brute force from now on...
	else if(daynr > 29220)	// > 01.01.1980
	{
		int i=0;
		int daycount=1;
		int currentyear = 1980;
		for(i=29220; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;		
	}
	// we entered the path to the dark side...so do a 20 year loop
	else if(daynr > 21915)	// > 01.01.1960
	{
		int i=0;
		int daycount=1;
		int currentyear = 1960;
		for(i=21915; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;		
	}	
	// enter the hall with the younglings...30 year loop
	else if(daynr > 10958)	// > 01.01.1930
	{
		int i=0;
		int daycount=1;
		int currentyear = 1930;
		for(i=10958; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;		
	}	
	// who will ever do a backtest on data that old?!
	else if(daynr > 1)	// > 01.01.1900
	{
		int i=0;
		int daycount=1;
		int currentyear = 1900;
		for(i=1; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;		
	}	
		
	return yearnr;
	
	// again: so sorry you had to read this code.
	// very thankful for an equally fast solution for the problem
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  determines the year and the day of this year from the daynr 
 * 
 * This function returns the year and the number of days in this year, based
 * on the given daynr
 * 
 * @note This uses a very ugly and time-consuming algorithm
 * @todo find a better (and faster!) algorithm for that *
 * @param daynr unsigned int number of days since 1900-01-01
 * @param dayofyear pointer to int with nr of days in this year
 * @return year as int
*/
///////////////////////////////////////////////////////////////////////////////
int get_year_dayofyear_from_daynr(int daynr, int *dayofyear)
{
	int yearnr=1900;
	// errm... again sorry to my future self...
	// couldn't come up with an elegant algorithm for that
	// a brute force loop would be too time consuming as this will be
	// called VERY often
	// what follows is kind of a ugly mutated quicksort for the yearnumber
	
	// today is 2016-08-06. Dunno why anyone should use dates beyond 2021... 
	// but if he does he/she should wait for this brute force loop :-P
	if(daynr >= 43830)	// > 01.01.2021
	{
		int i=0;
		int daycount=1;
		int currentyear = 2021;
		for(i=43830; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;
		*dayofyear = daycount;
	}		
	else if(daynr >= 42004)	// > 01.01.2015
	{
		if(daynr >= 43465)	// 01.01.2019
		{
			yearnr = 2019;
			*dayofyear = daynr-43465+1;
		}
		else if(daynr >= 43100)	// 01.01.2018
		{
			yearnr = 2018;
			*dayofyear = daynr-43100+1;
		}
		else if(daynr >= 42735)	// 01.01.2017
		{
			yearnr = 2017;
			*dayofyear = daynr-42735+1;
		}
		else if(daynr >= 42369)  // 01.01.2016
		{
			yearnr = 2016;
			*dayofyear = daynr-42369+1;
		}
		else if(daynr >= 42004)	// 01.01.2015
		{
			yearnr = 2015;
			*dayofyear = daynr-42004+1;
		}
	}
	else if(daynr >= 40178)	// > 01.01.2010
	{
		if(daynr >= 41639)	// 01.01.2014
		{
			yearnr = 2014;
			*dayofyear = daynr-41639+1;
		}
		else if(daynr >= 41274)	// 01.01.2013
		{
			yearnr = 2013;
			*dayofyear = daynr-41274+1;
		}
		else if(daynr >= 40908)	// 01.01.2012
		{
			yearnr = 2012;
			*dayofyear = daynr-40908+1;
		}
		else if(daynr >= 40543)	// 01.01.2011
		{
			yearnr = 2011;
			*dayofyear = daynr-40543+1;
		}
		else if(daynr >= 40178)	// 01.01.2010
		{
			yearnr = 2010;
			*dayofyear = daynr-40178+1;
		}
	}
	else if(daynr > 36525)	// > 01.01.2000
	{
		if(daynr >= 39813)	// 01.01.2009
		{
			yearnr = 2009;
			*dayofyear = daynr-39813+1;
		}
		else if(daynr >= 39447)	// 01.01.2008
		{
			yearnr = 2008;
			*dayofyear = daynr-39447+1;
		}
		else if(daynr >= 39082)	// 01.01.2007
		{
			yearnr = 2007;
			*dayofyear = daynr-39082+1;
		}
		else if(daynr >= 38717)	// 01.01.2006
		{
			yearnr = 2006;
			*dayofyear = daynr-38717+1;
		}
		else if(daynr >= 38352)	// 01.01.2005
		{
			yearnr = 2005;
			*dayofyear = daynr-38352+1;
		}
		else if(daynr >= 37986) 	// 01.01.2004
		{
			yearnr = 2004;
			*dayofyear = daynr-37986+1;
		}
		else if(daynr >= 37621)	// 01.01.2003
		{
			yearnr = 2003;
			*dayofyear = daynr-37621+1;
		}
		else if(daynr >= 37265)	// 01.01.2002
		{
			yearnr = 2002;
			*dayofyear = daynr-37265+1;
		}
		else if(daynr >= 36891)	// 01.01.2001
		{
			yearnr = 2001;
			*dayofyear = daynr-36891+1;
		}
		else if(daynr >= 36525) // 01.01.2000
		{
			yearnr = 2000;
			*dayofyear = daynr-36525+1;
		}
	}
	else if(daynr >= 32873)	// > 01.01.1990
	{
		if(daynr >= 36160)	// 01.01.1999
		{
			yearnr = 1999;
			*dayofyear = daynr-36160+1;
		}
		else if(daynr >= 35795)	// 01.01.1998
		{
			yearnr = 1998;
			*dayofyear = daynr-35795+1;
		}
		else if(daynr >= 35430)	// 01.01.1997
		{
			yearnr = 1997;
			*dayofyear = daynr-35430+1;
		}
		else if(daynr >= 35064)	// 01.01.1996
		{
			yearnr = 1996;
			*dayofyear = daynr-35064+1;
		}
		else if(daynr >= 34699)	// 01.01.1995
		{
			yearnr = 1995;
			*dayofyear = daynr-34699+1;
		}
		else if(daynr >= 34334) 	// 01.01.1994
		{
			yearnr = 1994;
			*dayofyear = daynr-34334+1;
		}
		else if(daynr >= 33969)	// 01.01.1993
		{
			yearnr = 1993;
			*dayofyear = daynr-33969+1;
		}
		else if(daynr >= 33603)	// 01.01.1992
		{
			yearnr = 1992;
			*dayofyear = daynr-33603+1;
		}
		else if(daynr >= 33238)	// 01.01.1991
		{
			yearnr = 1991;
			*dayofyear = daynr-33238+1;
		}
		else if(daynr >= 32873) // 01.01.1990
		{
			yearnr = 1990;
			*dayofyear = daynr-32873+1;
		}
	}	
	// ok F**k it. Mini-Brute force from now on...
	else if(daynr >= 29220)	// > 01.01.1980
	{
		int i=0;
		int daycount=1;
		int currentyear = 1980;
		for(i=29220; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;
		*dayofyear = daycount;		
	}
	// we entered the path to the dark side...so do a 20 year loop
	else if(daynr >= 21915)	// > 01.01.1960
	{
		int i=0;
		int daycount=1;
		int currentyear = 1960;
		for(i=21915; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;
		*dayofyear = daycount;		
	}	
	// enter the hall with the younglings...30 year loop
	else if(daynr >= 10958)	// > 01.01.1930
	{
		int i=0;
		int daycount=1;
		int currentyear = 1930;
		for(i=10958; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;		
		*dayofyear = daycount;		
	}	
	// who will ever do a backtest on data that old?!
	else if(daynr >= 1)	// > 01.01.1900
	{
		int i=0;
		int daycount=1;
		int currentyear = 1900;
		for(i=1; i<daynr;i++)
		{
			daycount++;
			if(isleapyear(currentyear))
			{
				if(daycount>366)
				{
					currentyear++;
					daycount=0;
				}
			}
			else 
			{
				if(daycount>365)
				{
					currentyear++;
					daycount=0;
				}
			}
		}
		yearnr=currentyear;	
		*dayofyear = daycount;		
	}	
	return yearnr;
	
	// again: so sorry you had to read this code.
	// very thankful for an equally fast solution for the problem
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  translates two integers (year, number of days in this year) into 
 *	   date bstring (YYYY-MM-DD) 
 * 
 * creates a bstring in form YYYY-MM-DD from given year and dayofyear,
 * leap years are considered, a flip between two years also (but not 
 * more than one year)
 * 
 * @note returned pointer gets malloc'ed inside this function, so from caller´s
 * perspektive the reserved memory has to be freed- use array.c's
 * free_1d_array_bstring(foo, 1)  for that
 * @param year int year number 
 * @param dayofyear int with nr of days in this year
 * @return date as bstring
*/
///////////////////////////////////////////////////////////////////////////////
bstring* get_bstr_from_dayofyear(int year, int dayofyear)
{
	bstring* datestringptr = init_1d_array_bstring(1);//@caller: free this!
	bstring datestring=NULL;
	bstring monthstring=NULL;
	bstring daystring=NULL;

// check if intervall is beyond December, 31st
	if (isleapyear(year))
	{
		if (dayofyear > 366)
		{
			year = year + 1;
			dayofyear = dayofyear - 366;
		}
	}
	else
	{
		if (dayofyear > 365)
		{
			year = year +1;
			dayofyear = dayofyear - 365;
		}
	}
	
	if (isleapyear(year))
	{
		if (dayofyear > 335)
		{
			monthstring=bformat("%02d", 12);
			daystring=bformat("%02d", dayofyear-335);
		}
		else if (dayofyear > 305)
		{
			monthstring=bformat("%02d", 11);
			daystring=bformat("%02d", dayofyear-305);
		}
		else if (dayofyear> 274)
		{
			monthstring=bformat("%02d", 10);
			daystring=bformat("%02d", dayofyear-274);
		}
		else if (dayofyear> 244)
		{
			monthstring=bformat("%02d", 9);
			daystring=bformat("%02d", dayofyear-244);
		}
		else if (dayofyear> 213)
		{
			monthstring=bformat("%02d", 8);
			daystring=bformat("%02d", dayofyear-213);
		}
		else if (dayofyear> 182)
		{
			monthstring=bformat("%02d", 7);
			daystring=bformat("%02d", dayofyear-182);
		}		
		else if (dayofyear> 152)
		{
			monthstring=bformat("%02d", 6);;
			daystring=bformat("%02d", dayofyear-152);
		}
		else if (dayofyear> 121)
		{
			monthstring=bformat("%02d", 5);
			daystring=bformat("%02d", dayofyear-121);
		}
		else if (dayofyear> 91)
		{
			monthstring=bformat("%02d", 4);
			daystring=bformat("%02d", dayofyear-91);
		}
		else if (dayofyear> 60)
		{
			monthstring=bformat("%02d", 3);
			daystring=bformat("%02d", dayofyear-60);
		}	
		else if (dayofyear> 31)
		{
			monthstring=bformat("%02d", 2);
			daystring=bformat("%02d", dayofyear-32);
		}
		else
		{
			monthstring=bformat("%02d", 1);
			daystring=bformat("%02d", dayofyear);
		}		
	}
	else
	{
		if (dayofyear > 334)
		{
			monthstring=bformat("%d", 12);
			daystring=bformat("%02d", dayofyear-334);
		}
		else if (dayofyear > 304)
		{
			monthstring=bformat("%d", 11);
			daystring=bformat("%02d", dayofyear-304);
		}
		else if (dayofyear> 273)
		{
			monthstring=bformat("%d", 10);
			daystring=bformat("%02d", dayofyear-273);
		}
		else if (dayofyear> 243)
		{
			monthstring=bformat("%02d", 9);
			daystring=bformat("%02d", dayofyear-243);
		}
		else if (dayofyear> 212)
		{
			monthstring=bformat("%02d", 8);
			daystring=bformat("%02d", dayofyear-212);
		}
		else if (dayofyear> 181)
		{
			monthstring=bformat("%02d", 7);
			daystring=bformat("%02d", dayofyear-181);
		}		
		else if (dayofyear> 151)
		{
			monthstring=bformat("%02d", 6);
			daystring=bformat("%02d", dayofyear-151);
		}
		else if (dayofyear> 120)
		{
			monthstring=bformat("%02d", 5);
			daystring=bformat("%02d", dayofyear-120);
		}
		else if (dayofyear> 90)
		{
			monthstring=bformat("%02d", 4);
			daystring=bformat("%02d", dayofyear-90);
		}
		else if (dayofyear> 59)
		{
			monthstring=bformat("%02d", 3);
			daystring=bformat("%02d", dayofyear-59);
		}	
		else if (dayofyear> 31)
		{
			monthstring=bformat("%02d", 2);
			daystring=bformat("%02d", dayofyear-31);
		}
		else
		{
			monthstring=bformat("%02d", 1);
			daystring=bformat("%02d", dayofyear);
		}					
	
	}
	bstring tmp1, tmp2, tmp3;
	
	bstring yearstring=bformat("%d",year);
	tmp1  = bstrcpy(datestring);
	datestring=bstrcpy(yearstring);
	tmp2 = bfromcstr("-");
	bconcat(datestring, tmp2);
	bconcat(datestring, monthstring);
	bconcat(datestring, tmp2);
 	tmp3 = bstrcpy(daystring);
	bconcat(datestring,tmp3);
	
	*datestringptr = bstrcpy(datestring);
	
	bdestroy(monthstring);
	bdestroy(daystring);
	bdestroy(datestring);
	bdestroy(yearstring);
	bdestroy(tmp1);
	bdestroy(tmp2);
	bdestroy(tmp3);
	return datestringptr;	// @calling function: remember to free the 
				// allocated memory datestringptr points at!
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  translates daynr into date bstring (YYYY-MM-DD) 
 * 
 * This is a wrapper function that uses get_year_dayofyear_from_daynr and
 * get_bstr_from_dayofyear to translate a daynr into a date bstring
 * 
 * @see get_year_dayofyear_from_daynr get_bstr_from_dayofyear
 * @note returned pointer gets malloc'ed inside this function, so from caller´s
 * perspektive the reserved memory has to be freed- use array.c's
 * free_1d_array_bstring(foo, 1)  for that
 * @param daynr int number of days since 1900-01-01 
 * @return date as bstring
*/
///////////////////////////////////////////////////////////////////////////////
bstring* get_bstr_from_daynr(int daynr)
{
	bstring* datestringptr = NULL;// gets malloc'ed in get_bstr_from_dayofyear()

	int year = 0; int dayofyear = 0;
	year = get_year_dayofyear_from_daynr(daynr, &dayofyear);
	datestringptr = get_bstr_from_dayofyear(year, dayofyear);

	return datestringptr;	// @calling function: remember to free the 
				// allocated memory datestringptr points at!
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  returns today´s date as bstring in YYYY-MM-DD format 
 * 
 * returns today´s date as bstring in YYYY-MM-DD format 
 * 
 * @return date as bstring
*/
///////////////////////////////////////////////////////////////////////////////
bstring* get_today_bstr()
{
	bstring* today=init_1d_array_bstring(1);
	char buff[20];
	time_t now = time(NULL);
	// get todays date
	strftime(buff, 20, "%Y-%m-%d", localtime(&now));
	// copy date to bstr
	*today=bfromcstr(buff);
	return today;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief  determines if given daynr is a weekday (not considering holidays)
 * 
 * determines if daynr is a weekday , using modulo of daynr
 * (1900-01-01 was a Monday)
 *
 * @param daynr daynumber starting from 1900-01-01 as unsigned integer
 * @return true if Mon-Fri, false if Sat/Sun
*/
///////////////////////////////////////////////////////////////////////////////
bool daynrIsWeekday(unsigned int daynr)
{
	bool isWeekday = true;
	switch ((daynr-1) % 7)
	{
		case SATURDAY:
			isWeekday = false;
			break;
		case SUNDAY:
			isWeekday = false;
			break;
		default:
			break;
	}	
	return isWeekday;
}

// end of file
