/* ichimoku.h
 * declarations for ichimoku.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file ichimoku.h
 * @brief Header file for ichimoku.c
 * 
 * This file contains the declarations for ichimoku.c
 * @author Denis Zetzmann
 */

#ifndef ICHIMOKU_H
#define ICHIMOKU_H

#include <stdbool.h>

#include "datatypes.h"
#include "class_market.h"
#include "class_indicators.h"
#include "class_signals.h"
#include "class_signal_list.h"

// int ichimoku_update_indicators(bstring symbol, int digits);
int ichimoku_evaluate_signals(class_market* the_market, class_signal_list* ichimoku_signals);
int ichimoku_create_plotfile(bstring symbol);
int ichimoku_append_signals_csv(bstring symbol, class_signal_list* the_signals);
int ichimoku_plot_chart(bstring symbol, class_indicators* open, class_indicators* high, class_indicators* low, class_indicators* close, class_indicators* tenkan, class_indicators* kijun, class_indicators* chikou, class_indicators* senkouA, class_indicators* senkouB, class_signal_list* the_signals);
unsigned int ichimoku_print_eval_summary(class_market* the_market, class_signal_list* the_signals);
void ichimoku_update_trendStat(class_market* the_market, trend_type kumo_trend, trend_type chikou_trend, unsigned int daynr_idx);

trend_type ichimoku_relation_to_kumo(float quote_close, float quote_senkA, float quote_senkB);
trend_type ichimoku_get_chikou_trend(float quote_close, float quote_chikou, float quote_senkouA, float quote_senkouB);
bool ichimoku_golden_X(unsigned int daynr, float* quotes_1, float* quotes_2, int offset_days);
bool ichimoku_death_X(unsigned int daynr, float* quotes_1, float* quotes_2, int offset_days);
bool execute_specific_signal(bstring signalname);

// int ichimoku_tni(indicator *tenkan, indicator *kijun, indicator *senkouA, indicator *senkouB, indicator *tni);

bool check_new_lowhigh(unsigned int day_index, float *quotes, unsigned int *indices_mimima, unsigned int *indices_maxima, unsigned int number_maxima, signal_type type);

#endif // ICHIMOKU_H

//end of file
