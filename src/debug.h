/* debug.h
 * macros for debugging and log output
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file debug.h
 * @brief debug macros and log output
 *
 * This file defines a couple of macros for easier debugging and 
 * standardized terminal output (for warnings, errors, infos)
 * Based on Zed Shaws awesome debug macros (see his book "learn c
 * the hard way" and expanded/modified to compile without GCC extensions 
 * @author Denis Zetzmann
 */

#ifndef DBG_H
#define DBG_H

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "constants.h"

#ifdef NDEBUG
#define log_debug(...)
#else
#define log_debug(...) fprintf(stdout, "[DEBUG] in %s(), %s:%d: "	\
	FIRST(__VA_ARGS__) "\n",__func__,__FILE__, __LINE__		\
	REST(__VA_ARGS__))
#endif // log_debug

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#ifdef DEBUG
    #define _GNU_SOURCE
    #include <fenv.h>       // to catch FPE using feenableeexcept()
#endif  // DEBUG

// replaced Zed´s log macros with a version not using
// GCC´s extensions (and producing a warning when compiling with -pedantic
// and using the macros with 1 argument only); for FIRST and REST see below
#define log_err(...) fflush(stdout); fprintf(stdout, ANSI_COLOR_RED"\n[ERROR] (in %s(), %s:%d: errno: %s) "\
	FIRST(__VA_ARGS__) ANSI_COLOR_RESET"\n",__func__,__FILE__, __LINE__, clean_errno()\
	REST(__VA_ARGS__)); fflush(stdout)
	
#define log_warn(...) fflush(stdout); fprintf(stdout, ANSI_COLOR_YELLOW"\n[WARNING] (in %s()) "\
	FIRST(__VA_ARGS__) ANSI_COLOR_RESET,__func__\
	REST(__VA_ARGS__)); fflush(stdout)

#define log_info(...) fflush(stdout); fprintf(stdout, "[INFO] "FIRST(__VA_ARGS__) "\n" REST(__VA_ARGS__)); fflush(stdout)
#define log_info_nonewline(...) fprintf(stdout, "[INFO] "FIRST(__VA_ARGS__) REST(__VA_ARGS__))

// get rid of following warning 
// src/dbg.h:68:14: warning: token pasting of ',' and __VA_ARGS__ is a GNU extension [-Wgnu-zero-variadic-macro-arguments]
// if compiling with clang
#ifdef __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
#endif

#define check(A, M, L, ...) if(!(A)) {\
 	log_err(M, ##__VA_ARGS__); errno=0; goto L; } 	

#ifdef __clang__
    #pragma clang diagnostic pop 	
#endif
	
#define sentinel(...) { log_err(FIRST(__VA_ARGS__), REST(__VA_ARGS__));\
	errno=0; goto error; }

#define check_mem(A) check((A), "Out of memory.")

#define check_debug(...) if(!(FIRST(__VA_ARGS__))) { debug(REST(__VA_ARGS__));\
	errno=0; goto error; }
	
// https://stackoverflow.com/questions/5588855/standard-alternative-to-gccs-va-args-trick
/* expands to the first argument */
#define FIRST(...) FIRST_HELPER(__VA_ARGS__, throwaway)
#define FIRST_HELPER(first, ...) first

/*
 * if there's only one argument, expands to nothing.  if there is more
 * than one argument, expands to a comma followed by everything but
 * the first argument.  only supports up to 9 arguments but can be
 * trivially expanded.
 */
#define REST(...) REST_HELPER(NUM(__VA_ARGS__), __VA_ARGS__)
#define REST_HELPER(qty, ...) REST_HELPER2(qty, __VA_ARGS__)
#define REST_HELPER2(qty, ...) REST_HELPER_##qty(__VA_ARGS__)
#define REST_HELPER_ONE(first)
#define REST_HELPER_TWOORMORE(first, ...) , __VA_ARGS__
#define NUM(...) \
    SELECT_10TH(__VA_ARGS__, TWOORMORE, TWOORMORE, TWOORMORE, TWOORMORE,\
                TWOORMORE, TWOORMORE, TWOORMORE, TWOORMORE, ONE, throwaway)
#define SELECT_10TH(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, ...) a10	
	
	
#endif // DBG_H

// end of file

