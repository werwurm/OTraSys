/* class_stoploss_record.c
 * Implements a "class"-like struct in C which handles stop loss records
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_stoploss_record.c
 * @brief routines to handle sl_record data structure
 *
 * This file contains the functions which deal with creating, destroying and
 * copying stop loss records
 */

#include <stdio.h>
#include <stdlib.h>

#include "arrays.h"
#include "class_stoploss_record.h"
#include "bstrlib.h"
#include "class_quote.h"
#include "database.h"
#include "debug.h"

static void class_sl_record_setMethods(class_sl_record* self);
static void class_sl_record_destroyImpl(class_sl_record* self);
static void class_sl_record_printTableImpl(const class_sl_record* self);
static void class_sl_record_saveToDBImpl(const class_sl_record* self);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for sl_record objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods.
 * 
 * @param quote_symbol  string with symbol name
 * @param nr_records uint number of records for this symbol
 * @param nrOfDigits uint with significant nr of digits for sl quote
 * @param *buydate bstring ptr with date when position was bought
 * @param *stoploss_type bstring ptr 
 * @return pointer to new object of class sl_record
*/
///////////////////////////////////////////////////////////////////////////////
class_sl_record *class_sl_record_init(char* quote_symbol, char* identifier, unsigned int nr_records, unsigned int nrOfDigits, char* buydate, char* stoploss_type)
{
   	class_sl_record *self = NULL;
	// allocate memory for object
	self = (class_sl_record*) ZCALLOC(1, sizeof(class_sl_record));
	
	// call base class constructor
	self->QuoteObj = class_quotes_init(quote_symbol, identifier, nr_records, nrOfDigits, "stoploss", "stoploss_record");
    
    // allocate memory for rest of object
	self->buydate = init_1d_array_bstring(1);
    self->selldate = init_1d_array_bstring(1);
	self->stoploss_type = init_1d_array_bstring(1);

	// init data
    *self->buydate  = bfromcstr(buydate);
    char tmp_selldate[] = "0000-00-00";
    bstring selldate = bfromcstr(tmp_selldate);
    *self->selldate = bstrcpy(selldate);
    *self->stoploss_type = bfromcstr(stoploss_type);
	
	// set methods
    class_sl_record_setMethods(self);
	
    bdestroy(selldate);
	return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for sl_record objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to sl_record struct
*/
////////////////////////////////////////////////////////////////////////////////
void class_sl_record_setMethods(class_sl_record* self)
{
    self->destroy = class_sl_record_destroyImpl;
    self->printTable = class_sl_record_printTableImpl;
    self->saveToDB = class_sl_record_saveToDBImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "destructor" function for sl_record objects
 * 
 * This function frees the memory consumed by sl_record objects
 * 
 * @param self pointer to sl_record object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_sl_record_destroyImpl(class_sl_record* self)
{    
    self->QuoteObj->destroy(self->QuoteObj);
    self->QuoteObj = NULL;
    
    free_1d_array_bstring(self->buydate, 1);
    free_1d_array_bstring(self->selldate, 1);
    free_1d_array_bstring(self->stoploss_type, 1);
    
    free(self);
    self=NULL;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints stop loss record info
 *
 * This function prints sl record info to terminal
 * 
 * @param self pointer to sl_record struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_sl_record_printTableImpl(const class_sl_record* self)
{
    printf("\nstop loss record for %s, buydate: %s, selldate: %s, sl type: %s\n", bdata(*self->QuoteObj->symbol), bdata(*self->buydate), bdata(*self->selldate), bdata(*self->stoploss_type));
    self->QuoteObj->printTable(self->QuoteObj);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves this stop loss record to database
 * 
 * saves this stop loss record to database
 * 
 * @param self pointer to sl_record object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_sl_record_saveToDBImpl(const class_sl_record* self)
{
    int return_flag = 0;
    return_flag = db_mysql_update_sl_record(self->QuoteObj, self->buydate, self->selldate, self->stoploss_type);
    
    if(return_flag)
    {
        log_warn("Could not save sl record for %s (buydate %s) to database! Continuing...", bdata(*self->QuoteObj->symbol), bdata(*self->buydate));
    }
}
