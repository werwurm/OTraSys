/* export_csv.c
 * routines to export to csv files
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file export_csv.c
 * @brief Routines for export to csv files
 *
 * This file contains the routines to export indicator and price data to .csv 
 * files, currently used for plotting with gnuplot
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "bstrlib.h"
#include "arrays.h"
#include "class_indicators.h"
#include "readconf.h"
#include "export_csv.h"

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief create a matrix of given indicator quotes
 *
 * Creates a matrix of the given indicator quotes. Note the variable 
 * argument list. The number of the given indicators must be given as
 * argument, followed by the indicator themselves.
 * 
 * @note this uses a variable length argument list, always call the function 
 *       with correct nr. of arguments in parameter number:
 * 			bstring symbol: symbol bstring
 * 			int number: number of the indicators to follow
 * 			indicator i1:
 * 			indicator i2:
 * 			indicator ..:
 * 			indicator n:
 * 
 * @param symbol bstring with the market for output
 * @param number integer with the number of indicators to follow
 * @return pointer to matrix of indicator quotes
 */
///////////////////////////////////////////////////////////////////////////////
int create_quote_table(bstring symbol, unsigned int number, ...)
{
	extern parameter parms; // parms are declared global in main.c
	class_indicators *theIndicator;
	
	bstring *dates;
	bstring filename;
	
	unsigned int rows,i,j;
	
	bstring header;
	header = bfromcstr("date");
	
	rows = parms.ICHI_PERIOD_MID;	
	
	float **quote_table=NULL;
	
	va_list indicatorlist;
	va_start(indicatorlist, number);	// initialize va_list
	
	theIndicator = va_arg(indicatorlist, class_indicators*);
	
	rows = rows + theIndicator->NR_ELEMENTS;
	quote_table = init_2d_array_float(number, rows);
	
 	dates = init_1d_array_bstring(rows);
 	for (i=0; i < theIndicator->NR_ELEMENTS; i++)
	{
		bstring tmp0 = bstrcpy(theIndicator->DATEVEC[i]);
		bassign(dates[i], tmp0);
		bdestroy(tmp0);
	}

	for (i=0; i < number; i++)	//loop through all arguments/indicators
	{
		bstring tmpstr = bfromcstr(";");
		bconcat(header, tmpstr);	// create header from indicator names
 		bconcat(header, *theIndicator->name);
		bdestroy(tmpstr);
		
		// copy quotes from indicator to quote_table, handle datetypes differently
		// as the have a different date offset, e.g. senkouB[j] will be 26 days
		// ahead of close[j], chikou[j] 26 days behind and so on
		if (!(biseqcstr(*theIndicator->name, "senkou_A")) && !(biseqcstr(*theIndicator->name, "senkou_B"))) // for non shifted dates do:
		{
			for (j = 0; j < (theIndicator->NR_ELEMENTS); j++)
			{
				quote_table[i][j]= theIndicator->QUOTEVEC[j];
				// fill up future quotes with 0
				quote_table[i][j+parms.ICHI_PERIOD_MID]= 0.0;
			}
		}
 		else
		{
 			for (j = 0; j < theIndicator->NR_ELEMENTS; j++)	//for shifted dates:
 			{
 				quote_table[i][j+parms.ICHI_PERIOD_MID]= theIndicator->QUOTEVEC[j];
 				// also complete the shifted future dates 
 				if(biseqcstr(*theIndicator->name, "senkou_B"))
					dates[j+parms.ICHI_PERIOD_MID] = bstrcpy(theIndicator->DATEVEC[j]);
 			}
 		}
		
		// get next function argument/indicator
		theIndicator = va_arg(indicatorlist, class_indicators*);
	}

	// clean memory reserved for indicatorlist
 	va_end(indicatorlist);
	
	//printf("\n%i",bassign(filename, symbol));
	filename= bstrcpy(symbol);
	write_csv(filename, header, dates, quote_table, number, rows);
	
	bdestroy(filename);
	bdestroy(header);
	
	free_2d_array_float(quote_table, number);
	free_1d_array_bstring(dates,rows);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief creates .csv file from given input data
 *
 * creates .csv file from given input data
 * 
 * @todo: make the plotdata path configurable in config file
 * @param filename bstring with filename (without .csv extension)
 * @param header bstring with the header of the csv file
 * @param dates bstring pointer to matrix with dates
 * @param quotes float pointer to matrix with quotes
 * @param cols unsigned int nr columns of matrix
 * @param rows unsigned int nr rows of matrix
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int write_csv(bstring filename, bstring header, bstring *dates, 
		float **quotes,	unsigned int cols, unsigned int rows)
{
	extern parameter parms; // parms are declared global in main.c
	FILE *fp;
 	unsigned int i,j;
	
	bstring path=bfromcstr("data/charts/");
	
	bconcat(path,filename);
	
	bassign(filename, path);
	
	bstring tmp1 = bfromcstr("_plotdata.csv");
	bconcat(filename, tmp1);
	bdestroy(tmp1);
	
	fp=fopen(bdata(filename),"w+");	// open file handler
 
	fprintf(fp,"%s",bdata(header));	// print header as first row
 
 	for(i=0; i<rows; i++)	// loop through all rows
 	{
		// start plot only if senkouB is not 0
		if(quotes[8][i])
		{
			fprintf(fp,"\n%s",bdata(dates[i]));		// add dates
			
			// loop through all columns a.k.a. indicators
			for (j=0; j< cols; j++)
				if(quotes[j][i])	// if datapoint is not 0
					fprintf(fp,";%0.5f",quotes[j][i]);	// write datapoint
				else if(!quotes[j][i])	// otherwise
					fprintf(fp,";");	// just write colon
		}
 	}
 
	fclose(fp);

	bdestroy(path);
	
	return 0;
}


// end of file
