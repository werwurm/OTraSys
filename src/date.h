/* date.h
 * declarations for date.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file date.h
 * @brief Header file for date.c
 *
 * This file contains the declarations for date.c
 * @author Denis Zetzmann d1z@gmx.de
 */


#ifndef DATE_H
#define DATE_H


#include <stdbool.h>

#define MONDAY		0
#define TUESDAY		1
#define WEDNESDAY	2
#define THURSDAY	3
#define FRIDAY		4
#define SATURDAY	5
#define SUNDAY		6

int get_yearnr_from_bstr(const bstring date);
int get_monthnr_from_bstr(const bstring date);
int get_dayofmonth_from_bstr(const bstring date);
int day_of_year(int year, int month, int monthday);
unsigned int get_daynr_from_bstr(bstring date);
int isleapyear(int year);
int daynumber(int year,int dayofyear);
void get_weekday(bstring* weekday, int daynr);
unsigned int get_workdays_between(bstring symbol, 
		unsigned int startdaynr, unsigned int enddaynr);

int get_year_from_daynr(int daynr);
int get_year_dayofyear_from_daynr(int daynr, int *dayofyear);

bstring* get_bstr_from_dayofyear(int year, int dayofyear);
bstring* get_bstr_from_daynr(int daynr);
bstring get_next_workdate(bstring date, int days);
bstring get_past_workdate(bstring symbol, bstring date, unsigned int days);
bstring* get_today_bstr();

bool daynrIsWeekday(unsigned int daynr);

#endif // DATE_H
//end of file
