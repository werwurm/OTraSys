/* indicators_ichimoku.h
 * declarations for indicators_ichimoku.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file indicators_ichimoku.h
 * @brief Header file for indicators_ichimoku.c
 * 
 * This file contains the declarations for indicators_ichimoku.c
 * @author Denis Zetzmann
 */
 
#ifndef INDICATORS_ICHIMOKU_H
#define INDICATORS_ICHIMOKU_H

#include "bstrlib.h"
#include "class_indicators.h"
#include "class_market.h"

void ichimoku_tenkan(unsigned int period, class_indicators *hhshort, class_indicators *llshort, class_indicators *tenkan_sen);
void ichimoku_kijun(unsigned int period, class_indicators *hhmid, class_indicators *llmid, class_indicators *kijun_sen);
void ichimoku_senkou_A(unsigned int shift_period, class_indicators *tenkan_sen, class_indicators *kijun_sen, class_indicators *senkou_span_A);
void ichimoku_senkou_B(unsigned int shift_period, class_indicators* hh_long, class_indicators* ll_long, class_indicators* senkou_span_B);
void ichimoku_chikou(unsigned int lag_period, class_indicators* closes, class_indicators* chikou_span);

int ichimoku_tni(class_indicators *tenkan, class_indicators *kijun, class_indicators *senkouA, class_indicators *senkouB, class_indicators *tni);

int  ichimoku_update_indicators(class_market* the_market);

#endif // INDICATORS_ICHIMOKU_H

// eof

