Feel free to use the code for whatever purpose, as long as you adhere to the 
licence.  
As I am not strictly speaking a real programmer, any help is very much 
appreciated.If you want to contribute to this little project --> great! :-D
Feel free to work on an area of interest of you. Or you can contact me via 
d1z@gmx.de  
If for whatever reason you do not want to communicate with me, the ToDo's that
are nested as comments into the code should give you a good starting point.

Please do not push changes that break the build :) Speaking of pushing, the git 
repository roughly follows Vincent Driessen`s branching model explained at  
http://nvie.com/posts/a-successful-git-branching-model/  

Thus the repository currently has 2 main branches: 
  * `master`: always contains the code for the last "stable" release
  * `dev`: is a kind of "integration" branch, where new features are integrated  
  
These two main branches are supported as needed by further branches:
  * `feature branches`: New features are being developed in own specific feature 
            branches, which are branched from dev and merged back to dev. They
            live only during development of the specific feature  
  * `release`: this branch is branched from dev and will be merged back into master
               while preparing the next release. In this branch no new features are
               being developed, only bugs are being fixed and documentation updated.
