<img src="pics/logo.png" alt="OTraSys Logo" style="width:50px ; float:left"/>

OTraSys- The Open Trading System  
An open source framework to create trading systems  

Configuration
=============
This document describes the configuration of the system. There are 
basically 2 ways to do so: 
  * via start parameter
  * via config file(s)  

Both will be covered within this document. A sample config file is
listed at the end of the document.  
Please note that this file deals only with the configuration of the trading system. 
How you can add markets to the system, the content and syntax of the symbol file is
described in /docs/config_markets.md

program start parameters
========================
  * `-?`, `--help`  show help page  
  * `-V`, `--version` shows version number  
  * `-v`, `--verbose` be verbose (flood terminal with status messages)  
  * `-c`, `--configfile` select configfile within /config (default is /config/otrasys.conf)

  * `-s`, `--setup_db` setup necessary database (db is cleared 
     if exists), root password needed!  Note that this is exclusive to the 
     other command line options
  * `-r`, `--reports` skip signal evaluation/execution, do statistics 
     based on orderbook in database
     <img src="pics/report.png" alt="sample output of --reports" style="width:500px"/>
  * `-p`, `--portfolio` skip signal evaluation/execution, print out current
     portfolio and account info from database
     <img src="pics/portfolio.png" alt="sample output of --portfolio" style="width:500px"/>
  * -`o`, `--orderbook` skip signal evaluation/execution, print out current
     orderbook and account info from database
     <img src="pics/orderbook.png" alt="sample output of --orderbook" style="width:500px"/>
  * -`a`, `--account` skip signal evaluation/execution, print out current
     account information from database
  * `-C`, `--set-cash` `AMOUNT` skip signal evaluation/execution, set cash for current 
    account to AMOUNT

Config file parameters
======================
This chapter describes the various parameters if the config file
`otrasys.conf`
As the config file is separated into different sections (indicated by the `[section]` headers,
this document follows this separation.
  * [General Settings](#GeneralSettings)
  * [Database related settings](#Database_related_settings)
  * [Terminal output settings](#Terminal_output_settings)
  * [General indicator settings](#General_indicator_settings)
  * [Ichimoku related settings](#Ichimoku_related_settings)
  * [Signal execution settings](#Signal_execution_settings)
  * [Portfolio related settings](#Portfolio_settings)
  * [Stop Loss settings](#Stop_Loss_settings)
  * [Risk Management settings](#Risk_management_settings)
  * [Chart related settings](#Chart_related_settings)

General Settings <a name="GeneralSettings"></a>
----------------

**CONFIG_SYMBOLFILE**  
With this parameter you set the file that holds the symbols 
that are to be used. This file defines the .csv data file(s),
a symbol name that will be used internally and some market information. How you
can add markets to the system, the content and syntax of the symbol file is
described in /docs/config_markets.md  
_Syntax:_  
`CONFIG_SYMBOLFILE = symbols.conf`  


**ACCOUNT_CURRENCY**  
This parameter sets the currency of your account. As the software
can trade many independent markets in different currencies (symbols
in different currencies than your account), there has to be internal
conversion to your account currency when determining position sizes,
profit/losses and so on. Currently implemented and valid values are:
  * EUR (Euro)
  * USD (US Dollar)
  * YPY (Japanese Yen)  
  
_Syntax:_  
`ACCOUNT_CURRENCY = EUR`  


**ACCOUNT_CURRENCY_DIGITS**  
This parameter sets the number of significant digits for the account currency.
Usually this will be 2, which means that the account currency will look like 
666.12 (hopefully yours will be higher). Default is 2.  
  
_Syntax:_
`ACCOUNT_CURRENCY_DIGITS = 2`  

**TRANSACTION_COSTS_FIX**  
Buying/selling something usually involves direct costs, e.g. a fee. Ignoring this
fact can lead to severe differences in performance between backtest and real-world 
trading.  
This option determines the value of this fee and can be used alone or in 
conjunction with `TRANSACTION_COSTS_PERCENTAGE`.  
`TRANSACTION_COSTS_FIX` specifies a fixed amount (in account currency) 
of fees when calculating the new balance after buying and selling. E.g. if 
`TRANSACTION_COSTS_FIX = 9.99` and the value of a new position would be 4.500 €, 
then the balance would be reduced by 4.509.99 € after buying (or increased 
by 4.490.01 when selling in this example).  
Note that `TRANSACTION_COSTS_FIX` and `TRANSACTION_COSTS_PERCENTAGE` can be both
used at the same time.  
_Syntax:_  
`TRANSACTION_COSTS_FIX = 9.99`

**TRANSACTION_COSTS_PERCENTAGE**  
When using `TRANSACTION COSTS_PERCENTAGE` this option specifies the trading 
fee in percent of the value of the position. E.g. if a position with a value 4.500 €
is to be bought, a `TRANSACTION_COSTS_PERCENTAGE = 0.015` would result in a 1.5% fee 
(22.5 €). Thus, when buying, the balance will be reduced by 4.522 € (value + fee). 
Accordingly, when selling the same position, in this case the cash would increase
by 4.477,50 €.  
Note that `TRANSACTION_COSTS_FIX` and `TRANSACTION_COSTS_PERCENTAGE` can be both
used at the same time.  
_Syntax:_
`TRANSACTION_COSTS_PERCENTAGE = 0.005`  


-------------------------------------------------------------------------------

Database related settings <a name="Database_related_settings"></a>
-------------------------
Note that since v0.5.1, OTraSys uses two separate databases: one for the
quotes and another for all trading-system specific data. Separating the databases 
allows a central storage of all quotes, used by many different
trading systems, running at the same time.  

**DB_SYSTEM_NAME**  
Specifies the name of the system-specific database. The program will 
try to connect to this database, so it must exist or the program will 
terminate.  
Starting the program with the `-s` or `--setup_db` will create the
user, the database and grants the user access with the specified 
password.  
_Syntax:_  
`DB_SYSTEM_NAME = yourdatabasename`  

**DB_SYSTEM_HOSTNAME**  
Sets the hostname of the mySQL server you want to use for the 
system-specific database.  
You can specify `localhost` or an IP adress like `192.168.5.300`
Note that the server has to be up and running, the program will
not start it by itself.  
_Syntax:_  
`DB_SYSTEM_HOSTNAME = localhost`  

**DB_SYSTEM_USERNAME**  
Sets the user for the system-specific database. Note that the 
program will try to connect with this username, so the user 
must exist. Starting the program with the `-s` or `--setup_db` 
will create the user, the database and grants the user access 
with the specified password.  
_Syntax:_  
`DB_SYSTEM_USERNAME = username`  

**DB_SYSTEM_PASSWORD**  
Sets the password for the user access to the system-specific 
database. Starting the program with the `-s` or `--setup_db` 
will create the user, the  database and grants the user access 
with the specified password.  
_Syntax:_  
`DB_SYSTEM_PASSWORD = yoursecretpassword`  

**DB_SYSTEM_PORT**  
Specifies the port for the system-specific database, that 
the mySQL server is running. Usually you don't need this 
parameter, uncomment and set only if you need to.  
_Syntax:_  
`#DB_SYSTEM_PORT = `  

**DB_QUOTES_NAME**  
Specifies the name of the quote repository database. 
_Syntax:_  
`DB_QUOTES_NAME = yourdatabasename`  

**DB_QUOTES_HOSTNAME**  
Sets the hostname of the mySQL server you want to use for the 
quote repository database.  
You can specify `localhost` or an IP adress like `192.168.5.300`
Note that the server has to be up and running, the program will
not start it by itself.  
_Syntax:_  
`DB_QUOTES_HOSTNAME = localhost`  

**DB_QUOTES_USERNAME**  
Sets the user for the quote repository database. Note that the 
program will try to connect with this username, so the user 
must exist. 
_Syntax:_  
`DB_QUOTES_USERNAME = username`  

**DB_QUOTES_PASSWORD**  
Sets the password for the user access to the quote repository 
database. 
_Syntax:_  
`DB_QUOTES_PASSWORD = yoursecretpassword`  

**DB_QUOTES_PORT**  
Specifies the port for the quote repository database, that 
the mySQL server is running. Usually you don't need this 
parameter, uncomment and set only if you need to.  
_Syntax:_  
`#DB_QUOTES_PORT = `  

-------------------------------------------------------------------------------

Terminal output settings <a name="Terminal_output_settings"></a>
------------------------

**TERMINAL_EVAL_RESULT**  
If you want an overview of the trending status of your symbols,
including the last three strong signals, you can set this config
parameter. Note that the evaluated time period is from 
`ICHI_DAYS_TO_ANALYZE` until the last date in your data set.  
Valid options are:  
  * `none`: no terminal output  
  * `text`: print a human readable table to terminal (default)  
  * `html`: throw html code into the terminal (note that this option
     was meant to be used for the weekly Ichimoku analysis/ overview
     as regularly posted on https://www.spare-time-trading.de  

_Syntax:_  
`TERMINAL_EVAL_RESULT = text`   

**TERMINAL_STATISTICS**  
Prints statistical evaluation to the terminal. Statistics are
based on the `sell` entries in the db table `orderbook_daily`.
Note that this makes sense for backtesting, less so for daily use.  
Statistic report can also be generated when starting the program 
using the `-r` or `--report` parameter. When doing so, the signal
generation/execution part of the program will be skipped, only
the statistics will be calculated and the db will not be altered.
With`TERMINAL_STATISTICS = text` on the contrary, the program behaves 
normally, will import new quotes, calculate indicators and signals, 
execute them and present the statistics after that.  

Valid options are:
  * `none`: no statistics in terminal (default)  
  * `text`: print statistics to terminal after completion  

_Syntax:_  
`TERMINAL_STATISTICS = none`

-------------------------------------------------------------------------------

General indicator settings <a name="General_indicator_settings"></a>
--------------------------

**INDI_DAYS_TO_UPDATE**  
This option specifies for which period the indicators (like highest
highs, true range or the Ichimoku components) in the database 
should be updated. Usually this should match the number of data 
points in your price .csv files, can be less but MUST NOT be larger
(as then you would try to calculate indicators based on non-existing
price data). `200` periods is a reasonable default, for backtesting
you can increase that number accordingly.  
_Syntax:_  
`INDI_DAYS_TO_UPDATE = 200`  

**INDI_ADX_PERIOD**  
This option specifies the period, over which the ADX shall be smoothed. 
Basically, the ADX is an exponential average of the DMI, which in turn
contains the ATR, an exponential average of the True Range. `INDI_ADX_PERIOD` 
specifies the number of periods in those exponential moving averages.
Default is 14 days.  
_Syntax:_  
`INDI_ADX_PERIOD = 14`  

**INDI_TRACK_TREND**  
This option controls, if the trends of all tradeable markets are tracked and
stored. If used, the system calculates for each `ICHI_DAYS_TO_ANALYSE` day the
trend status (uptrend/downtrend/sideways) and stores them internally. For
ongoing trends the duration is tracked, also. At program exit, for each 
tradeable market, the trend states are stored in the database table 
`market_trend_record`. Also, at startup, the record for the first 
`ICHI_DAYS_TO_ANALYSE` day is loaded.  
Valid options are  
  * `true` (default)  
  * `false`  
  
_Syntax:_  
`INDI_TRACK_TREND = true`  

-------------------------------------------------------------------------------

Ichimoku related settings <a name="Ichimoku_related_settings"></a>
-------------------------

**ICHI_DAYS_TO_ANALYZE**  
This option specifies, for which period the Ichimoku components
should be evaluated for signals. Note that this period MUST be smaller
than INDI_DAYS_TO_UPDATE, as some Ichimoku components need some 
past data to be calculated. At the time if writing this is not checked
at runtime, so ignoring this rule will make the program abort. Usually  
`ICHI_DAYS_TO_ANALYSE = INDI_DAYS_TO_UPDATE - ICHI_PERIOD_LONG`  
should work as a rule of thumb. Default is `40` days, for backtesting you
can increase that number accordingly.  
_Syntax:_
`ICHI_DAYS_TO_ANALYZE = 40`  

**ICHI_PERIOD_SHORT**  
This sets the short term period for Ichimoku to the given bars. As 
the Ichimoku default is `9`, you can experiment with this value. Note
that is must be shorter than `ICHI_PERIOD_MID` and `ICHI_PERIOD_LONG`  
_Syntax:_  
`ICHI_PERIOD_SHORT = 9`  

**ICHI_PERIOD_MID**  
This sets the medium term period for Ichimoku to the given bars. As 
the Ichimoku default is `26`, you can experiment with this value. Note
that is must be shorter than `ICHI_PERIOD_LONG` and larger than 
`ICHI_PERIOD_SHORT`  
_Syntax:_  
`ICHI_PERIOD_MID = 26`  

**ICHI_PERIOD_LONG**  
This sets the long term period for Ichimoku to the given bars. As 
the Ichimoku default is `52`, you can experiment with this value. Note
that is must be larger than `ICHI_PERIOD_SHORT` and `ICHI_PERIOD_MID`  
_Syntax:_  
`ICHI_PERIOD_LONG = 52` 

**ICHI_CHIKOU_CONFIRM**  
This uses the Chikou Span as confirmation/trend filter for all ichimoku 
strong signals. If e.g. a strong long signal occurs, turning this option 
on will check if the chikou span is above the price (which means if price 
today is higher than ICHI_PERIOD_MID days ago, which is a symptom of an 
uptrend. Vice versa for strong short signals. Note that this filter is 
not applied to neutral and weak signals. It is also not applied to the 
Chikou Cross Signal (which wouldn't make sense, as this Signal triggers 
the filter)  
Default is true.  
_Syntax:_  
`ICHI_CHIKOU_CONFIRM = true`  

**ICHI_KUMO_CONFIRM**  
If set to true, for a Kumo breakout to be valid the price has to make a 
higher high/lower low on close above/below previous (horizontal) reaction 
high/ reaction low. This drastically improves signal quality of Kumo breakouts
at the cost of making a Kumo breakout a pretty rare event.  
Default is true.  
_Syntax:_  
`ICHI_KUMO_CONFIRM = true`  

**ICHI_EXECUTION_SENKOU_X**  
This option defines if the Ichimoku signal Senkou Span Cross will 
be executed. Note that even if set to `false`, the Indicators Senkou 
Span A and B and the Signal will still be calculated and stored in 
the database. The execution management part will just not execute 
the signal if it occurs. Default is `true`.  
_Syntax:_  
`ICHI_EXECUTION_SENKOU_X = true`  

**ICHI_EXECUTION_KIJUN_X**  
This option defines if the Ichimoku signal Kijun Cross will be executed. 
Note that even if set to `false`, the indicator Kijun-Sen and the signal
will still be calculated and stored in the database. The execution 
management part will just not execute the signal if it occurs. Default
is `true`.  
_Syntax:_  
`ICHI_EXECUTION_KIJUN_X = true`

**ICHI_EXECUTION_CHIKOU_X**  
This option defines if the Ichimoku signal Chikou Span Cross will be
executed. Note that even if set to `false`, the indicator Chikou Span 
and the signal will still be calculated and stored in the database.
The execution management part will just not execute the signal if it
occurs. Default is `true`.  
_Syntax:_  
`ICHI_EXECUTION_CHIKOU_X = true'  

**ICHI_EXECUTION_TK_X**  
This option defines if the Ichimoku signal Tenkan/Kijun Cross will be 
executed. Note that even if set to `false`, the indicators Tenkan-Sen
and Kijun-Sen (and their crossing as signal) will be calculated and 
stored in the database. The execution management part will just not 
execute the signal if it occurs. Default is `true`.  
_Syntax:_  
`ICHI_EXECUTION_TK_X = true`  

**ICHI_EXECUTION_KUMOBREAK**  
This option defines if the Ichimoku signal Kumo breakout will be 
executed. Note that even if set to `false`, the indicators Senkou 
Span A and B and the Signal will still be calculated and stored in 
the database. The execution management part will just not execute 
the signal if it occurs. Default is `true`.  
_Syntax:_  
`ICHI_EXECUTION_KUMOBREAK = true`  

-------------------------------------------------------------------------------

Signal execution settings <a name="Signal_execution_settings"></a>
-------------------------

**SIGNAL_REGIME_FILTER**  
This option switches the market regime filter on (and choses which one
to use). In sideways/non- trending markets signals will not be executed, 
resulting in lower trading frequency but higher quality signals.  
Following options are valid:
  * `none` Turn market regime filter off
  * `ADX` Use Average Directional Index
  * `TNI` Trend Normality Indicator by @tasciccac, usable only in conjunction with 
  ichimoku-based systems; check https://twitter.com/tasciccac/status/867392176296210432  
  
_Syntax:_  
`SIGNAL_REGIME_FILTER = none`  

**SIGNAL_REGIME_ADX_THRESH**  
When `SIGNAL_REGIME_FILTER` is set to `ADX`, this option specifies the 
threshhold under which a market will be considered as sideways (=trendless).
In this case signals in this specific market won't be executed. Default is 30.  
_Syntax:_
`SIGNAL_REGIME_FILTER = 30`  

**SIGNAL_DAYS_TO_EXECUTE**  
This option defines how many days backward signals in the database 
should be executed. The program takes the last available date in the
database as a reference for each symbol and looks back the specified
number of days. Note that only existing date/quote pairs in the database
are considered, so weekend and holidays do not count.  
_Syntax:_  
`SIGNAL_DAYS_TO_EXECUTE = 1`

**SIGNAL_EXECUTION_DATE**  
Specifies when a found signal or stop loss should be executed. Three 
options are valid:  
  * `real_date` Use the date when the program is run to execute all found signals; 
    use the open price for this day  
  * `signal_date` Use the date when the signal/SL was triggered; use the
    close price for this day  
  * `signal_next` Use the next date after the signal/SL was triggered; use 
    the open price for this day   
 
Usually when doing live trading, you would use real_date. When doing
backtests you can use signal_date/signal_next with the latter emulating 
the behaviour of execution based on eod data (normally you could not execute
a signal using eod data, because the markets have already closed, but do
it the next day).  
_Syntax:_  
`SIGNAL_EXECUTION_DATE = signal_date`

**SIGNAL_MANUAL_CONFIRM**  
When set to `true` the user will be asked to confirm the execution of each 
signal/stop loss. When set to `false` (default), it will be executed automatically. 
This at the moment is without much functionality besides the confirmation, the
idea is to implement manual input of the manually executed trade into the database 
like the actual price, commission and so on.  
_Syntax:_  
`SIGNAL_MANUAL_CONFIRM = false`  

**SIGNAL_EXECUTION_PYRAMID**  
Determines if the system should use pyramiding, e.g. buy more than 1 position 
for the same symbol/market in the same direction (long/short). This has influence
on your risk management and the commisions when executing/ backtesting the 
trading system.   
Valid options are:
  * `none`: do not pyramid at all, buy only 1 position per symbol and direction  
  * `daily`: execute only 1 long or short signal, when multiple signals occure at
     at the same day  
  * `full`: do not impose any limits, buy every single signal  

_Syntax:_  
`SIGNAL_EXECUTION_PYRAMID = full`  

**SIGNAL_EXECUTION_WEAK**  
This option determines if weak signals should be executed. Currently only the 
implementation of the Ichimoku rule set uses the concept of weak/ neutral/ strong 
signals. Note that when set to `false` (default) the signals still will be available
in the database, but not executed by the system.  
_Syntax:_  
`SIGNAL_EXECUTION_WEAK = false`

**SIGNAL_EXECUTION_NEUTRAL**  
This option determines if neutral signals should be executed. Currently only the 
implementation of the Ichimoku rule set uses the concept of weak/ neutral/ strong 
signals. Note that when set to `false` (default) the signals still will be available
in the database, but not executed by the system.  
_Syntax:_  
`SIGNAL_EXECUTION_NEUTRAL = false`   

**SIGNAL_EXECUTION_STRONG**  
This option determines if strong signals should be executed (default: `true`). 
Currently only the implementation of the Ichimoku rule set uses the concept 
of weak/ neutral/ strong signals. Note that when set to `false` the signals still 
will be available in the database, but not executed by the system.  
_Syntax:_  
`SIGNAL_EXECUTION_STRONG = true`  

**SIGNAL_EXECUTION_LONG**  
This option determines if long signals should be executed (default: `true`).
Note that when set to `false` the signals still will be available in the database, 
but not executed by the system.  
_Syntax:_  
`SIGNAL_EXECUTION_LONG = true` 

**SIGNAL_EXECUTION_SHORT**  
This option determines if short signals should be executed (default: `true`).
Note that when set to `false` the signals still will be available in the database, 
but not executed by the system.  
_Syntax:_  
`SIGNAL_EXECUTION_SHORT = true`  

**SIGNAL_EXECUTION_SUNDAYS**  
This option determines if signals are executed that are based on quotes that were
pulled in on sundays. This may happen if you work with futures quote data, when 
some markets open on monday morning (their local time) and your home time/date
is still sunday. Default is `false`.  
_Sytax:_  
`SIGNAL_EXECUTION_SUNDAYS = false`  

-------------------------------------------------------------------------------
Portfolio related settings <a name="Portfolio_settings"></a>
--------------------------

**PORTFOLIO_ALLOCATION**  
This option determines how the portfolio allocation is done. Currently there are
two ways implemented:  
  * `equal`: no special allocation, all positions have the same size 
  * `fixed`: use fixed weights as defined in symbol file.
  * `kelly`: use optimal Kelly allocation to determine position size (default)  
  
More methods might be added in the future. The default `kelly` will calculate 
the optimal weights of all markets by maximizing the expected values of returns.

_Syntax:_  
`PORTFOLIO_ALLOCATION = kelly`  

**PORTFOLIO_RETURNS_PERIOD**  
This option defines, for which period the mean returns are calulated. If you
have daily data and want a month for mean returns use 20. For a year use 252
and so on. Default is 20.  
_Syntax:_
`PORTFOLIO_RETURNS_PERIOD = 20`__

**PORTFOLIO_RETURNS_UPDATE**  
This option defines after how many periods the mean returns are recalculated.
It is related to `PORTFOLIO_RETURNS_PERIOD`, however a different number is
possible. E.g. you could decide to recalculate the mean returns of last 50
periods, and do so after a cyle of 20 periods. If you chose 1, the mean
return will be recalculated for each step (e.g. after each day), which will
imposes a cost on calculation time if you have a lot of markets. It is
reasonable to set this to half of `PORTFOLIO_RETURNS_PERIOD`.
_Syntax:_
`PORTFOLIO_RETURNS_UPDATE = 10`__

-------------------------------------------------------------------------------

Stop Loss settings <a name="Stop_Loss_settings"></a>
------------------

**SL_TYPE**  
This option determines the type of stop loss the system uses.   
Valid options are:  
  * `percentage`: use a fixed percentage of the buy price to determine SL (default) 
  for an example, see picture in [docs/pics/SL/sl_percent.png](pics/SL/sl_percent.png)
  * `chandelier`: use a stop loss based on volatility (Average True Range) 
    to determine SL, for details see:
    see http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:chandelier_exit
    ; for an example, see picture in [docs/pics/SL/sl_chandelier.png](pics/SL/sl_chandelier.png)
  * `atr`: simple ATR based stop; for an example, see picture in 
    [docs/pics/SL/sl_atr.png](pics/SL/sl_atr.png)  
      
Note that in market conditions with high momentum a new chandelier sl could be over
current price (for long, under current price for short). In this cases the percentage
stop will be used as fallback, so while configuring chandelier stop it is important
to have sane values for percentage stop as well.  

More types of SL might be added in future versions.  
_Syntax:_  
`SL_TYPE	= percentage`  

**SL_PERCENTAGE_RISK**  
When using `SL_TYPE = percentage` this option specifies the percentage you want to risk,
based on the buy price (default is 0.02).  
_Syntax:_  
`SL_PERCENTAGE_RISK = 0.02`  

**SL_ATR_FACTOR**  
When using `SL_Type = chandelier` or `SL_TYPE = atr` this option specifies the factor 
that the ATR will be multiplied with (in other words place the SL x-times the ATR away 
from current price). Default is `3`.  
_Syntax:_  
`SL_ATR_FACTOR = 3`

**SL_ATR_PERIOD**  
When using `SL_type = chandelier` or `SL_TYPE = atr` this option specifies the 
_Average_-part of ATR (in other words the period for averaging the true range). Based 
on this option the ATR will be calculated vand stored in the db table `indicators_daily`. 
You can choose your period freely, using the same period as the mid term period for 
Ichimoku should give a good starting point.  
NOTE: SIGNAL_DAYS_TO_EXECUTE must be at least SL_CHANDELIER_ATR_PERIOD days shorter than 
INDI_DAYS_TO_UPDATE, because the first ATR calculation needs SL_CHANDELIER_ATR_PERIOD days.  
If earlier ATR values are queried, mysql will return 0 for the ATR so when using chandelier 
stop the calculation of the stop might be wrong!  
_Syntax:_  
`SL_ATR_PERIOD = 26`  

**SL_ADJUST**  
This option specifies how the the stop loss will be adjusted from day to day/ bar to bar.  
The default is a trailing stop loss. The other 2 options do not really make sense in live
trading, you can see example pictures for fixed/updown SL.
behaviour in the `docs/pics` folder:  
[docs/pics/SL/sl_fixed.png](pics/SL/sl_fixed.png)  
[docs/pics/SL/sl_atr_updown.png](pics/SL/sl_atr_updown.png)

Valid options are:  
  * `fixed`: do not adjust the SL at all, keep the initial SL. This might be useful if you
  plan to sell manually and use price targets (currently the software does not support this).
  * `trailing`: Default behaviour, adjust the SL in the direction of the trade (when bought long
  only raise the SL, when short lower it)
  * `updown`: move the SL with price, this makes little sense with `SL_TYPE = percentage` as
  the SL will only be reached when a daily movement is bigger than `SL_PERCENTAGE_RISK`. More 
  useful when using `SL_TYPE = chandelier` as SL will get closer to price in times of lower
  volatility.  
_Syntax:_  
`SL_ADJUST = trailing`  

**SL_SAVE_DB**  
This option specifies if the SL are stored within the mysql database. Valid options are
`true` and `false` (default).  
_Syntax:_
`SL_SAVE_DB = false`  

-------------------------------------------------------------------------------

Risk Management settings <a name="Risk_management_settings"></a>
------------------

**STARTING_BALANCE**  
This option sets the account balance at the start of the backtest/of the live trading.  
_Syntax:_ 
`STARTING_BALANCE = 1000000`  

**RISK_PER_POSITION**  
This option specifies the risk of a new position in % of current balance plus risk free equity. 
In conjunction with the calculated initial stop loss this determines the size of the new 
position (in terms of how many units will be bought). This ensures that the size of a new 
position will always be adapted to the current account situation (and not based on a fixed 
starting condition).  
_Syntax:_
`RISK_PER_POSITION = 0.02`  

**POS_SIZE_CAP**  
This option defines the cap of a new position. For very tight SL, the theoretical new
position size can be very high, resulting in a non-desirable allocation of too much capital
for a new position. This option limits that to x % of the current balance + risk free equity.  
Default is 0.1 (10% cap).  
_Syntax:_ 
`POS_SIZE_CAP = 0.1`  

**RISK_FREE_RATE**  
This option sets the theoretical rate of return of an investment with 0 risk. The period
of time should have the same horizon as `PORTFOLIO_RETURNS_PERIOD`, as the risk free rate
is used to calculate the mean excess returns (and thus the sharpe ratio and the optimal
portfolio weights using the Kelly criterion).
A good approximation for a real world minimum risk investment (note: not zero risk) for
US investors is the 3-month US Treasury bill.
_Syntax:_
`RISK_FREE_RATE = 0.02`
-------------------------------------------------------------------------------                             

Chart related settings <a name="Chart_related_settings"></a>
----------------------

**CHART_PLOT**  
Specifies if a `.plot`-File should be generated within `/data/charts/` which can be plotted 
using _GNUPLOT_ (http://gnuplot.sourceforge.net/). Note that even when set to `true` (default) 
you have to run gnuplot afterwards to actually create an image. See https://spare-time-trading.de 
for examples of the generated charts.   
_Syntax:_  
`CHART_PLOT = true`  

**CHART_COLORSCHEME**  
Currently two color schemes are available for the charts:  
  * `light` (default)  
  * `contrast`  

_Syntax:_  
`CHART_COLORSCHEME = contrast`  

**CHART_DAYS_TO_PLOT**  
This option specifies how many days should be plotted in the charts. Default is `120` which 
translates roughly to six months.  
_Syntax:_  
`CHART_DAYS_TO_PLOT = 120`  

-------------------------------------------------------------------------------     
Example config file
===================

```
#################################################
# Config file for main program otrasys          #
# example file- remove '.EXAMPLE' from filename #
# to use it, fit to your needs                  #
#                                               #
# comments start with #, in line                #
# comments are allowed                          #
#################################################

###############################################################################
[General]
# General Settings
###############################################################################
CONFIG_SYMBOLFILE = symbols.conf    # file that holds symbols
ACCOUNT_CURRENCY = EUR              # atm EUR/USD/JPY
ACCOUNT_CURRENCY_DIGITS = 2         # nr of significant digits
TRANSACTION_COSTS_FIX = 15          # fixed amount fee in account currency
TRANSACTION_COSTS_PERCENTAGE = 0.0025  # fee as percentage of position´s value

###############################################################################
[Database]
# Database related settings
###############################################################################
# settings for trading-system database
DB_SYSTEM_NAME = yourdatabasename
DB_SYSTEM_HOSTNAME = 123.123.123.123
DB_SYSTEM_USERNAME = youruser 
DB_SYSTEM_PASSWORD = yourpassword
#DB_SYSTEM_PORT = 

# settings for central quote repository
DB_QUOTES_NAME = yourquotedatabasename
DB_QUOTES_HOSTNAME = 123.123.123.123
DB_QUOTES_USERNAME = youruser 
DB_QUOTES_PASSWORD = yourpassword
#DB_QUOTES_PORT = 

###############################################################################
[Terminal]
# terminal output settings
###############################################################################
TERMINAL_EVAL_RESULT = none	# text, html, none
TERMINAL_STATISTICS = none      # text, none

###############################################################################
[General_Indicator]
# General indicator settings
###############################################################################
INDI_DAYS_TO_UPDATE = 200	# default 200
INDI_ADX_PERIOD = 14            # number of periods in ADX' EMAs, default: 14

###############################################################################
[Ichimoku]
# Ichimoku related settings
###############################################################################
ICHI_DAYS_TO_ANALYZE = 40	# default 40
ICHI_PERIOD_SHORT = 9		# default 9	
ICHI_PERIOD_MID = 26		# default 26
ICHI_PERIOD_LONG = 52  		# default: 52
ICHI_CHIKOU_CONFIRM = true      # use chikou span as confirmation/filter for 
                                # trend (default true)
ICHI_KUMO_CONFIRM = true        # check whether Kumo breakout is higher/lower 
                                # than last reaction high/low
ICHI_EXECUTION_SENKOU_X = true  # execute Signal Senkou Cross, default true
ICHI_EXECUTION_KIJUN_X = true   # execute Signal Kijun Cross, default true
ICHI_EXECUTION_CHIKOU_X = true  # execute Chikou Cross, default true
ICHI_EXECUTION_TK_X = true      # execute Tenkan/Kijun Cross, default true
ICHI_EXECUTION_KUMOBREAK = true # execute Kumo breakout, default true       

###############################################################################
[Signal_Execution]
# Signal execution settings
###############################################################################
SIGNAL_REGIME_FILTER = ADX      # Market regime filter: ADX, TNI,none (default)
SIGNAL_REGIME_ADX_THRESH = 30   # ADX market filter threshold, default 30
SIGNAL_DAYS_TO_EXECUTE = 5	# how many days backwards should signals in 
                                # database be considered? default 5
SIGNAL_EXECUTION_DATE=real_date # when to execute signal: 
                                # signal_date (date when signal was triggered),
                                # real_date (date when program is run)
SIGNAL_MANUAL_CONFIRM = true    # true: confirm execution of signals (default); 
                                # false: auto buy/sell
SIGNAL_EXECUTION_PYRAMID= none  # pyramid buy/sell signals, e.g. buy more than 
                                # 1 position in same direction?
                                # none: buy max 1 position in long/short, 
                                # daily: execute only 1 long/ 1 short per day, 
                                # pyramid on consecutive days, full: do not
                                # impose any limits, buy every single signal
SIGNAL_EXECUTION_WEAK = false	# default false
SIGNAL_EXECUTION_NEUTRAL= false	# default false
SIGNAL_EXECUTION_STRONG = true	# default true
SIGNAL_EXECUTION_LONG = true	# default true
SIGNAL_EXECUTION_SHORT = true	# default true
SIGNAL_EXECUTION_SUNDAYS =false # execute signals that occur on Sundays? (can 
                                # happen when pulling in futures quotes)

###############################################################################
[Portfolio]
# Portfolio related settings
###############################################################################
PORTFOLIO_ALLOCATION = equal    # Model to perform portfolio allocation: 
                                # equal (equal position sizes),
                                # kelly (use Kelly criterion)
PORTFOLIO_RETURNS_PERIOD = 50    # period for which mean returns are calulated
PORTFOLIO_RETURNS_UPDATE = 25   # update mean returns every X periods                                

###############################################################################
[Stop_Loss]
# Stop Loss settings
###############################################################################
SL_TYPE	= percentage            # percentage (percentage stop, default), 
                                # chandelier, atr (ATR based)
SL_PERCENTAGE_RISK = 0.05       # the SL percentage for SL_TYPE = percentage, 
                                # default 0.02
SL_ATR_FACTOR = 3               # SL_TYPE=chandelier/atr: Place the SL x-times
                                # the ATR away (default 3)
SL_ATR_PERIOD = 26              # SL_TYPE=chandelier/atr: Period for Averaging 
                                # the True Range (default 26)
SL_ADJUST = trailing            # fixed (use initial SL only), trailing (adjust
                                # SL in trend direction only)
                                # updown (adjust SL in both directions), 
                                # default: trailing
SL_SAVE_DB = true               # save all SL records in mysql database? 
                                # (true/false)

###############################################################################
[Risk_Management]
# Risk management settings
###############################################################################
STARTING_BALANCE = 10000        # starting account balance at begin of 
                                # backtest/live trading
RISK_PER_POSITION = 0.02        # Risk of a new position in % of current 
                                # balance + risk free Equity   
POS_SIZE_CAP = 0.1              # new positions: cap size to x% of current
                                # balance + risk free Equity
RISK_FREE_RATE  = 0.02          # rate of returns for 0 risk investment
                                
###############################################################################                                
[Chart]
# Chart related settings
###############################################################################
CHART_PLOT = true               # plot the chart, true/false
CHART_PLOT_WEAK = false	        # plot weak signals; default false
CHART_PLOT_NEUTRAL = false	    # plot neutral signals; default false
CHART_PLOT_STRONG = true	    # plot strong signals; default true
CHART_COLORSCHEME = contrast    # light/ contrast
CHART_DAYS_TO_PLOT = 120        # default: 120

#eof

```

