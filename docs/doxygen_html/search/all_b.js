var searchData=
[
  ['lagged',['lagged',['../datatypes_8h.html#aaa7c39524358ff79aa8f59a3a4252b4baf6188e28c54c8094690da5bdb24c1747',1,'datatypes.h']]],
  ['lastupdate_5fidx',['lastUpdate_idx',['../struct__statistics.html#a20295cf47b8a3cad04c97cb8fa7bb2df',1,'_statistics']]],
  ['leftedgeindex',['LeftEdgeIndex',['../structp1d_1_1TComponent.html#a76fd89c0bdfb59c2d7a4866786c36f70',1,'p1d::TComponent']]],
  ['loadfromdb',['loadFromDB',['../struct__class__accounts.html#a7cc59085b970d1e18b7ab53d25137a60',1,'_class_accounts::loadFromDB()'],['../struct__class__indicators.html#aaede0c16a85754ebf85c6cf361e5bf8b',1,'_class_indicators::loadFromDB()'],['../struct__class__orderbook.html#ae825d78690df46703c6a3d487ffaf659',1,'_class_orderbook::loadFromDB()'],['../struct__class__portfolio.html#acc27f23261f2a6054c53a5a30ada9fc0',1,'_class_portfolio::loadFromDB()'],['../struct__class__quotes.html#ae176ebb44e9edd41c036969714c743d8',1,'_class_quotes::loadFromDB()'],['../struct__class__signal__list.html#a195a6af583af7f4c8241dfe6a44f7249',1,'_class_signal_list::loadFromDB()']]],
  ['loadindicatordb',['loadIndicatorDB',['../struct__class__market.html#ad650756754bf0613d6e442a62aedbf10',1,'_class_market']]],
  ['loadtrendstatedbbyindex',['loadTrendStateDBbyIndex',['../struct__class__market.html#a4f963f27416667140c4e9bb9b191c71e',1,'_class_market']]],
  ['longsignal',['longsignal',['../datatypes_8h.html#abc74969e0af4034334768febc7b72564a52060e58bc86ae3ce1767543d2c8793c',1,'datatypes.h']]],
  ['lowest_5flow',['lowest_low',['../indicators__general_8c.html#ad3b6fb0253924440866be9e8891fe77f',1,'lowest_low(unsigned int period, const class_indicators *lows, class_indicators *lowest_lows):&#160;indicators_general.c'],['../indicators__general_8h.html#ad3b6fb0253924440866be9e8891fe77f',1,'lowest_low(unsigned int period, const class_indicators *lows, class_indicators *lowest_lows):&#160;indicators_general.c']]]
];
