var searchData=
[
  ['tablename',['tablename',['../struct__class__quotes.html#a675f119942e0c55bc3bc98264d4c5725',1,'_class_quotes']]],
  ['tagbstring',['tagbstring',['../structtagbstring.html',1,'']]],
  ['tcomponent',['TComponent',['../structp1d_1_1TComponent.html',1,'p1d']]],
  ['terminal_5feval_5fresult',['TERMINAL_EVAL_RESULT',['../structparameters.html#ac6e93662458ca00a3609a72355624a76',1,'parameters']]],
  ['terminal_5fstatistics',['TERMINAL_STATISTICS',['../structparameters.html#a04aa4cf5438bf82109d4e0d7f5767307',1,'parameters']]],
  ['tidxanddata',['TIdxAndData',['../structp1d_1_1TIdxAndData.html',1,'p1d']]],
  ['todo_20list',['Todo List',['../todo.html',1,'']]],
  ['tpairedextrema',['TPairedExtrema',['../structp1d_1_1TPairedExtrema.html',1,'p1d']]],
  ['tradeable',['tradeable',['../struct__class__market.html#a15b068beeffcb526277d672a235bdae8',1,'_class_market']]],
  ['trading_5fdays',['trading_days',['../struct__class__order.html#a70395db8fd545b97a6399df3d105bb87',1,'_class_order::trading_days()'],['../struct__class__portfolio__element.html#a30b9ba82d2d580deaf09489ac5d09c21',1,'_class_portfolio_element::trading_days()'],['../struct__portfolio.html#ad84ec0e026c28b4c9681fc76d1f0eb1b',1,'_portfolio::trading_days()']]],
  ['transaction_5fcosts_5ffix',['TRANSACTION_COSTS_FIX',['../structparameters.html#a3eaa1014a825813ef3bb4784a2ed6d9c',1,'parameters']]],
  ['transaction_5fcosts_5fpercentage',['TRANSACTION_COSTS_PERCENTAGE',['../structparameters.html#a1da8a6643ee47869d3fe9a196e089db0',1,'parameters']]],
  ['translation_5fcurrency',['translation_currency',['../struct__market__private.html#aec0348a69d580fa4167e7e106bd4ba80',1,'_market_private']]],
  ['trend_5fstats',['trend_stats',['../struct__market__private.html#a248e5283964753dce81bb22ea751cb9d',1,'_market_private']]],
  ['trendduration',['trendDuration',['../struct__trend__stats.html#af627787f720de0cb5a041e7f1129fba8',1,'_trend_stats']]],
  ['type',['type',['../struct__class__portfolio__element.html#aa0b30af456c1e4f6b101d18b7e8ccc38',1,'_class_portfolio_element::type()'],['../struct__class__signal.html#a62e31afbaa2aa09de264210ff3384eac',1,'_class_signal::type()'],['../struct__signal.html#a40c4991ec8dff3b7abdf9eb52986efa9',1,'_signal::type()'],['../struct__portfolio.html#a16cefa371e4ea84a721951d275332b01',1,'_portfolio::type()']]]
];
