var searchData=
[
  ['watershed',['Watershed',['../classp1d_1_1Persistence1D.html#aca85d894f3934f773635ced347785207',1,'p1d::Persistence1D']]],
  ['weak',['weak',['../datatypes_8h.html#abdb68a7c657b0336a7e7453fc9418a14a31c1c8268ad852cfabdc81d506946476',1,'datatypes.h']]],
  ['weight',['weight',['../struct__class__market.html#a3031d6fa7e0d0c3652c75a37367ef20c',1,'_class_market']]],
  ['wrapper_5farray2vector',['WRAPPER_array2vector',['../p1d__wrapper_8cpp.html#a5d43475acbf329bfd831ff9ff593626d',1,'WRAPPER_array2vector(float *datavec, int nr_data):&#160;p1d_wrapper.cpp'],['../p1d__wrapper_8h.html#a5d43475acbf329bfd831ff9ff593626d',1,'WRAPPER_array2vector(float *datavec, int nr_data):&#160;p1d_wrapper.cpp']]],
  ['wrapper_5fpersistence1d_5fdelete',['WRAPPER_Persistence1D_delete',['../p1d__wrapper_8cpp.html#a3689993e5fd15935f39c739669941638',1,'WRAPPER_Persistence1D_delete(Class_Persistence1D *object):&#160;p1d_wrapper.cpp'],['../p1d__wrapper_8h.html#a3689993e5fd15935f39c739669941638',1,'WRAPPER_Persistence1D_delete(Class_Persistence1D *object):&#160;p1d_wrapper.cpp']]],
  ['wrapper_5fpersistence1d_5fnew',['WRAPPER_Persistence1D_new',['../p1d__wrapper_8cpp.html#ad13a4229ace12a7656a47e20f3f439dd',1,'WRAPPER_Persistence1D_new():&#160;p1d_wrapper.cpp'],['../p1d__wrapper_8h.html#ad13a4229ace12a7656a47e20f3f439dd',1,'WRAPPER_Persistence1D_new():&#160;p1d_wrapper.cpp']]],
  ['write_5fcsv',['write_csv',['../export__csv_8c.html#a40027a3d3dc62d278de70ac4ed83f242',1,'write_csv(bstring filename, bstring header, bstring *dates, float **quotes, unsigned int cols, unsigned int rows):&#160;export_csv.c'],['../export__csv_8h.html#a40027a3d3dc62d278de70ac4ed83f242',1,'write_csv(bstring filename, bstring header, bstring *dates, float **quotes, unsigned int cols, unsigned int rows):&#160;export_csv.c']]]
];
