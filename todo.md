ToDo/feature wishlist/brainstorm
================================

  - when executing manually, ask user for price, size etc. and store in db
  - include database flag for signal filter (ichimoku: ichi_chikou_confirm)
  - try a bollinger breakout system as independent 2nd signal generator --> more code abstraction needed
  - try to fix/work on all FIXME/TODO within code :-D
  - split monolithic config file into several specialized ones, use a kind of include statement to read them in 
  - speedup possibility: in portfolio.c do not check correlation if symbol_new equals symbol_in_portfolio; do not calculate same correlations twice (if a symbol is in portfolio more than once)
  - parallelisation of processes: compute multiple symbols in parallel
  - database table which stores complete config --> database dump would contain all information which is usefull for  backtests (for example to store recall candidates)
  - make .csv import with price data more flexible: configure column order for each symbol (o,l,h,c,v or o,c,l,h,v or whatever)
  - config sanity check: SIGNAL_DAYS_TO_EXECUTE must be at least SL_CHANDELIER_ATR_PERIOD days shorter than INDI_DAYS_TO_UPDATE, otherwise there won't be ATR in the database yet, so when using chandelier stop the calculation of the stop might be wrong as mysql will return 0 for the ATR
  - looks like there is a bug with duplicated senkou cross signals: check Gold August 08/07, BuFu August 03/04
  - kumo filter: limit the lookback period of the filter (currently unlimited), otherweise chances are that there is an extrema in the history that is more 
    persistent than the current breakout
  - indicators_general/indicators_ichimoku: add sanity checks for given arguments (period vs. nr of elements), see highest_high/lowest_low for example, spit out warning if not useful, error and exit if
    out of boundary access might get possible
  - add removeXY methods to market and signal_list class
  - bug: eval summary mixes signals with wrong amplifying info
  - all clone methods should have a string with clone´s name as argument
  - implement clone method for class_account
  - class_signals.c/class_market_list: class_signalList_getSortedListByDaynrImpl uses bubblesort, look again into quicksort implementation, market_list dito
  - completely redo sanity check on config. Makes no sense to call validation for each parameter separately as there might be
    mutually exclusive combinations of parameters. Find a way to do it once and for all parameters
  - add SL_TRIGGER or so config file option: what should be used to evaluate a SL against: high/low or close? Add logic to stoploss.c´s new_stop_loss()
  - implement a safety net/ alternative stop loss, based on account currency´s P/L (preventing situations where a trade is still above SL but because of currency drops a loss is way bigger in account currency)
  - revisit class_market->weight and class_market_list->market_list_private->statistics.weight as now there is a duplication 
  - think about new, easier atr/adx based stops
  - all saveToDB methods should check if mysql function did exit properly and issue a warning if not
  - v0.5.1 feature list: 
	o remove elements from portfolio from command line (for latest price)
	o alter terminal output for buy signals (use signal name only for verbose, use symbol and ISIN)


