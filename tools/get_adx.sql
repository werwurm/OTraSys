# this query extracts dates, symbol name, close and ADX for a specific symbol
# use it standalone or with sql2csv.sh

SELECT * FROM 
    (SELECT 
        quotes.date,quotes.symbol,quotes.close,
        indicators.adx 
    FROM quotes_daily quotes 
    INNER JOIN 
        indicators_daily indicators ON 
            (indicators.date = quotes.date AND 
            quotes.symbol='DAIML') 
    GROUP BY quotes.date ORDER BY quotes.date DESC limit 200) 
SUB ORDER BY date;
