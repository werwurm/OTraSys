#!/bin/bash
﻿# little bash script to extract performance data and plot it

echo "SELECT date, daynr, cash, equity, total, total_high FROM performance_record ORDER BY DATE;" > performance.sql
sh sql2csv.sh -c otrasys -i performance -o performance
rm performance.sql

echo "set key font \",20\"" 										> performance.plot
echo "set title \"OTraSys v0.5.1 Performance Chart\" textcolor \"blue\" offset char -60,0 font \",20\""	>> performance.plot
echo "set terminal pngcairo size 1600,1200 nocrop enhanced font \"arial,12\"" 				>> performance.plot
echo "set output \"performance.png\"" 									>> performance.plot
echo "" 												>> performance.plot
echo "set xdata time" 											>> performance.plot
echo "set timefmt \"%Y-%m-%d\"" 									>> performance.plot
echo "set format x \"%m/%Y\"" 										>> performance.plot
echo "" 												>> performance.plot
echo "set datafile separator \",\" " 									>> performance.plot
echo "" 												>> performance.plot
echo "set key left top"											>> performance.plot
echo "set ylabel \"Cash + Equity\" font \",20\"" 							>> performance.plot
echo "set xlabel 'Time' font \",20\"" 									>> performance.plot
echo "" 												>> performance.plot
echo "set style line 100 lt 1 lc rgb \"0x808080\" lw 0.5 " 						>> performance.plot
echo "" 												>> performance.plot
echo "set grid ls 100" 											>> performance.plot
echo "" 												>> performance.plot
echo "set style line 102 lw 5 lt rgb \"0x0080FF\" " 							>> performance.plot
echo "set style fill solid 0.1" 									>> performance.plot
echo "" 												>> performance.plot
echo "plot 'performance.csv' using 1:5:6 with filledcurve title \"Drawdown\" ,\\" 			>> performance.plot
echo "     'performance.csv' using 1:5 with lines ls 102 title \"Performance\"" 			>> performance.plot

gnuplot performance.plot
rm performance.plot
rm performance.csv
echo "DONE!"



