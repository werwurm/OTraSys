#!/bin/bash
# This script is a shortcut for typing in the full mysql console command. It
# reads in the config file specified with the command line option -c and 
# located under ../config, uses credentials stored there to start mysql


# check if script is called with 2 arguments
if [ $# -lt 2 ]; then
  echo
  echo "ARGUMENTS:"
  echo "-c, --configfile        name of configfile located in ../config, extension .conf is optional and added if missing"
  exit 2
elif [ $# -gt 2 ]; then
  echo 1>&2 "$0: too many arguments"
  exit 2
fi

# loop through arguments
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    # specify config file name
    -c|--configfile)
    CONFIGFILE="../config/"
    CONFIGFILE=$CONFIGFILE"$2"
    # check if extension is missing
    if [ ${CONFIGFILE: -5} != ".conf" ]; then
        CONFIGFILE=$CONFIGFILE".conf"
    fi 
    shift # past argument
    ;;
esac
shift # past argument or value
done

echo
echo "using config file ""${CONFIGFILE}"
echo

user=$(awk '/^DB_SYSTEM_USERNAME/{print $3}' "${CONFIGFILE}")  # parse config for 
pw=$(awk '/^DB_SYSTEM_PASSWORD/{print $3}' "${CONFIGFILE}")    # db credentials
host=$(awk '/^DB_SYSTEM_HOSTNAME/{print $3}' "${CONFIGFILE}")
db=$(awk '/^DB_SYSTEM_NAME/{print $3}' "${CONFIGFILE}")

mysql -u $user -p$pw -h $host $db                   # connect to db
