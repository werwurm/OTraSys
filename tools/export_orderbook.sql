# this query extracts the orderbook. For each roundtrip (buy and sell) one line will be created: 
# buydate,symbol,price-P_L_piece,date,price,P_L_total,hold_days
# use it standalone or with sql2csv.sh

SELECT 
    concat (buydate,',',symbol,',',price-P_L_piece,',',date,',',price,',',P_L_total,',',hold_days) 
    AS 'buydate,symbol,price-P_L_piece,date,price,P_L_total,hold_days' 
    FROM orderbook_daily 
    WHERE buy_sell='sell'
