CC = gcc
CXX = g++
CSRCPATH = $(wildcard src/*.c)
CXXSRCPATH = $(wildcard src/*.cpp)
OBJPATH = $(addprefix build/,$(notdir $(CSRCPATH:.c=.o) $(CXXSRCPATH:.cpp=.o)))
INCLUDES = -I/usr/include/mysql -I/usr/include
LIBS = -lmysqlclient -lm
CFLAGS = -flto -Wall -O3 -pedantic -Wextra -std=c11 -DNDEBUG -fms-extensions -Wno-microsoft -funroll-loops
CCFLAGS= -flto -Wall -std=c++11
LDFLAGS= -flto -Wall -lstdc++

.PHONY: all clean debug

all:otrasys

debug: CFLAGS := $(filter-out -O3, $(CFLAGS))
debug: CFLAGS := $(filter-out -flto, $(CFLAGS))
debug: CFLAGS := $(filter-out -DNDEBUG, $(CFLAGS))
debug: CCFLAGS:= $(filter-out -O3, $(CCFLAGS))
debug: CCFLAGS:= $(filter-out -DNDEBUG, $(CCFLAGS))
debug: LDFLAGS:= $(filter-out -flto, $(LDFLAGS))
debug: CFLAGS += -DDEBUG -g -O0
debug: CCFLAGS += -DDEBUG -g -O0
debug: otrasys

otrasys: $(OBJPATH)
	$(CC) -o $@ $^ $(LIBS) $(LDFLAGS) 

build/%.o: src/%.c
	$(CC) $(CFLAGS) -c $(INCLUDES) -o $@ $<

build/%.o: src/%.cpp
	$(CXX) $(CCFLAGS) -c -o $@ $<
	
clean:
	rm -f $(OBJPATH)
	rm -f otrasys

